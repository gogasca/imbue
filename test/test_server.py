__author__ = 'gogasca'

import unittest
import psycopg2
import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
from server import Server
from utils import validator


class LibServer():
    def __init__(self):
        pass

    # https://docs.python.org/2/library/unittest.html
    def count_servers(self):
        query = """
                    SELECT COUNT(*) FROM server;
                    """
        try:
            # print "count_servers()"
            num_servers = execute_query(self.connect(), query, 1)
            return num_servers

        except Exception as e:
            print str(e)

    def delete_servers(self):
        query = """
                TRUNCATE server RESTART IDENTITY CASCADE;
                """
        try:
            # print "delete_servers()"
            execute_query(self.connect(), query, 2)

        except Exception as e:
            print str(e)

    """
        Database settings
    """

    def connect(self):
        """Connect to the PostgreSQL database.  Returns a database connection."""
        from conf import settings
        host = settings.test_dbhost
        dbname = settings.test_dbname
        user = settings.test_dbusername
        password = settings.test_dbpassword
        port = settings.test_dbport
        conn_string = "host='%s' dbname='%s' user='%s' password='%s' port='%i'" \
                      % (host, dbname, user, password, port)

        return psycopg2.connect(conn_string)


def execute_query(conn=None, query="", operation=0, content="", **kwargs):
    """
    Execute
    """
    try:
        if conn:
            cur = conn.cursor()
            if operation == 0:
                # Query All
                cur.execute(query)
                rows = cur.fetchall()
                return rows
            elif operation == 1:
                # Query One Record value
                cur.execute(query)
                rows = cur.fetchone()[0]
                return rows
            elif operation == 2:
                # Delete Records
                cur.execute(query)
                conn.commit()
            else:
                # Anything else...
                print 'Unknow option'
                cur.execute(query)
                conn.commit()
                # conn.close()
    except Exception, e:
        print str(e)
        raise


class TestServer(unittest.TestCase):
    """
    Our basic test class
    """

    def test_name(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        server_instance = Server.Server(name='localhost')
        self.assertTrue(True, validator.isValidString(server_instance.name))

    def test_invalid_name(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        from error import exceptions

        server_instance = Server.Server(name=')(*&^%$#@!!!~')
        self.assertTrue(True, validator.isValidString(server_instance.name))

    def test_name_numbers(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        server_instance = Server.Server(name='localhost1234567890')
        self.assertTrue(True, validator.isValidHostName(server_instance.name))

    def test_long_name(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        from error import exceptions

        try:
            Server.Server(
                name='qwertyuioplkjhgfdsaxcvbnmpoiuytrewqasdfghjklmnbvcxzqwertyuioplkjhgfdsazxcvbnmpoiuytrewqasdfghjklmnbvcxzqwertyuioplkjhgfdsazxcvbnmpoiuytrewqlkjhgfdsamnbvcxzpoiuytrewqlkjhgfdsamnbvcxz')
        except exceptions.InvalidParameter:
            pass
        except Exception as e:
            self.fail('Unexpected exception thrown:', e)
        else:
            self.fail('ExpectedException not thrown')

    def test_dns(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        server_instance = Server.Server(name='localhost.localhost')
        self.assertTrue(True, validator.isValidHostName(server_instance.name))

    def test_domain(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        test1 = Server.Server(name='localhost.parzee.com')
        self.assertTrue(True, validator.isValidHostName(test1.name))

    @unittest.skip("Skip")
    def test_clean_servers(self):
        lib_server = LibServer()
        lib_server.delete_servers()
        self.assertEquals(0, lib_server.count_servers())

    @unittest.skip("Skip")
    def test_insert(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        test1 = Server.Server(hostname='localhost.parzee.com')
        test1.insert(ip_address='localhost.parzee.com', interface_name='eth0', description='Test server',
                     properties='{"ssh": { "active": 1, "port": 8126 }, "https": { "active": 1, "port": 8026 }, "vnc": { "active": 1, "port": 5901 }}',
                     ssh_username='root', ssh_password='password')

        self.assertTrue(True, validator.isValidHostName(test1.name))

    @unittest.skip("Skip")
    def test_count_server(self):
        lib_server = LibServer()
        self.assertEquals(15, lib_server.count_servers())

    @unittest.skip("Skip")
    def test_clean_servers(self):
        lib_server = LibServer()
        lib_server.delete_servers()
        self.assertEquals(0, lib_server.count_servers())


if __name__ == '__main__':
    unittest.main()
