__author__ = 'rupali'
import platform
import json
import logging
import sys
import time
import datetime
import re

#if platform.system() == 'Linux':
 #   LOGFILEPATH = '/usr/local/src/imbue/application/imbue/log/'   # + 'trace_apitest_' + DATE_STR + '.log'
#else:
#     LOGFILEPATH = 'C:/Users/rupal/Documents/software/development/log/'    # + 'trace_apitest_' + DATE_STR + '.log'

sys.path.append('/usr/local/src/imbue/application/imbue')
from conf import settings
from conf import logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
#, logging_file=settings.logfile_apitest)
log.setLevel(level=logging.DEBUG)

def check_api_ok(response):
    """
    Function to check version of API
    """
    if response.text == '"version: 1.0"':
        return True
    else:
        return False

def report_toolserver(response):
    """
    check the status. If all hosts have status = -1, then toolserver is down
    :param response:
    :return:
    """
    try:
        count = 0
        list_data = (response.text).split('}')
        for data in list_data:
            st_data = data.split(',')
            for status in st_data:
                stat = status.split(':')
                stat[0].strip('\n\r\t')
                if '"status"' in stat[0]:
                    stat[1].strip('\n\r\t')
                    if int(stat[1]) > 0 or int(stat[1]) == 0:
                        count = count + 1
                        pass
        if count == 0:
            #print 'Tool server down'
            return False
        return True
    except Exception as ex:
        print 'In except of report_toolserver: ', ex
        log.error('In except of report_toolserver: ' + str(ex))
        return False


def get_vmid(response):
    """
    Function to get vmid
    """
    try:
        vmid = ''
        lst = (response.text).split(",")
        status_id = ''
        for job in lst:
            jobs = job.split(':')
            if '"status"' in jobs[0]:
                status_id = jobs[1]
            if '"vmid"' in jobs[0]:
                if int(status_id) > -1:
                    vid = jobs[1].split('}')
                    vid[0].strip('\n\r\t')
                    v_id = vid[0].split(' ')
                    vmid = v_id[1]
                    vmid.strip()
                    break
        return vmid
    except Exception as ex:
        print 'In except of get_vmid: ',ex
        log.error('In except of get_vmid: ' + str(ex))
        return vmid

def get_hostid_fromjob(respond):
    """
    Get jobid from job/jobid GET request
    """
    try:
        hostid = ''
        flag = False
        job_info = (respond.text).split(',')
        for job in job_info:
            if '"host"' in job:
               #print 'if'
                hosts = job.split('{')
                hostids = hosts[1].split(':')
               # print hostids
                #print hosts[1]
                if hostids[1] == 'null':
                   # print 'if hosts'
                    #hostid = hostids[1]
                    return hostid
                else:
                   # print 'else'
                    hosts = job.split('{')
                   # print 'hosts = ',hosts
                    hostids = hosts[2].split(':')
                #print hostids[0]
                    host = hostids[0].split('"')
                #print host[1]
                    hostid = host[1]
        return hostid
    except Exception as ex:
        print 'In except of get_hostid_fromjob: ', ex
        log.error("In except of get_hostid_fromjob: " + str(ex))
        return hostid

def get_infra_id(response):
    """
    Function to get id of infrastructure GET
    """
    try:
        infra_id = ''
        lst = (response.text).split(",")
        id1 = ''
        for job in lst:
            jobs = job.split(':')
            if '"id"' in jobs[0]:
                id1 = jobs[1].strip()
                infra_id = id1.strip()
                break
        return infra_id
    except Exception as ex:
        print 'In except of get_infra_id: ', ex
        log.error('In except of get_infra_id: ' + str(ex))

def report_error_instance(response):
    """
    Check the text of response received by
    job/jobid from instance POST for errors
    If error, returns true
    """
    data = (response.text).split(',')
    if(re.search('error', response.text, re.IGNORECASE)) or ('completed', response.text, re.IGNORECASE) or ('"null"' not in data[5]):
        return True
    return False






