#!/bin/bash
#
# Program: Deploy new code
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3

echo ''
echo 'imbue() Testing API'

API_USERNAME="ACda38cb38f1654200b439724edec211c1"
API_PASSWORD="43b6e91ca96217e0be09c6f3fe94ec31"
BASE_URL="http://0.0.0.0:8080/api/1.0"
HEADERS="Content-Type: application/json"

processHttpStatus() {

    if [ "$#" -ne 2 ]; then
        echo "imbue() Usage: $0 processHttpStatus HTTP_CODE HTTP_URL " >&2
        exit 1
    fi

    if [ $2 -eq 200 ] || [ $2 -eq 202 ]; then
        echo "imbue() Request HTTP code: "$2" Request success"
    else
        echo "imbue() Error HTTP Error code: "$2" Request failed"
        exit 1
    fi
}

testUrl() {

    BASE_URL=$BASE_URL$1
    HTTP_RESULT="$(curl -s -o /dev/null -w %{http_code}:%{time_total} -u ${API_USERNAME}:${API_PASSWORD} -H "${HEADERS}" ${BASE_URL})";
    HTTP_STATUS=`echo $HTTP_RESULT | cut -d ':' -f 1`
    TOTAL_TIME=`echo $HTTP_RESULT | cut -d ':' -f 2`
    end=$(date +"%T")
    echo "imbue() Response: HTTP Code: ${HTTP_STATUS} | Start time: $start End time: $end |  Total time: ${TOTAL_TIME} | ${BASE_URL} "
    processHttpStatus $BASE_URL $HTTP_STATUS
    BASE_URL="http://0.0.0.0:8080/api/1.0"
}

echo 'imbue() get Hosts'
testUrl "/host/"
echo 'imbue() get Jobs()'
testUrl "/job/"
echo 'imbue() get Infrastructure()'
testUrl "/infrastructure/"

