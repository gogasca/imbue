
__author__ = 'rupal'
import platform

     #198.199.110.24:8080/api/1.0/'

USERNAME = 'ACda38cb38f1654200b439724edec211c1'
PASSWORD = '43b6e91ca96217e0be09c6f3fe94ec31'
HEADERS = {'Content-Type': 'application/json'}
TIME = 1

if platform.system() == 'Linux':
    URL = 'https://parzee-dev-app-sfo1:8443/api/1.0/'
    LOGFILEPATH = '/usr/local/src/imbue/application/imbue/log/'
else:
    URL = 'https://198.199.110.24:8443/api/1.0/'
    LOGFILEPATH = 'C:/Users/rupal/Documents/software/development/log/'

#URL = 'https://' + SERVERNAME + ':8443/api/1.0/'

# Variables for test_clusterandcuc.py
#server26 = "esxi26-ziro1"
server27 = "esxi27-install-test"
server28 = "esxi28-install-test"
server26 = "esxi26-install-test"
OWNER_ID = '37'
host26 = "ziro1.noip.me"
host27 = "ziro2.noip.me"
host28 = "ziro3.noip.me"
user = "root"
password = "password"
httpport28 = "8028"
httpport26 = "8026"
httpport27 = "8027"
sshport28 = "8128"
sshport26 = "8126"
sshport27 = "8127"
vmname = ['cuc-test', 'vcs-test1', 'tps-test1', 'condr-test1']                                #"cuc-test"
clustervms = ['cucm11-pub', 'cucm11-sub', 'cucm11-pub1', 'cup11']    #'cucm-pub-test', 'cucm-sub-test']
#server28 = 'esxi28-install-test'
logfile_apitest = LOGFILEPATH + 'apimonitoringd.log'
vcs_endtime = None
tps_endtime = None
conductor_endtime = None
vcs_flag = False
tps_flag = False
condr_flag = False

DATA_HOST = {"hypervisor": {"type": 101001, "name": "esx26-ziro1-apitest", "description": "ESXi Server", "ip": "ziro1.noip.me", "username": "root", "password": "password", "ssh": 8126, "https": 8026, "vnc": "5900:5909", "discover": "true", "sync": "true", "owner_id":37}}

DATA_INSTANCE = {"application": {"type": 200001, "publisher": "true", "version": "11", "esx_network": "VM Network", "directory": "/vmfs/volumes/datastore1/parzee", "answer_file": "/usr/local/src/imbue/configs/11/cucm/platformConfig.xml", "params": {"hostname": "cucm11-apitest", "ip": "192.168.1.20", "mask": "255.255.255.0", "gw": "192.168.1.1", "admin_username": "admin", "admin_password": "password_clear_text", "ntp_1": "64.71.128.26"}}}

DATA_INFRASTRUCTURE = {"instance":{"type": 100001, "name": "parzee-dev-app-sfo1", "ip": "198.199.110.24", "ssh": 8022, "username": "root", "password": "M1Nub3"}}

DATA_VCS = {"application":{"type":200006,"datastore":"datastore1","directory":"/vmfs/volumes/datastore1/parzee","ova_file":"/vmfs/volumes/datastore1/OVA-ISO/s42700x8_7_1.ova","esx_network":"VM Network","size": 10,"params":{"hostname":vmname[1], "annotation": "vcs", "ip":"192.168.1.19", "mask":"255.255.255.0", "gw": "110.10.0.254"}}}

DATA_TPS = {"application":{"type": 200007,"datastore":"datastore1","directory":"/vmfs/volumes/datastore1/parzee","ova_file":"/vmfs/volumes/datastore1/OVA-ISO/Cisco_ts_VirtualMachine_4.2_4.23.ova","esx_network":"VM Network","size": 10,"params":{"hostname":vmname[2], "annotation": "Cisco TelePresence Server","ip":"192.168.1.20", "mask":"255.255.255.0", "gw": "110.10.0.254" }}}

DATA_CONDUCTOR = {"application":{"type": 200008,"datastore":"datastore1","directory":"/vmfs/volumes/datastore1/parzee","ova_file":"/vmfs/volumes/datastore1/OVA-ISO/conductor-xc4_1_0.ova","esx_network":"VM Network","size": 10,"params":{"hostname":vmname[3],"annotation": "Cisco TelePresence Conductor","ip":"192.168.1.21","mask":"255.255.255.0","gw": "110.10.0.254"}}}

DATA_CLUSTER = {"instance":{"host":{"id":533}, "application":{"type":200001, "params": {"hostname":'pub-apitest', "ip": "192.168.1.14", "mask": "255.255.255.0", "gw": "192.168.1.1", "is_installed":"false"}, "publisher":"true", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file": "/usr/local/src/imbue/configs/11/cucm/cluster/pub/platformConfig.xml", "cluster_file":"/usr/local/src/imbue/configs/11/cucm/cluster/pub/clusterConfig.xml"}}}, {"instance":{"host": {"id":533}, "application":{"type": 200001, "params": {"hostname": "sub-apitest"  , "ip": "192.168.1.15", "mask":"255.255.255.0", "gw": "192.168.1.1"}, "publisher":"false", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file":"/usr/local/src/imbue/configs/11/cucm/cluster/sub/platformConfig.xml"}}}
