"""
unittests to atomate API test cases
"""

import unittest

import test_api
import test_configuration
import test_clusterandcuc

__author__ = 'rupali'

host_jobid = ''
hostid = ''
instance_jobid = ''
cluster_jobid = ''
vmid = ''
infra_id = ''
instance_host_id = ''


class TestAPI(unittest.TestCase):
    """
    class for API test cases
    """
#test 1 API OK
    def test_0(self):
        """
        test 1 API OK
        """
        result = test_api.api_ok()
        self.assertEqual(True, result)

#test 2 infrastructure GET
    def test_1(self):
        """
        test 2 infrastructure GET
        """
        global infra_id
        infra_id = test_api.get_infrastructure()
        self.assertNotEqual(infra_id, '')

#test 3 infrastructure/<id> GET
    def test_2(self):
        """
        test 3 infrastructure/<id> GET
        """
        result = test_api.get_req(urlstr='infrastructure/' + infra_id)
        self.assertEqual(True, result)

#test 4 echo GET
    def test_3(self):
        """
        test 4 echo GET
        """
        result = test_api.get_req(urlstr='echo/')
        self.assertEqual(result, True)

#test 5 job/?status=0&owner_id=37 GET
    def test_4(self):
        """
        test 5 job/?status=0&owner_id=37 GET
        """
        result = test_api.get_req(urlstr='job/?status=0&owner_id=37')
        self.assertEqual(result, True)

#test 6 host POST
    def test_5(self):
        """
        test 6 host POST
        """
        global host_jobid
        host_jobid = test_api.post_req(urlstr='host/', data=test_configuration.DATA_HOST)
        self.assertNotEqual(host_jobid, '')

#test 7 host GET
    def test_6(self):
        """
        test 7 host GET
        """
        result = test_api.get_req(urlstr='host/')
        self.assertEqual(result, True)

#test 8 job/job reference GET
    def test_7(self):
        """
        test 8 job/job reference GET
        """
        global hostid
        hostid = test_api.get_hostid_fromhostjobid(jobid=host_jobid)
        self.assertNotEqual(hostid, '')

#test 9 host/<id> GET
    def test_8(self):
        """
        test 9 host/<id> GET
        """
        global hostid
        #print 'hostid = ',hostid
        if hostid == '':
            hostid = '*'
        result = test_api.get_req(urlstr='host/' + hostid)
        self.assertTrue(result)

#test 10 host/<id>/instance POST
    def test_9(self):
        """
        test 10 host/<id>/instance POST
        """
        global instance_jobid
        global instance_host_id
        instance_host_id = test_install_support.get_hostid(test_configuration.server26)
        instance_jobid = test_api.post_req(urlstr='host/' + instance_host_id + '/instance/',
                                           data=test_configuration.DATA_INSTANCE)
        self.assertNotEqual(instance_jobid, '')

# test 10 host/<id>/instance GET
    def test_90(self):
        """
        test 10 host/<id>/instance GET
        """
        global vmid
        vmid = test_api.get_instance(urlstr='host/' + instance_host_id + '/instance/')
        self.assertNotEqual(vmid, '')

#test 11 host/<id>/instance/vmid GET
    def test_91(self):
        """
        test 11 host/<id>/instance/vmid GET
        """
        global vmid
        if not vmid:
            vmid = '*'
        result = test_api.get_req(urlstr='host/' + instance_host_id + '/instance/' + vmid)
        self.assertEqual(True, result)

#test 12 job/instance_job_reference GET
    def test_92(self):
        """
        test 12 job/instance_job_reference GET
        """
        result = test_api.get_req(urlstr='job/' + instance_jobid)
        self.assertEqual(result, True)

#test 13 cluster POST
    def test_93(self):
        """
        test 13 cluster POST
        """
        global cluster_jobid
        cluster_jobid = test_api.post_req(urlstr='cluster/', data=test_configuration.DATA_CLUSTER)
        self.assertNotEqual(cluster_jobid, '')

#test 14 job/cluster_job_reference GET
    def test_94(self):
        """
        test 14 job/cluster_job_reference GET
        """
        result = test_api.get_req(urlstr='job/' + cluster_jobid)
        self.assertEqual(result, True)

#test 15 job GET
    def test_95(self):
        """
        test 15 job GET
        """
        result = test_api.get_req(urlstr='job/')
        self.assertEqual(True, result)

#test 16 (update server) host PUT
    def test_96(self):
        """
        test 16 host PUT
        """


#test 17 job/instance_job_reference DELETE
    def test_97(self):
        """
        test 17 job/instance_job_reference DELETE
        """
        result = test_api.delete_jobs_ids(urlstr='job/' + instance_jobid)
        self.assertTrue(result)

#test 18 job/cluster_job_reference DELETE
    def test_98(self):
        """
        test 18 job/cluster_job_reference DELETE
        """
        result = test_api.delete_jobs_ids(urlstr='job/' + cluster_jobid)
        self.assertTrue(result)

#test 19 host/<id>/instance/vnid DELETE
    def test_99(self):
        """
        test 19 host/<id>/instance/vnid DELETE
        """
        result = test_api.delete_jobs_ids(urlstr='host/' + hostid + '/instance/' + vmid)
        self.assertTrue(result)

#test 20 infrastructure/<id> DELETE
    def test_990(self):
        """
        test 20 infrastructure/<id> DELETE
        """
        result = test_api.delete_jobs_ids(urlstr='infrastructure/' + infra_id)
        self.assertTrue(result)

#test 21 host/<id> DELETE
    def test_991(self):
        """
        test 21 host/<id> DELETE
        """
        result = test_api.delete_jobs_ids(urlstr='host/' + hostid)
        self.assertEqual(result, True)

if __name__ == '__main__':
    unittest.main()


