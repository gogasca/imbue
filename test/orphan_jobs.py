"""
Check for orphan jobs and update it.
"""
__author__ = 'rupali'
import platform
import sys
import time
import datetime
from datetime import timedelta
import logging
import sched

sys.path.append('/usr/local/src/imbue/application/imbue')

file_name = 'trace_orphanjobs_' + datetime.datetime.now().strftime("%m%d") + '.log'

if platform.system() == 'Linux':
    LOGFILEPATH = '/usr/local/src/imbue/application/imbue/log/' + file_name
else:
    LOGFILEPATH = 'C:/Users/rupal/Documents/software/development/log/' + file_name

logging.basicConfig(filename=LOGFILEPATH, level=logging.INFO)
logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
print 'Date: ', datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
print 'Log File : ', LOGFILEPATH

def track_orphan_jobs():
    """Check the end time of jobs. If it is NULL and current time is > 2 hours
    update description, status and endtime
    """
    try:
        logging.info('\n')
        from database import database_session
        from conf import settings
        from database import Db
        sqlquery = """SELECT * FROM job"""
        db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
        result = db.execute(sqlquery)
        if result:
            #print result
            print 'Processing ................'
            for row in result:
                if row['job_end']:
                    logging.info("No NULL job_end: " + str(row['reference']))
                   # print 'no null job_end'
                else:
                    query = ''
                    current = datetime.datetime.now()
                    start_job = row['job_start']
                    diff = current - start_job
                    description = "'job expired'"
                    if row['is_cluster']:
                        if diff > timedelta(hours=4):
                            logging.info(row)
                            query = """UPDATE job SET status=-1, description=""" + description + """, job_end=""" +  """'""" + str(current) + """' WHERE job.reference='""" + str(row['reference']) + """'"""
                        else:
                            logging.info('........no cluster update.........')
                            logging.info(row)
                           # print 'no cluster update'
                    else:
                        if diff > timedelta(hours=2):
                            logging.info(row)
                            query = """UPDATE job SET status=-1, description=""" + description + """, job_end=""" + """'""" + str(current) + """' WHERE job.reference='""" + str(row['reference']) + """'"""
                        else:
                            logging.info('........No update........')
                            logging.info(row)
                           # print 'no update'
                    result = db.execute(query)
            print 'Completed'        

        else:
            print 'No rows are selected '
            logging.error('No rows are selected')
    except Exception as ex:
        print 'In except of track_orphan_jobs: ', ex
        logging.info('In except of track_orphan_jobs: ' + str(ex))


job_start = datetime.datetime.now()
print 'start time = ', job_start
logging.info('start time = ' + str(job_start))
track_orphan_jobs()
schedule_hour = sched.scheduler(time.time, time.sleep)

def run_orphanjobs_script(schedule_hour):
    """
    run track_orphan_jobs() every hour
    """
    track_orphan_jobs()
    end = datetime.datetime.now()
    print 'End time = ', end
    logging.info('end time = ' + str(end))
    schedule_hour.enter(3600, 1, run_orphanjobs_script, (schedule_hour, ))

schedule_hour.enter(3600, 1, run_orphanjobs_script, (schedule_hour, ))
schedule_hour.run()
