__author__ = 'gogasca'

import unittest
import platform
import sys
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
from utils import validator
import json

class TestHostConfig(unittest.TestCase):
    """
    Our basic test class
    """
    def test_host(self):
        configuration = """{
            "host": {
                "hypervisor":{
                      "type": 101001,
                      "name": "esx-milpitas",
                      "description": "Milpitas ESX",
                      "ip": "110.10.0.144",
                      "username": "root",
                      "password": "password",
                      "ssh": 22,
                      "https": 443,
                      "vnc": "5900:5910",
                      "discover": true,
                      "sync": true,
                      "owner_id": 1
                }
            }
        }"""

        configuration = json.loads(configuration)
        self.assertTrue(validator.check_host_req(configuration))

    def test_host_invalid(self):
        """
        No type under hypervisor
        :return:
        """
        configuration = """{
            "host": {
                "hypervisor":{
                      "name": "esx-milpitas",
                      "ip": "110.10.0.144",
                      "username": "root",
                      "password": "password",
                      "ssh": 22,
                      "https": 443,
                      "vnc": "5900:5910",
                      "discover": true,
                      "sync": true,
                      "owner_id": 1
                }
            }
        }"""

        configuration = json.loads(configuration)
        self.assertFalse(validator.check_host_req(configuration))

    def test_host_sync(self):
        """
        No type under hypervisor
        :return:
        """
        configuration = """{ "force": true, "reset": true }"""
        configuration = json.loads(configuration)
        self.assertTrue(validator.check_host_sync_req(configuration))

    def test_tools(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        print self._testMethodName
        configuration = """{
          "infrastructure": {
            "instance": {
              "type": 100001,
              "name": "parzee-db-tools-sfo1",
              "ip": "198.199.110.24",
              "ssh": 8022,
              "username": "root",
              "password": "M1Nub3"
            }
          }
        }"""

        configuration = json.loads(configuration)
        self.assertTrue(validator.check_infrastructure_req(configuration))

    def test_tools_invalid(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        print self._testMethodName
        configuration = """{
          "infrastructure": {
            "instance": {
              "name": "parzee-db-tools-sfo1",
              "ip": "198.199.110.24",
              "ssh": 22,
              "username": "root",
              "password": "M1Nub3"
            }
          }
        }"""

        configuration = json.loads(configuration)
        self.assertFalse(validator.check_infrastructure_req(configuration))

    def test_instance_200001(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        print self._testMethodName
        configuration = """{
        "instance": {
            "host": {
              "id": 15
            },
            "application": {
              "type": 200001,
              "publisher": true,
              "version": "90",
              "esx_network": "VM Network",
              "directory": "/vmfs/volumes/datastore1/parzee",
              "answer_file": "/Users/gogasca/Desktop/configs/test1a/platformConfig.xml",
              "params": {
                "hostname": "pub90",
                "ip": "110.10.0.201",
                "mask": "255.255.255.0",
                "gw": "110.10.0.254"
              }
            }
          }
        }
        """

        configuration = json.loads(configuration)
        self.assertTrue(True, validator.check_imbue_instance(configuration))


    def test_instance_200001_invalid(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        print self._testMethodName
        configuration = """{
        "instance": {
            "application": {
              "type": 200001,
              "publisher": true,
              "version": "90",
              "esx_network": "VM Network",
              "directory": "/vmfs/volumes/datastore1/parzee",
              "answer_file": "/Users/gogasca/Desktop/configs/test1a/platformConfig.xml",
              "params": {
                "hostname": "pub90",
                "ip": "110.10.0.201",
                "mask": "255.255.255.0",
                "gw": "110.10.0.254"
              }
            }
          }
        }
        """

        configuration = json.loads(configuration)
        self.assertFalse(validator.check_imbue_instance(configuration))

    def test_cluster_200001(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        configuration = """[
            { "instance":{
                "host": {
                    "id": 281
                },
                "application":{
                "type": 200001,
                "params": {
                    "hostname": "cucm11-pub",
                    "ip": "192.168.1.13",
                    "mask": "255.255.255.0",
                    "gw": "192.168.1.1",
                    "is_installed": true
                },
                "publisher":true,
                "version":"11",
                "esx_network":"VM Network",
                "directory":"/vmfs/volumes/datastore1/parzee",
                "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/platformConfig.xml",
                "cluster_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/clusterConfig.xml"
                }
            }
        },{
            "instance":{
                "host": {
                    "id": 281
                },
                "application":{
                "type": 200001,
                "params": {
                    "hostname": "cucm11-sub",
                    "ip": "192.168.1.14",
                    "mask": "255.255.255.0",
                    "gw": "192.168.1.1"
                },
                "publisher":false,
                "version":"11",
                "esx_network":"VM Network",
                "directory":"/vmfs/volumes/datastore1/parzee",
                "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/sub/platformConfig.xml"
                }
            }
        }]
        """

        configuration = json.loads(configuration)
        self.assertTrue(validator.check_cluster_req(configuration))

    def test_cluster_200001_invalid(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        configuration = """[
            { "instance":{
                "host": {
                    "id": 281
                },
                "application":{
                "type": 200004,
                "params": {
                    "hostname": "cucm11-pub",
                    "ip": "192.168.1.13",
                    "mask": "255.255.255.0",
                    "gw": "192.168.1.1",
                    "is_installed": true
                },
                "publisher":true,
                "version":"11",
                "esx_network":"VM Network",
                "directory":"/vmfs/volumes/datastore1/parzee",
                "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/platformConfig.xml",
                "cluster_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/clusterConfig.xml"
                }
            }
        },{
            "instance":{
                "host": {
                    "id": 281
                },
                "application":{
                "type": 200001,
                "params": {
                    "hostname": "cucm11-sub",
                    "ip": "192.168.1.14",
                    "mask": "255.255.255.0",
                    "gw": "192.168.1.1"
                },
                "publisher":false,
                "version":"11",
                "esx_network":"VM Network",
                "directory":"/vmfs/volumes/datastore1/parzee",
                "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/sub/platformConfig.xml"
                }
            }
        }]
        """

        configuration = json.loads(configuration)
        self.assertFalse(validator.check_cluster_req(configuration))

    def test_cluster_200001_200003(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        configuration = """[
        { "instance":{
            "host": {
                "id": 281
            },
            "application":{
            "type": 200001,
            "params": {
                "hostname": "cucm11-pub",
                "ip": "192.168.1.11",
                "mask": "255.255.255.0",
                "gw": "192.168.1.1",
                "is_installed": true
            },
            "publisher":true,
            "version":"11",
            "esx_network":"VM Network",
            "directory":"/vmfs/volumes/datastore1/parzee",
            "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/platformConfig.xml",
            "cluster_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/pub/clusterConfig.xml"
            }
        }
    },{
        "instance":{
            "host": {
                "id": 281
            },
            "application":{
            "type": 200003,
            "params": {
                "hostname": "cups",
                "ip": "192.168.1.14",
                "mask": "255.255.255.0",
                "gw": "192.168.1.1"
            },
            "publisher":true,
            "version":"11",
            "esx_network":"VM Network",
            "directory":"/vmfs/volumes/datastore1/parzee",
            "answer_file":"/Users/gogasca/Desktop/configs/11/cucm/cluster/sub/platformConfig.xml"
            }
        }
    }]
        """

        configuration = json.loads(configuration)
        self.assertTrue(validator.check_cluster_req(configuration))

if __name__ == '__main__':
    unittest.main()
