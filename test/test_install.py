import unittest
import platform
import sys
import json

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
from utils import validator

class TestInstallApplication(unittest.TestCase):
    """
    Our basic test class
    """
    def test_install_cucm(self):
        request = """
        {
          "application": {
            "type": 200001,
            "publisher": true,
            "version": "11",
            "esx_network": "VM Network",
            "directory": "/vmfs/volumes/datastore1/parzee",
            "answer_file": "/Users/gogasca/Desktop/configs/11/cucm/platformConfig.xml",
            "params": {
              "hostname": "cucm11pub",
              "ip": "192.168.1.20",
              "mask": "255.255.255.0",
              "gw": "192.168.1.1",
              "admin_username": "admin",
              "app_username": "ccmadmin",
              "dns": {
                "domain_name": "parzee.com",
                "dns_1": "64.71.128.26"
              },
              "time": {
                "ntp_1": "192.5.41.41",
                "ntp_2": "192.5.41.42",
                "ntp_3": "192.5.41.43"
              },
              "certificate": {
                "cert_org": "Parzee Ltd",
                "cert_unit": "Sales",
                "cert_location": "Oxford",
                "cert_state": "Oxfordshire",
                "cert_country": "UK"
              },
              "mtu": 1500,
              "smtp": "smtp.parzee.com",
              "is_installed": false
            }
          }
        }
        """

        request = json.loads(request)
        self.assertTrue(validator.check_instance_req(request))

if __name__ == '__main__':
    unittest.main()
