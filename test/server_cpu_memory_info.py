__author__ = 'rupali'
import re
import sys
import logging
sys.path.append('/usr/local/src/imbue/application/imbue')
from test_api import test_configuration
from conf import settings, logging_conf
from hypervisor.esxi import general

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

def memory_info(command, host, username, password, port, display):
    info = ' Memory: '
    Memory = 0
    res = general.send_command(
                commandList=command,           #['esxcli hardware memory get'],
                server=host,                #test_configuration.cuchost,
                username=username,              #test_configuration.user,
                password=password,               #test_configuration.password,
                port=port,                  #test_configuration.sshport,
                display=display)               #True)
    if res is not None:
        for result in res:
            if (re.search('Memory', result, re.IGNORECASE)):
                r = result.split(':')
                memory = r[1].split('Bytes')
                Memory = Memory + float(memory[0])/1073741824
        info = info + str(Memory) + ' GB'
    log.info('memory_info(): ' + info)
    return info

def cpu_info(command, host, username, password, port, display):
    strinfo =''
    result = general.send_command(
                commandList=command,
                server=host,
                username=username,
                password=password,
                port=port,
                display=display)
    if result is not None:
        for res in result:
            if(re.search('cpu', res, re.IGNORECASE)) or (re.search('thread', res, re.IGNORECASE)):
                res.strip('\n\r\t')
                info = res.split('\n')
                strinfo = strinfo + str(info[0]) + ' |'
    log.info('cpu_info(): ' + strinfo)
    return strinfo

def platform_info(command, host, username, password, port, display):
    strinfo =''
    result = general.send_command(
                commandList=command,
                server=host,
                username=username,
                password=password,
                port=port,
                display=display)
    if result is not None:
        for res in result:
            if(re.search('Product', res, re.IGNORECASE)) or (re.search('Vendor', res, re.IGNORECASE)):
                r = res.split('\n')
                strinfo = strinfo + r[0] + ' |'
    log.info('platform_info(): ' + strinfo)
    return strinfo

def str_info():
    info = ''
    info = platform_info(['esxcli hardware platform get'], test_configuration.clusterhost, test_configuration.user, test_configuration.password, test_configuration.clustersshport, True)
    info = info + cpu_info(['esxcli hardware cpu global get'], test_configuration.clusterhost, test_configuration.user, test_configuration.password, test_configuration.clustersshport, True)
    info = info + memory_info(['esxcli hardware memory get'], test_configuration.clusterhost, test_configuration.user, test_configuration.password, test_configuration.clustersshport, True)
    log.info('str_info(): ' + info)
    return info

def main():
    info = str_info()
    print info

if __name__ == "__main__":
    main()
