"""
Create the .mp4 video file with given .png images
"""

__author__ = 'Rupali'
import os
import subprocess
import imghdr
#sys.path.append('/usr/local/src/imbue/application/imbue')
#import /utils/shell/rename_screen.sh

JOBID = '994929F0DB'
PATH = '/usr/local/src/imbue/application/imbue/log/jobs/' + JOBID

def sequence_files(path):
    """
    to determine if the files' names
    are already in sequence
    """
    try:
        count = 0
        num = 1
        files = os.listdir(path)
        files.sort()
        for name in files:
            filename = os.path.join(path, name)
            if os.path.isfile(filename):
                if imghdr.what(filename) == 'png':
                    count = count + 1
        for fle in files:
            filename = os.path.join(path, fle)
            if os.path.isfile(filename):
                if imghdr.what(filename) == 'png':
                    if num > 99:
                        newfile = 'screen_0' + str(num) + '.png'
                    if num > 9:
                        newfile = 'screen_00' + str(num) + '.png'
                    else:
                        newfile = 'screen_000' + str(num) + '.png'
                    if fle == newfile:
                        num = num + 1
        if (num-1) == count:
            print 'Filenames are in sequence'
            return True
        else:
            print 'Filenames are NOT in sequence'
            return False
    except Exception as ex:
        print 'In except of sequence_files(), ', ex
        return False


def create_video(filepath):
    """
    create mp4 file from png images
    """
    try:
        if True is not sequence_files(filepath):
            subprocess.call(['/usr/local/src/imbue/application/imbue/utils/shell/rename_screen.sh'])
        out_movie = filepath + '/' + JOBID + '.mp4'
        subprocess.call('ffmpeg -f image2 -r 1 -i ' + str(filepath)+'/screen_%04d.png -vcodec mpeg4 '+ str(out_movie), shell=True)
        #shutil.rmtree(temp_folder)
        return str(out_movie)
    except Exception as ex:
        print 'In except of create_video: ', ex
        return None

output = create_video(PATH)
print output
