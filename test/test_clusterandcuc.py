"""
Overnight Script of installation
"""
__author__ = 'rupali'
import json
import logging
import sys
import datetime
import time
from datetime import timedelta

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.append('/usr/local/src/imbue/application/imbue')

from alarms import send_sms
import test_configuration
#import test_api_support
from conf import logging_conf
import test_install_support
logfile = test_configuration.LOGFILEPATH + 'trace_clusterandcuctest_' + datetime.datetime.now().strftime("%m%d") + '.log'
log = logging_conf.LoggerManager().getLogger("__app___", logging_file=logfile)
log.setLevel(level=logging.DEBUG)


def count():
    """
    Count the test cases
    """
    count.counter += 1
    #print "Counter is %d" % foo.counter
count.counter = 0
#count = 0

class CheckVM(object):
    """
    class CheckVM to install, check status and monitor
    CUC, CUCM+CUCM, CUCM+CUP
    """
    def __init__(self):
        self.URLSSL = test_configuration.URL
        #url = 'https://prod:
        self.USERNAME = test_configuration.USERNAME
        self.PASSWORD = test_configuration.PASSWORD
        self.HEADERS = test_configuration.HEADERS
        self.OWNER_ID = test_configuration.OWNER_ID
        #self.count = 0
        self.host26 = test_configuration.host26
        self.host27 = test_configuration.host27
        self.host28 = test_configuration.host28
        self.user = test_configuration.user
        self.password = test_configuration.password
        self.httpport28 = test_configuration.httpport28
        self.httpport27 = test_configuration.httpport27
        self.httpport26 = test_configuration.httpport26
        self.sshport28 = test_configuration.sshport28
        self.sshport27 = test_configuration.sshport27
        self.sshport26 = test_configuration.sshport26
        self.vmname = test_configuration.vmname[0]
        self.clustervms = test_configuration.clustervms
        count.counter = 0
        self.end_time = None

    def count(self):
        """
        Count the test cases
        """
        count.counter += 1
    #print "Counter is %d" % foo.counter

    def sethost(self, hostid=None):
        """
        Set hostid value
        """
        self.hostid = hostid

    def gethost(self):
        """
        Get hostid value
        """
        return self.hostid


    def install_cluster_cucm(self):
        """
        Install cluster - post request with cluster/instance data
        :return:
        """
        try:
            job_id = ''
            url = self.URLSSL + 'cluster/'
            hostid = test_install_support.get_hostid(test_configuration.server27)
            data = {"instance":{"host":{"id":int(hostid)}, "application":{"type":200001, "params": {"hostname":self.clustervms[0], "ip": "192.168.1.14", "mask": "255.255.255.0", "gw": "192.168.1.1", "is_installed":"false"}, "publisher":"true", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file": "/usr/local/src/imbue/configs/11/cucm/cluster/pub/platformConfig.xml", "cluster_file":"/usr/local/src/imbue/configs/11/cucm/cluster/pub/clusterConfig.xml"}}}, {"instance":{"host": {"id": int(hostid)}, "application":{"type": 200001, "params": {"hostname": self.clustervms[1], "ip": "192.168.1.15", "mask":"255.255.255.0", "gw": "192.168.1.1"}, "publisher":"false", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file":"/usr/local/src/imbue/configs/11/cucm/cluster/sub/platformConfig.xml"}}}
            log.info('cluster_cucm hostid = ' + hostid)
            log.info('install_cluster_cucm(): ' + url)
            if hostid:
                self.sethost(hostid=hostid)
                print 'cluster_cucm hostid = ', hostid
                response_post = requests.post(url=url, data=json.dumps(data), verify=False,
                                              auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
                if response_post.status_code == 202:
                    log.info(str(response_post.status_code))
                    log.info(response_post.text)
                    job = (response_post.text).split(':')
                    jobid = job[1].split('"')
                    print jobid[1]
                    job_id = jobid[1]
                    return job_id
                else:
                    log.error(str(response_post.raise_for_status()))
                    log.error(response_post.text)
                    print 'ERROR in installation of cluster CUCM + CUCM'
                    return job_id
            else:
                log.error('install_cluster_cucm(): hostid is not available')
                print 'install_cluster_cucm(): hostid is not available'
                return job_id
        except Exception as ex:
            log.error('In except of install_cluster_cucm(): ' + str(ex))
            print 'In except install_cluster_cucm(): ', ex
            return job_id

    def install_cluster_cup(self):
        """
        Install cluster - post request with cluster/instance data
        :return:
        """
        try:
            job_id = ''
            url = self.URLSSL + 'cluster/'
            hostid = test_install_support.get_hostid(test_configuration.server28)
            #data = {"instance":{"host":{"id":int(hostid)}, "application":{"type":200001, "params": {"hostname":self.clustervms[2], "ip": "192.168.1.16", "mask": "255.255.255.0", "gw": "192.168.1.1", "is_installed":"false"}, "publisher":"true", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file": "/usr/local/src/imbue/configs/11/cucm/cluster/pub/platformConfig.xml", "cluster_file":"/usr/local/src/imbue/configs/11/cucm/cluster/pub/clusterConfig.xml"}}}, {"instance":{"host": {"id": int(hostid)}, "application":{"type": 200003, "params": {"hostname": self.clustervms[3], "ip": "192.168.1.17", "mask":"255.255.255.0", "gw": "192.168.1.1"}, "publisher":"true", "version":"11", "esx_network":"VM Network", "directory":"/vmfs/volumes/datastore1/parzee", "answer_file":"/usr/local/src/imbue/configs/11/cup/platformConfig.xml"}}}
            data = """ [
                          { "instance":{
                               "host": {
                                 "id": """ + str(hostid) + """},
                               "application":{
                               "type": 200001,
                               "params": {
                                  "hostname": "cucm11-pub",
                                  "ip": "192.168.1.16",
                                  "mask": "255.255.255.0",
                                  "gw": "192.168.1.1",
                                  "is_installed": false
                               },
                               "publisher":true,
                               "version":"11",
                               "esx_network":"VM Network",
                               "directory":"/vmfs/volumes/datastore1/parzee",
                               "answer_file":"/usr/local/src/imbue/configs/11/cucm/cluster/pub/platformConfig.xml",
                               "cluster_file":"/usr/local/src/imbue/configs/11/cucm/cluster/pub/clusterConfig.xml"
                               }
                           }
                          },{
                             "instance":{
                                "host": {
                                   "id": """ + str(hostid) +   """},
                                "application":{
                                    "type": 200003,
                                    "params": {
                                    "hostname": "cup11",
                                    "ip": "192.168.1.17",
                                    "mask": "255.255.255.0",
                                    "gw": "192.168.1.1"
                                },
                               "publisher":true,
                               "version":"11",
                               "esx_network":"VM Network",
                               "directory":"/vmfs/volumes/datastore1/parzee",
                               "answer_file":"/usr/local/src/imbue/configs/11/cup/platformConfig.xml"
                                }
                             }
                          }]"""
            log.info('cluster_cup hostid = ' + hostid)
            log.info('install_cluster_cup(): ' + url)
            if hostid:
                self.sethost(hostid=hostid)
                print 'cluster_cup hostid = ', hostid
                response_post = requests.post(url=url, data=json.dumps(data), verify=False,
                                              auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
                if response_post.status_code == 202:
                    log.info(str(response_post.status_code))
                    log.info(response_post.text)
                    job = (response_post.text).split(':')
                    jobid = job[1].split('"')
                    print jobid[1]
                    job_id = jobid[1]
                    return job_id
                else:
                    log.error(str(response_post.raise_for_status()))
                    log.error(response_post.text)
                    print 'ERROR in installation of cluster CUCM + CUP'
                    return job_id
            else:
                log.error('install_cluster_cup(): hostid is not available')
                print 'install_cluster_cup(): hostid is not available'
                return job_id
        except Exception as ex:
            log.error('In except of install_cluster_cup(): ' + str(ex))
            print 'In except install_cluster_cup(): ', ex
            return job_id

    def install_cuc(self):
        """
        Install cuc - post request with cluster/instance data
        :return:
        """
        try:
            job_id = ''
            hostid = test_install_support.get_hostid(test_configuration.server26)
            data = {"application":{"type": 200001, "publisher": "true", "version": "11", "esx_network": "VM Network", "directory": "/vmfs/volumes/datastore1/parzee", "answer_file":"/usr/local/src/imbue/configs/11/cucm/platformConfig.xml", "params":{"hostname": self.vmname, "ip": "192.168.1.18", "mask":"255.255.255.0", "gw": "192.168.1.1"}}}
            url = self.URLSSL + 'host/' + hostid + '/instance/'
            log.info('cuc hostid = ' + hostid)
            log.info('install_cuc(): ' + url)
            if hostid:
                self.sethost(hostid=hostid)
                print 'cuc hostid = ', hostid
                response_post = requests.post(url=url, data=json.dumps(data), verify=False,
                                              auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
                if response_post.status_code == 202:
                    log.info(str(response_post.status_code))
                    log.info(response_post.text)
                    job = (response_post.text).split(':')
                    jobid = job[1].split('"')
                    print jobid[1]
                    job_id = jobid[1]
                    return job_id
                else:
                    log.error(str(response_post.raise_for_status()))
                    log.error(response_post.text)
                    print 'ERROR in installation of cluster'
                    return job_id
            else:
                log.error('install_cuc(): hostid is not available')
                print 'install_cuc(): hostid is not available'
                return job_id
        except Exception as ex:
            log.error('In except of install_cuc(): ' + str(ex))
            print 'In except install_cuc(): ', ex
            return job_id

    def check_status_cuc(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status for cuc
        """
        try:
            statusflag = True
            url = self.URLSSL + 'job/' + jobid
            log.info('test_clusterandcuc: check_status_cuc(): ' + url)
            response = requests.get(url=url, verify=False,
                                    auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
            if response.status_code == 200:
                log.info(str(response.status_code))
                log.info(response.text)
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                    statusflag = False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                          #  print 'stat = ', status[1]
                            if status[1] == -1:
                               # print 'status = ', status[1]
                                statusflag = False
                                break
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                           # print 'jobend = ',jobend[1]
                            jobd = jobend[1].split(',')
                            #print 'job = ', jobd[0]
                            job = jobd[0].split(' ')
                           # print 'job = ',job[1]
                            self.end_time = job[1]
                           # print self.end_time
                            if job[1] not in ' null':
                                if 'completed' in status_job[0]:
                                    statusflag = True
                                else:
                                    statusflag = False
                                break
            else:
                log.error(str(response.raise_for_status()))
                log.error(response.text)
                statusflag = False
            return statusflag
        except Exception as ex:
            log.error('In except of check_status_cuc(): ' + str(ex))
            print 'In except of check_status_cuc: ', ex
        return False

    def check_status_cluster_cucm(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status
        """
        try:
            statusflag = True
            url = self.URLSSL + 'job/' + jobid
            log.info('test_clusterandcuc: check_status_cluster_cucm(): ' + url)
            response = requests.get(url=url, verify=False,
                                    auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
            if response.status_code == 200:
                log.info(str(response.status_code))
                log.info(response.text)
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                    statusflag = False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                          #  print 'stat = ', status[1]
                            if status[1] == -1:
                               # print 'status = ', status[1]
                                statusflag = False
                                break
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                           # print 'jobend = ',jobend[1]
                            jobd = jobend[1].split(',')
                            #print 'job = ', jobd[0]
                            job = jobd[0].split(' ')
                           # print 'job = ',job[1]
                            self.end_time = job[1]
                           # print self.end_time
                            if job[1] not in ' null':
                                statusflag = False
                                break
            else:
                log.error(str(response.raise_for_status()))
                log.error(response.text)
                statusflag = False
            return statusflag
        except Exception as ex:
            log.error('In except of check_status_cluster_cucm(): ' + str(ex))
            print 'In except of check_status_cluster_cucm: ', ex
        return False

    def check_status_cluster_cup(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status
        """
        try:
            statusflag = True
            url = self.URLSSL + 'job/' + jobid
            log.info('test_clusterandcuc: check_status_cluster_cup(): ' + url)
            response = requests.get(url=url, verify=False,
                                    auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
            if response.status_code == 200:
                log.info(str(response.status_code))
                log.info(response.text)
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                    statusflag = False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                          #  print 'stat = ', status[1]
                            if status[1] == -1:
                               # print 'status = ', status[1]
                                statusflag = False
                                break
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                           # print 'jobend = ',jobend[1]
                            jobd = jobend[1].split(',')
                            #print 'job = ', jobd[0]
                            job = jobd[0].split(' ')
                           # print 'job = ',job[1]
                            self.end_time = job[1]
                           # print self.end_time
                            if job[1] not in ' null':
                                statusflag = False
                                break
            else:
                log.error(str(response.raise_for_status()))
                log.error(response.text)
                statusflag = False
            return statusflag
        except Exception as ex:
            log.error('In except of check_status_cluster_cup(): ' + str(ex))
            print 'In except of check_status_cluster_cup: ', ex
        return False

    def monitor_cuc(self, jobid):
        """
        Monitor the process of cuc installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if count.counter > 60:
                    if not self.end_time:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(minutes=60):
                                success = False
                    break
                delta = 60 - count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install CUC..............')
                print 'Wait for ', delta, ' minutes or less to install CUC........................'
                time.sleep(60)
                result = self.check_status_cuc(jobid)
                if not result:
                    success = False
                    log.error('Installation CUC FAILED: ' + jobid)
                    print 'Installation CUC FAILED: ', jobid
                #sms
                    break
                self.count()
            return success
        except Exception as ex:
            print 'In except of monitor_cuc(): ', ex
            log.error('In except of monitor_cuc(): ' + str(ex))
            return False

    def monitor_cluster_cucm(self, jobid):
        """
        Monitor the process of cluster installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if count.counter > 120:
                    if not self.end_time:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(hours=2):
                                success = False
                    break
                delta = 120 - count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install Cluster CUCM+CUCM..............')
                print 'Wait for ', delta, ' minutes or less to install Cluster CUCM+CUCM........................'
                time.sleep(60)
                result = self.check_status_cluster_cucm(jobid)
                if not result:
                    success = False
                    log.error('Installation CUCM+CUCM FAILED: ' + jobid)
                    print 'Installation CUCM+CUCM FAILED: ', jobid
                #sms
                    break
                self.count()
            return success
        except Exception as ex:
            print 'In except of monitor_cluster_cucm(): ', ex
            log.error('In except of monitor_cluster_cucm(): ' + str(ex))
            return False

    def monitor_cluster_cup(self, jobid):
        """
        Monitor the process of cluster installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if count.counter > 120:
                    if not self.end_time:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(hours=2):
                                success = False
                    break
                delta = 120 - count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install Cluster CUCM+CUP..............')
                print 'Wait for ', delta, ' minutes or less to install Cluster CUCM+CUP.......................'
                time.sleep(60)
                result = self.check_status_cluster_cup(jobid)
                if not result:
                    success = False
                    log.error('Installation CUCM+CUP FAILED: ' + jobid)
                    print 'Installation CUCM+CUP FAILED: ', jobid
                #sms
                    break
                self.count()
            return success
        except Exception as ex:
            print 'In except of monitor_cluster_cup(): ', ex
            log.error('In except of monitor_cluster_cup(): ' + str(ex))
            return False

    def wrapper(self, func, arg, queue):
        """
        To call functions in threads 
        """
        queue.put(func(arg))

    def monitor_report(self):
        """
        run monitor_cuc and monitor_cluster simultaneously
        :return:
        """
        """
        :return:
        """
        try:
            from test_install_vcs import InstallApps
            install_app = InstallApps()
            install_app.report_vcs_tps_condr()

            from threading import Thread
            from Queue import Queue
            cuc_jobid = ''
            cluster_cucm_jobid = ''
            cluster_cup_jobid = ''
            result_cuc = False
            result_cluster_cucm = False
            result_cluster_cup = False
            cuc_jobid = self.install_cuc()
            cuc_host = self.gethost()
            cluster_cucm_jobid = self.install_cluster_cucm()
            cluster_cucm_host = self.gethost()
            cluster_cup_jobid = self.install_cluster_cup()
            cluster_cup_host = self.gethost()
            q_cuc = Queue()
            q_cucm = Queue()
            q_cup = Queue()

            if cuc_jobid and cluster_cucm_jobid and cluster_cup_jobid:
                Thread(target=self.wrapper, args=(self.monitor_cuc, cuc_jobid, q1_cuc)).start()
                Thread(target=self.wrapper, args=(self.monitor_cluster_cucm, cluster_cucm_jobid, q_cucm)).start()
                Thread(target=self.wrapper, args=(self.monitor_cluster_cup, cluster_cup_jobid, q_cup)).start()
                result_cuc = q_cuc.get()
                result_cluster_cucm = q_cucm.get()
                result_cluster_cup = q_cup.get()
            else:
                if cuc_jobid and cluster_cucm_jobid:
                    Thread(target=self.wrapper, args=(self.monitor_cuc, cuc_jobid, q_cuc)).start()
                    Thread(target=self.wrapper, args=(self.monitor_cluster_cucm, cluster_cucm_jobid, q_cucm)).start()
                    result_cuc = q_cuc.get()
                    result_cluster_cucm = q_cucm.get()
                else:
                    if cuc_jobid and cluster_cup_jobid:
                        Thread(target=self.wrapper, args=(self.monitor_cuc, cuc_jobid, q_cuc)).start()
                        Thread(target=self.wrapper, args=(self.monitor_cluster_cup, cluster_cup_jobid, q_cup)).start()
                        result_cuc = q_cuc.get()
                        result_cluster_cup = q_cup.get()
                    else:
                        if cluster_cucm_jobid and cluster_cup_jobid:
                            Thread(target=self.wrapper, args=(self.monitor_cluster_cucm, cluster_cucm_jobid, q_cucm)).start()
                            Thread(target=self.wrapper, args=(self.monitor_cluster_cup, cluster_cup_jobid, q_cup)).start()
                            result_cluster_cucm = q_cucm.get()
                            result_cluster_cup = q_cup.get()
                        else:
                            if cuc_jobid:
                                result_cuc = self.monitor_cuc(cuc_jobid)
                            if cluster_cucm_jobid:
                                result_cluster_cucm = self.monitor_cluster_cucm(cluster_cucm_jobid)
                            if cluster_cup_jobid:
                                result_cluster_cup = self.monitor_cluster_cup(cluster_cup_jobid)
            if result_cuc:
                print 'CUC installation SUCCCESS: ', cuc_jobid
                log.info('CUC installation SUCCCESS: ' + cuc_jobid)
                send_sms.send_sms_alert(body='CUC installation SUCCCESS: ' + cuc_jobid )
            else:
                print 'CUC installation FAILED: ', cuc_jobid
                log.info('CUC installation FAILED: ' + cuc_jobid)
                send_sms.send_sms_alert(body='CUC installation FAILED: ' + cuc_jobid )
            if result_cluster_cucm:
                print 'Cluster CUCM+CUCM installation SUCCCESS: ', cluster_cucm_jobid
                log.info('Cluster CUCM+CUCM installation SUCCCESS: ' + cluster_cucm_jobid)
                send_sms.send_sms_alert(body='Cluster CUCM+CUCM installation SUCCCESS: ' + cluster_cucm_jobid)
            else:
                print 'Cluster CUCM+CUCM installation FAILED: ', cluster_cucm_jobid
                log.info('Cluster CUCM+CUCM installation FAILED: ' + cluster_cucm_jobid)
                send_sms.send_sms_alert(body='Cluster CUCM+CUCM installation FAILED: ' + cluster_cucm_jobid)
            if result_cluster_cup:
                print 'Cluster CUCM+CUP installation SUCCCESS: ', cluster_cup_jobid
                log.info('Cluster CUCM+CUP installation SUCCCESS: ' + cluster_cup_jobid)
                send_sms.send_sms_alert(body='Cluster CUCM+CUP installation SUCCESS: ' + cluster_cup_jobid)
            else:
                print 'Cluster CUCM+CUP installation FAILED: ', cluster_cup_jobid
                log.info('Cluster CUCM+CUP installation FAILED: ' + cluster_cup_jobid)
                send_sms.send_sms_alert(body='Cluster CUCM+CUP installation FAILED: ' + cluster_cup_jobid)                
            test_install_support.check_vm(host=test.test_configuration.host27, hostid=cluster_cucm_host, sshport=test.test_configuration.sshport27,
                                          httpport=test.test_configuration.httpport27, vmname=test.test_configuration.clustervms[0])
            test_install_support.check_vm(host=test.test_configuration.host27, hostid=cluster_cucm_host, sshport=test.test_configuration.sshport27,
                                          httpport=test.test_configuration.httpport27, vmname=test.test_configuration.clustervms[1])
            test_install_support.check_vm(host=test.test_api.test_configuration.host26, hostid=cuc_host, sshport=test.test_api.test_configuration.sshport26,
                                          httpport=test.test_api.test_configuration.httpport26, vmname=test.test_api.test_configuration.vmname[0])
            test_install_support.check_vm(host=test.test_configuration.host28, hostid=cluster_cup_host, sshport=test.test_configuration.sshport28,
                                          httpport=test.test_configuration.httpport28, vmname=test.test_configuration.clustervms[2])
            test_install_support.check_vm(host=test.test_configuration.host28, hostid=cluster_cup_host, sshport=test.test_configuration.sshport28,
                                          httpport=test.test_configuration.httpport28, vmname=test.test_configuration.clustervms[3])
        except Exception as ex:
            print 'In except of monitor_report: ', ex
            log.error('In except of monitor_report: ' + str(ex))

def main():
    """
    Run the script
    """
    print 'Date: ', datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    print 'Log File : ', logfile
    myclass = CheckVM()
    myclass.monitor_report()

if __name__ == "__main__":
    main()




