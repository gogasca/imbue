"""
If NTP server is not active, replace with its backup
and get new backup.
"""
__author__ = 'Rupali'
import sys
import time
import datetime
import sched
import logging
import platform
import ntplib

sys.path.append('/usr/local/src/imbue/application/imbue')
from alarms import send_sms

if platform.system() == 'Linux':
    LOGFILEPATH = '/usr/local/src/imbue/application/imbue/log/ntp_log.log'
else:
    LOGFILEPATH = 'C:/Users/rupal/Documents/software/development/log/ntp_log.log'

logging.basicConfig(filename=LOGFILEPATH, level=logging.INFO)
logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))

def is_ntp_active(host):
    """
    Check whether ntp server is active or not
    """
    try:
        active_ntp = ntplib.NTPClient()
        try:
            response = active_ntp.request(host, version=3)
        except ntplib.NTPException as ex:
            return False
        return True
    except Exception as ex:
        print 'In except of is_ntp_active: ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of is_ntp_active: ' + str(ex))
        return False

def track_ntp_activity(host, count):
    """
    If a ntp server is down, check it again for 3 times in row. If it is
    down for 3 times a row return true
    """
    try:
        if is_ntp_active(host):
            count = 0
            print host, ' active'
            logging.info(host + ' active')
            return False
        else:
            count = count + 1
            time.sleep(10)
            if is_ntp_active(host):
                return False
            else:
                count = count + 1
                time.sleep(10)
                if is_ntp_active(host):
                    return False
                else:
                    count = count + 1
        if count == 3:
            print host, ' not active'
            logging.info(host + ' not active')
        return True
    except Exception as ex:
        print 'In except of track_ntp_activity: ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of track_ntp_activity: ' + str(ex))
        return False


def backup_server():
    """
    Get active ntp server from pool ntp
    """
    host = ''
    try:
        active_ntp = ntplib.NTPClient()
        response = active_ntp.request('0.pool.ntp.org')
        host = ntplib.ref_id_to_text(response.ref_id)
    except ntplib.NTPException as ex:
        try:
            active_ntp = ntplib.NTPClient()
            response = active_ntp.request('1.pool.ntp.org')
            host = ntplib.ref_id_to_text(response.ref_id)
        except ntplib.NTPException as ex:
            try:
                active_ntp = ntplib.NTPClient()
                response = active_ntp.request('2.pool.ntp.org')
                host = ntplib.ref_id_to_text(response.ref_id)
            except ntplib.NTPException as ex:
                try:
                    active_ntp = ntplib.NTPClient()
                    response = active_ntp.request('3.pool.ntp.org')
                    host = ntplib.ref_id_to_text(response.ref_id)
                except ntplib.NTPException as ex:
                    print 'In except of backup_server: ', ex
                    logging.info('-----------------------ERROR-------------------------')
                    logging.info('In except of backup_server: ' + str(ex))
                    return ''
    if is_ntp_active(host):
        return host
    else:
        return ''


def backup_list():
    """
    Create list of ntp servers and its backup and return that list of 6 ntp servers
    """
    try:
        print 'Wait getting backup ntp servers list .....................'
        logging.info('Wait getting backup ntp servers list .....................')
        num = 0
        flag = False
        backuplist = ['']*6
        while 1:
            host = backup_server()
            if host:
                if num == 0:
                    backuplist[0] = host
                    num = num + 1
                else:
                    for backup in backuplist:
                        if backup:
                            if backup == host:
                                flag = True
                                break
                            else:
                                flag = False
                        else:
                            if flag:
                                pass
                            else:
                                backuplist[n] = host
                                num = num + 1
                                flag = False
                                break
            else:
                continue
            if num > 5:
                break
        return backuplist
    except Exception as ex:
        print 'In except of backup_list: ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of backup_list: ' + str(ex))

def add_backups(hosts):
    """
    Add backup servers to ntp server list
    """
    try:
        print 'Wait. Adding backup ntp servers to the ntp server list ..................'
        logging.info('Wait. Adding backup ntp servers to the ntp server list ..................')
        num = 2
        while 1:
            backup = backup_server()
            if backup:
                for host in hosts:
                    if host == backup:
                        flag = False
                        break
                    else:
                        flag = True
                if flag:
                    hosts.append(backup)
                    num = num + 1
            else:
                continue
            if num == 5:
                break
        return hosts
    except Exception as ex:
        print 'In except of add_backups: ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of add_backups: ' + str(ex))
        return None

def replace_backup(hosts, i):
    """
    Replace backup and return new hosts list
    """
    print 'Replacing backup server.....................................'
    logging.info('Replacing backup server.....................................')
    num = 0
    while 1:
        backup = backup_server()
        if backup:
            for host in hosts:
                if host == backup:
                    flag = False
                    break
                else:
                    flag = True
            if flag:
                hosts[i] = backup
                break
        else:
            continue
        num = num + 1
    return hosts

def trackandreplace_backup(hosts):
    """
    Check for backup and replace it if needs
    """
    try:
        print 'Checking backup servers.........................................'
        logging.info('Checking backup servers.........................................')
        count = 0
        num = 3
        for num in range(3, 6):
           # print 'n = ',n
           # print 'host = ',hosts[n]
            if track_ntp_activity(hosts[num], count):
                hosts = replace_backup(hosts, num)
                send_sms.send_sms_alert(body='Backup server = ' + hosts[num] + ' is not active. So is replaced')
                break
            num = num + 1
        return hosts
    except Exception as ex:
        print 'In except of trackandreplace_backup : ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of trackandreplace_backup: '+ str(ex))
        return None


def replace_ntp(hosts):
    """
    if ntp is not active replace it with backup server
    and get the server from pool for that backup
    """
    try:
        print 'Checking ntp servers...........................'
        logging.info('Checking ntp servers...........................')
        num = 0
        count = 0
        for host in hosts:
            if num > 2:
                break
            if track_ntp_activity(host, count):
                print ' We need to repace ', host
                i = hosts.index(host)
                send_sms.send_sms_alert(body='ntp server = ' + hosts[i] + ' is not active. So is replaced')
                hosts[i] = hosts[i+3]
                hosts = replace_backup(hosts, i+3)
                break
            num = num + 1
        return hosts
    except Exception as ex:
        print 'In except of replace_ntp: ', ex
        logging.info('-----------------------ERROR-------------------------')
        logging.info('In except of replace_ntp: ' + str(ex))
        return None

def rotate_list(hosts):
    """
    Replace 1st ntp in the list with 2nd and 2nd with 3rd and so on
    """
    try:
        temp = hosts[0]
        i = 0
        for host in hosts:
            if i == len(hosts) - 1:
                break
            hosts[i] = hosts[i+1]
            i = i+1
        hosts.pop()
        hosts.append(temp)
        print hosts
        return hosts
    except Exception as ex:
        print 'In except of rotate_list: ', ex
        return None

print 'Log file: ', LOGFILEPATH
hosts = ['192.5.41.41', '192.5.41.209', '192.6.15.30']
hosts = add_backups(hosts)
print 'hosts list = ', hosts
logging.info('First 3 of hosts list are active ntp servers and last 3 of the list are backup ntp servers')
logging.info('hosts list with backups = ' + str(hosts))
#backups = backup_list()
#print 'backup list = ', backups

start = datetime.datetime.now()
print 'start time = ', start
logging.info('start time = ' + str(start))

s = sched.scheduler(time.time, time.sleep)

def run_ntp_script(s, hosts):
    """
    To call run_ntp_script() evry hour.
    """
    hosts = replace_ntp(hosts)
    print 'newhosts = ', hosts
    logging.info('new hosts = ' + str(hosts))
   # backups = replace_ntp(backups)
  #  print 'backuphosts = ', backups
    hosts = trackandreplace_backup(hosts)
    print 'newhostbackups = ', hosts
    logging.info('hosts with new backups = ' + str(hosts))
#    backups = trackandreplace_backup(backups)
 #   print 'newbackups = ', backups
    end = datetime.datetime.now()
    print 'End time = ', end
    logging.info('end time = ' + str(end))
    s.enter(3600, 1, run_ntp_script, (s, hosts, ))

s.enter(3600, 1, run_ntp_script, (s, hosts, ))
s.run()





