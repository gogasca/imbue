"""
 =========================================================
 This file is to automate the API test cases
 =========================================================
"""
__author__ = 'rupali'
import json
import logging
import sys
import time
import datetime
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.append('/usr/local/src/imbue/application/imbue')

from alarms import send_sms
import test_configuration
# import imbue.test.test_api.test_configuration
import test_api_support
from conf import settings, logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
# , logging_file=settings.logfile_apitest)
log.setLevel(level=logging.DEBUG)

print 'Date: ', datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
# print 'Log File : ', settings.logfile_apitest

log.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))


def count():
    """
    Function to count total tests
    """
    count.counter += 1
    # print "Counter is %d" % foo.counter


count.counter = 0


def print_result(msg):
    """
    Function to print result of test on screen
    """
    print count.counter, '. ', msg


def api_ok(url=test_configuration.URL, username=test_configuration.USERNAME,
           password=test_configuration.PASSWORD, headers=test_configuration.HEADERS,
           timeout=test_configuration.TIME):
    """
    :param url:
    :param username:
    :param password:
    :param header:
    :return:
    """
    try:
        log.info(url + ' GET test:')
        count()
        start = time.time()
        response_get = requests.get(url=url, timeout=timeout,
                                    auth=(username, password), verify=False, headers=headers)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('1.time required by request to complete = ' + str(diff) + ' seconds')
        if response_get.status_code == 200:
            log.info('1.API OK')
            print_result('API OK: test PASSED')
            return True
        else:
            log.error(str(response_get.raise_for_status()))
            time.sleep(10)
            start = time.time()
            response_get = requests.get(url=url, timeout=timeout,
                                        auth=(username, password), verify=False, headers=headers)
            end = time.time()
            diff = end - start
            if diff > 0.6:
                log.warning('2.time required by request to complete = ' + str(diff) + ' seconds')
            if response_get.status_code == 200:
                log.info('2.API OK')
                return True
            else:
                log.error(str(response_get.raise_for_status()))
                time.sleep(10)
                start = time.time()
                response_get = requests.get(url=url, timeout=timeout,
                                            auth=(username, password),
                                            verify=False, headers=headers)
                end = time.time()
                diff = end - start
                if diff > 0.6:
                    log.warning('3.time required by request to complete = ' +
                                str(diff) + ' seconds')
                if response_get.status_code == 200:
                    log.info('3.API OK')
                    return True
                else:
                    log.error('API stopped working')
                    log.error('status_code = ' + str(response_get.raise_for_status()))
                    log.error(response_get.text)
                    print_result('API test FAILED')
                    send_sms.send_sms_alert(body='API stopped working')
                    return False

    except Exception as ex:
        print 'In except get_infrastructure: ', ex
        logging('\nIn get_infrastructure():')
        logging.info('--------------------------ERROR---------------------------------------------')
        logging.info('In except get_infrastructure: ')
        logging.info('------------------------------End of ERROR----------------------------------')
        return False


def post_req(self, urlstr, data):
    try:
        global COUNT
        jobid = ''
        COUNT = COUNT + 1
        url = self.url + urlstr
        response_post = requests.post(url=url, data=json.dumps(data), verify=False, auth=(self.username, self.password),
                                      headers=self.headers)
        if response_post.status_code == 202:
            # PASSEDTESTS = PASSEDTESTS + 1
            test_api_support.info_log(url + 'POST test PASSED', response_post)
            print COUNT, url + ' POST test PASSED'
            job = (response_post.text).split(':')
            job_id = job[1].split('"')
            jobid = job_id[1]
            jobid.strip('\n\r\t')
            return jobid
        else:
            test_api_support.error_log(url + ' POST test FAILED', response_post)
            print str(COUNT) + '. ' + url + ' POST test FAILED'
            return jobid
    except Exception as ex:
        print 'In except of post_host: ', ex
        logging.info('\n')
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        logging.info('--------------------------ERROR---------------------------------------------')
        logging.info('In except post_host: ')
        logging.info('------------------------------End of ERROR----------------------------------')
        return jobid


def get_infrastructure(url=test_configuration.URL + 'infrastructure/',
                       timeout=test_configuration.TIME, username=test_configuration.USERNAME,
                       password=test_configuration.PASSWORD, headers=test_configuration.HEADERS):
    """
    Request get infrastructure to check if toolserver down
    :return:
    """
    """
    :return:
    """
    try:
        log.info(url + ' GET test: ')
        infra_id = ''
        count()
        start = time.time()
        response_get = requests.get(url=url, timeout=timeout,
                                    auth=(username, password), verify=False, headers=headers)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        try:
            json.loads(response_get.text)
        except Exception as ex:
            print 'In get_infrastructure: response is not a valid json string: ', ex
            log.error('In get_infrastructure - response is not valid json: ' + str(ex))
            return infra_id
        if response_get.status_code == 200:
            if test_api_support.report_toolserver(response_get):
                log.info('status_code = ' + str(response_get.status_code))
                log.info('PASSED')
                print_result('infrastructure/ GET test PASSED')
                infra_id = test_api_support.get_infra_id(response_get)
                # print infra_id
                return infra_id
            else:
                # print str(COUNT) + '. ' + url + ' GET test FAILED'
                log.error("ToolServer is Down")
                print_result('ToolsServer test FAILED')
                send_sms.send_sms_alert(body='ToolsServer is down')
                log.error('status_code = ' + response_get.text)
                return infra_id
        else:
            # print str(COUNT) + '. ' + url + ' GET test FAILED'
            log.error(str(response_get.raise_for_status()))
            print_result('infrastructure/ GET test FAILED')
            log.error('FAILED')
            log.error(response_get.text)
            return infra_id
    except Exception as ex:
        print_result('In except get_infrastructure: infrastructure/ GET test FAILED')
        log.error('In except get_infrastructure: ' + str(ex))
        return infra_id


def post_req(url=test_configuration.URL, urlstr='', data=None, ):
    """
    Function to make POST request. Returns job reference if success and
    empty string if fails
    """
    try:
        # global COUNT
        count()
        jobid = ''
        # COUNT = COUNT + 1
        url = url + urlstr
        log.info(url + ' POST test: ')
        start = time.time()
        response_post = requests.post(url=url, timeout=test_configuration.TIME,
                                      data=json.dumps(data), verify=False, auth=(test_configuration.USERNAME,
                                                                                 test_configuration.PASSWORD),
                                      headers=test_configuration.HEADERS)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        if response_post.status_code == 202:
            # PASSEDTESTS = PASSEDTESTS + 1
            log.info(response_post.status_code)
            log.info('PASSED')
            # print str(COUNT) + '. ' + urlstr + ' POST test PASSED'
            job = (response_post.text).split(':')
            job_id = job[1].split('"')
            jobid = job_id[1]
            jobid.strip('\n\r\t')
            log.info(jobid)
            print_result(urlstr + ' POST test PASSED: jobid = ' + jobid)
            return jobid
        else:
            log.error('FAILED')
            log.error(response_post.raise_for_status())
            print_result(urlstr + ' POST test FAILED: jobid = ' + jobid)
            log.error(response_post.text)
            # print str(COUNT) + '. ' + urlstr + ' POST test FAILED'
            return jobid
    except Exception as ex:
        print_result('In except of post_req: ' + urlstr + ' POST test FAILED')
        log.error('In except post_req: ' + str(ex))
        return jobid


def get_hostid_fromhostjobid(url=test_configuration.URL, jobid=''):
    """
    GET request for job/jobid
    :param urlstr:
    :return:
    """
    try:
        print '   Next test is processing...........................'
        time.sleep(25)
        hostid = ''
        count()
        url = url + 'job/' + jobid
        # print 'url = ', url
        log.info(url + ' GET test: ')
        start = time.time()
        response_get = requests.get(url=url, timeout=test_configuration.TIME,
                                    auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                    verify=False, headers=test_configuration.HEADERS)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        if response_get.status_code == 200:
            hostid = test_api_support.get_hostid_fromjob(response_get)
            log.info(str(response_get.status_code))
            log.info('PASSED')
            print_result('job/' + jobid + ' GET test PASSED')
            # print str(COUNT) + '. ' + urlstr + ' GET test PASSED'
            return hostid
        else:
            log.error(str(response_get.raise_for_status()))
            log.error('FAILED')
            log.error(response_get.text)
            print_result('job/' + jobid + ' GET test FAILED')
            # print str(COUNT) + '. ' + urlstr + ' GET test FAILED'
            return hostid
    except Exception as ex:
        print_result('In except of get_hostid_fromhostjobid: job/' + jobid + ' GET test FAILED')
        log.error('In except get_hostid_fromhostjobid: ' + str(ex))
        return hostid


def get_req(url=test_configuration.URL, urlstr=''):
    """
    get reuest for /1.0/api, echo, job
    :param urlstr:
    :return:
    """
    try:
        # global COUNT
        # COUNT = COUNT + 1
        count()
        url = url + urlstr
        log.info(url + ' GET test: ')
        start = time.time()
        response_get = requests.get(url=url, timeout=test_configuration.TIME,
                                    auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                    verify=False, headers=test_configuration.HEADERS)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        if response_get.status_code == 200:
            log.info(str(response_get.status_code))
            log.info('PASSED')
            print_result(urlstr + ' GET test PASSED')
            # print str(COUNT) + '. ' + urlstr + ' GET test PASSED'
            return True
        else:
            log.error(str(response_get.raise_for_status()))
            log.error('FAILED')
            log.error(response_get.text)
            print_result(urlstr + ' GET test FAILED')
            # print str(COUNT) + '. ' + urlstr + ' GET test FAILED'
            return False
    except Exception as ex:
        print_result('In except of get_req: ' + urlstr + 'GET test FAILED')
        log.error('In except get_reg: ' + str(ex))
        return False


def get_instance(url=test_configuration.URL, urlstr=''):
    """
    function to request GET for 1.0/api/host/
    :return:
    """
    try:
        #  global COUNT
        # COUNT = COUNT + 1
        vmid = ''
        count()
        url = url + urlstr
        log.info(url + ' GET test:')
        # print 'url = ',url
        start = time.time()
        response_get = requests.get(url=url, timeout=test_configuration.TIME,
                                    auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                    verify=False, headers=test_configuration.HEADERS)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        if response_get.status_code == 200:
            log.info(str(response_get.status_code))
            log.info('PASSED')
            vmid = test_api_support.get_vmid(response_get)
            print_result(urlstr + ' GET test PASSED')
            # print str(COUNT) + '. ' + urlstr + ' GET test PASSED'
            return vmid
        else:
            log.error(str(response_get.raise_for_status()))
            log.error('FAILED')
            log.error(response_get.text)
            print_result(urlstr + ' GET test FAILED')
            # print str(COUNT) + '. ' + urlstr + ' GET test FAILED'
            return vmid
    except Exception as ex:
        print_result('In except of get_instance: ' + urlstr + 'GET test FAILED')
        log.error('In except get_instance: ' + str(ex))
        return vmid


def delete_jobs_ids(url=test_configuration.URL, urlstr=''):
    """
    :param url:
    :param timeout:
    :param username:
    :param password:
    :param headers:
    :return:
    """
    try:
        count()
        url = url + urlstr
        log.info(url + ' DELETE test: ')
        start = time.time()
        response_delete = requests.delete(url=url, timeout=test_configuration.TIME,
                                          auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                          verify=False, headers=test_configuration.HEADERS)
        end = time.time()
        diff = end - start
        if diff > 0.6:
            log.warning('time taken by request to complete = ' + str(diff) + ' seconds')
        if response_delete.status_code == 202 or response_delete.status_code == 204:
            log.info(str(response_delete.status_code))
            log.info('PASSED')
            print_result(urlstr + ' DELETE test PASSED')
            return True
        else:
            log.error(str(response_delete.raise_for_status()))
            log.error('FAILED')
            log.error(response_delete.text)
            print_result(urlstr + ' DELETE test FAILED')
            return False
    except Exception as ex:
        print_result('In except of delete_jobs_ids: ' + 'FAILED')
        log.error('In except of delete_jobs_ids: ' + str(ex))
        return False
