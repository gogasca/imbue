__author__ = 'rupali'
import json
import logging
import sys
import datetime
import requests
import time
import threading
from datetime import timedelta

from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.append('/usr/local/src/imbue/application/imbue')

from alarms import send_sms
from test.test_api import test_configuration
from conf import settings
from test.test_api import test_api_support
from conf import settings, logging_conf
from utils import helper
#logfile = test_configuration.LOGFILEPATH + 'trace_install_support_' + datetime.datetime.now().strftime("%m%d") + '.log'
log = logging_conf.LoggerManager().getLogger("__app___")                                    #, logging_file=logfile)                         #settings.logfile_apitest)
log.setLevel(level=logging.DEBUG)


def count():
    count.counter += 1
    #print "Counter is %d" % foo.counter
count.counter = 0
#count = 0

def get_endtime(endtime):
    return endtime

def check_vm(host=None, hostid=None, username=test_configuration.user, password=test_configuration.password, sshport=None, httpport=None, vmname=None):
    """
    Check if vm for cuc is already exists in the database. If it
    presents, delete it
    """
    try:
        from hypervisor.esxi import vm_operations
        esx_instance = helper.get_server_instance(hostid)
        #from server import Server
       # esx_instance = Server.Server(name=test_configuration.host26)
        #print esx_instance
        syncflag = esx_instance.synchronize(force=True)
        time.sleep(30)
        found = False
        #syncflag = True
       # print 'syncflag = ', syncflag
        if syncflag:
           # print 'in if'
            vms = vm_operations.get_virtualmachines(server=host, username=username, password=password,  port=httpport)
            log.info('In check_vm(): vms = ' + str(vms))
            for vm in vms:
                #print 'vm = ', vm
               # print vms[vm]['name']
                if vms[vm]['name'] == vmname:
                    found = True
                    vm_operations.power_off(server=host, username=username, password=password, port=sshport, vmId=vm)
                    if vm_operations.destroy(server=host, username=username, password=password, port=sshport, vmId=vm):
                        log.info(msg='In check_cuc_vm(): ' + str(vm) + ' destroyed successfully')
                        print 'vmid = ',vm, ' destroyed successfully'
                        return True
                    else:
                        log.error(msg='In check_cuc_vm(): ' + str(vm) + ' can not destroyed')
                        print 'vmid = ', vm, 'can not destroyed'
                        return False
            if not found:
                log.info(msg='In check_cuc_vm(): ' + str(vmname) + ' does not exist')
                print 'VM with name = ',vmname, ' does not exist'
                return True
        else:
            print 'no sync'
    except Exception as ex:
        print 'In except of check_cuc_vm():, ',ex
        log.info(msg='In except of check_cuc_vm(): ' + str(ex))
        return False

def get_hostid(hostname):
    """
    Get hostid from hostname to use for insatalling app
    :param hostname:
    :return:
    """
    try:
        host_id = ''
        url = test_configuration.URL + 'host/?name=esx&owner_id=' + test_configuration.OWNER_ID
        log.info('get_hostid(): ' + url)
           # print 'url = ',url
        log.info('In test_install_support, get_hostid(): ' + url)
        nohostflag = False
        response_get = requests.get(url=url, verify=False, auth=(test_configuration.USERNAME, test_configuration.PASSWORD), headers=test_configuration.HEADERS)
        if response_get.status_code == 200:
            log.info(str(response_get.status_code))
            log.info(response_get.text)
            hosts = (response_get.text).split('}')
            for host in hosts:
                if hostname in host:
                    nohostflag = False
                    hostids = host.split(",")
                    for ids in hostids:
                        if '"status"' in ids:
                            stid = ids.split(':')
                            stid[1].strip(' ')
                            if stid[1] > -1:
                                nohostflag = False
                                for ids in hostids:
                                    if '"id"' in ids:
                                        hostid = ids.split(':')
                                        #print 'host =',hostid[1]
                                        host_id = hostid[1].strip(' ')
                                        return host_id
                            else:
                                nohostflag = True
                else:
                        nohostflag = True
            else:
                log.error(str(response_get.raise_for_status()))
                log.error(response_get.text)
                return host_id
        if nohostflag:
            return host_id
    except Exception as ex:
        log.error('In except of get_hostid(): '+str(ex))
        print 'In except of get_hostid(): ', ex
        return  host_id


def get_start_time(jobid):
    from database import database_session
    from conf import settings
    print settings.SQLALCHEMY_DATABASE_URI
    from database import Db
    sqlquery  = """SELECT * FROM job WHERE job.reference = """ + str("'" + jobid + "'")
   # query  = """SELECT * FROM job WHERE job.reference = """ + str("'862FF2E4D0'")
  #  print 'q = ', query
    print sqlquery
    db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
    print db
    result = db.execute(sqlquery)
    print result
    if result:
        for row in result:
            print row
            if row:
                return row['job_start']
            else:
                return ''

#def delete_vm(vmid):

