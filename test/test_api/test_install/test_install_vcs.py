"""
Script to install and monitor VCS,
TPS and Conductor
"""
__author__ = 'rupali'

import json
import logging
import sys
import datetime
import time
import threading
from datetime import timedelta
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.append('/usr/local/src/imbue/application/imbue')

from alarms import send_sms
from test.test_api import test_configuration
from test.test_api import test_api_support
from conf import settings, logging_conf
import test_install_support

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)
class InstallApps(object):
    """
    Class for installation and monitor progress of VCS, TPS, Conductor
    """

    def __init__(self):
        self.hostid = ''

    def sethost(self, hostid=None):
        """
        Set hostid in install_app()
        """
        self.hostid = hostid

    def gethost(self):
        """
        Get hostid from check_status()
        """
        return self.hostid

    def install_app(self, host=None, data=None, appstr=None):
        """
        Install vcs - post request with vcs data
        :return:
        """
        try:
            job_id = ''
            hostid = ''
            hostid = test_install_support.get_hostid(host)
            url = test_configuration.URL + 'host/' + hostid + '/instance/'
            log.info('install_app() ' + appstr + ': ' + url)
            log.info(appstr + '_hostid: ' + hostid)
            if hostid:
                self.sethost(hostid=hostid)
                response_post = requests.post(url=url, data=json.dumps(data), verify=False,
                                              auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                              headers=test_configuration.HEADERS)
                if response_post.status_code == 202:
                    log.info(str(response_post.status_code))
                    log.info(response_post.text)
                    job = (response_post.text).split(':')
                    jobid = job[1].split('"')
                    #print jobid[1]
                    job_id = jobid[1]
                    print appstr + ' installation job reference: ' + job_id
                    return job_id
                else:
                    log.error(str(response_post.raise_for_status()))
                    log.error(response_post.text)
                    print 'ERROR in installation of ' + appstr
                    return job_id
            else:
                log.error('install_vcs() ' + appstr + ': hostid is not available')
                print 'install_app() ' + appstr + ': hostid is not available'
                return job_id
        except Exception as ex:
            log.error('In except of install_app() ' + appstr + ': ' + str(ex))
            print 'In except install_vcs() ' + appstr + ': ', ex
            return job_id


    def check_status_vcs(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status for vcs
        :param jobid:
        :return:
        """
        try:
            if jobid:
                statusflag = True
                url = test_configuration.URL + 'job/' + jobid
                log.info('test_install_vcs: check_status_vcs(): ' + url)
                response = requests.get(url=url, verify=False,
                                        auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                        headers=test_configuration.HEADERS)
                if response.status_code == 200:
                    log.info(str(response.status_code))
                    log.info(response.text)
                    status_job = (response.text).split('}')
                    if 'failed' in status_job[0]:
                       #  print 'desc failed'
                        statusflag = False
                    else:
                        for jobs in status_job:
                            if "status" in jobs:
                                status = jobs.split(':')
                              #  print 'stat = ', status[1]
                                if status[1] == -1:
                                   # print 'status = ', status[1]
                                    print 'status -1'
                                    statusflag = False
                                    break
                        for jobs in status_job:
                            if 'job_end' in jobs:
                                jobend = jobs.split(':')
                               # print 'jobend = ',jobend[1]
                                jobd = jobend[1].split(',')
                                #print 'job = ', jobd[0]
                                job = jobd[0].split(' ')
                               # print 'job = ',job[1]
                                test_configuration.vcs_endtime = job[1]
                               # print self.end_time
                                if job[1] not in ' null':
                                    if 'completed' in status_job[0]:
                                        statusflag = True
                                        test_configuration.vcs_flag = True
                                        return statusflag
                                    else:
                                        statusflag = False
                                        test_configuration.vcs_flag = False
                                    break

                else:
                    log.error(str(response.raise_for_status()))
                    log.error(response.text)
                    statusflag = False
                return statusflag
            else:
                print 'VCS jobid = ', jobid, ' not valid'
                log.error('VCS jobid = ' + jobid + ' is not valid')
        except Exception as ex:
            log.error('In except of check_status_vcs(): ' + str(ex))
            print 'In except of check_status_vcs: ', ex
            return False

    def monitor_vcs(self, jobid):
        """
        Monitor the process of vcs installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if test_install_support.count.counter > 5:
                    if not test_configuration.vcs_endtime:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(minutes=5):
                                success = False
                    break
                delta = 5 - test_install_support.count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install VCS..............')
                if delta > 0:
                    print 'Wait for ',delta , ' minutes or less to install VCS....................'
                time.sleep(60)
                result = self.check_status_vcs(jobid)
                if not result:
                   # print 'failed'
                    success = False
                    break
                else:
                    if result and test_configuration.vcs_flag:
                        test_configuration.vcs_flag = False
                        return success
                test_install_support.count()
            return success
        except Exception as ex:
            print 'In except of monitor_vcs(): ', ex
            log.error('In except of monitor_vcs(): ' + str(ex))
            return False

    def check_status_tps(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status for tps
        :param jobid:
        :return:
        """
        try:
            statusflag = True
            url = test_configuration.URL + 'job/' + jobid
            log.info('test_install_vcs: check_status_tps(): ' + url)
            response = requests.get(url=url, verify=False,
                                    auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                    headers=test_configuration.HEADERS)
            if response.status_code == 200:
                log.info(str(response.status_code))
                log.info(response.text)
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                   # print 'desc failed'
                    statusflag = False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                              #  print 'stat = ', status[1]
                            if status[1] == -1:
                                   # print 'status = ', status[1]
                                print 'status -1'
                                statusflag = False
                                break
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                               # print 'jobend = ',jobend[1]
                            jobd = jobend[1].split(',')
                                #print 'job = ', jobd[0]
                            job = jobd[0].split(' ')
                               # print 'job = ',job[1]
                            test_configuration.tps_endtime = job[1]
                               # print self.end_time
                            if job[1] not in ' null':
                                if 'completed' in status_job[0]:
                                    statusflag = True
                                    test_configuration.tps_flag = True
                                    return statusflag
                                else:
                                    statusflag = False
                                    test_configuration.tps_flag = False
                                break

            else:
                log.error(str(response.raise_for_status()))
                log.error(response.text)
                statusflag = False
            return statusflag
        except Exception as ex:
            log.error('In except of check_status_tps(): ' + str(ex))
            print 'In except of check_status_tps: ', ex
            return False

    def monitor_tps(self, jobid):
        """
        Monitor the process of tps installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if test_install_support.count.counter > 5:
                    if not test_configuration.vcs_endtime:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(minutes=5):
                                success = False
                    break
                delta = 5 - test_install_support.count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install TPS..............')
                if delta > 0:
                    print 'Wait for ', delta, ' minutes or less to install TPS....................'
                time.sleep(60)
                result = self.check_status_tps(jobid)
                if not result:
                   # print 'failed'
                    success = False
                    break
                else:
                    if result and test_configuration.tps_flag:
                        test_configuration.tps_flag = False
                        return success
                test_install_support.count()
            return success
        except Exception as ex:
            print 'In except of monitor_tps(): ', ex
            log.error('In except of monitor_tps(): ' + str(ex))
            return False

    def check_status_conductor(self, jobid):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status for conductor
        :param jobid:
        :return:
        """
        try:
            statusflag = True
            url = test_configuration.URL + 'job/' + jobid
            log.info('test_install_vcs: check_status_conductor(): ' + url)
            response = requests.get(url=url, verify=False,
                                    auth=(test_configuration.USERNAME, test_configuration.PASSWORD),
                                    headers=test_configuration.HEADERS)
            if response.status_code == 200:
                log.info(str(response.status_code))
                log.info(response.text)
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                   # print 'desc failed'
                    statusflag = False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                              #  print 'stat = ', status[1]
                            if status[1] == -1:
                                   # print 'status = ', status[1]
                                print 'status -1'
                                statusflag = False
                                break
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                               # print 'jobend = ',jobend[1]
                            jobd = jobend[1].split(',')
                                #print 'job = ', jobd[0]
                            job = jobd[0].split(' ')
                               # print 'job = ',job[1]
                            test_configuration.conductor_endtime = job[1]
                               # print self.end_time
                            if job[1] not in ' null':
                                if 'completed' in status_job[0]:
                                    statusflag = True
                                    test_configuration.condr_flag = True
                                    return statusflag
                                else:
                                    statusflag = False
                                    test_configuration.condr_flag = False
                                break

            else:
                log.error(str(response.raise_for_status()))
                log.error(response.text)
                statusflag = False
            return statusflag
        except Exception as ex:
            log.error('In except of check_status_conductor(): ' + str(ex))
            print 'In except of check_status_conductor: ', ex
            return False

    def monitor_conductor(self, jobid):
        """
        Monitor the process of conductor installation per minute
        :param jobid:
        :return:
        """
        try:
            success = True
            while 1:
                if test_install_support.count.counter > 5:
                    if not test_configuration.vcs_endtime:
                        start = test_install_support.get_start_time(jobid)
                        if start:
                            current = datetime.datetime.now()
                            diff = current - start
                            if diff > timedelta(minutes=5):
                                success = False
                    break
                delta = 5 - test_install_support.count.counter
                log.info('Wait for ' + str(delta) + ' minutes or less to install Conductor...........')
                if delta > 0:
                    print 'Wait for ', delta, ' minutes or less to install Conductor..................'
                time.sleep(60)
                result = self.check_status_conductor(jobid)
                if not result:
                    #print 'failed'
                    success = False
                    break
                else:
                    if result and test_configuration.condr_flag:
                        test_configuration.condr_flag = False
                        return success
                test_install_support.count()
            return success
        except Exception as ex:
            print 'In except of monitor_conductor(): ', ex
            log.error('In except of monitor_conductor(): ' + str(ex))
            return False

    def wrapper(self, func, arg, queue):
        queue.put(func(arg))

    def report_vcs_tps_condr(self):
        """
        report installation of vcs, tps and conductor
        :return:
        """
        try:
            vcs_job = ''
            tps_job = ''
            conductor_job = ''
        #install VCS
            vcs_job = self.install_app(host=test_configuration.server26,
                                       data=test_configuration.DATA_VCS, appstr='VCS')
            vcs_host = self.gethost()
            print 'vcs_host = ', vcs_host
        #install TPS
            tps_job = self.install_app(host=test_configuration.server26,
                                       data=test_configuration.DATA_TPS, appstr='TPS')
            tps_host = self.gethost()
            print 'tps_host = ', tps_host

        #install conductor
            conductor_job = self.install_app(host=test_configuration.server26,
                                             data=test_configuration.DATA_CONDUCTOR, appstr='Conductor')
            condr_host = self.gethost()
            print 'condr_host = ', condr_host
            from threading import Thread
            from Queue import Queue
            if vcs_job and tps_job and conductor_job:
                q_vcs = Queue()
                q_tps = Queue()
                q_condr = Queue()
                Thread(target=self.wrapper, args=(self.monitor_vcs, vcs_job, q_vcs)).start()
                Thread(target=self.wrapper, args=(self.monitor_tps, tps_job, q_tps)).start()
                Thread(target=self.wrapper, args=(self.monitor_conductor, conductor_job, q_condr)).start()
                result_vcs = q_vcs.get()
                result_tps = q_tps.get()
                result_conductor = q_condr.get()
                #print result_vcs, result_tps, result_conductor
                if result_vcs:
                    print 'installed VCS successfully: ', vcs_job
                    log.info('installed VCS successfully: ' + vcs_job)
                else:
                    print 'Failed to install VCS: ', vcs_job
                    log.error('Failed to install VCS: ' + vcs_job)
                   # send_sms.send_sms_alert(body='VCS installation FAILED: ' + vcs_job)
                if result_tps:
                    print 'installed TPS successfully: ', tps_job
                    log.info('installed TPS successfully: ' + tps_job)
                else:
                    print 'Failed to install TPS: ', tps_job
                    log.error('Failed to install TPS: ' + tps_job)
                    #send_sms.send_sms_alert(body='TPS installation FAILED: ' + tps_job)
                if result_conductor:
                    print 'installed Conductor successfully: ', conductor_job
                    log.info('installed Conductor successfully: ' + conductor_job)
                else:
                    print 'Failed to install Conductor: ', conductor_job
                    log.error('Failed to install Conductor: ' + conductor_job)
                    #send_sms.send_sms_alert(body='Conductor installation FAILED: ' + conductor_job)
            else:
                if vcs_job:
                    if self.monitor_vcs(vcs_job):
                        print 'installed VCS successfully: ', vcs_job
                        log.info('installed VCS successfully: ' + vcs_job)
                    else:
                        print 'Failed to install VCS: ', vcs_job
                        log.error('Failed to install VCS: ' + vcs_job)
                     #   send_sms.send_sms_alert(body='VCS installation FAILED: ' + vcs_job)
                if tps_job:
                    if self.monitor_tps(tps_job):
                        print 'installed TPS successfully: ', tps_job
                        log.info('installed TPS successfully: ' + tps_job)
                    else:
                        print 'Failed to install TPS: ', tps_job
                        log.error('Failed to install TPS: ' + tps_job)
                      #  send_sms.send_sms_alert(body='TPS installation FAILED: ' + tps_job)
                if conductor_job:
                    if self.monitor_conductor(conductor_job):
                        print 'installed Conductor successfully: ', conductor_job
                        log.info('installed Conductor successfully: ' + conductor_job)
                    else:
                        print 'Failed to install Conductor: ', conductor_job
                        log.error('Failed to install Conductor: ' + conductor_job)
                       # send_sms.send_sms_alert(body='Conductor installation FAILED: ' + conductor_job)

            test_install_support.check_vm(host=test_configuration.host26, hostid=vcs_host,
                                          sshport=test_configuration.sshport26, httpport=test_configuration.httpport26,
                                          vmname=test_configuration.vmname[1])
            test_install_support.check_vm(host=test_configuration.host26, hostid=tps_host,
                                          sshport=test_configuration.sshport26, httpport=test_configuration.httpport26,
                                          vmname=test_configuration.vmname[2])
            test_install_support.check_vm(host=test_configuration.host26, hostid=condr_host,
                                          sshport=test_configuration.sshport26, httpport=test_configuration.httpport26,
                                          vmname=test_configuration.vmname[3])
        except Exception as ex:
            print 'In except of report_vcs_tps_condr(): ', ex
            log.error('In except of report_vcs_tps_condr(): ' + str(ex))
            send_sms(body='In except of report_vcs_tps_condr(): ' + str(ex), destination_numbers='9098599612')
