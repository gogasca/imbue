import json
import logging
import sys
import datetime
import requests
import time
import threading

from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.append('/usr/local/src/imbue/application/imbue')

from alarms import send_sms
import test_configuration
from conf import settings

count = 0

"""
class CheckVM has all following methods
check_cuc_vm()  -  Check if vmname for cuc installation already exists. If yes, power off and destroy it.
check_cluster_vm() -  Check if vmname for cluster installation already exists. If yes, power off and destroy it.
get_hostid() - providing server, get the available hostid for the server
install_app() - make cluster or instance POST request depending on url
check_status  - make GET request for job/jobid and check description, job_end, status parameters
moniter_report() - call check_status every minute and report success of failure
"""


class CheckVM(object):
    def __init__(self):
        self.URLSSL = test_configuration.URL
        # url = 'https://prod:
        self.USERNAME = test_configuration.USERNAME
        self.PASSWORD = test_configuration.PASSWORD
        self.HEADERS = test_configuration.HEADERS
        self.OWNER_ID = test_configuration.OWNER_ID
        self.count = 0
        self.cuchost = test_configuration.cuchost
        self.clusterhost = test_configuration.clusterhost
        self.user = test_configuration.user
        self.password = test_configuration.password
        self.httpport = test_configuration.httpport
        self.clusterhttpport = test_configuration.clusterhttpport
        self.sshport = test_configuration.sshport
        self.clustersshport = test_configuration.clustersshport
        self.vmname = test_configuration.vmname
        self.clustervms = test_configuration.clustervms  # 'cucm-pub-test', 'cucm-sub-test']

    def check_cuc_vm(self):
        """
        Check if vm for cuc is already exists in the database. If it
        presents, delete it
        """
        try:
            foundflag = False
            cleanflag = True
            # print 'vm_operations'
            from hypervisor.esxi import vm_operations
            vms = vm_operations.get_virtualmachines(server=self.cuchost, username=self.user, password=self.password,
                                                    port=self.httpport)
            for vm in vms:
                #  print 'vm = ', vm
                if vms[vm]['name'] == self.vmname:
                    foundflag = True
                    vm_operations.power_off(server=self.cuchost, username=self.user, password=self.password,
                                            port=self.sshport, vmId=vm)
                    if vm_operations.destroy(server=self.cuchost, username=self.user, password=self.password,
                                             port=self.sshport, vmId=vm):
                        print 'vmid = ', vm, ' destroyed successfully'
                    else:
                        cleanflag = False
                        print 'vmid = ', vm, 'can not destroyed'
                        # if vm_operations.power_off(server=self.host, username=self.user,password=self.password, port=self.sshport, vmId=self.vmid):
            if foundflag == False:
                print 'VM with name = ', self.vmname, ' does not exist'
            return cleanflag
        except Exception as ex:
            print 'In except, ', ex
            return False

    def check_cluster_vm(self):
        """
        Check if vm for cluster is already exists in the database. If it
        presents, delete it
        """
        try:
            cleanflag = True
            foundflag = False
            # print 'cluster_vm_operations'
            from hypervisor.esxi import vm_operations
            vmids = vm_operations.get_virtualmachines(server=self.clusterhost, username=self.user,
                                                      password=self.password, port=self.clusterhttpport)
            # print 'vms = ', vmids
            for clustervm in self.clustervms:
                for vm in vmids:
                    #      print 'vm = ', vm
                    if vmids[vm]['name'] == clustervm:
                        foundflag = True
                        vm_operations.power_off(server=self.clusterhost, username=self.user, password=self.password,
                                                port=self.clustersshport, vmId=vm)
                        if vm_operations.destroy(server=self.clusterhost, username=self.user, password=self.password,
                                                 port=self.clustersshport, vmId=vm):
                            print 'vmid = ', vm, ' destroyed successfully'
                        else:
                            cleanflag = False
                            print 'vmid = ', vm, 'can not destroyed'
                if foundflag == False:
                    print 'VM with name = ', clustervm, ' does not exist'
            return cleanflag
        except Exception as ex:
            print 'In except, ', ex
            return False

    def get_hostid(self, hostname):
        """
        Get hostid from hostname to use for insatalling app
        :param hostname:
        :return:
        """
        try:
            #   print 'in get_hostid '
            url = self.URLSSL + 'host/?name=esx&owner_id=' + self.OWNER_ID
            nohostflag = False
            response_get = requests.get(url=url, verify=False, auth=(self.USERNAME, self.PASSWORD),
                                        headers=self.HEADERS)
            if response_get.status_code == 200:
                hosts = (response_get.text).split('}')
                for host in hosts:
                    if hostname in host:
                        nohostflag = False
                        hostids = host.split(",")
                        for ids in hostids:
                            if '"status"' in ids:
                                stid = ids.split(':')
                                stid[1].strip(' ')
                                if stid[1] > -1:
                                    nohostflag = False
                                    for ids in hostids:
                                        if '"id"' in ids:
                                            hostid = ids.split(':')
                                            return hostid[1].strip(' ')
                        else:
                            nohostflag = True
                    else:
                        nohostflag = True
            else:
                return ''
            if nohostflag:
                return ''
        except Exception as ex:
            print ex

    def install_app(self, url):
        """
        Install cluster/instance - post request with cluster/instance data
        :param url:
        :return:
        """
        try:
            if 'cluster' in url:
                hostid = self.get_hostid('esxi26-install-test')
                if hostid:
                    print 'cluster hostid = ', hostid
                    data = {"instance": {"host": {"id": int(hostid)}, "application": {"type": 200001, "params": {
                        "hostname": self.clustervms[0], "ip": "192.168.1.13", "mask": "255.255.255.0",
                        "gw": "192.168.1.1", "is_installed": "false"}, "publisher": "true", "version": "11",
                                                                                      "esx_network": "VM Network",
                                                                                      "directory": "/vmfs/volumes/datastore1/parzee",
                                                                                      "answer_file": "/usr/local/src/imbue/configs/11/cucm/cluster/pub/platformConfig.xml",
                                                                                      "cluster_file": "/usr/local/src/imbue/configs/11/cucm/cluster/pub/clusterConfig.xml"}}}, {
                               "instance": {"host": {"id": int(hostid)}, "application": {"type": 200001, "params": {
                                   "hostname": self.clustervms[1], "ip": "192.168.1.14", "mask": "255.255.255.0",
                                   "gw": "192.168.1.1"}, "publisher": "false", "version": "11",
                                                                                         "esx_network": "VM Network",
                                                                                         "directory": "/vmfs/volumes/datastore1/parzee",
                                                                                         "answer_file": "/usr/local/src/imbue/configs/11/cucm/cluster/sub/platformConfig.xml"}}}
                else:
                    print 'ERROR: hostid is not available'
                    return ''
            else:
                data = {
                    "application": {"type": 200001, "publisher": "true", "version": "11", "esx_network": "VM Network",
                                    "directory": "/vmfs/volumes/datastore1/parzee",
                                    "answer_file": "/usr/local/src/imbue/configs/11/cucm/platformConfig.xml",
                                    "params": {"hostname": self.vmname, "ip": "192.168.1.15", "mask": "255.255.255.0",
                                               "gw": "192.168.1.1"}}}
            print 'url = ', url
            response_post = requests.post(url=url, data=json.dumps(data), verify=False,
                                          auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
            if response_post.status_code == 202:
                job = (response_post.text).split(':')
                jobid = job[1].split('"')
                print jobid[1]
                return jobid[1]
            else:
                print response_post.text
                print 'ERROR in installation'
                return ''
        except Exception as ex:
            print 'In except install_app(): ', ex
            return ''

    def check_status(self, jobid, strjob):
        """
        check the response from job/jobid GET request and check
        description and job_end values and status
        """
        try:
            url = self.URLSSL + 'job/' + jobid
            response = requests.get(url=url, verify=False, auth=(self.USERNAME, self.PASSWORD), headers=self.HEADERS)
            if response.status_code == 200:
                status_job = (response.text).split('}')
                if 'failed' in status_job[0]:
                    return False
                else:
                    for jobs in status_job:
                        if "status" in jobs:
                            status = jobs.split(':')
                    for jobs in status_job:
                        if 'job_end' in jobs:
                            jobend = jobs.split(':')
                            if strjob == 'cuc':
                                if (count < 60):
                                    if 'null' not in jobend[1] and int(status[1]) < 0:
                                        return False
                                    else:
                                        if int(status[1]) > -1:
                                            return True
                                        else:
                                            return False
                            else:
                                if (count < 110):
                                    if 'null' not in jobend[1] and int(status[1]) < 0:
                                        return False
                                    else:
                                        if int(status[1]) > -1:
                                            return True
                                        else:
                                            return False
                                            # return True
        except Exception as ex:
            print 'In except of check_status ', ex
        return False

    def monitor_report(self):
        """
        check response from job/jobid GET request every minute
        :return:
        """
        """
        :return:
        """
        try:
            global count
            cuc_jobid = ''
            cluster_errflag = False
            cuc_errflag = False
            cucsuccess = False
            clustersuccess = False
            cluster_jobid = self.install_app(self.URLSSL + 'cluster/')
            hostid = self.get_hostid('esxi28-install-test')
            print 'cuc hostid = ', hostid
            if hostid:
                url = self.URLSSL + 'host/' + hostid + '/instance/'
                cuc_jobid = self.install_app(url)
            else:
                print 'ERROR: hostid is not available'
            while (1):
                if count > 110:
                    break
                if count > 60 and cluster_errflag == True:
                    break
                if cluster_jobid:
                    if cluster_errflag == False:
                        if self.check_status(cluster_jobid, 'cluster') == False:
                            cluster_errflag = True
                            # send_sms.send_sms_alert(body='cluster installation with jobid = ' + cluster_jobid + 'FAILED', destination_number=settings.phone_numbers)
                            # print 'cluster job failed'
                        else:
                            clustersuccess = True
                else:
                    cluster_errflag = True
                if cuc_jobid:
                    if cuc_errflag == False:
                        if count < 60:
                            if self.check_status(cuc_jobid, 'cuc') == False:
                                cuc_errflag = True
                            # send_sms.send_sms_alert(body='cluster installation with jobid = ' + cuc_jobid + 'FAILED', destination_number=settings.phone_numbers)
                            # print 'cuc job failed'
                            else:
                                cucsuccess = True
                else:
                    cuc_errflag = True
                if cluster_errflag and cuc_errflag:
                    break
                time.sleep(60)
                t = 110 - count
                print 'Wait for ', t, ' minutes or less. progressing...............................'
                count = count + 1
            output = str(count) + ' minutes'
            print output
            if cucsuccess and (cuc_errflag == False):
                print 'cuc success ', cuc_jobid
                send_sms.send_sms_alert(body='cuc installation SUCCCESS: ' + cuc_jobid)
            else:
                print 'cuc failed ', cuc_jobid
                send_sms.send_sms_alert(body='cuc installation FAILED: ' + cuc_jobid)
            if clustersuccess and (cluster_errflag == False):
                print 'cluster success ', cluster_jobid
                send_sms.send_sms_alert(body='cluster installation SUCCESS: ' + cluster_jobid)
            else:
                print 'cluster failed ', cluster_jobid
                send_sms.send_sms_alert(body='cluster installation FAILED: ' + cluster_jobid)
        except Exception as ex:
            print 'In scept of monitor_report: ', ex


def main():
    myclass = CheckVM()
    if myclass.check_cuc_vm() and myclass.check_cluster_vm():
        time.sleep(30)
        myclass.monitor_report()
    else:
        send_sms.send_sms_alert(body='Failed to destroy existing VM')
        print 'Failed to destroy existing VM'


if __name__ == "__main__":
    main()
