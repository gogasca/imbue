__author__ = 'rupali'
import platform
import json
import logging
import sys
import time
import datetime
import re

if platform.system() == 'Linux':
    LOGFILEPATH = '/usr/local/src/imbue/application/imbue/log/'   # + 'trace_apitest_' + DATE_STR + '.log'
else:
     LOGFILEPATH = 'C:/Users/rupal/Documents/software/development/log/'    # + 'trace_apitest_' + DATE_STR + '.log'

logfile = LOGFILEPATH + datetime.datetime.now().strftime("%m%d") + '.log'
logging.basicConfig(filename=logfile, level=logging.INFO)

def error_log(url, response):
    """
    method to write error in log
    """
    logging.info('\n')
    logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
    logging.info('--------------------------ERROR---------------------------------------------')
    logging.info('ERROR : '+url+", status_code = '%s'" %(response.status_code))
    logging.info('---------------------------------------------------------------------------')
    logging.info('ERROR : '+response.text)
    logging.info('-------------------------------------------------------------------------')

def info_log(url, response):
    """
    method to write info in log
    """
    logging.info('\n')
    logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
    logging.info('\n'+url+", status_code = '%s'" %(response.status_code))
    logging.info('\n '+response.text)


def report_toolserver(response):
    """
    check the status. If all hosts have status = -1, then toolserver is down
    :param response:
    :return:
    """
    try:
        count = 0
        list_data = (response.text).split('}')
        for data in list_data:
            st_data = data.split(',')
            for status in st_data:
                stat = status.split(':')
                stat[0].strip('\n\r\t')
                if '"status"' in stat[0]:
                    stat[1].strip('\n\r\t')
                    if int(stat[1]) > 0 or int(stat[1]) == 0:
                        count = count + 1
                        pass
        if count == 0:
            print 'Tool server down'
            return False
        return True
    except Exception as ex:
        print 'In except of report_toolserver: ', ex
        logging.info('\nIn report_toolserver:')
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        logging.info('--------------------------ERROR---------------------------------------------')
        logging.info('In except get_infrastructure: ' )
        logging.info('------------------------------End of ERROR----------------------------------')
        return False


def get_vmid(response):
    """
    Function to get vmid
    """
    try:
        vmid = ''
        lst = (response.text).split(",")
        status_id = ''
        for job in lst:
            jobs = job.split(':')
            if '"status"' in jobs[0]:
                status_id = jobs[1]
            if '"vmid"' in jobs[0]:
                if int(status_id) > -1:
                    vid = jobs[1].split('}')
                    vid[0].strip('\n\r\t')
                    v_id = vid[0].split(' ')
                    vmid = v_id[1]
                    vmid.strip()
                    break
        return vmid
    except Exception as ex:
        print 'In except of get_vmid: ',ex
        logging.info('\n')
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        logging.info('--------------------------ERROR---------------------------------------------')
        logging.info('In except get_vmid: ' )
        logging.info('------------------------------End of ERROR----------------------------------')
        return vmid

def get_hostid_fromjob(respond):
    """
    Get jobid from job/jobid GET request
    """
    try:
        hostid = ''
        job_info = (respond.text).split('}')
        if 'status": -1' in job_info[3]:
            return hostid
        hosts = job_info[0]
        host = hosts.split('{')
        hostids = host[3].split(':')
        hostid = hostids[0]
        hostid.strip('\n\r\t')
        h_id = hostid.split('"')
        hostid = h_id[1]
        return hostid
    except Exception as ex:
        print 'In except of get_hostid_fromjob: ', ex
        logging.info('\n')
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        logging.info('--------------------------ERROR---------------------------------------------')
        logging.info('In except get_hostid_fromjob: ')
        logging.info('------------------------------End of ERROR----------------------------------')
        return hostid

def report_error_instance(response):
    """
    Check the text of response received by
    job/jobid from instance POST for errors
    If error, returns true
    """
    data = (response.text).split(',')
    if(re.search('error', response.text, re.IGNORECASE)) or ('completed' in data[0]) or ('"null"' not in data[5]):
        return True
    return False




