__author__ = 'gogasca'

import logging
import requests
import pytesseract
import os
import datetime
from shutil import copyfile
from PIL import Image
from PIL import ImageFilter
from conf import logging_conf
from error import exceptions

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def save_screenshot(screen_download_path):
    """

    :param downloadPath:
    :return:
    """
    now = datetime.datetime.now()
    date_str = now.strftime("_%Y%m%d%H%M%S")
    dst = screen_download_path + date_str
    copyfile(screen_download_path, dst)
    log.info('image_recognition.save_screenshot() File saved {}'.format(dst))


def get_screenshot(url=None, username=None, password=None, httpsPort='443', downloadPath=None, **kwargs):
    """

    :param url:
    :param username:
    :param password:
    :param httpsPort:
    :param downloadPath:
    :param kwargs:
    :return:
    """
    import ssl
    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context

    r = requests.get(url, auth=(username, password), verify=False)
    if r.status_code == 200:
        with open(downloadPath, 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)
        log.info('image_recognition.get_screenshot() File download completed: ' + downloadPath)
        f.close()
        save_screenshot(downloadPath)
        return True

    else:
        raise exceptions.ImageDownloadException('Error downloading ' + url)


def processImage(image_path):
    """

    :param image_path:
    :return:
    """
    try:
        log.info('image_recognition.processImage() Processing image: ' + image_path)
        if os.path.isfile(image_path) and os.access(image_path, os.R_OK) and image_path:
            log.info('image_recognition.processImage() Image File exists and is readable: ' + image_path)
        else:
            log.error('image_recognition.processImage() Image File doesnt exist or is not readable: ' + image_path)
            return None
        # https://pypi.python.org/pypi/pytesseract
        im = Image.open(image_path)
        im.filter(ImageFilter.SHARPEN)
        im.filter(ImageFilter.EDGE_ENHANCE_MORE)
        # Default Language is English
        text = pytesseract.image_to_string(im, lang='eng')
        return text

    except Exception, exception:
        log.exception(str(exception))


def is_prompt_ready(list=[], image_to_text='', coefficient=5, check_error=False, **kwargs):
    """

    :param list:
    :param image_to_text:
    :param coefficient:
    :param check_error:
    :param kwargs:
    :return:
    """
    try:

        log.info('image_recognition.is_prompt_ready() Text found in image: ' + image_to_text)
        coefficient_scaled = 0

        if len(image_to_text) <= 0:
            log.warn('image_recognition.is_prompt_ready() No text found in image')
            return False

        log.info('image_recognition.is_prompt_ready() Trying to find string in current screenshot')
        for word in list:
            if word in image_to_text:
                log.info('image_recognition.is_prompt_ready(): Match found: ' + word)
                coefficient_scaled += 1

        if coefficient_scaled >= coefficient:
            return True
        else:
            log.warn(
                'image_recognition.is_prompt_ready() No match found. Coefficient value: ' + str(coefficient_scaled))
            return False

    except Exception, exception:
        log.exception(str(exception))
