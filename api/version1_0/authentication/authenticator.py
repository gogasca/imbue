__author__ = 'gogasca'

from conf import settings
from functools import wraps
from flask import Flask, jsonify, request, g, Response
from database.models import Model
from flask.ext.httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

# =========================================================
# Flask Application
# =========================================================
@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = Model.ApiUser.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = Model.ApiUser.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

def check_auth(username, password):
    return username == settings.api_account and password == settings.api_password

def authenticate():
    message = {'message': "Authenticate."}
    resp = jsonify(message)
    resp.status_code = 401
    resp.headers['WWW-Authenticate'] = 'Basic realm="imbue"'
    return resp

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return authenticate()

        elif not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated

# =========================================================
# Token implementation
# =========================================================

