__author__ = 'gogasca'

# =========================================================
# IMBUE main application | gunicorn imbue_api:api_app --bind 0.0.0.0:8081
# =========================================================

from application.app import api_app

if __name__ == "__main__":
    api_app.run(debug=False, threaded=True)
