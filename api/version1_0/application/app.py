__author__ = 'gogasca'

import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
import warnings

warnings.filterwarnings("ignore")
from conf import logging_conf, settings
from utils import banner

log = logging_conf.LoggerManager().getLogger("___app___", logging_file=settings.api_logfile)

# =========================================================
# Flask Application imports and database
# =========================================================
from flask import Flask
from flask.ext import restful
from database.models import Model
from api_main import ApiBase, ApiUser, ApiUserList, Cluster, Echo, JobList, Job, HostList, Host, InstanceList, Instance, \
    InfrastructureList, \
    Infrastructure, Status
from werkzeug.contrib.fixers import ProxyFix

# =========================================================
# Flask Main Application
# =========================================================

api_app = Flask(__name__)  # Flask Application
api_app.config.from_pyfile("../../../conf/settings.py")  # Flask configuration

imbue_api = restful.Api(api_app)  # Define API
# limiter = Limiter(api_app, key_func=get_remote_address, global_limits=["10 per second"])
db = Model.db.init_app(api_app)  # Initialize database

# =========================================================
# API Definition
# =========================================================

imbue_api.add_resource(ApiBase, settings.base_api_url, strict_slashes=False)
imbue_api.add_resource(Echo, '/api/1.0/echo/', strict_slashes=False)
imbue_api.add_resource(ApiUserList, '/api/1.0/user/')
imbue_api.add_resource(ApiUser, '/api/1.0/user/<string:username>')
imbue_api.add_resource(Cluster, '/api/1.0/cluster/')
imbue_api.add_resource(JobList, '/api/1.0/job/')
imbue_api.add_resource(Job, '/api/1.0/job/<string:ref>')
imbue_api.add_resource(HostList, '/api/1.0/host/')
imbue_api.add_resource(Host, '/api/1.0/host/<int:id>')
imbue_api.add_resource(InstanceList, '/api/1.0/host/<int:id>/instance/')
imbue_api.add_resource(Instance, '/api/1.0/host/<int:id>/instance/<int:vmid>')
imbue_api.add_resource(InfrastructureList, '/api/1.0/infrastructure/')
imbue_api.add_resource(Infrastructure, '/api/1.0/infrastructure/<int:id>')
imbue_api.add_resource(Status, '/api/1.0/status/<string:task_id>')

# =========================================================
# API Proxy WSGi for gunicorn
# =========================================================

api_app.wsgi_app = ProxyFix(api_app.wsgi_app)

# =========================================================
# API Logs
# =========================================================

log.info('Initializing IMBUE API >>>')
banner.horizontal(' IMBUE API v1.0 ')
