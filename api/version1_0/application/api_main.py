__author__ = 'gogasca'

import json
import logging
import time
import warnings

warnings.filterwarnings("ignore")

# =========================================================
# IMBUE imports
# =========================================================

from celery import Celery
from conf import logging_conf, settings
from shared import Jobd
from shared import Report
from utils import helper

# =========================================================
# Flask Application imports and database
# =========================================================

from api.version1_0.authentication import authenticator
from api.version1_0.controller import api_controller
from database.models import Model
from database.models import ModelOperations
from sqlalchemy import desc
from flask import current_app
from flask import jsonify, request, Response, url_for
from flask.ext import restful
from flask.ext.restful import Resource

# =========================================================
# API Controller
# =========================================================

app_controller = api_controller.Controller()
api = restful.Api

# =========================================================
# API Logs
# =========================================================

log = logging_conf.LoggerManager().getLogger("___app___", filemode='a+', logging_file=settings.api_logfile)


# =========================================================
# API Version information
# =========================================================

class ApiUserList(Resource):
    """Used for verifying Job status"""

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:

            # =========================================================
            # Get Users
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            if len(request.args) > 0:
                users = ModelOperations.user_filter(request)
            else:

                # =========================================================
                # Limit the number of User List in request.
                # =========================================================

                users = Model.ApiUser.query.order_by(desc(Model.ApiUser.username)).limit(settings.items_per_page).all()

            response = []

            if users:
                values = ['username', 'created']
                response = [{value: getattr(d, value) for value in values} for d in users]

            return jsonify(users=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class ApiBase(Resource):
    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET API
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())
            if request.headers['Content-Type'] == 'application/json':
                # =========================================================
                # Send API version information
                # =========================================================
                log.info('api() | GET | Version' + settings.api_version)
                response = json.dumps('version: ' + settings.api_version)
                resp = Response(response, status=200, mimetype='application/json')
                return resp

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class ApiUser(Resource):
    @authenticator.requires_auth
    def get(self, username):
        """

        """
        try:
            # =========================================================
            # GET APIUser by username
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # GET APIUser by username
            # =========================================================

            log.info('api() | Finding user: ' + username)
            user = Model.ApiUser.query.filter(Model.ApiUser.username == username).first()  # Check if host exists.

            if user:
                # Return Api details.
                log.info('api() | User found: ' + username)
                return jsonify(user.serialize())
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    @api_controller.validate_json
    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()
            # =========================================================
            # POST Server by Id
            # =========================================================

            if request.headers['Content-Type'] == 'application/json':
                username = request.json.get('username')
                password = request.json.get('password')

                if username is None or password is None:
                    response = json.dumps('Missing arguments')
                    resp = Response(response, status=400, mimetype='application/json')
                    return resp

                user = Model.ApiUser.query.get(username)
                if user:
                    response = json.dumps('Username already exists')
                    resp = Response(response, status=409, mimetype='application/json')
                    return resp
                else:
                    # Insert user
                    user.id = ModelOperations.insert_api_user(username, password)

                    # =========================================================
                    # Return 202 Response accepted
                    # =========================================================

                    if user.id:
                        response = jsonify({'username': username})
                        response.headers['Location'] = api.url_for(api(current_app),
                                                                   ApiUser,
                                                                   ref=username,
                                                                   _external=True,
                                                                   _scheme=settings.api_scheme)

                        end = time.time()
                        log.info('api() | Task creation took: {} ms'.format(end - start))
                        response.status_code = 202
                        return response
                    else:
                        log.exception('Unable to insert user')
                        response = json.dumps('Unable to insert user')
                        resp = Response(response, status=500, mimetype='application/json')
                        return resp

            else:
                resp = Response(status=415, mimetype='application/json')
                return resp

        except KeyError:
            response = json.dumps('415 Unsupported Media Type')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API Echo information
# =========================================================

class Echo(Resource):
    """Used for verifying API status"""

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | GET | Received request for Echo')
        response = json.dumps('Echo: GET. Parzee works')
        resp = Response(response, status=200, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def delete(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | DELETE | Received request for Echo')
        response = json.dumps('Echo: DELETE. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | POST | Received request for Echo')
            start = time.time()
            job_instance = Jobd.Jobd()
            job_instance.id = ModelOperations.insert_job(job_instance.reference,
                                                         request.data,
                                                         settings.esx_vendor_id)
            celery = Celery()
            celery.config_from_object('conf.celeryconfig')
            celery.send_task("utils.monitor.tasks.create_celery_task",
                             exchange='imbue',
                             queue='bronze',
                             routing_key='imbue.bronze',
                             kwargs={'job': job_instance.reference},
                             task_id=job_instance.reference,
                             retries=3)

            response = jsonify({'job': job_instance.reference})
            response.headers['Location'] = api.url_for(api(current_app),
                                                       Job,
                                                       ref=job_instance.reference,
                                                       _external=True,
                                                       _scheme=settings.api_scheme)

            helper.update_job(job_reference=job_instance.reference, description='Echo API Test job', test=True)
            end = time.time()
            log.info('api() | Task creation took: {} ms'.format(end - start))
            response.status_code = 202
            return response
        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def put(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | PUT | Received request for Echo')
        response = json.dumps('Echo: PUT. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp


# =========================================================
# API GET Jobs information
# =========================================================

class JobList(Resource):
    """Used for verifying Job status"""

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:

            # =========================================================
            # Get JOBS
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            if len(request.args) > 0:
                jobs = ModelOperations.job_filter(request)
            else:

                # =========================================================
                # Limit the number of Jobs in request.
                # =========================================================

                jobs = Model.Job.query.order_by(desc(Model.Job.job_start)).limit(settings.items_per_page).all()

            response = []

            if jobs:
                values = ['description', 'status', 'reference']
                response = [{value: getattr(d, value) for value in values} for d in jobs]

            return jsonify(jobs=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Job information
# =========================================================

class Job(Resource):
    """
        Job information
    """

    @authenticator.requires_auth
    def post(self, ref):
        """

        :param ref:
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()
            # =========================================================
            # Create JOB report
            # =========================================================

            job = Model.Job.query.filter(Model.Job.reference == ref).first()
            # Job exists. Now will generate report
            if job and job.report_created:
                job_instance = Jobd.Jobd()
                job_instance.id = ModelOperations.insert_job(job_instance.reference,
                                                             request.data,
                                                             settings.esx_vendor_id)

                log.info('api() | POST | Received report request for job')
                report_instance = Report.Report(settings.report_path, job.reference)
                report_instance.zip_file = report_instance.report_path + job.reference + '.zip'

                # =========================================================
                # Launch celery Task
                # =========================================================
                log.info('api() | Creating task: ' + job_instance.reference)

                celery = Celery()
                celery.config_from_object('conf.celeryconfig')
                celery.send_task("shared.Report.send_report",
                                 exchange='imbue',
                                 queue='silver',
                                 routing_key='imbue.silver',
                                 kwargs={'zip_file': report_instance.zip_file},
                                 task_id=job_instance.reference,
                                 retries=3)

                log.info('api() | Task sent: ' + job_instance.reference)
                response = jsonify({'job': job_instance.reference + ' sending report'})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)
                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

            if job:
                response = json.dumps('Report is not completed. Try again later')
                resp = Response(response, status=423, mimetype='application/json')
                return resp

            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def get(self, ref):
        """

        :param ref:
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Get JOB by reference
            # =========================================================
            log.info('api() | Finding task: ' + ref)
            job = Model.Job.query.filter(Model.Job.reference == ref).first()

            if job:
                log.info('api() | Task found: ' + ref)
                return jsonify(job.serialize())

            log.warn('api() | Task not found: ' + ref)
            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def delete(self, ref):
        """

        :param ref:
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Get JOB by ID
            # =========================================================

            job = Model.Job.query.filter(Model.Job.reference == ref).first()

            # =========================================================
            # Get JOB by ID
            # =========================================================

            if job:
                log.info('api() | DELETE | Received request deleting job')

                # =========================================================
                # Launch celery Task
                # =========================================================
                start = time.time()
                if job.is_cancelled:
                    response = json.dumps('Conflict. Job already cancelled')
                    resp = Response(response, status=423, mimetype='application/json')
                    return resp
                if job.job_end:
                    response = json.dumps('Not acceptable. Job already completed')
                    resp = Response(response, status=406, mimetype='application/json')
                    return resp
                if job.status == 0:
                    response = json.dumps('Job cancellation in progress')
                    resp = Response(response, status=423, mimetype='application/json')
                    return resp

                # Create JobD object
                job_instance = Jobd.Jobd()
                job_instance.id = ModelOperations.insert_job(job_instance.reference,
                                                             request.data,
                                                             settings.esx_vendor_id)
                job_instance.parent_id = job.id  # Assign id as parent id
                job_instance.parent_reference = job.reference  # Assign reference as parent reference
                job_instance.is_cluster = job.is_cluster

                # =========================================================
                # Launch celery Task
                # =========================================================
                log.info('api() | Creating task: ' + job_instance.reference)
                celery = Celery()
                celery.config_from_object('conf.celeryconfig')
                if job.is_cluster:
                    celery.send_task("hypervisor.esxi.vm_operations.cancel_cluster_job",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     kwargs={'job': job_instance},
                                     task_id=job_instance.reference)
                else:
                    celery.send_task("hypervisor.esxi.vm_operations.cancel_job",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     kwargs={'job': job_instance},
                                     task_id=job_instance.reference)

                log.info('api() | Task sent: ' + job_instance.reference)
                response = jsonify({'job': job.reference + ' task being cancelled'})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)
                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Hosts
# =========================================================

class HostList(Resource):
    """Used for verifying API status"""

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET Host information
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Limit the number of hosts in request. Filter by owner_id
            # =========================================================

            if len(request.args) > 0:
                servers = ModelOperations.server_filter(request)
            else:
                servers = Model.Server.query.order_by(desc(Model.Server.created)).limit(settings.items_per_page).all()

            # =========================================================
            # Limit the number of hosts in request.
            # =========================================================

            response = []

            if servers:
                values = ['id', 'name', 'description', 'discovered', 'status', 'created']
                response = [{value: getattr(d, value) for value in values} for d in servers]

            return jsonify(hosts=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('host_update')
    def put(self):
        try:
            # =========================================================
            # PUT API
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()

            if request.headers['Content-Type'] == 'application/json':

                log.info('api() | Received PUT request: ' + str(request))

                # =========================================================
                # PUT API
                # =========================================================

                configuration = app_controller.get_host(request.json)

                # =========================================================
                # Add Host to Request.
                # =========================================================

                host_type = app_controller.get_host_type(configuration)

                # =========================================================
                # ESXi Host
                # =========================================================

                if host_type == settings.esx_vendor_id:

                    # =========================================================
                    # Create Job and insert it
                    # =========================================================

                    job_instance = Jobd.Jobd()
                    job_instance.id = ModelOperations.insert_job(job_instance.reference, request.data,
                                                                 settings.esx_vendor_id)

                    # =========================================================
                    # Launch celery Task
                    # =========================================================

                    log.info('api() | Creating task: ' + job_instance.reference)
                    celery = Celery()
                    celery.config_from_object('conf.celeryconfig')
                    celery.send_task("hypervisor.esxi.deploy_esx.imbue_esx",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     task_id=job_instance.reference,
                                     kwargs={'configuration': configuration,
                                             'job': job_instance})
                    log.info('api() | Task sent: ' + job_instance.reference)

                elif host_type == settings.ec2_type:

                    # =========================================================
                    # Not implemented
                    # =========================================================

                    log.error('api() | EC2 Host type not implemented')
                    resp = Response(status=501, mimetype='application/json')
                    return resp

                else:

                    # =========================================================
                    # Not implemented
                    # =========================================================

                    log.error('api() | Host type not implemented')
                    resp = Response(status=501, mimetype='application/json')
                    return resp

                # =========================================================
                # Return 202 Response
                # =========================================================

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)

                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            if job_instance:
                helper.update_job(job_instance.reference, error=True)
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('host')
    def post(self):
        try:
            # =========================================================
            # POST API
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            if request.headers['Content-Type'] == 'application/json':

                log.info('api() | Received POST request: ' + str(request))

                # =========================================================
                # POST API
                # =========================================================

                configuration = app_controller.get_host(request.json)

                # =========================================================
                # Add Host to Request.
                # =========================================================

                host_type = app_controller.get_host_type(configuration)

                # =========================================================
                # ESXi Host
                # =========================================================

                if host_type == settings.esx_vendor_id:

                    # =========================================================
                    # Create Job and insert it
                    # =========================================================

                    job_instance = Jobd.Jobd()
                    job_instance.id = ModelOperations.insert_job(job_instance.reference, request.data,
                                                                 settings.esx_vendor_id)

                    # =========================================================
                    # Launch celery Task
                    # =========================================================

                    log.info('api() | Creating task: ' + job_instance.reference)
                    start = time.time()
                    celery = Celery()
                    celery.config_from_object('conf.celeryconfig')
                    celery.send_task("hypervisor.esxi.deploy_esx.imbue_esx",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     task_id=job_instance.reference,
                                     kwargs={'configuration': configuration,
                                             'job': job_instance}
                                     )
                    log.info('api() | Task sent: ' + job_instance.reference)

                elif host_type == settings.ec2_type:

                    # =========================================================
                    # Not implemented
                    # =========================================================

                    log.error('api() | EC2 Host type not implemented')
                    resp = Response(status=501, mimetype='application/json')
                    return resp

                else:

                    # =========================================================
                    # Not implemented
                    # =========================================================

                    log.error('api() | Host type not implemented')
                    resp = Response(status=501, mimetype='application/json')
                    return resp

                # =========================================================
                # Return 202 Response
                # =========================================================

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response


        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            if job_instance:
                helper.update_job(job_instance.reference, error=True)
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Hosts information
# =========================================================

class Host(Resource):
    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('sync')
    def post(self, id):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()
            # =========================================================
            # POST Server by Id
            # =========================================================

            if request.headers['Content-Type'] == 'application/json':
                host = Model.Server.query.get(id)
                if host:
                    job_instance = Jobd.Jobd()
                    job_instance.id = ModelOperations.insert_job(job_instance.reference, request.data,
                                                                 settings.esx_vendor_id)

                    force_ = app_controller.get_host_sync(request.json)
                    reset_ = app_controller.get_host_reset(request.json)
                    # =========================================================
                    # Launch celery Task
                    # =========================================================
                    log.info('api() | Creating task: ' + job_instance.reference)
                    celery = Celery()
                    celery.config_from_object('conf.celeryconfig')
                    celery.send_task("hypervisor.esxi.deploy_esx.imbue_sync",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     kwargs={'job': job_instance,
                                             'host_id': id,
                                             'force': force_,
                                             'reset': reset_
                                             },
                                     task_id=job_instance.reference)
                    log.info('api() | Task sent: ' + job_instance.reference)
                else:
                    response = json.dumps('Host not found')
                    resp = Response(response, status=404, mimetype='application/json')
                    return resp

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

            else:
                resp = Response(status=415, mimetype='application/json')
                return resp

        except KeyError:
            response = json.dumps('415 Unsupported Media Type')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def delete(self, id):
        try:

            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # DELETE Server by Id
            # =========================================================

            host = Model.Server.query.get(id)
            log.warn('api() DELETE Server: ' + str(id))

            if host:

                # =========================================================
                # DELETE Server by Id
                # =========================================================

                if ModelOperations.delete_object(host):
                    log.warn('api() Server (' + str(id) + ') deleted')
                    response = json.dumps('DELETE operation initiated for server id: {0} '.format(id))
                    resp = Response(response, status=202, mimetype='application/json')
                    return resp
                else:
                    response = json.dumps('DELETE operation failed for server id: {0} '.format(id))
                    resp = Response(response, status=500, mimetype='application/json')
                    return resp
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def get(self, id):
        """

        """
        try:
            # =========================================================
            # GET Server by Id
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # GET Server by Id
            # =========================================================

            host = Model.Server.query.get(id)  # Check if host exists.
            if host:
                # Return Server details.
                return jsonify(ModelOperations.server_details(host, id))
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API Instance
# =========================================================

class InstanceList(Resource):
    @authenticator.requires_auth
    def get(self, id):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # GET Host information
            # =========================================================

            host = Model.Server.query.get(id)  # Check if host exists.
            if host:

                if len(request.args) > 0:
                    vmmachines = ModelOperations.vm_filter(request, id)
                else:
                    vmmachines = Model.Virtualmachine.query.filter(Model.Virtualmachine.fk_server == id).all()

                response = []

                if vmmachines:
                    values = ['vmid', 'hostname', 'description', 'status', 'app_id']
                    response = [{value: getattr(d, value) for value in values} for d in vmmachines]

                return jsonify(vm=response)
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('instance')
    def post(self, id):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()

            if request.headers['Content-Type'] == 'application/json':

                # =========================================================
                # Validate host exist then send request to it.
                # =========================================================

                host = Model.Server.query.get(id)
                # Verify Server exists
                if host:

                    # =========================================================
                    # Obtain server id information from database.
                    # =========================================================

                    configuration = app_controller.get_instance(request.json, host.get_public(), False)
                    log.info('api() | Application received request processing Task: {0}'.format(configuration))

                    # =========================================================
                    # Get application name
                    # =========================================================

                    app_instance = app_controller.get_application_type(configuration)
                    if app_instance:

                        # =========================================================
                        # Create Job, generate job id and insert it into database
                        # =========================================================

                        job_instance = Jobd.Jobd()
                        job_instance.id = ModelOperations.insert_job(job_instance.reference,
                                                                     request.data,
                                                                     settings.cisco_vendor_id)

                        # =========================================================
                        # Launch celery Task
                        # =========================================================

                        log.info('api() | Creating task: ' + job_instance.reference)
                        celery = Celery()
                        celery.config_from_object('conf.celeryconfig')
                        celery.send_task("catalogue.cisco.common.install_uc.imbue_uc",
                                         exchange='imbue',
                                         queue='gold',
                                         routing_key='imbue.gold',
                                         kwargs={'configuration': configuration,
                                                 'job': job_instance},
                                         task_id=job_instance.reference)
                        log.info('api() | Task sent: ' + job_instance.reference)
                    else:
                        response = json.dumps('Invalid application type request')
                        resp = Response(response, status=400, mimetype='application/json')
                        return resp
                else:
                    response = json.dumps('Host not found')
                    resp = Response(response, status=404, mimetype='application/json')
                    return resp

                # =========================================================
                # Return 202 Response accepted
                # =========================================================

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Instance information
# =========================================================

class Instance(Resource):
    @authenticator.requires_auth
    def get(self, id, vmid):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # GET Server by Id
            # =========================================================
            host = Model.Server.query.get(id)
            if host:

                # =========================================================
                # Check if hosts exists
                # =========================================================

                vm = Model.Virtualmachine.query.filter(Model.Virtualmachine.vmid == vmid,
                                                       Model.Virtualmachine.fk_server == id).first()
                if vm:
                    return jsonify(vm.get_public())

                response = json.dumps('VM not found')
                resp = Response(response, status=404, mimetype='application/json')
                return resp
            else:
                response = json.dumps('Host not found')
                resp = Response(response, status=404, mimetype='application/json')
                return resp


        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def delete(self, id, vmid):

        try:
            log.warn('api() | Received DELETE request: ' + str(request))
            # =========================================================
            # DELETE Server by Id
            # =========================================================

            host = Model.Server.query.get(id)
            if host:

                vm = Model.Virtualmachine.query.filter(Model.Virtualmachine.vmid == vmid,
                                                       Model.Virtualmachine.fk_server == id).first()
                if vm:

                    # =========================================================
                    # Create new job
                    # =========================================================

                    job_instance = Jobd.Jobd()
                    job_instance.id = ModelOperations.insert_job(job_instance.reference, request.data,
                                                                 settings.esx_vendor_id)

                    # =========================================================
                    # Launch celery Task
                    # =========================================================

                    log.info('api() | Creating task: ' + job_instance.reference)
                    start = time.time()
                    celery = Celery()
                    celery.config_from_object('conf.celeryconfig')
                    celery.send_task("hypervisor.esxi.vm_operations.remove_vm",
                                     exchange='imbue',
                                     queue='silver',
                                     routing_key='imbue.silver',
                                     kwargs={'host_id': id,
                                             'vm_id': vmid,
                                             'job': job_instance},
                                     task_id=job_instance.reference)
                    log.info('api() | Task sent: ' + job_instance.reference)

                    # =========================================================
                    # Return 202 Response accepted
                    # =========================================================

                    response = jsonify({'job': job_instance.reference})
                    response.headers['Location'] = api.url_for(api(current_app),
                                                               Job,
                                                               ref=job_instance.reference,
                                                               _external=True,
                                                               _scheme=settings.api_scheme)

                    response.headers['Status'] = api.url_for(api(current_app),
                                                             Status,
                                                             task_id=job_instance.reference,
                                                             _external=True,
                                                             _scheme=settings.api_scheme)
                    end = time.time()
                    log.info('api() | Task creation took: {} ms'.format(end - start))
                    response.status_code = 202
                    return response

                else:
                    response = json.dumps('VM not found')
                    resp = Response(response, status=404, mimetype='application/json')
                    return resp

            else:
                response = json.dumps('Host not found')
                resp = Response(response, status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            if job_instance:
                helper.update_job(job_instance.reference, error=True)
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Infrastructure information
# =========================================================

class InfrastructureList(Resource):
    @authenticator.requires_auth
    def get(self):
        try:

            # =========================================================
            # GET Tools information
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Get Tools
            # =========================================================

            tools = Model.Tool.query.limit(settings.items_per_page).all()
            values = ['id', 'hostname', 'status', 'description']
            response = [{value: getattr(d, value) for value in values} for d in tools]
            return jsonify(infrastructure=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('infrastructure')
    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()
            log.info('api() | POST | Received request adding Tools Server')
            job_instance = None

            if request.headers['Content-Type'] == 'application/json':
                # =========================================================
                # Create Job and insert it
                # =========================================================

                job_instance = Jobd.Jobd()
                job_instance.id = ModelOperations.insert_job(job_instance.reference,
                                                             request.data,
                                                             settings.tools_type)

                # =========================================================
                # Launch celery Task
                # =========================================================
                log.info('api() | Creating task: ' + job_instance.reference)
                celery = Celery()
                celery.config_from_object('conf.celeryconfig')
                celery.send_task("tools.deploy_tools.imbue_tools",
                                 kwargs={'configuration': request.json,
                                         'job': job_instance},
                                 task_id=job_instance.reference)

                log.info('api() | Task sent: ' + job_instance.reference)

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            if job_instance:
                helper.update_job(job_instance.reference, error=True)
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Infrastructure information
# =========================================================

class Infrastructure(Resource):
    @authenticator.requires_auth
    def delete(self, id):
        try:
            log.info('api() | Received DELETE request: ' + str(request))

            # =========================================================
            # DELETE Server by Id
            # =========================================================

            tool = Model.Tool.query.get(id)
            if tool:
                if ModelOperations.delete_object(tool):
                    log.warn('api() Tools (' + str(id) + ') deleted')
                    response = json.dumps('DELETE operation initiated for Tools Server: {0} '.format(id))
                    resp = Response(response, status=202, mimetype='application/json')
                    return resp
                else:
                    response = json.dumps('DELETE operation failed for Tools Server: {0} '.format(id))
                    resp = Response(response, status=500, mimetype='application/json')
                    return resp
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    @authenticator.requires_auth
    def get(self, id):
        try:
            log.info(
                request.remote_addr + ' ' + request.__repr__())  # =========================================================
            # GET Server by Id
            # =========================================================

            infrastructure = Model.Tool.query.get(id)
            if infrastructure:
                return jsonify(ModelOperations.infrastructure_details(infrastructure, id))
            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API Cluster information
# =========================================================

class Cluster(Resource):
    @authenticator.requires_auth
    @api_controller.validate_json
    @api_controller.validate_schema('cluster')
    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()

            if request.headers['Content-Type'] == 'application/json':
                # =========================================================
                # CLUSTER:Convert API JSON List request to dictionary. Indicate is a cluster
                # =========================================================

                configuration = app_controller.get_cluster(request.json)

                # Display cluster information
                log.info('api() | Cluster received request processing Task: ' + str(configuration))

                # =========================================================
                # Create Job and insert it
                # =========================================================

                job_instance = Jobd.Jobd()
                job_instance.id = ModelOperations.insert_job(job_instance.reference, request.data,
                                                             settings.cisco_vendor_id)

                # =========================================================
                # Launch celery Task
                # =========================================================

                log.info('api() | Creating task: ' + job_instance.reference)
                celery = Celery()
                celery.config_from_object('conf.celeryconfig')
                celery.send_task("catalogue.cisco.voice.cluster.deploy_cluster.imbue_cluster",
                                 exchange='imbue',
                                 queue='gold',
                                 routing_key='imbue.gold',
                                 kwargs={'configuration': configuration,
                                         'job': job_instance},
                                 task_id=job_instance.reference)

                log.info('api() | App Task sent: ' + job_instance.reference)

                # =========================================================
                # Return 202 Response accepted
                # =========================================================

                response = jsonify({'job': job_instance.reference})
                response.headers['Location'] = api.url_for(api(current_app),
                                                           Job,
                                                           ref=job_instance.reference,
                                                           _external=True,
                                                           _scheme=settings.api_scheme)

                response.headers['Status'] = api.url_for(api(current_app),
                                                         Status,
                                                         task_id=job_instance.reference,
                                                         _external=True,
                                                         _scheme=settings.api_scheme)
                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                response.status_code = 202
                return response

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            if job_instance:
                helper.update_job(job_instance.reference, error=True)
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API GET Status information
# =========================================================

class Status(Resource):
    @authenticator.requires_auth
    def get(self, task_id):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            log.info('api() | Creating task: ' + task_id)
            start = time.time()
            celery = Celery()
            celery.config_from_object('conf.celeryconfig')
            log.info('api() | Status received request for Task: ' + str(task_id))
            task = celery.AsyncResult(task_id)

            # =========================================================
            # Verify task exists and match
            # =========================================================

            if task and len(task_id) == 10:
                log.info(task.state)

                if task.state == 'PENDING':
                    # job did not start yet
                    response = {
                        'state': task.state,
                        'status': 'Task is in progress...'

                    }
                elif task.state != 'FAILURE':
                    response = {
                        'state': task.state,
                        'status': str(task.info),
                    }
                    if task.info:
                        if 'result' in task.info:
                            response['result'] = task.info['result']

                else:
                    response = {
                        'state': task.state,
                        'status': str(task.info),  # this is the exception raised
                    }

                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                return jsonify(response)

            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp
