__author__ = 'gogasca'

import json
from catalogue.application import Application
from functools import wraps
from conf import settings
from utils import validator
from flask import (
    request,
    abort
)


# =========================================================
# Validate Request comes in valid JSON form
# =========================================================

def validate_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except Exception:
            msg = "Request must be a valid JSON. Use Content-Type: application/json"
            abort(400, description=msg)
        return f(*args, **kw)

    return wrapper


# =========================================================
# Validate Request comes in valid JSON form
# =========================================================

def validate_schema(schema_name):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                if schema_name == 'infrastructure':
                    # =========================================================
                    # Validate POST for new cluster
                    # =========================================================
                    validator.check_infrastructure_req(request.json)

                if schema_name == 'sync':
                    # =========================================================
                    # Validate POST for new HOST
                    # =========================================================

                    validator.check_host_sync_req(request.json)

                if schema_name == 'host':
                    # =========================================================
                    # Validate POST for new HOST
                    # =========================================================

                    validator.check_host_req(request.json)

                if schema_name == 'host_update':
                    # =========================================================
                    # Validate PUT for new HOST
                    # =========================================================

                    validator.check_host_put_req(request.json)

                if schema_name == 'instance':
                    # =========================================================
                    # Validate POST for new UC instance
                    # =========================================================
                    validator.check_instance_req(request.json)

                if schema_name == 'cluster':
                    # =========================================================
                    # Validate POST for new cluster
                    # =========================================================
                    validator.check_cluster_req(request.json)

            except Exception, exception:
                print exception
                msg = "Request must be a valid JSON for " + str(schema_name)
                abort(400, description=msg)
            return f(*args, **kw)

        return wrapper

    return decorator


# =========================================================
# Validate Request comes in valid JSON form
# =========================================================

class Controller(object):
    """
    Handles business logic in API method
    """

    def __init__(self):
        pass

    # =========================================================
    # Get Hosts
    # =========================================================

    def get_host(self, configuration):
        """

        :param configuration:
        :return:
        """
        instance = dict()
        instance['host'] = configuration
        return instance

    def get_instance(self, configuration, server, cluster):
        """

        :param configuration:
        :param server:
        :param cluster:
        :return:
        """

        try:

            instance = dict()
            # Assign information
            # Get Server information
            if server['id']:
                if cluster:
                    # Multiple server configuration
                    instance['cluster'] = configuration
                else:
                    # Single server
                    configuration['host'] = json.loads("""{ "id": """ + str(server['id']) + """}""")
                    instance['instance'] = configuration
                return instance
            else:
                return False
        except Exception, e:
            print e
            return False

    def get_cluster(self, configuration):
        """

        :param configuration:
        :param server:
        :return:
        """
        try:

            cluster = dict()
            cluster['cluster'] = configuration
            return cluster

        except Exception, e:
            print e
            return False

    def get_cluster_app(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            configuration = json.loads(configuration)
            return validator.get_imbue_main_cluster_application(configuration)

        except Exception, e:
            print e
            return False

    def get_configuration(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            configuration = json.loads(configuration)
            return configuration

        except Exception, e:
            print e
            return None

    def check_application(self, configuration):
        """

        :param configuration:
        :return:
        """
        app_instance = Application.Application(configuration['instance']['application']['type'], {})
        # Check if application parameter is valid
        if app_instance.is_valid():
            return True
        else:
            return False

    def get_host_reset(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            if 'reset' in configuration:
                return configuration['reset']
            else:
                return False
        except Exception, e:
            print e
            return False

    def get_host_sync(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            if 'force' in configuration:
                return configuration['force']
            else:
                return False
        except Exception, e:
            print e
            return False

    def get_host_type(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            if configuration['host']['hypervisor']:
                return configuration['host']['hypervisor']['type']

        except Exception, e:
            print e
            return False

    def get_application_type(self, configuration):
        """

        :param configuration:
        :return:
        """
        try:
            # Applications are defined in settings file JSON object
            app_name = settings.cisco_list[configuration['instance']['application']['type']]
            app_instance = Application.Application(app_name, {})
            # Check if application parameter is valid in YAML file
            if app_instance.is_valid():
                return configuration['instance']['application']['type']
            else:
                return None
        except Exception, e:
            print e
            return None

    def __repr__(self):
        print "Controller() instance"
