# coding=utf-8

from conf import settings
from database import database_session
from database import Db

def execute_query(sqlquery):
    """

    :param sqlquery:
    :return:
    """

    try:
        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        db_instance.initialize()
        return db_instance.query(sqlquery)

    except Exception,exception:
        print exception

def get_record(sqlquery):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
        db.execute(sqlquery)
    except Exception, exception:
        print exception

def update_database(sqlquery):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
        db.execute(sqlquery)
    except Exception, exception:
        print exception