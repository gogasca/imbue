#!/bin/bash
#
# Program: Check performance
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3

echo 'imbue() Checking performance'
API_USER='ACda38cb38f1654200b439724edec211c1'
API_PWD='43b6e91ca96217e0be09c6f3fe94ec31'
URL='https://0.0.0.0:8443/api/1.0/'
REQUESTS=1000
CONCURRENCY=10
SLEEP_TIMER=5

testUrl() {

    ab -A  $API_USER:$API_PWD -n $REQUESTS -c $CONCURRENCY $URL$1

}

while true; do
    echo 'imbue() Echo()'
    testUrl "echo/"
    echo "Waiting..."
    sleep $SLEEP_TIMER;
    echo 'imbue() Hosts()'
    testUrl "host/"
    echo "Waiting..."
    sleep $SLEEP_TIMER;
    echo 'imbue() Jobs()'
    testUrl "job/"
    echo "Waiting..."
    sleep $SLEEP_TIMER;
    echo 'imbue() Infrastructure()'
    testUrl "infrastructure/"
    echo "Waiting..."
    sleep $SLEEP_TIMER;
done
