__author__ = 'gogasca'


def get_status_code(host):
    """

    :param host:
    :return:
    """
    import requests
    try:
        r = requests.head(host)
        return r.status_code
        #prints the int of the status code. Find more at httpstatusrappers.com :)
    except requests.ConnectionError:
        print 'Failed to connect'
        return None

