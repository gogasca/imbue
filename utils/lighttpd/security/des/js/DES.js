/*
 * JavaScript code written by Robert Mars (rmars@cisco.com) based on
 * Java code written by Jeremy Allison (jra@cygnus.com) based on
 * C code from Eric Young (eay@mincom.oz.au).
 * 
 * arrayCopy and getBytes functions are Copyright 2006 by Cisco Systems, Inc.
 */

/*
 * Java version of Eric Young's code created from the
 * original source by Jeremy Allison. <jra@cygnus.com>.
 * Version 1.0.
 */

/* Copyright (C) 1995 Eric Young (eay@mincom.oz.au)
 * All rights reserved.
 *
 * This file is part of an SSL implementation written
 * by Eric Young (eay@mincom.oz.au).
 * The implementation was written so as to conform with Netscapes SSL
 * specification.  This library and applications are
 * FREE FOR COMMERCIAL AND NON-COMMERCIAL USE
 * as long as the following conditions are aheared to.
 *
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.  If this code is used in a product,
 * Eric Young should be given attribution as the author of the parts used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Eric Young (eay@mincom.oz.au)
 *
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */


/**
 * Singleton instances
 */
var int32Manipulator=new Int32Manipulator();


/**
 * Des class. Does 56-bit Des encryption.
 */

//public class Des
  /**
   * Constructor for a Des Object. Takes an already
   * existing DesKey which will be used for all future
   * encryption/decryption with this Des object.
   * The passed in key is not checked for parity of weakness.
   *
   * @param key         Existing DesKey.
   */
function Des(key) {
  this.ks_ = key;

  /**
   * Constant. Pass this to functions that have a boolean
   * encrypt parameter to tell them to encrypt the data.
   */
  this.ENCRYPT = true;

  /**
   * Constant. Pass this to functions that have a boolean
   * encrypt parameter to tell them to decrypt the data.
   */
  this.DECRYPT = false;

  this.des_SPtrans_ = new Array(8);
    this.des_SPtrans_[0] = new Array(
      /* nibble 0 */
      0x00820200, 0x00020000, 0x80800000, 0x80820200,
      0x00800000, 0x80020200, 0x80020000, 0x80800000,
      0x80020200, 0x00820200, 0x00820000, 0x80000200,
      0x80800200, 0x00800000, 0x00000000, 0x80020000,
      0x00020000, 0x80000000, 0x00800200, 0x00020200,
      0x80820200, 0x00820000, 0x80000200, 0x00800200,
      0x80000000, 0x00000200, 0x00020200, 0x80820000,
      0x00000200, 0x80800200, 0x80820000, 0x00000000,
      0x00000000, 0x80820200, 0x00800200, 0x80020000,
      0x00820200, 0x00020000, 0x80000200, 0x00800200,
      0x80820000, 0x00000200, 0x00020200, 0x80800000,
      0x80020200, 0x80000000, 0x80800000, 0x00820000,
      0x80820200, 0x00020200, 0x00820000, 0x80800200,
      0x00800000, 0x80000200, 0x80020000, 0x00000000,
      0x00020000, 0x00800000, 0x80800200, 0x00820200,
      0x80000000, 0x80820000, 0x00000200, 0x80020200
    );
    this.des_SPtrans_[1] = new Array(
      /* nibble 1 */
      0x10042004, 0x00000000, 0x00042000, 0x10040000,
      0x10000004, 0x00002004, 0x10002000, 0x00042000,
      0x00002000, 0x10040004, 0x00000004, 0x10002000,
      0x00040004, 0x10042000, 0x10040000, 0x00000004,
      0x00040000, 0x10002004, 0x10040004, 0x00002000,
      0x00042004, 0x10000000, 0x00000000, 0x00040004,
      0x10002004, 0x00042004, 0x10042000, 0x10000004,
      0x10000000, 0x00040000, 0x00002004, 0x10042004,
      0x00040004, 0x10042000, 0x10002000, 0x00042004,
      0x10042004, 0x00040004, 0x10000004, 0x00000000,
      0x10000000, 0x00002004, 0x00040000, 0x10040004,
      0x00002000, 0x10000000, 0x00042004, 0x10002004,
      0x10042000, 0x00002000, 0x00000000, 0x10000004,
      0x00000004, 0x10042004, 0x00042000, 0x10040000,
      0x10040004, 0x00040000, 0x00002004, 0x10002000,
      0x10002004, 0x00000004, 0x10040000, 0x00042000
    );
    this.des_SPtrans_[2] = new Array(
      /* nibble 2 */
      0x41000000, 0x01010040, 0x00000040, 0x41000040,
      0x40010000, 0x01000000, 0x41000040, 0x00010040,
      0x01000040, 0x00010000, 0x01010000, 0x40000000,
      0x41010040, 0x40000040, 0x40000000, 0x41010000,
      0x00000000, 0x40010000, 0x01010040, 0x00000040,
      0x40000040, 0x41010040, 0x00010000, 0x41000000,
      0x41010000, 0x01000040, 0x40010040, 0x01010000,
      0x00010040, 0x00000000, 0x01000000, 0x40010040,
      0x01010040, 0x00000040, 0x40000000, 0x00010000,
      0x40000040, 0x40010000, 0x01010000, 0x41000040,
      0x00000000, 0x01010040, 0x00010040, 0x41010000,
      0x40010000, 0x01000000, 0x41010040, 0x40000000,
      0x40010040, 0x41000000, 0x01000000, 0x41010040,
      0x00010000, 0x01000040, 0x41000040, 0x00010040,
      0x01000040, 0x00000000, 0x41010000, 0x40000040,
      0x41000000, 0x40010040, 0x00000040, 0x01010000
    );
    this.des_SPtrans_[3] = new Array(
      /* nibble 3 */
      0x00100402, 0x04000400, 0x00000002, 0x04100402,
      0x00000000, 0x04100000, 0x04000402, 0x00100002,
      0x04100400, 0x04000002, 0x04000000, 0x00000402,
      0x04000002, 0x00100402, 0x00100000, 0x04000000,
      0x04100002, 0x00100400, 0x00000400, 0x00000002,
      0x00100400, 0x04000402, 0x04100000, 0x00000400,
      0x00000402, 0x00000000, 0x00100002, 0x04100400,
      0x04000400, 0x04100002, 0x04100402, 0x00100000,
      0x04100002, 0x00000402, 0x00100000, 0x04000002,
      0x00100400, 0x04000400, 0x00000002, 0x04100000,
      0x04000402, 0x00000000, 0x00000400, 0x00100002,
      0x00000000, 0x04100002, 0x04100400, 0x00000400,
      0x04000000, 0x04100402, 0x00100402, 0x00100000,
      0x04100402, 0x00000002, 0x04000400, 0x00100402,
      0x00100002, 0x00100400, 0x04100000, 0x04000402,
      0x00000402, 0x04000000, 0x04000002, 0x04100400
    );
    this.des_SPtrans_[4] = new Array(
      /* nibble 4 */
      0x02000000, 0x00004000, 0x00000100, 0x02004108,
      0x02004008, 0x02000100, 0x00004108, 0x02004000,
      0x00004000, 0x00000008, 0x02000008, 0x00004100,
      0x02000108, 0x02004008, 0x02004100, 0x00000000,
      0x00004100, 0x02000000, 0x00004008, 0x00000108,
      0x02000100, 0x00004108, 0x00000000, 0x02000008,
      0x00000008, 0x02000108, 0x02004108, 0x00004008,
      0x02004000, 0x00000100, 0x00000108, 0x02004100,
      0x02004100, 0x02000108, 0x00004008, 0x02004000,
      0x00004000, 0x00000008, 0x02000008, 0x02000100,
      0x02000000, 0x00004100, 0x02004108, 0x00000000,
      0x00004108, 0x02000000, 0x00000100, 0x00004008,
      0x02000108, 0x00000100, 0x00000000, 0x02004108,
      0x02004008, 0x02004100, 0x00000108, 0x00004000,
      0x00004100, 0x02004008, 0x02000100, 0x00000108,
      0x00000008, 0x00004108, 0x02004000, 0x02000008
    );
    this.des_SPtrans_[5] = new Array(
      /* nibble 5 */
      0x20000010, 0x00080010, 0x00000000, 0x20080800,
      0x00080010, 0x00000800, 0x20000810, 0x00080000,
      0x00000810, 0x20080810, 0x00080800, 0x20000000,
      0x20000800, 0x20000010, 0x20080000, 0x00080810,
      0x00080000, 0x20000810, 0x20080010, 0x00000000,
      0x00000800, 0x00000010, 0x20080800, 0x20080010,
      0x20080810, 0x20080000, 0x20000000, 0x00000810,
      0x00000010, 0x00080800, 0x00080810, 0x20000800,
      0x00000810, 0x20000000, 0x20000800, 0x00080810,
      0x20080800, 0x00080010, 0x00000000, 0x20000800,
      0x20000000, 0x00000800, 0x20080010, 0x00080000,
      0x00080010, 0x20080810, 0x00080800, 0x00000010,
      0x20080810, 0x00080800, 0x00080000, 0x20000810,
      0x20000010, 0x20080000, 0x00080810, 0x00000000,
      0x00000800, 0x20000010, 0x20000810, 0x20080800,
      0x20080000, 0x00000810, 0x00000010, 0x20080010
    );
    this.des_SPtrans_[6] = new Array(
      /* nibble 6 */
      0x00001000, 0x00000080, 0x00400080, 0x00400001,
      0x00401081, 0x00001001, 0x00001080, 0x00000000,
      0x00400000, 0x00400081, 0x00000081, 0x00401000,
      0x00000001, 0x00401080, 0x00401000, 0x00000081,
      0x00400081, 0x00001000, 0x00001001, 0x00401081,
      0x00000000, 0x00400080, 0x00400001, 0x00001080,
      0x00401001, 0x00001081, 0x00401080, 0x00000001,
      0x00001081, 0x00401001, 0x00000080, 0x00400000,
      0x00001081, 0x00401000, 0x00401001, 0x00000081,
      0x00001000, 0x00000080, 0x00400000, 0x00401001,
      0x00400081, 0x00001081, 0x00001080, 0x00000000,
      0x00000080, 0x00400001, 0x00000001, 0x00400080,
      0x00000000, 0x00400081, 0x00400080, 0x00001080,
      0x00000081, 0x00001000, 0x00401081, 0x00400000,
      0x00401080, 0x00000001, 0x00001001, 0x00401081,
      0x00400001, 0x00401080, 0x00401000, 0x00001001
    );
    this.des_SPtrans_[7] = new Array(
      /* nibble 7 */
      0x08200020, 0x08208000, 0x00008020, 0x00000000,
      0x08008000, 0x00200020, 0x08200000, 0x08208020,
      0x00000020, 0x08000000, 0x00208000, 0x00008020,
      0x00208020, 0x08008020, 0x08000020, 0x08200000,
      0x00008000, 0x00208020, 0x00200020, 0x08008000,
      0x08208020, 0x08000020, 0x00000000, 0x00208000,
      0x08000000, 0x00200000, 0x08008020, 0x08200020,
      0x00200000, 0x00008000, 0x08208000, 0x00000020,
      0x00200000, 0x00008000, 0x08000020, 0x08208020,
      0x00008020, 0x08000000, 0x00000000, 0x00208000,
      0x08200020, 0x08008020, 0x08008000, 0x00200020,
      0x08208000, 0x00000020, 0x00200020, 0x08008000,
      0x08208020, 0x00200000, 0x08200000, 0x08000020,
      0x00208000, 0x00008020, 0x08008020, 0x08200000,
      0x00000020, 0x08208000, 0x00208020, 0x00000000,
      0x08000000, 0x08200020, 0x00008000, 0x00208020
    );
}


  /*
   * Initial permutation.
   */
  Des.prototype.IP=function(ref_to_l, ref_to_r) {
    var tt = 0;
    int32Manipulator.PERM_OP(ref_to_r, ref_to_l, tt, 4, 0x0f0f0f0f);
    int32Manipulator.PERM_OP(ref_to_l, ref_to_r, tt, 16, 0x0000ffff);
    int32Manipulator.PERM_OP(ref_to_r, ref_to_l, tt, 2, 0x33333333);
    int32Manipulator.PERM_OP(ref_to_l, ref_to_r, tt, 8, 0x00ff00ff);
    int32Manipulator.PERM_OP(ref_to_r, ref_to_l, tt, 1, 0x55555555);
  }

  /*
   * Final permutation.
   *
   */
  Des.prototype.FP=function(ref_to_l, ref_to_r) {
    var tt = 0;
    int32Manipulator.PERM_OP(ref_to_l, ref_to_r, tt, 1, 0x55555555);
    int32Manipulator.PERM_OP(ref_to_r, ref_to_l, tt, 8, 0x00ff00ff);
    int32Manipulator.PERM_OP(ref_to_l, ref_to_r, tt, 2, 0x33333333);
    int32Manipulator.PERM_OP(ref_to_r, ref_to_l, tt,16, 0x0000ffff);
    int32Manipulator.PERM_OP(ref_to_l, ref_to_r, tt, 4, 0x0f0f0f0f);
  }

  Des.prototype.D_ENCRYPT=function(ref_to_Q, R, S, ref_to_u) {
    var s = this.ks_.get_keysced();
    S = S*4; /* Remember, S is a int offset, int C */
    var s_at_S_offset = int32Manipulator.bytes_to_int(s, S);
    var s_at_S_plus_one_offset = int32Manipulator.bytes_to_int(s, S+4);

    ref_to_u[0] = R^s_at_S_offset;
    var tmp = R^s_at_S_plus_one_offset;
    tmp = (tmp>>>4) + (tmp<<28);
    ref_to_Q[0] ^= this.des_SPtrans_[1][(tmp             )&0x3f]|
      this.des_SPtrans_[3][(tmp>>>8         )&0x3f]|
      this.des_SPtrans_[5][(tmp>>>16        )&0x3f]|
      this.des_SPtrans_[7][(tmp>>>24        )&0x3f]|
      this.des_SPtrans_[0][(ref_to_u[0]     )&0x3f]|
      this.des_SPtrans_[2][(ref_to_u[0]>>> 8)&0x3f]|
      this.des_SPtrans_[4][(ref_to_u[0]>>>16)&0x3f]|
      this.des_SPtrans_[6][(ref_to_u[0]>>>24)&0x3f];
  }

  /*
   * This is the internal encrypt routine, called by all the
   * other des encrypt/decrypt functions. It is not callable
   * outside the Des class.
   */
  Des.prototype.des_encrypt=function(data, encrypt) {
    var tmp = 0;
    var ref_to_u = new Array(1);
    var ref_to_r = new Array(1);
    var ref_to_l = new Array(1);

    ref_to_u[0] = data[0];
    ref_to_r[0] = data[1];

    this.IP(ref_to_u, ref_to_r);

    ref_to_l[0]=(ref_to_r[0]<<1)|(ref_to_r[0]>>>31);
    ref_to_r[0]=(ref_to_u[0]<<1)|(ref_to_u[0]>>>31);

    if (encrypt == this.ENCRYPT) {
      for (var i = 0; i < 32; i+=4) {
        this.D_ENCRYPT(ref_to_l,ref_to_r[0],i+0, ref_to_u); /*  1 */
        this.D_ENCRYPT(ref_to_r,ref_to_l[0],i+2, ref_to_u); /*  2 */
      }
    } else {
      /* Decrypt */
      for (var i = 30; i > 0; i-=4) {
        this.D_ENCRYPT(ref_to_l,ref_to_r[0],i-0, ref_to_u); /* 16 */
        this.D_ENCRYPT(ref_to_r,ref_to_l[0],i-2, ref_to_u); /* 15 */
      }
    }

    ref_to_l[0] = (ref_to_l[0]>>>1)|(ref_to_l[0]<<31);
    ref_to_r[0] = (ref_to_r[0]>>>1)|(ref_to_r[0]<<31);

    this.FP(ref_to_r, ref_to_l);

    data[0]=ref_to_l[0];
    data[1]=ref_to_r[0];
  }

  /**
   * Do the ecb (Encrypt/Decrypt 8 bytes electronic code book)
   * mode. Encrypts 8 bytes starting at offset input_start in
   * byte array input and writes them out at offset output_start in
   * byte array output.
   *
   * @param input          Input byte [] array.
   * @param input_start    Offset into input to start encryption.
   * @param output         Output byte [] array.
   * @param output_start   Offset into output to place result.
   * @param encrypt        Pass Des.ENCRYPT to encrypt, Des.DECRYPT to
   * decrypt.
   *
   */
  Des.prototype.ecb_encrypt=function(input, input_start,
                            output, output_start,
                            encrypt) {
    //alert("ecb_encrypt: input="+input+", encrypt="+encrypt);
    var ll = new Array(2);

    ll[0] = int32Manipulator.bytes_to_int(input, input_start);
    ll[1] = int32Manipulator.bytes_to_int(input, input_start+4);
    this.des_encrypt(ll, encrypt);
    int32Manipulator.set_int(output, output_start, ll[0]);
    int32Manipulator.set_int(output, output_start+4, ll[1]);
  }

function memset(out, c, len) {
    for(var i = 0; i < len; i++) {
        out[i] = c;
    }
}

function arraycopy(src, srcPos, dest, destPos, length) {
    var s = srcPos;
    var d = destPos;

    for (var i=0; i<length; i++) {
        dest[d++] = src[s++];
    }
}

function getBytes() {
    var s = this.toString();
    var bytes = new Array(s.length);

    for (var i=0; i<s.length; i++) {
        bytes[i] = s.charCodeAt(i);
    }

    return bytes;
}

String.prototype.getBytes = getBytes;

function encrypt(seed, smessage) {
    //alert("encrypt: seed="+seed+", smessage="+smessage);

    //var ks = new DesKey(seed.getBytes(), false);
    var ks = new DesKey(seed, false);
    var des = new Des(ks);

    var sb = smessage;
    //alert("sb.length="+sb.length);
    for (var i=0; i<8-(smessage.length % 8); i++) {
        sb += ' ';
    }

    var bmessage = sb.getBytes();
    //alert("bmessage="+bmessage);
    var length = bmessage.length;
    //alert("bmessage.length="+length);
    var out = new Array(8);
    var crypted = new Array(length);
    var blocks = length / 8;
    var index = 0;
    var tocrypt = new Array(8);
    for (var i=0; i<blocks; i++) {
      arraycopy(bmessage, index, tocrypt, 0, 8);
      memset(out, 0, 8);
      des.ecb_encrypt(tocrypt, 0, out, 0, des.ENCRYPT);
      arraycopy(out, 0, crypted, index, 8);
      index += 8;
    }

    return crypted;
}