/*
 * Copyright 2006 by Cisco Systems, Inc.,
 * 170 West Tasman Drive, San Jose, California, 95134, U.S.A.
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of Cisco
 * Systems, Inc. ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with Cisco.
 *
 * Purpose: encrypt a password using DES 56-bit encryption.
 * Author: Robert Mars (rmars@cisco.com)
 * Date: 10/25/06
 */

/**
 * Encrypts a password into a hexadecimal string using 56-bit DES encryption.
 *
 * Password cannot be longer than 32 characters.
 * A hard-coded seed/key is used.
 *
 * @param s binary string of '0's and '1's
 * @return byte array of decimal equivalent
 */
function encryptPassword(password) {
    try {
        var PASSWORD_WIDTH = 32;

        if (password.length > PASSWORD_WIDTH) {
            alert("Password must be "+PASSWORD_WIDTH+" or fewer characters.");
            return "Password exceeds "+PASSWORD_WIDTH+" characters.";
        }

        // a 64 bit key (even though only 56 bits are used)
        var BINARY_KEY = "1110111100000000111111110000001011111011000000001111111101000010";
                       //   239       0     255       2     251       0      255     66

        // need key as 2 integers (4 bytes each -> 8 bytes) in string form
        //alert("typeof(asciiBinary2bytes)="+typeof(asciiBinary2bytes));
        var keyBytes = asciiBinary2bytes(BINARY_KEY);

        password = padString(password, '\0', PASSWORD_WIDTH);

        var encryptedBytes = encrypt(keyBytes, password);
        //alert("encryptedBytes=\'"+encryptedBytes+"\'");

        var encryptedHex = bytes2hex(encryptedBytes).substring(0,64);
        //alert("encryptedHex="+encryptedHex);

        return encryptedHex;
        
    } catch(e) {
        alert("Exception name="+e.name+"; description="+e.description+"; message="+e.message+"; number="+e.number);
    }
}