<?php 
include ('../config/setup.php');

if ($_REQUEST) {
	$password  = $_REQUEST['password'];
	if ($password) {
		// Return encrypted password
		echo '<script type="text/javascript">document.write(encryptPassword("'.$password.'"));</script>';
	} 
	else {
		// Password not found
		echo 'Password not found';
	}
	    
} else {
	// No parameters
	echo 'Not a valid HTTP request';
} 

?>