__author__ = 'gogasca'

import socket
from multiprocessing import Pool
import multiprocessing.pool
from functools import partial
from errno import ECONNREFUSED
from error import exceptions
from conf import settings, logging_conf
import validator
import logging
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

NUM_CORES = settings.num_cores

def is_port_opened(server=None, port=None, **kwargs):
    log.info('port_scanner.is_port_opened() Checking port: ' + str(port))
    try:
        # Add More proccesses in case we look in a range Default = 1

        if validator.isValidIpAddress(server=server) or validator.isValidHostName(server=server):
            if validator.isValidPort(port):
                try:
                    # Create Socket
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    if hasattr(settings, 'socketTimeout'):
                        if settings.socketTimeout != '' and isinstance(int(settings.socketTimeout), int):
                            s.settimeout(settings.socketTimeout)
                    else:
                        s.settimeout(2)
                    s.connect((server, int(port)))
                    log.info('port_scanner.is_port_opened() ' + str(port) + " is opened")
                    return True
                except socket.error as err:
                    log.exception(str(err))
                    if err.errno == ECONNREFUSED:
                        return False

                return False
            else:
                log.exception('port_scanner.is_port_opened() Invalid port')
                raise exceptions.InvalidPort('port_scanner.is_port_opened() Invalid port')
                return False

        else:
            log.error('port_scanner.is_port_opened() Invalid target')

    except Exception, e:
        log.exception(str(e))
        return False


def portscan(target, port):
    try:
        # Create Socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if hasattr(settings, 'socketTimeout'):
            if settings.socketTimeout != '' and isinstance(int(settings.socketTimeout), int):
                s.settimeout(settings.socketTimeout)

        s.connect((target, port))
        log.info('port_scanner.is_port_opened() ' + str(port) + " is opened")
        return port
    except socket.error as err:
        log.exception(str(err))
        if err.errno == ECONNREFUSED:
            return False


# Wrapper function that calls portscanner
def scan_ports(server=None, port=None, portStart=None, portEnd=None, **kwargs):
    p = Pool(NUM_CORES)
    ping_host = partial(portscan, server)
    if portStart and portStart:
        return filter(bool, p.map(ping_host, range(portStart, portStart)))
    else:
        return filter(bool, p.map(ping_host, range(port, port + 1)))


# Check if port is opened
def is_port_opened_mt(server=None, port=None, **kwargs):
    log.info('port_scanner.is_port_opened() Checking port: ' + str(port))
    try:
        # Add More proccesses in case we look in a range Default = 1

        if validator.isValidIpAddress(server=server) or validator.isValidHostName(server=server):
            if validator.isValidPort(port):
                try:
                    ports = list(scan_ports(server=server, port=int(port)))
                    log.info("port_scanner.is_port_opened() Port scanner done.")
                    if len(ports) != 0:
                        log.info('port_scanner.is_port_opened() ' + str(len(ports)) + " port(s) available.")
                        return True
                    else:
                        log.error('port_scanner.is_port_opened() port not opened: (' + str(port) + ')')
                        return False
                except Exception, e:
                    log.exception(str(e))
                    raise
            else:
                log.exception('port_scanner.is_port_opened() Invalid port')
                raise exceptions.InvalidPort('port_scanner.is_port_opened() Invalid port')
                return False

        else:
            log.error('port_scanner.is_port_opened() Invalid target')

    except Exception, e:
        log.exception(str(e))
        return False

if __name__ == '__main__':
    print is_port_opened(server='ziro1.noip.me', port=8180)
