import subprocess
import re

import utils
from error import exceptions


class Pinger():
    def __init__(self, host):
        if utils.isIP(host):
            if utils.isValidIpAddress(host):
                self.target = host
            else:
                print 'Invalid IP address'
                raise exceptions.InvalidHostName('Invalid IP address')
        else:
            if utils.isValidHostName(host):
                self.target = host
            else:
                print 'Invalid host'
                raise exceptions.InvalidHostName('Invalid hostname')

        self.rttMin = None
        self.rttAvg = None
        self.rttMax = None
        self.rttStdDev = None
        self.count = 4

    def ping(self, count=4, **kwargs):
        if count and isinstance(count, int):
            self.count = count

        if self.target:
            ping = subprocess.Popen(
                ["ping", "-c", str(self.count), self.target],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )

        out, error = ping.communicate()
        if out:
            message = re.search("round-trip min/avg/max/stddev = (\d+.\d+)/(\d+.\d+)/(\d+.\d+)/(\d+.\d+)", out)
            if message:
                self.rttMin = message.group(1)
                self.rttAvg = message.group(2)
                self.rttMax = message.group(3)
                self.rttStdDev = message.group(4)

            else:
                message = re.search("(\d+.\d+)% packet loss", out)
                if message:
                    raise exceptions.ConnectivityError(
                        'Unable to connect to host. %s %% packet loss' % (message.group(1)))
        if error:
            raise exceptions.ConnectivityError('Unknown error')

    def __str__(self):
        return "round-trip min/avg/max/stddev = %s/%s/%s/%s ms" % (
        str(self.rttMin), str(self.rttAvg), str(self.rttMax), str(self.rttStdDev))


if __name__ == '__main__':
    p = Pinger(host='www.yahoo.com')
    p.ping(count=10)
    print p



