import logging
import time
import random
from celery.task.control import revoke
from celery.result import AsyncResult
from conf import settings, logging_conf
from utils import db_utils
from shared import Report

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def revoke_task(job, signal='SIGTERM', force=True, **kwargs):
    """

    :param job:
    :param signal:
    :param force:
    :param kwargs:
    :return:
    """
    log.info('revoke_task() Revoking task: {}'.format(job.parent_reference))

    try:

        # =========================================================
        # Get job status
        # =========================================================

        for n in xrange(0, 5):
            try:
                job_status = AsyncResult(job.parent_reference).state
                if job_status == 'SERVER_VERIFY':
                    log.info(
                        'revoke_task() [{}]. Verifying server. Wait for operation to complete'.format(job_status))
                    time.sleep((15 * n) + random.randint(0, 9))
                elif job_status == 'SERVER_BUSY':
                    log.info(
                        'revoke_task() [{}]. Deploying VM in server. Wait for operation to complete'.format(job_status))
                    time.sleep((15 * n) + random.randint(0, 9))
                elif job_status == 'SERVER_FAILED':
                    log.info('revoke_task() [{}]. Server deployment failed'.format(job_status))
                    return -1
                elif job_status == 'REVOKED' or job_status == 'SUCCESS':
                    log.info(
                        'revoke_task() [{}]. Task already revoked'.format(job_status))
                    return 1
                else:
                    break
            except Exception:
                log.exception("revoke_task() [{}] Retrying {} times".format(n))

        # =========================================================
        # Revoke job.
        # =========================================================

        log.info('revoke_task() Task will be revoked Job status...[{}] '.format(job_status))
        revoke(job.parent_reference, terminate=True, signal=signal)

        # Allow time for Task to be revoked
        time.sleep(settings.cancel_task_wait)

        # =========================================================
        # Get job status
        # =========================================================

        job_status = AsyncResult(job.parent_reference).state

        log.info(
            'revoke_task() After sending Revoke request status is: [{}] with signal: {}'.format(job_status,
                                                                                                signal))
        if job_status == 'SUCCESS':
            log.info('revoke_task() Task was cancelled successfully')
            return 1
        else:
            # Task is in an UNKNOWN state
            if force:
                signal = 'SIGKILL'
                log.warn('revoke_task() Forcing job cancellation status  [{}] with signal: {}'.format(job_status,
                                                                                                      signal))
                # =========================================================
                # Revoke job.
                # =========================================================

                revoke(job.parent_reference, terminate=True, signal=signal)
                sqlquery = """UPDATE job SET status=-1, job_end='now()',is_cancelled=true, description='Installation cancelled' WHERE job.id=""" + \
                           str(job.parent_id)
                db_utils.update_database(sqlquery)

                # =========================================================
                # Get job status
                # =========================================================

                job_status = AsyncResult(job.parent_reference).state
                log.info(
                    'revoke_task() Status after sending Revoke request is: [{}] with signal: {}'.format(job_status,
                                                                                                        signal))
                report = Report.Report(settings.report_path, job.parent_reference)
                if report:
                    report.initialized = True
                    report.add_log('Installation ' + job.parent_reference + ' has been cancelled')
                    report.generate_report(video_supported=True)
                if job_status == 'REVOKED':
                    return 1
                else:
                    return -1

        return -1

    except Exception as exception:
        log.exception('cancel_job() {}'.format(exception))
        return -1
