import warnings

warnings.filterwarnings("ignore")
import json
from conf import settings
from conf import logging_conf
from database import Db
from utils import helper
import logging

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def add_vm(app_params):
    """

    :param vm_response:
    :return:
    """
    try:
        if app_params.has_key('installation_request') and app_params.has_key('vmid'):
            id = app_params['installation_request']
            vm_id = app_params['vmid']
            if id and vm_id:
                sqlquery = "UPDATE installation_request SET vmid=" + str(
                    vm_id) + " WHERE installation_request.id=" + str(id)
                helper.update_database(sqlquery)
                log.info('complete_request() Added VirtualMachine')
                return True

        return False

    except Exception, exception:
        log.exception('complete_request() Installation request failed {}'.format(exception))
        return False


def complete_request(app_params):
    """

    :param app_params:
    :return:
    """
    try:
        if 'installation_request' in app_params:
            id = app_params['installation_request']
            sqlquery = "UPDATE installation_request SET is_completed=true WHERE installation_request.id=" + str(id)
            helper.update_database(sqlquery)
            log.info('complete_request() Installation request completed')
            return True

        return False

    except Exception, exception:
        log.exception('complete_request() Installation request failed {}'.format(exception))
        return False


def save_history(app_params, return_id=False):
    """

    :param app_params:
    :return:
    """
    app_params['job'] = app_params['job'].reference
    json_db = json.dumps(app_params)
    log.info('save_history(): JSON information:' + json_db)
    sqlquery = """
                        INSERT INTO installation_request (job, timestamp, application_params, owner_id, fk_server)"""

    content = "\'" + app_params['job'] + \
              '\',now(),\'' + \
              json_db + "\'," + \
              str(app_params['owner_id']) + ',' + str(app_params['host_id'])

    db_instance = Db.Db(server=settings.dbhost,
                        username=settings.dbusername,
                        password=settings.dbpassword,
                        database=settings.dbname,
                        port=settings.dbport)

    db_instance.initialize()
    res = db_instance.insert(sqlquery, content)

    if res:
        # =========================================================
        # Install successful started
        # =========================================================
        log.info('save_history() Installation request saved to installation_requests id: {}'.format(res))
        if return_id:
            log.info('save_history() Returning id: {}'.format(res))
            return res

        return True
    else:

        # =========================================================
        # Install failed to start
        # =========================================================
        log.error('save_history() Installation request not saved to installation_requests')
        return False
