import logging
from celery.task import task
from celery.result import AsyncResult
from conf import logging_conf
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


@task(bind=True, default_retry_delay=30, max_retries=3)
def create_celery_task(self, job):
    """

    :param self:
    :return:
    """
    message = 'Celery task created'
    log.info('create_celery_task()  {} Job: {} '.format(message, job))
    job_status = AsyncResult(self.request.id).state
    log.info('create_celery_task() Job status: [{}] Task: {}'.format(job_status, self.request.id))

    if self.request.id:
        return {'status': message, 'result': 1}
