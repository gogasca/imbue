import hashlib
import json
import numbers
import platform
import re
import sys
import socket
from error import exceptions
from conf import settings

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')

__author__ = 'gogasca'


# =========================================================
# Validate POST for new Host
# =========================================================

def check_host_req(configuration):
    """

    :param configuration:
    :return:
    """
    if not 'host' in configuration:
        configuration['host'] = configuration
    return check_hypervisor(configuration['host']['hypervisor'])


# =========================================================
# Validate POST for new Host
# =========================================================

def check_host_put_req(configuration):
    """

    :param configuration:
    :return:
    """
    if not 'host' in configuration:
        configuration['host'] = configuration
    return check_hypervisor_update(configuration['host']['hypervisor'])


# =========================================================
# Validate POST for sync
# =========================================================

def check_host_sync_req(request):
    """

    :param configuration:
    :return:
    """
    params = ['force', 'reset']
    if any(key in request for key in params):
        return True

    return False


# =========================================================
# Validate POST for new infrastructure
# =========================================================

def check_infrastructure_req(request):
    """

    :param configuration:
    :return:
    """
    if not 'infrastructure' in request:
        request['infrastructure'] = request
    return check_infrastructure_instance(request['infrastructure']['instance'])


# =========================================================
# Validate POST for new instance
# =========================================================

def check_instance_req(request):
    """

    :param configuration:
    :return:
    """

    if not 'instance' in request:
        request['instance'] = request

    return check_imbue_uc_req(request['instance']['application'])


# =========================================================
# Validate POST for new cluster
# =========================================================

def check_cluster_req(request):
    """

    :param configuration:
    :return:
    """
    try:
        if isinstance(request, list):
            configuration = dict()
            configuration['cluster'] = request
            return check_cluster_configuration(configuration)
        else:
            return False

    except KeyError:
        raise exceptions.InvalidClusterRequest('Invalid cluster Request. No DB key')


# =========================================================
# Validate CLI for new instance
# =========================================================
def check_imbue_instance(configuration):
    """
    :param configuration:
    :return:
    """
    try:

        if 'instance' in configuration:
            if all(k in configuration['instance'] for k in ('host', 'application')):
                if 'id' in configuration['instance']['host'] and \
                        check_imbue_uc_req(configuration['instance']['application']):
                    # Verify host_id is an Integer
                    if isinstance(configuration['instance']['host']['id'], int):
                        return True
        return False

    except Exception, e:
        print e


# =========================================================
# Validate POST for new infrastructure
# =========================================================
def check_cluster_configuration(configuration):
    """

    :param configuration:
    :return:
    """
    try:

        # =========================================================
        # CLUSTER: Check cluster configuration
        # =========================================================
        # print 'check_cluster_configuration'
        if check_cluster_details(configuration):
            return True
        else:
            raise exceptions.InvalidClusterRequest('Invalid Cluster Request')

    except Exception:
        return False


# =========================================================
# Validate CLUSTER details
# =========================================================

def check_cluster_details(configuration):
    """

    :param configuration:
    :param api_call:
    :return:
    """
    try:
        # print 'check_cluster_details'
        cluster_instances = [instance for instance in configuration["cluster"]]
        if 0 < len(cluster_instances) <= settings.cluster_max_nodes:
            pass
        else:
            return False

        # =========================================================
        # We process cluster list of instance objects
        # =========================================================

        for cluster_instance in cluster_instances:

            # =================================================================================
            # We verified that all subscribers are the same type of Application or CUCM and CUP
            # =================================================================================
            if cluster_instance['instance']['application']['type'] in [200001, 200002]:
                pass
            elif cluster_instance['instance']['application']['type'] in [200001, 200003]:
                pass
            else:
                return False

        return True

    except KeyError, e:
        return False
    except Exception, e:
        print e


# =========================================================
# Validate CLUSTER
# =========================================================

def get_cluster_configuration_mode(configuration):
    """

    :param configuration:
    :return:
    """

    app_ids = get_imbue_cluster_applications(configuration)

    # =========================================================
    #  Validate cluster configuration
    # =========================================================
    if not app_ids:
        raise AttributeError

    mode = -1

    #  mode=0 Undefined
    #  mode=1 CUCM only
    #  mode=2 CUC only
    #  mode=3 CUP only
    #  mode=4 CUCM Pub + CUC Pub
    #  mode=5 CUCM Pub + CUP Pub

    ucm = [x for x in app_ids if x == 200001]
    cuc = [x for x in app_ids if x == 200002]
    cup = [x for x in app_ids if x == 200003]

    if len(ucm) == len(app_ids):
        return 1
    if len(cuc) == len(app_ids):
        return 2
    if len(cup) == len(app_ids):
        return 3

    # Extract duplicates
    app_ids = dict.fromkeys(app_ids).keys()
    app_ids = sorted(app_ids, key=int)

    mode4 = [200001, 200002]
    mode5 = [200001, 200003]

    if app_ids == mode4:
        mode = 4
    elif app_ids == mode5:
        mode = 5
    else:
        mode = -1

    # See if mode=4
    return mode


def get_imbue_cluster_installation(configuration, mode):
    """

    :param configuration:
    :return:
    """
    # =========================================================
    # get cluster information
    # =========================================================
    cluster_instances = [instance for instance in configuration["cluster"]]
    if mode == 1 or mode == 2 or mode == 3:
        pub_found = False
        index = -1

        # =========================================================
        # Verify there is only 1 publisher
        # =========================================================

        for cluster_instance in cluster_instances:
            if cluster_instance["instance"]["application"]["publisher"]:
                # =========================================================
                # Only 1 publisher per request
                # =========================================================
                pub_found = True
                index += 1

        if pub_found and index == 0:
            return True
        else:
            return False

    elif mode == 4:

        # =========================================================
        # CUCM Pub and CUC Pub
        # =========================================================

        cucm_pub = False
        cucm_index = -1
        cuc_pub = False
        cuc_index = -1
        index = -1
        for cluster_instance in cluster_instances:
            index += 1
            if cluster_instance['instance']['application']['type'] == 200001:

                if cluster_instance['instance']['application']['publisher']:
                    # print 'deploy_cluster() CUCM Publisher found'
                    cucm_pub = True
                    cucm_index = index
            else:
                if cluster_instance['instance']['application']['type'] == 200002:

                    if cluster_instance['instance']['application']['publisher']:
                        # print 'deploy_cluster() CUP Publisher found'
                        cuc_pub = True
                        cuc_index = index

        if cucm_pub and cuc_pub and (cucm_index > -1 and cuc_index > -1):
            return True

        return False

    elif mode == 5:

        # =========================================================
        # CUCM Pub and CUP Pub
        # =========================================================
        cucm_pub = False
        cucm_index = -1
        cup_pub = False
        cup_index = -1
        index = -1
        for cluster_instance in cluster_instances:
            index += 1
            if cluster_instance['instance']['application']['type'] == 200001:

                if cluster_instance['instance']['application']['publisher']:
                    # print 'deploy_cluster() CUCM Publisher found'
                    cucm_pub = True
                    cucm_index = index
            else:
                if cluster_instance['instance']['application']['type'] == 200003:

                    if cluster_instance['instance']['application']['publisher']:
                        # print 'deploy_cluster() CUP Publisher found'
                        cup_pub = True
                        cup_index = index

        if cucm_pub and cup_pub and (cucm_index > -1 and cup_index > -1):
            return True

        return False

    else:
        # print 'Invalid mode'
        return False


# =========================================================
# Validate CLUSTER
# =========================================================

def get_imbue_cluster_applications(configuration):
    """

    :param configuration:
    :param api_call:
    :return:
    """
    try:
        cluster_instances = [instance for instance in configuration['cluster']]
        app_type = []
        for cluster_instance in cluster_instances:
            app_type.append(cluster_instance['instance']['application']['type'])
        return app_type

    except Exception, exception:
        print exception
        return None


# =========================================================
# Validate CLUSTER
# =========================================================

def get_imbue_cluster_instances(configuration):
    """

    :param configuration:
    :return:
    """
    try:
        instances = [str(instance) for instance in configuration['cluster']]
        # Sort indexes
        instances.sort()
        return instances
    except Exception, e:
        print e


# =========================================================
# Validate CLI for new host
# =========================================================

def check_host(configuration):
    """

    :param configuration:
    :return:
    """
    try:

        if 'host' in configuration:
            return check_hypervisor(configuration['host']['hypervisor'])
        else:
            return False
    except Exception, e:
        print e


# =========================================================
# Validate Request for new host
# =========================================================

def check_hypervisor_update(configuration):
    """

    :param configuration:
    :return:
    """
    try:

        if all(k in configuration for k in
               ("type", "description", "name", "ip", "username", "ssh", "https", "vnc")):
            return True
        else:
            raise exceptions.InvalidHostRequest('Invalid Host Request')
    except Exception, e:
        print e


def check_hypervisor(configuration):
    """

    :param configuration:
    :return:
    """
    try:

        if all(k in configuration for k in
               ("type", "description", "name", "ip", "username", "password", "ssh", "https", "vnc")):
            return True
        else:
            raise exceptions.InvalidHostRequest('Invalid Host Request')
    except Exception, e:
        print e


def check_infrastructure_instance(configuration):
    """

    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in ("type", "name", "ip", "username", "password", "ssh")):
            return True
        else:
            raise exceptions.InvalidInfrastructureRequest('Invalid Infrastructure Request')

    except Exception, e:
        print e


def check_imbue_uc_req(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if 'type' in configuration:
            if 200001 <= configuration["type"] <= 200005:
                if all(k in configuration for k in
                       ("type", "params", "publisher", "version", "directory", "answer_file", "esx_network")):
                    return True
            if 200006 <= configuration["type"] <= 200008:
                if all(k in configuration for k in
                       ("type", "params", "datastore", "ova_file", "version", "directory", "esx_network")):
                    return True

        return False

    except Exception, e:
        print str(e)


def check_imbue_options(configuration):
    """

    :param configuration:
    :return:
    """
    if 'hostname' in configuration:
        return True
    else:
        return False


def check_imbue_uc(configuration, cluster, publisher, additional):
    """
    Validate mandatory parameters
    :param configuration:
    :param cluster:
    :param publisher:
    :param additional:
    :return:
    """
    try:

        # =============================================================
        # Cluster support CUCM+CUC, CUCM+CUP. I dont need cluster file
        # =============================================================
        type = configuration['type']

        if cluster:
            if additional:
                # =============================================================
                # Cluster support CUCM, CUP, CUC same type.
                # =============================================================
                if type == additional['instance']['application']['type']:
                    if publisher:
                        if all(k in configuration for k in
                               ("params", "publisher", "version", "directory", "answer_file", "cluster_file",
                                "esx_network")):
                            return check_imbue_options(configuration["params"])
                        else:
                            return False
                    else:
                        if all(k in configuration for k in
                               ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
                            return check_imbue_options(configuration["params"])
                        else:
                            return False

                # =============================================================
                # Cluster support CUCM+CUC, CUCM+CUP
                # =============================================================
                if type == 200001:
                    if publisher:
                        if all(k in configuration for k in
                               ("params", "publisher", "version", "directory", "answer_file", "cluster_file",
                                "esx_network")):
                            return check_imbue_options(configuration["params"])
                        else:
                            return False

                if (type == 200002 or type == 200003) \
                        and additional['instance']['application']['type'] == 200001:
                    if publisher:
                        if all(k in configuration for k in
                               ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
                            return check_imbue_options(configuration["params"])
                        else:
                            return False
            else:

                # =============================================================
                # Cluster support CUCM, CUC, CUP install now, install sub later
                # =============================================================

                if publisher:
                    if all(k in configuration for k in
                           ("params", "publisher", "version", "directory", "answer_file", "cluster_file",
                            "esx_network")):
                        return check_imbue_options(configuration["params"])


        else:

            # =============================================================
            # Single install CUCM, CUC, CUP, VCS
            # =============================================================
            if 200001 <= type <= 200005:
                if all(k in configuration for k in
                       ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
                    return check_imbue_options(configuration["params"])
            elif 200006 <= type <= 200008:
                if all(k in configuration for k in
                       ("params", "datastore", "ova_file", "version", "directory", "esx_network")):
                    return check_imbue_options(configuration["params"])
            else:
                return False

        return False


    except Exception, e:
        print str(e)


def check_imbue_cucm(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in
               ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
            return True
        else:
            return False
    except Exception, e:
        print str(e)


def check_imbue_cucm_cluster(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in
               ("params", "publisher", "version", "directory", "cluster_file", "answer_file", "esx_network")):
            return True
        else:
            return False
    except Exception, e:
        print str(e)


def check_imbue_cuc(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in
               ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
            return True
        else:
            return False
    except Exception, e:
        print str(e)


def check_imbue_cuc_cluster(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in
               ("params", "publisher", "version", "directory", "cluster_file", "answer_file", "esx_network")):
            return True
        else:
            return False
    except Exception, e:
        print str(e)


def check_imbue_cup(configuration):
    """
        Validate mandatory parameters
    :param configuration:
    :return:
    """
    try:
        if all(k in configuration for k in
               ("params", "publisher", "version", "directory", "answer_file", "esx_network")):
            return True
        else:
            return False
    except Exception, e:
        print str(e)


def check_discover_flag(configuration):
    """

    :param configuration:
    :return:
    """
    try:
        # Default is TRUE
        execute_discovery = settings.esx_discovery

        if configuration['host']['hypervisor']['discover']:
            execute_discovery = True
        else:
            execute_discovery = False

    except KeyError:
        pass
    except Exception, exception:
        print(str(exception))
    finally:
        return execute_discovery


def check_host_config(configuration):
    """

    :param configuration:
    :return:
    """
    if configuration:
        try:

            # =========================================================
            # Check for IP address
            # =========================================================

            if configuration['host']['hypervisor']['ip']:
                if is_valid_ipv4_address(configuration['host']['hypervisor']['ip']) or \
                        isValidHostName(configuration['host']['hypervisor']['ip']):
                    pass
            # =========================================================
            # Check for HTTPS and SSH ports
            # =========================================================

            if configuration['host']['hypervisor']['ssh'] and \
                    configuration['host']['hypervisor']['https']:
                if isValidPort(configuration['host']['hypervisor']['ssh']) and \
                        isValidPort(configuration['host']['hypervisor']['https']):
                    pass

            # =========================================================
            # Check for username and password
            # =========================================================

            if configuration['host']['hypervisor']['username']:
                pass

            # =========================================================
            # Check for VNC ports
            # =========================================================

            if configuration['host']['hypervisor']['vnc']:
                if check_vnc_port(configuration['host']['hypervisor']['vnc']) > 0:
                    return True

            return False

        except KeyError:
            return False

        except Exception as exception:
            print str(exception)
    return False


def check_parameter_flag(configuration, parameter):
    """

    :param configuration:
    :return:
    """
    try:
        # Default is TRUE
        parameter_flag = False

        if configuration['host']['hypervisor'][parameter]:
            parameter_flag = True
        else:
            parameter_flag = False

    except KeyError:
        pass
    except Exception, exception:
        print exception
    finally:
        return parameter_flag


def check_yaml(filename):
    """
    Validate Yaml configuration
    :param filename:
    :return:
    """
    import yaml
    with open(filename) as f:
        doc = yaml.load(f)
    try:
        pass
    except KeyError:
        print 'Invalid key'
    except Exception, e:
        print e


def is_customer_valid(customer_id):
    """

    :param customer_id:
    :return:
    """
    if customer_id != '' and isinstance(customer_id, (str, unicode)):
        # return readCustomerId(customerId)
        return True
    else:
        return False

    return True


def check_folder(param):
    """

    :param param:
    :return:
    """
    if param != '' and isinstance(param, (str, unicode)):
        return True
    else:
        return False


def isIP(server):
    """

    :param server:
    :return:
    """
    if isValidIpAddress(server):
        return True
    else:
        return False


def is_valid_ipv4_address(address):
    """

    :param address:
    :return:
    """
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True


def is_valid_ipv6_address(address):
    """

    :param address:
    :return:
    """
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:  # not a valid address
        return False
    return True


# Check if is Valid IP Address
def isValidIpAddress(server, ipv6=False, **kwargs):
    """

    :param server:
    :param ipv6:
    :param kwargs:
    :return:
    """
    if server:
        return is_valid_ipv4_address(server)
    if ipv6:
        return is_valid_ipv6_address(server)


def isValidHostName(server):
    """

    :param server:
    :return:
    """
    try:
        # Docker Support
        if server.startswith('application'):
            return True

        # This is a very long hostname
        if len(server) > 255:
            return False
        if server[-1] == ".":
            server = server[:-1]  # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in server.split("."))
    except Exception, exception:
        import sys
        exc_type, exc_value, exc_traceback = sys.exc_info()
        import traceback, os.path
        top = traceback.extract_stack()[-1]
        print ', '.join([type(exception).__name__, os.path.basename(top[0]), str(top[1])])
        return False


def is_json(myjson):
    """

    :param myjson:
    :return:
    """

    try:
        json.loads(myjson)
    except ValueError, e:
        return False

    return True


def isAlphaNumeric(param, checkLength=False):
    """

    :param param:
    :param checkLength:
    :return:
    """
    if len(param) > 0 and param != None and param.isalnum():
        if checkLength:
            if len(param) < 255:
                return True

        else:
            return True

    return False


def isValidString(param, checkLength=False):
    """

    :param param:
    :param checkLength:
    :return:
    """
    if len(param) > 0 and param != None and isinstance(param, (str, unicode)):
        if checkLength:
            if len(param) < 64:
                return True

        else:
            return True

    return False


def isValidTarget(server):
    """

    :param server:
    :return:
    """
    if isIP(server):
        return isValidIpAddress(server)
    else:
        return isValidHostName(server)


def check_authentication_parameters(server=None,
                                    username=None,
                                    password=None,
                                    sshPort=None,
                                    httpsPort=None,
                                    vncPort=None,
                                    vncPassword=None,
                                    sshTimeout=10,
                                    debug=False, **kwargs):
    """


    :rtype : object
    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param httpsPort:
    :param vncPort:
    :param sshTimeout:
    :param kwargs:
    :return:
    """

    if debug: print 'utils.check_authentication_parameters()'

    # Server is not defined, check default parameters
    if server:
        try:
            if debug: print 'utils.check_authentication_parameters() Checking server: ' + server
            if server != '' and isinstance(server, (str, unicode)):  # String type
                if isIP(server):
                    pass
                else:
                    if isValidHostName(server):
                        pass
                    else:
                        raise exceptions.InvalidHostName('Invalid server hostname parameter')
            else:
                raise exceptions.InvalidHostName('Invalid server parameter')

        except Exception, e:
            print e
            raise

    else:  # Server is undefined. Validate it
        pass

    # ###### Username

    if username:
        try:
            if debug: print 'utils.check_authentication_parameters() Checking username: ' + username
            if username != '' and isinstance(username, (str, unicode)):
                if 1 < len(username) < 255:
                    pass
                else:
                    raise exceptions.InvalidUserName('Invalid username length')
            else:
                raise exceptions.InvalidUserName('Invalid username')
        except Exception, e:
            print e
            raise

    else:  # Username is undefined. Validate it
        pass

    # ######## Password

    if password:
        try:
            hash_object = hashlib.md5(password)
            if debug: print 'utils.check_authentication_parameters() Checking password: ' + hash_object.hexdigest()
            if password != '' and isinstance(password, (str, unicode)):
                if 1 < len(password) < 255:
                    pass
                else:
                    raise exceptions.InvalidPassword('Invalid password length')
            else:
                raise exceptions.InvalidPassword('Invalid password type')

        except Exception, e:
            print e
            raise

    else:  # Password is not defined. Skip it
        pass

    # SSH port is not defined, we will use default value
    if sshPort:
        ## SSH port is defined need validation
        try:
            if debug:
                print 'utils.check_authentication_parameters() Checking sshPort'.format(sshPort)
            sshPort = int(sshPort)
            if isinstance(sshPort, int):  # Prevent exception for isstance
                if 22 <= sshPort < 65535:
                    pass
                else:
                    raise exceptions.InvalidPort('Invalid ssh port range')

        except Exception:
            raise exceptions.InvalidPort('Invalid ssh port')
    else:
        pass

    # HTTPS port is not defined, we will use default value
    if httpsPort:
        try:
            if debug: print 'utils.check_authentication_parameters() Checking httpsPort (' + httpsPort + ')'
            httpsPort = int(httpsPort)
            if isinstance(int(httpsPort), int):
                if 80 <= httpsPort < 65535:
                    pass
                else:
                    raise exceptions.InvalidPort('Invalid httpsPort port range')

        except Exception:
            raise exceptions.InvalidPort('Invalid httpsPort port')
    else:
        pass

    # VNC port is not defined, we will use default value
    if vncPort:
        try:
            if debug:
                print 'utils.check_authentication_parameters() Checking vncPort: {0}'.format(vncPort)
            vncPort = int(vncPort)
            if isinstance(vncPort, int):
                if 5900 <= vncPort <= 65535:
                    pass
                else:
                    raise exceptions.InvalidPort('Invalid vncPort port range >=5900 && <65535')
            else:
                raise exceptions.InvalidPort('Invalid vncPort port range >=5900 && <65535')
        except Exception:
            raise exceptions.InvalidPort('Invalid vncPort port')
    else:
        # Not requested
        pass

    if vncPassword:
        try:
            if debug: print 'utils.check_authentication_parameters() Checking vncPassword'
            if vncPassword != '' and isinstance(vncPassword, (str, unicode)):
                if 1 < len(vncPassword) < 255:
                    pass
                else:
                    raise exceptions.InvalidPassword('Invalid password type')
            else:
                raise exceptions.InvalidPassword('Invalid vncPassword')

        except Exception:
            raise exceptions.InvalidPassword('Exception Invalid vncPassword')
    else:  # Server is defined. Validate it
        pass

    # timeout
    if sshTimeout:
        try:
            if debug:
                print 'utils.check_authentication_parameters() sshTimeout value (' + str(sshTimeout) + ')'
            sshTimeout = int(sshTimeout)
            if isinstance(sshTimeout, int):
                if 0 < sshTimeout < 60:
                    pass
                else:
                    raise exceptions.InvalidParameter('Invalid sshTimeout value')
            else:
                raise exceptions.InvalidParameter('Exception Invalid sshTimeout value')

        except Exception:
            raise exceptions.InvalidParameter('Exception Invalid sshTimeout port')
    else:
        # Use default 5900/None
        pass

    # If all is good returns True
    return True


def check_vnc_port(ports):
    """

    :param ports:
    :return:
    """
    try:
        if ports:
            vnc_ports = ports.split(':')
        else:
            return -1
        if len(vnc_ports) == 1:
            if isinstance(int(vnc_ports[0]), numbers.Integral):
                return 1
        if len(vnc_ports) == 2:
            if isinstance(int(vnc_ports[0]), numbers.Integral) and isinstance(int(vnc_ports[1]), numbers.Integral):
                return 2
        return -1
    except Exception, e:
        print e
        return -1


def isValidPort(port):
    """

    :param port:
    :return:
    """
    try:
        port = int(port)
        if port and isinstance(port, int):  # Prevent exception for isstance
            if 0 < port < 65535:
                return True
            else:
                raise exceptions.InvalidPort('Invalid port range')
        return False

    except Exception:
        raise exceptions.InvalidPort('Invalid  port ')
