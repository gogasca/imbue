import os
import imghdr
from utils import helper
import logging
from conf import settings
from conf import logging_conf
from catalogue import Version

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def sequence_files(job_path):
    """
        To determine if the files are already in sequence
    :param path:
    :return:
    """
    try:
        if not job_path:
            return -1

        file_count = 0
        file_num = 1
        files = os.listdir(job_path)
        files.sort()

        for name in files:
            filename = os.path.join(job_path, name)
            if os.path.isfile(filename):
                if imghdr.what(filename) == 'png':
                    file_count += 1

        log.info('sequence_files() Image files total: {}'.format(file_count))
        if file_count < 1:
            log.warn('sequence_files() Image files are not available')
            return -1

        for file in files:
            filename = os.path.join(job_path, file)
            if os.path.isfile(filename):
                if imghdr.what(filename) == 'png':
                    if 99 < file_num:
                        newfile = 'screen_0' + str(file_num) + '.png'
                    elif 9 < file_num < 99:
                        newfile = 'screen_00' + str(file_num) + '.png'
                    else:
                        newfile = 'screen_000' + str(file_num) + '.png'
                    if file == newfile:
                        file_num += 1
        if (file_num - 1) == file_count:
            log.info('sequence_files() Files are in sequence')
            return 0
        else:
            log.warn('sequence_files() Files are NOT in sequence')
            return 1
    except Exception, exception:
        log.exception('sequence_files() In except of sequence_files() {} '.format(exception))


def get_iso_file(configuration, params):
    """

    :param configuration:
    :return:
    """
    try:

        if configuration['instance']['application'].has_key('bootable_iso'):
            iso_file = configuration['instance']['application']['bootable_iso']
        else:
            iso_file = Version.get_default_iso(settings.cisco_list[params['app_id']],
                                               configuration['instance']['application']['version'])

        return iso_file

    except Exception, exception:
        log.exception(exception)
