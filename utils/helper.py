__author__ = 'gogasca'

import logging

from datetime import datetime

import validator
from alarms import send_sms
from catalogue.cisco.common.xml import xml_processor
from conf import logging_conf
from conf import settings
from database import Db
from database import database_session
from tools import Tools
from utils.security import encrypt

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def update_job(job_reference, error=False, description='', end=False, test=False, **kwargs):
    """

    :param job_reference:
    :param error:
    :param description:
    :param end:
    :param kwargs:
    :return:
    """
    if job_reference:
        if error:
            if not description:
                description = 'Job failed'
            if end:
                sqlquery = """UPDATE job SET status='-1',description='""" + description + """',job_end='now()' WHERE job.reference='""" + \
                           str(job_reference) + "'"
            else:
                sqlquery = """UPDATE job SET status='-1',description='""" + description + """' WHERE job.reference='""" + \
                           str(job_reference) + "'"
        else:
            if not description:
                description = 'Job failed'
            if end:
                sqlquery = """UPDATE job SET status='0',description='""" + description + """' WHERE job.reference='""" + \
                           str(job_reference) + "'"
            else:
                sqlquery = """UPDATE job SET status='0',description='""" + description + """',job_end='now()' WHERE job.reference='""" + \
                           str(job_reference) + "'"
        update_database(sqlquery)

        if test:
            sqlquery = """UPDATE job SET is_test=true WHERE job.reference='""" + str(job_reference) + "'"
            update_database(sqlquery)
        return True


def get_user_information(field='cellphone', owner_id=None):
    """

    :param field:
    :param owner_id:
    :return:
    """
    if owner_id:
        sqlquery = "SELECT " + field + " FROM auth_user WHERE id=" + str(owner_id) + " AND enable_alerts=true"
        result = execute_query(sqlquery)
        if result:
            if result[0][0]:
                return result[0][0]

    return


def update_cluster_xml_single(pub_config, filename):
    """

    :param pub_config:
    :param filename:
    :return:
    """

    if 'hostname' in pub_config:
        xml_processor.update_cluster_hostname(filename, True, pub_config['hostname'])
    else:
        return False

    if 'ip' in pub_config:
        xml_processor.update_cluster_ip(filename, True, pub_config['ip'])
    else:
        return False

    return True


def update_cluster_xml(pub_config, sub_config, filename):
    """

    :param pub_config:
    :param sub_config:
    :param filename:
    :return:
    """

    if 'hostname' in pub_config and 'hostname' in sub_config:
        xml_processor.update_cluster_hostname(filename, True, pub_config['hostname'])
        xml_processor.update_cluster_hostname(filename, False, sub_config['hostname'])
    else:
        return False

    if 'ip' in pub_config and 'ip' in sub_config:
        xml_processor.update_cluster_ip(filename, True, pub_config['ip'])
        xml_processor.update_cluster_ip(filename, False, sub_config['ip'])
    else:
        return False

    return True


def add_publisher_information(configuration, params):
    """
    Update subscriber answer file information
    :param configuration:
    :param params:
    :return:
    """
    try:
        if 'hostname' in configuration:
            xml_processor.set_param_value(params['answer_file'], "CcmDBHost",
                                          configuration['hostname'])

            xml_processor.set_param_value(params['answer_file'], "IPSecMasterHost",
                                          configuration['hostname'])

            xml_processor.set_param_value(params['answer_file'], "NTPServerHost",
                                          configuration['hostname'])
        else:
            return False
        if 'ip' in configuration:
            xml_processor.set_param_value(params['answer_file'], "CcmDBIpAddr",
                                          configuration['ip'])
            xml_processor.set_param_value(params['answer_file'], "IPSecMasterIpAddr",
                                          configuration['ip'])
            xml_processor.set_param_value(params['answer_file'], "NTPServerIpAddr",
                                          configuration['ip'])
        else:
            return False

        return True

    except Exception, exception:
        print exception


def update_xml(configuration, params):
    """
    Update answer file parameters
    :param configuration:
    :param params:
    :return:
    """
    try:

        if 'hostname' in configuration:
            xml_processor.set_param_value(params['answer_file'], "LocalHostName",
                                          configuration['hostname'])

        if 'ip' in configuration:
            xml_processor.set_param_value(params['answer_file'], "LocalHostIP0",
                                          configuration['ip'])

        if 'mask' in configuration:
            xml_processor.set_param_value(params['answer_file'], "LocalHostMask0",
                                          configuration['mask'])

        if 'gw' in configuration:
            xml_processor.set_param_value(params['answer_file'], "LocalHostGW0",
                                          configuration['gw'])

        if 'admin_username' in configuration:
            xml_processor.set_param_value(params['answer_file'], "LocalHostAdminName",
                                          configuration['admin_username'])

        if 'admin_password' in configuration:
            encrypted_password = encrypt.encrypt(configuration['admin_password'], settings.url_encryption)
            if encrypted_password:
                xml_processor.set_param_value(params['answer_file'], "LocalHostAdminPwCrypt",
                                              encrypted_password)
            else:
                log.error('update_xml() admin_password failed')
                return False

        if 'app_username' in configuration:
            xml_processor.set_param_value(params['answer_file'], "ApplUsername",
                                          configuration['app_username'])

        if 'app_password' in configuration:
            encrypted_password = encrypt.encrypt(configuration['app_password'], settings.url_encryption)
            if encrypted_password:
                xml_processor.set_param_value(params['answer_file'], "ApplUserPwCrypt",
                                              encrypted_password)
            else:
                log.error('update_xml() app_password failed')
                return False

        if 'security_password' in configuration:
            encrypted_password = encrypt.encrypt(configuration['security_password'], settings.url_encryption)
            if encrypted_password:
                xml_processor.set_param_value(params['answer_file'], "SftpPwCrypt",
                                              encrypted_password)
                xml_processor.set_param_value(params['answer_file'], "IPSecSecurityPwCrypt",
                                              encrypted_password)
            else:
                log.error('update_xml() security_password failed')
                return False

        # SMTPHostName
        if 'smtp' in configuration:
            if 'location' in configuration['smtp']:
                log.info('update_xml() Updating SMTP')
                xml_processor.insert_element(params['answer_file'],
                                             "SMTPHostName",
                                             "SMTP location for this node",
                                             'smtp.host',
                                             configuration['smtp']['location'])
            else:
                log.error('update_xml() Missing SMTP location')
        # MTU size
        if 'other' in configuration:
            if 'mtu' in configuration['other']:
                log.info('update_xml() Updating MTU')
                xml_processor.insert_element(params['answer_file'],
                                             "LocalHostMTUSize",
                                             "MTU Size",
                                             '1500',
                                             str(configuration['other']['mtu']))
                xml_processor.insert_element(params['answer_file'],
                                             "ChangeMTUSize",
                                             "MTU Size changed from OS default",
                                             'no',
                                             'yes')
        # NTP
        if 'time' in configuration:
            log.info('update_xml() Updating NTP')
            # LocalHostTimezone
            # LocalHostContinent
            # LocalHostCity
            if 'tz' in configuration['time']:
                xml_processor.set_param_value(params['answer_file'],
                                              "LocalHostTimezone",
                                              configuration['time']['tz'])
            if 'tz_continent' in configuration['time']:
                xml_processor.set_param_value(params['answer_file'],
                                              "LocalHostContinent",
                                              configuration['time']['tz_continent'])
            if 'tz_city' in configuration['time']:
                xml_processor.set_param_value(params['answer_file'],
                                              "LocalHostCity",
                                              configuration['time']['tz_city'])

            if 'ntp_1' in configuration['time']:
                xml_processor.set_param_value(params['answer_file'],
                                              "NtpServer",
                                              configuration['time']['ntp_1'])
            if 'ntp_2' in configuration['time']:
                xml_processor.insert_element(params['answer_file'],
                                             "NtpServer",
                                             "Address Range for NTP server",
                                             'None',
                                             configuration['time']['ntp_2'])
            if 'ntp_3' in configuration['time']:
                xml_processor.insert_element(params['answer_file'],
                                             "NtpServer",
                                             "Address Range for NTP server",
                                             'None',
                                             configuration['time']['ntp_3'])
        # DNS
        if 'dns' in configuration:
            log.info('update_xml() Updating DNS')
            if all(k in configuration['dns'] for k in ("dns_1", "domain_name")):
                if 'dns_1' in configuration['dns']:
                    xml_processor.insert_element(params['answer_file'],
                                                 "LocalHostDnsPrimary",
                                                 "Primary DNS server IP address",
                                                 '0.0.0.0',
                                                 configuration['dns']['dns_1'])
                if 'domain_name' in configuration['dns']:
                    xml_processor.insert_element(params['answer_file'],
                                                 "LocalHostDomain",
                                                 "Domain name for this machine",
                                                 'cisco.com',
                                                 configuration['dns']['domain_name'])
            else:
                log.error('update_xml() dns failed')
                return False

            if 'dns_2' in configuration['dns']:
                xml_processor.insert_element(params['answer_file'],
                                             "LocalHostDnsSecondary",
                                             "Secondary DNS server IP address",
                                             '0.0.0.0',
                                             configuration['dns']['dns_2'])
        # Certificate
        if 'certificate' in configuration:
            log.info('update_xml() Updating Certificate')
            if all(k in configuration['certificate'] for k in
                   ("cert_org", "cert_unit", "cert_location", "cert_state", "cert_country")):
                parameters = ['Org', 'Unit', 'Location', 'State', 'Country']
                parameters_values = list()
                parameters_values.append(configuration['certificate']['cert_org'])
                parameters_values.append(configuration['certificate']['cert_unit'])
                parameters_values.append(configuration['certificate']['cert_location'])
                parameters_values.append(configuration['certificate']['cert_state'])
                parameters_values.append(configuration['certificate']['cert_country'])
                xml_processor.set_param_value_subtree(params['answer_file'],
                                                      parameters=parameters,
                                                      parameters_value=parameters_values,
                                                      subtree='CertX509')
            else:
                log.error('update_xml() certificate failed')
                return False

        if params['app_id'] == 200003:
            if 'impdomain' in configuration:
                xml_processor.set_param_value(params['answer_file'], "ImpDomainName", configuration['impdomain'])

        return True

    except Exception, exception:
        print exception


def generate_alert(job_id, description, job):
    """

    :param job_id:
    :param description:
    :param job:
    :return:
    """
    try:
        send_sms.send_sms_alert(body=description + ' ' + job)
        sqlquery = """UPDATE job SET status=-1,description='""" + description + """' WHERE job.id=""" + str(job_id)
        db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
        db.execute(sqlquery)
    except Exception, exception:
        print exception


def get_tools_instance(tools_name, discover=False):
    """

    :param tools_name:
    :param discover:
    :return:
    """

    try:
        if validator.isValidHostName(tools_name):
            # =========================================================
            # Tools server
            # =========================================================

            tools_instance = Tools.Tools(name=tools_name)
            tools_instance.initialize(name=tools_name)

            if discover:
                if tools_instance.discover():
                    return tools_instance
            else:
                return tools_instance

        return

    except Exception, exception:
        print exception


def clean_up(host_id, working_directory):
    """

    :param host_id:
    :param working_directory:
    :return:
    """
    esx = get_server_instance(host_id)
    if esx:
        if esx.delete_working_directory(remote_directory=working_directory):
            return True

    return False


def process_host_information(host_information):
    """
    Process host information for 1 server
    :param host_information:
    :return:
    """
    host_info = []

    if host_information.keys() and host_information.values():
        host_info.append(host_information.keys()[0])
        if host_information.values()[0]:
            host_info.append(host_information.values()[0][0])

    return host_info


def process_cluster_host_information(host_information):
    """
    Process host information for cluster

    {}
    {u'1': []}
    {u'1': [10]}
    {u'1': [10, 20]}
    {u'1': [10], u'2': []}
    {u'1': [10], u'2': [20]}

    :param host_information:
    :return:
    """
    host_info = []

    if len(host_information) == 1:  # {u'1': []} | {u'1': [10]} | {"1": [10, 20]}

        if host_information.keys() and host_information.values():
            host_info.append(host_information.keys()[0])
            if host_information.values()[0]:
                if len(host_information.values()[0]) == 1:
                    host_info.append(host_information.values()[0][0])
                if len(host_information.values()[0]) == 2:  # {"1": [10, 20]} = {u'1': [10], u'1': [20]} Fix Issue 215
                    host_info.append(host_information.values()[0][0])
                    host_info.append(host_information.keys()[0])
                    host_info.append(host_information.values()[0][1])

    if len(host_information) == 2:  # {u'1': [10], u'2': []} | {u'1': [10], u'2': [20]}

        if host_information.keys() and host_information.values():
            if host_information.values()[0]:
                host_info.append(host_information.keys()[0])
                host_info.append(host_information.values()[0][0])
            if host_information.values()[1]:
                host_info.append(host_information.keys()[1])
                host_info.append(host_information.values()[1][0])

    return host_info


def get_job_host_information(job_reference):
    """

    :param job_reference:
    :return:
    """

    try:
        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        db_instance.initialize()

        # =========================================================
        # Initialize job in database
        # =========================================================

        host_info = None
        if job_reference:
            sqlquery = """SELECT host_information FROM job WHERE job.reference='""" + job_reference + "'"
            host_info = db_instance.query(sqlquery)
            if host_info:
                host_info = str(list(host_info[0])[0])

        return host_info

    except Exception, exception:
        print exception


def get_owner_id(configuration=None, host_id=None, **kwargs):
    """

    :param configuration:
    :param host_id:
    :param kwargs:
    :return:
    """

    try:
        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        # =========================================================
        # Initialize job in database
        # =========================================================

        db_instance.initialize()  # Initialize Database
        if host_id:
            sqlquery = """SELECT owner_id FROM server WHERE server.id=""" + str(host_id)
            owner_id = db_instance.query(sqlquery)
            if owner_id:
                owner_id = int(list(owner_id[0])[0])
                return owner_id

        if configuration['instance']['host']['id']:
            sqlquery = """SELECT owner_id FROM server WHERE server.id=""" + \
                       str(configuration['instance']['host']['id']) + ""

            owner_id = db_instance.query(sqlquery)
            if owner_id:
                owner_id = int(list(owner_id[0])[0])
                return owner_id

            return

    except Exception, exception:
        print exception


def get_server_instance(host_id=None):
    """

    :param host_id:
    :return:
    """

    try:
        from server import Server

        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        # =========================================================
        # Initialize job in database
        # =========================================================

        db_instance.initialize()  # Initialize Database
        if host_id:
            sqlquery = """SELECT name FROM server WHERE server.id=""" + \
                       str(host_id) + ""

            esx_name = db_instance.query(sqlquery)
            if esx_name:
                esx_name = str(list(esx_name[0])[0])
                if validator.isValidHostName(esx_name):
                    # =========================================================
                    # ESXi server
                    # =========================================================
                    esx_instance = Server.Server(hostname=esx_name)
                    if esx_instance:
                        owner_id = get_owner_id(host_id=host_id)
                        esx_instance.initialize(name=esx_name, owner_id=owner_id)
                        return esx_instance
            return
    except Exception, exception:
        print exception


def execute_query(sqlquery):
    """

    :param sqlquery:
    :return:
    """

    try:
        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        db_instance.initialize()
        return db_instance.query(sqlquery)

    except Exception, exception:
        print exception


def update_database(sqlquery):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
        db.execute(sqlquery)
    except Exception, exception:
        print exception


def print_time():
    """
    :return:
    """
    return datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
