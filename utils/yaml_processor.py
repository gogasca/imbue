__author__ = 'gogasca'

class YamlProcessor(object):
    """

    """
    def __init__(self,f):
        self.filename = f

    def read_file(self,pattern):
        import yaml
        with open(self.filename, 'r') as f:
            doc = yaml.load(f)
        try:
            import pprint
            elements = []
            for element in doc[pattern]:
                elements.append(element)
            return elements
        except KeyError:
            print 'Invalid key'
            return None
        except Exception,e:
            print str(e)

