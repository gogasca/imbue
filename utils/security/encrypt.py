import logging
from conf import logging_conf
from selenium import webdriver
from bs4 import BeautifulSoup
from utils.api import api_tools

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def encrypt(password, url):
    """

    :param password:
    :param url:
    :return:
    """
    if not len(password) < 32:
        log.error('encrypt() Invalid password length')
        return
    try:
        if url:
            url = url + password
            driver = webdriver.PhantomJS(executable_path='/usr/bin/phantomjs', port=8084)
            if api_tools.get_status_code(url) != 200:
                log.exception('encrypt() API encryption is down')
                return
            driver.get(url)
            source_code = driver.page_source
            soup = BeautifulSoup(source_code, "html5lib")
            body = soup.body.string
            log.error('encrypt() New password: ' + str(body))
            return body
        else:
            return
    except AttributeError:
        pass
    except Exception, e:
        log.exception('encrypt() Exception found ' + e.message)
