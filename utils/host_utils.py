from utils import db_utils
import copy
import json


def get_install_information(configuration, job_reference, type):
    """

    :param configuration:
    :param job:
    :param type:
    :return:
    """
    try:
        # =========================================================
        # Check UC instance
        # =========================================================

        if type == 1:
            servers = list()
            server_object = dict()
            host_ = configuration['instance']['host']['id']
            vm_ = configuration['instance']['application']['params']['hostname']
            sqlquery = """SELECT name FROM server WHERE server.id=""" + str(host_)
            server_name = db_utils.execute_query(sqlquery)
            if server_name and len(server_name) == 1:
                server_name = (server_name[0])[0]
            else:
                return False

            if server_name and vm_:
                server_object["server"] = server_name
                server_object["type"] = configuration['instance']['application']['type']
                # Verify version field exists
                if 'version' in configuration['instance']['application']:
                    server_object["version"] = configuration['instance']['application']['version']
                # VCS, TPS, Conductor
                if 'ova_file' in configuration['instance']['application']:
                    server_object["ova_file"] = configuration['instance']['application']['ova_file']
                server_object["hostname"] = vm_
                server_object["ip"] = configuration['instance']['application']['params']['ip']
                servers.append(server_object)
                server_info = json.dumps(servers)
            sqlquery = """UPDATE job SET install_information='""" + str(
                server_info) + "' WHERE job.reference='" + job_reference + "'"
            db_utils.update_database(sqlquery)
            return True

        # =========================================================
        # Cluster installation
        # =========================================================

        if type == 2:
            servers = list()
            cluster_configuration = copy.deepcopy(configuration)
            for x in xrange(0, len(cluster_configuration)):
                configuration = cluster_configuration[x]
                server_object = dict()
                host_ = configuration['instance']['host']['id']
                vm_ = configuration['instance']['application']['params']['hostname']
                sqlquery = """SELECT name FROM server WHERE server.id=""" + str(host_)
                server_name = db_utils.execute_query(sqlquery)
                if server_name and len(server_name) == 1:
                    server_name = (server_name[0])[0]
                else:
                    return False

                if server_name and vm_:
                    server_object["server"] = server_name
                    server_object["type"] = configuration['instance']['application']['type']
                    # Verify version field exists
                    if 'version' in configuration['instance']['application']:
                        server_object["version"] = configuration['instance']['application']['version']
                    # VCS, TPS, Conductor
                    if 'ova_file' in configuration['instance']['application']:
                        server_object["ova_file"] = configuration['instance']['application']['ova_file']

                    server_object["hostname"] = vm_
                    server_object["ip"] = configuration['instance']['application']['params']['ip']
                    servers.append(server_object)

            server_info = json.dumps(servers)
            sqlquery = """UPDATE job SET install_information='""" + str(
                server_info) + "' WHERE job.reference='" + job_reference + "'"
            db_utils.update_database(sqlquery)
            return True

        return False

    except Exception:
        return False
