#!/bin/bash
#
# Program: Deploy new code
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3
# sh commit.sh 'Development' 'Tools update'
#echo "$# arguments"
if [ "$#" -gt 2 ]; then
	echo "imbue() Illegal number of parameters. Usage: sh commit.sh 'Tools update' 'branchname'"
	exit 1
fi
CHECK_API=false
RUN_TESTS=false
REMOVE_CACHE=true
LOG_DIRECTORY="log/jobs/"       # Jobs directory

read -p "Are you sure you wish to commit new code in git Repository? Please enter 'commit' to continue? "
if [ "$REPLY" != "commit" ]; then
   exit 0
fi

safeRunCommand() {
   cmnd="$@"                    #...insure whitespace passed and preserved
   $cmnd
   if [ $? != 0 ]; then
      printf "Error when executing tests: '$1'"
      exit $ERROR_CODE
   fi

}

if [ "$RUN_TESTS" = true ]; then
    echo 'imbue() Running test cases'
    command="nosetests test/"
    safeRunCommand "$command"
fi


if [ "$CHECK_API" = true ]; then
    echo 'imbue() Checking API'
    command="sh test/test_http.sh"
    safeRunCommand "$command"
fi

if [ "$REMOVE_CACHE" = true ]; then
    echo 'imbue() Removing cached files...'
    # Remove log files
    find . -type f -name '*.pyc' -delete
    find . -name '*.DS_Store' -type f -delete
    find . -name '*.err' -type f -delete
    rm -rf log/imbue*.log

    echo 'imbue() Deleting log directories'
    # Control will enter here if $DIRECTORY doesn't exist.
    rm -rf $LOG_DIRECTORY
fi


if [ ! -d "$LOG_DIRECTORY" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  mkdir -p $LOG_DIRECTORY
  echo 'imbue() Created logs directory'
fi

echo 'imbue() Processing to connect to git repo'
echo 'imbue() Pushing latest code to remote repository...'

git status
echo 'imbue() Adding new files:'
git add . -A
echo 'imbue() Commiting changes: '$1
commit_message="$1"
git commit -m "$commit_message"
echo 'imbue() Pushing latest code:'
git push --set-upstream origin $2
echo 'imbue() Code uploaded successfully'
