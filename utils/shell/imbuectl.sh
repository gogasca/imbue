#!/bin/bash
#
# Program: Restart services
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3

echo 'imbue() manage services'
if [ "$#" -gt 1 ]; then
	echo "Illegal number of parameters. Usage: sh $0 'start|stop|restart'"
	exit 1
fi

safeRunCommand() {
   cmnd="$@"                    #...insure whitespace passed and preserved
   $cmnd
   if [ $? != 0 ]; then
      printf "Error when executing command: '$1'"
      exit $ERROR_CODE
   fi

}

if [ "$1" = 'restart' ]; then
      echo 'Restarting imbue services...'

      command="cd /etc/supervisor/; supervisorctl restart all"
      safeRunCommand "$command"

elif [ "$1" = "start" ]; then
      echo 'Starting imbue services...'
      command="cd /etc/supervisor/; supervisorctl start all"
      safeRunCommand "$command"

elif [ "$1" = "stop" ]; then
      echo 'Stopping imbue services...'
      command="cd /etc/supervisor/; supervisorctl stop all"
      safeRunCommand "$command"
else
   echo 'Invalid command'
fi


