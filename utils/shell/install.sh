#!/usr/bin/env bash

apt-get update; sudo apt-get upgrade -y; sudo apt-get dist-upgrade -y; sudo apt-get install -y build-essential git-core
apt-get install python-dev -y python-pip libjpeg-dev zlib1g-dev tesseract-ocr libffi-dev libpq-dev imagemagick supervisor -y
apt-get install postgresql-client-9.3 -y
apt-get install curl -y
apt-get install lighttpd -y
apt-get install php5-common php5-cgi php5 -y
lighty-enable-mod fastcgi-php
service lighttpd force-reload

mkdir -p /usr/local/src/imbue/application/
mkdir -p /usr/local/src/imbue/external/customers/reports/
cd /usr/local/src/imbue/application/
git clone https://gogasca@bitbucket.org/gogasca/imbue.git

cd imbue
pip install -r requirements.txt
pip install --upgrade pyvmomi
pip install vncdotool
pip install coverage
pip install -I Pillow

crontab -e
