#!/usr/bin/env bash
for pid in $(ps -ef | grep "imbue" | awk '{print $2}'); do kill -9 $pid; done
for pid in $(ps -ef | grep "celery" | awk '{print $2}'); do kill -9 $pid; done