#!/bin/bash
# Rename Screen files

if [ "$#" -ne 1 ]; then
	echo "imbue() Illegal number of parameters. Usage: sh rename_screen.sh /your/directory/"
	exit 1
fi

DIRECTORY="$1"
echo 'imbue() Will rename screens'

function directory_exists {
	if [ -d $1 ]; then
  		# Control will enter here if $DIRECTORY exists.
		echo 1
	else
		echo -1
	fi
}

DIRECTORY_EXISTS=$(directory_exists $DIRECTORY)
if [ $DIRECTORY_EXISTS -ne 1 ]; then
        echo "Directory $DIRECTORY doesnt exist"
        exit 1
fi

command=`ls -tr $DIRECTORY/screen*`

n=1
for f in $command; do
  mv $f $f$n.png
  n=$(( n + 1 ))
done
echo 'imbue() Screens renamed successfully'
