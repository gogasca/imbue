#!/bin/bash
#
# Program: Create Floppy Drive
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3
FLOPPY_SIZE=1440
# A POSIX variable
OPTIND=1 # Reset in case getopts has been used previously in the shell.

# Show User help
function show_help {
        echo "create_floppy -d directory -f filename -p platformConfig.xml"
        return 1
}


#echo "$# arguments"
if [ "$#" -lt 6 ] || [ "$#" -gt 8 ]; then
	echo "Illegal number of parameters:"
	echo $#
	show_help;
	exit 1
fi


# Initialize our own variables:
DIRECTORY=""
FLOPPY_IMAGE=""
CLUSTER_FILE=""
CLUSTER=0
ANSWER_FILE=""
MOUNT_DIRECTORY="/media/floppy1"
VERBOSE=0
ENABLE_CHECK=1

# Check Error
function check {
	ERROR_CODE=$(echo "$?")
    	if [ $ERROR_CODE -ne 0 ]; then
        	 if [ $ENABLE_CHECK -eq 1 ]; then
	        	 echo "Error ($ERROR_CODE) Fatal Exception occurred"
		         #TODO Pass filename
    		     	 #TODO Generate log file
	         	exit 1
	         fi
    	fi
     return 1
}


function file_exists {
	if [ -f $1 ]; then
		#echo "$1 file was found."
		echo 1
	else
		#echo "$1 was not found."
		echo -1
	fi
}

function directory_exists {
	if [ -d $1 ]; then
  		# Control will enter here if $DIRECTORY exists.
		echo 1
	else
		echo -1
	fi
}

while getopts "h?:v::d:p:c:f:" opt; do
    case "$opt" in
    \?|h )
        show_help;
        exit 0
        ;;
    v )  VERBOSE=1
        ;;
    d )  #echo "-d was triggered, Parameter: $OPTARG" >&2
	DIRECTORY=$OPTARG
	;;
    p )  #echo "-p was triggered, Parameter: $OPTARG" >&2
	ANSWER_FILE=$OPTARG
	;;
    c )  #echo "-c was triggered, Parameter:
	set -f # disable glob
	IFS=,
	array=($OPTARG)
	echo "Number of cluster arguments: ${#array[@]}"
	if [ ${#array[@]} -ne 2 ]; then
		echo "Illegal number of XML parameters:"
    		show_help;
	    	exit 1
    	fi
	echo -n "XML Arguments are:"
	for i in "${array[@]}"; do
	  echo -n " ${i}"
	done
	echo
	ANSWER_FILE=${array[@]:0:1}
	echo $ANSWER_FILE
    	CLUSTER_FILE=${array[@]:1}
	echo $CLUSTER_FILE
	CLUSTER=1
	echo $CLUSTER
	;;
    f )  #echo "-f was triggered, Parameter: $OPTARG" >&2
	FLOPPY_IMAGE=$OPTARG
        ;;
    : )
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
   esac
done

# Floppy directory
DIRECTORY_EXISTS=$(directory_exists $DIRECTORY)
if [ $DIRECTORY_EXISTS -ne 1 ]; then
        echo "Directory $DIRECTORY doesnt exist"
        exit 1
fi

ANSWER_FILE_EXISTS=$(file_exists $ANSWER_FILE)
if [ $ANSWER_FILE_EXISTS -ne 1 ]; then
        echo "Answer file: $ANSWER_FILE doesnt exist"
	exit 1
fi

if [ $CLUSTER -eq 1 ]; then
    CLUSTER_FILE_EXISTS=$(file_exists $CLUSTER_FILE)
    if [ $CLUSTER_FILE_EXISTS -ne 1 ]; then
            echo "Cluster file: $CLUSTER_FILE doesnt exist"
        exit 1
    fi
fi

FILE_EXISTS=$(file_exists $DIRECTORY'/'$FLOPPY_IMAGE)
if [ $FILE_EXISTS -ne 1 ]; then
    sudo /sbin/mkfs.msdos -C $DIRECTORY'/'$FLOPPY_IMAGE $FLOPPY_SIZE
	check;
else
	echo "File: $FLOPPY_IMAGE already exists, will be re-created"
	mv $DIRECTORY'/'$FLOPPY_IMAGE $DIRECTORY'/'$FLOPPY_IMAGE'.bak'
	rm -rf $DIRECTORY'/'$FLOPPY_IMAGE
	sudo /sbin/mkfs.msdos -C $DIRECTORY'/'$FLOPPY_IMAGE $FLOPPY_SIZE
	#Validate floppy was created succesfully
	check;
fi

# Create floppy directory
MOUNT_DIRECTORY_EXISTS=$(directory_exists $MOUNT_DIRECTORY)
if [ $MOUNT_DIRECTORY_EXISTS -ne 1 ]; then
	echo "Creating mounting directory..."
	sudo mkdir $MOUNT_DIRECTORY
	check;
fi

# Mount Floppy image
sudo mount -o loop $DIRECTORY'/'$FLOPPY_IMAGE $MOUNT_DIRECTORY
check;
echo "Copying answer file..."
echo $CLUSTER
cp $ANSWER_FILE $MOUNT_DIRECTORY
if [ $CLUSTER -eq 1 ]; then
    echo "Copying cluster file..."
    cp $CLUSTER_FILE $MOUNT_DIRECTORY
fi
echo "Unmounting directory..."
umount $MOUNT_DIRECTORY
check;
echo "Floppy image created succesfully"