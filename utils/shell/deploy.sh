#!/bin/bash
#
# Program: Deploy new code
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee Inc 2016
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3
DOCKER=0
BRANCH='Development'

echo 'imbue() Deploying latest available version'
if [ "$#" -eq 1 ]; then
    if [ "$1" = 'docker' ]; then
        echo "imbue() Deploying Docker instance."
        DOCKER=1
    fi
fi

echo "Are you sure you wish to Deploy new code in IMBUE? Please enter 'deploy' to continue? "
IFS= read -r REPLY
if [ "$REPLY" != "deploy" ]; then
   exit 0
fi

echo 'imbue() Removing cached files'
# Remove log files
find . -type f -name '*.pyc' -delete
find . -name '*.DS_Store' -type f -delete
find . -name '*.err' -type f -delete
rm -rf log/*.log
rm -rf log/*.err

LOG_DIRECTORY="log/jobs/"

if [ ! -d "$LOG_DIRECTORY" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  mkdir -p $LOG_DIRECTORY
  echo 'imbue() Created logs directory'
fi

git branch
git status
git config --global user.email "gonzalo@parzee.com"
git config --global user.name "Gonzalo Gasca Meza"
git stash
echo 'imbue() Getting latest code from remote repository...'
git pull
sleep 2

safeRunCommand() {
   cmnd="$@"                    #...insure whitespace passed and preserved
   $cmnd
   if [ $? != 0 ]; then
      printf "Error when executing command: '$1'"
      exit $ERROR_CODE
   fi

}

if [ "$DOCKER" -eq 1 ]; then
     echo 'imbue() Code deployed successfully'
     echo "imbue() Now restart services in Docker instance itself"
     cp requirements.txt /usr/local/src/infrastructure/application/
     exit 1
fi

echo 'imbue() Updating libraries'
pip install -r requirements.txt -U
echo 'Restarting IMBUE services...'
cd /etc/supervisor/
command="supervisorctl restart all"
safeRunCommand "$command"
echo 'imbue() Code deployed successfully. Please update settings accordingly.'
