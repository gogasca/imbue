# -*- coding: utf-8 -*-
__author__ = 'gogasca'
import time
import sys, getopt, platform
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')
import logging
from conf import logging_conf, settings
from server import Server
log = logging_conf.LoggerManager().getLogger("__app___", logging_file=settings.tools_logfile)
log.setLevel(level=logging.DEBUG)
log.info(
            '---------------------------------------Initializing Parzee Tools---------------------------------------')

class IP_address(object):
    """docstring for IP_address"""

    def __init__(self, A_, B_, C_, D_):
        super(IP_address, self).__init__()
        self.A_ = A_
        self.B_ = B_
        self.C_ = C_
        self.D_ = D_

    def get(self):
        return str(self.A_) + '.' + str(self.B_) + '.' + str(self.C_) + '.' + str(self.D_)


def clear_database():
    from database import db_connection

    if db_connection.start():
        # psql -h <HOSTNAME> -d <DATABASE_NAME> -U <USERNAME> -W
        # \dt
        # \l
        print 'imbue_dump|DB|Database connection is successful'

        log.info('imbue_dump|DB|Database connection is successful')
        # TODO Delete database
    else:
        log.exception('imbue_dump|DB|Unable to connect to database')
        print 'imbue_dump|DB|Unable to connect to database'
        sys.exit(1)


def generate_ip(num_hosts=1, class_subnet='A', **kwargs):
    """

    :param num_hosts:
    :param class_subnet:
    :param kwargs:
    :return:
    """
    if num_hosts > 1 and num_hosts < 254:
        pass
    else:
        print 'Invalid hosts'
        return
    lista = range(1, num_hosts)

    ip_addresses = []
    for element in lista:
        if class_subnet == 'A':
            ip_addresses.append(IP_address(10, 10, 1, element))
        elif class_subnet == 'B':
            ip_addresses.append(IP_address(172, 16, 1, element))
        elif class_subnet == 'C':
            ip_addresses.append(IP_address(192, 168, 1, element))
        else:
            ip_addresses.append(IP_address(1, 1, 1, element))

    for ip in ip_addresses:
        print ip.get()

    return ip_addresses


def imbue_app(option=None, log_file=None, jobId=None, **kwargs):
    """
    IMBUE APP CLI application
    python -W ignore imbue_dump
    :return:
    """

    log = logging_conf.LoggerManager().getLogger("__app___", cli=True, logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '----------------------------------------Initializing IMBUEapp CLI----------------------------------------')
    # Check time
    start = time.time()
    esx_ip = ''

    if option.lower() == 'insert':
        manual_mode = True

        try:
            # ********* DATABASE Connection ************* #
            # Check if we read from Database or not
            from database import db_connection

            if db_connection.start():
                # psql -h <HOSTNAME> -d <DATABASE_NAME> -U <USERNAME> -W
                # \dt
                #\l
                print 'imbue_dump|DB|Database connection is successful'
                log.info('imbue_dump|DB|Database connection is successful')
            else:
                log.exception('imbue_dump|DB|Unable to connect to database')
                print 'imbue_dump|DB|Unable to connect to database'
                sys.exit(1)

            # *********** Generates IP Addresses ******************
            ip_addresses = generate_ip(num_hosts=200, class_subnet='C')

            log.info('imbue_dump|DB|ESX will be inserted')
            for esx_ip in ip_addresses:
                esx_name = 'esxi-' + esx_ip.get()
                if esx_name:
                    # *********** Initialize ESXi server. ESXi server ******************
                    # *********** Initialize server. Creates logical object and assign properties from database. ***********

                    esx1 = Server.Server(hostname=esx_name)
                    log.info('imbue_dump Inserting ESXi ' + esx_ip.get())

                    # # *********** Insert server into database
                    esx1.insert(ip_address=esx_ip.get(), interface_name='eth0', description='ESX server ',
                                properties='{"ssh": { "active": 1, "port": 22 }, "https": { "active": 1, "port": 443 }, "vnc": { "active": 1, "port":  5900 }}',
                                ssh_username='root', ssh_password='password')
                    print 'imbue_dump Inserting ESXi ' + esx_ip.get()
                else:
                    log.info('imbue_dump| Invalid ESXi server')
                    sys.exit(1)

            log.info('imbue_dump|DB|ESX initialization starting...')

        except Exception, e:
            log.exception(str(e.message))
            raise


    elif option == '':
        pass
    else:
        pass


def generate_job(job_length):
    """

    :param job_length:
    :return:
    """
    import string
    import random

    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(job_length))


def main(argv):
    try:
        logfile = ''
        num_hosts = 1
        from utils import banner

        print '\n***********************************************************************************************************************'
        banner.horizontal('Parzee INC Tools')
        print '***********************************************************************************************************************'
        opts, args = getopt.getopt(argv, "ho:l:", ["option=", "lfile="])
    except getopt.GetoptError:
        print 'Invalid option'
        print 'imbue_dump.py -o <option insert|delete|update> -h <number of hosts> -l <log>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print 'imbue_dump.py -o <option insert|delete|update> -h <number of hosts> -l <log>'
            sys.exit()
        elif opt in ("-o", "--option"):
            option_selected = arg

        elif opt in ("-l", "--lfile"):
            logfile = arg

    print 'imbue_dump| Option selected is: ', option_selected
    print 'imbue_dump| Log file is: ', logfile

    imbue_app(option=option_selected, log_file=logfile, jobId=generate_job(10))


if __name__ == "__main__":
    main(sys.argv[1:])
