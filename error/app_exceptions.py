__author__ = 'gogasca'


class AppNotFound(Exception):
    def __init__(self, value):
        self.value = value
        self.code = 101

    def __str__(self):
        return repr(self.value)

class CiscoVmxNotFound(Exception):
    def __init__(self, value):
        self.value = value
        self.code = 102

    def __str__(self):
        return repr(self.value)


class UcmVirtualMachineException(Exception):
    def __init__(self, value):
        self.value = value
        self.code = 103

    def __str__(self):
        return repr(self.value)

class UcmInstallError(Exception):
    def __init__(self, value):
        self.value = value
        self.code = 104

    def __str__(self):
        return repr(self.value)