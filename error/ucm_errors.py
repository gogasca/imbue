__author__ = 'gogasca'

"""
Error
The NTP server are mistyped inaaccessible or unreliable

Verify that valid NTP Server names or IP addresses were entered, that they are running NTPv4 at stratum 6
or less and that port 123 is not blocked by a firewall

Image recognition

, Errur ,

The nwr server(s) are mistyped, inaccessible ur
unreliable.

Uerifg that valid nwr server names Dr IP addresses were
entered, that they are running nwrua at stratum 5 ur
1ess, and that part 123 is nut blacked by a firewall.

"""
# Constant definition
ucm_install_errors = {'NTP_ERROR':                  -1, 'DEFAULT_GATEWAY_ERROR': -2, 'NETWORK_CONNECTIVITY_ERROR': -3,
                      'NETWORK_CONNECTIVITY_CHECK': -4, 'WARNINGS_EXCEEDED': -5, 'INVALID_HARDWARE': -6}

# List of Errors
media_check_words = ['ciseu', 'cisco', 'unified', 'cummuuicatiuns', 'communications', 'tab',
                     'space', 'between', 'elements', 'selects']

hardware_validation = ['please','wait...','hardware','this','can','take','several','minutes',
                       'unware:','passed','deteetian','validatian','neteeting','setup','firmware','nanagenent',
                       'hardware','setwp','canplete']

install_words = ['versions', 'versiuu', 'uersiuus','hard', 'drive','drive:' 'none', 'nun:','version',
                 'dvd', 'is', 'do', 'you', 'want', 'to', 'proceed', 'with',
                 'the', 'install', 'install?','pruceed','gun','dud','is:','11.3.1.1uuaass','tu']

cuc_install_words = ['pruceed', 'with', 'install', 'uersiuns', 'hard', 'drive:',
                     'nun:', 'the', 'versiuu', 'du', 'this', 'nun', 'is:', 'du',
                     'gun', 'pruceed', 'with', 'the', 'install']

reinitialize_error = ['Warning', 'Uirtual', 'disk', 'This', 'device', 'mag', 'need', 'reiuitialized', 'screen',
                      'elements'
                      'nEiniTinLizinG', 'uiLL', 'cnusn', 'nLL', 'nnTn', 'an', 'LUST!', 'actiuu', 'alsu', 'needing',
                      'reiuitializatiuu', 'disks', 'reinitializatiun', 'applied', 'peeeessing', 'a1szunn', 'errur']

ntp_error = ['Errur', 'The', 'nwr', 'server(s)', 'are', 'mistyped', 'inaccessible', 'ur', 'unreliable', 'Uerifg',
             'that', 'valid', 'server', 'names', 'Dr', 'IP', 'addresses', 'were', 'entered', 'they', 'running',
             'nwrua', 'at', 'stratum', '5', 'less', 'and', 'part', '123', 'is', 'nut', 'blacked', 'by', 'a', 'firewall',
             'ntpv4']

default_gateway_error = ['Nethmrk', 'Cunnectivitg', 'Check', 'Failure', 'Default', 'gatewag', 'is', 'net',
                         'respunding', 'Du', 'gun', 'wish', 'tn', 'Retry', 'Review', 'the',
                         'cunfignratiun', 'Halt', 'Install', 'ur', 'lguure', 'Netwurking', 'errur(s)',
                         'and', 'euhtihue', 'Install?', 'lgnure', 'uptiun', 'recummended', 'cuuld',
                         'lead', 'sgstem', 'failures']

network_connectivity_error = ['T1', 'Cunfigure', 'Setup', 'Netwurk', '[T', 'Checking', 'Cunnectivitg...',
                              'running', 'Descriptiun', 'a', 'cummand', 'Items', 'Haximum', 'Time', 'Timed', 'event',
                              'has', 'exceeded', 'its', 'estimate', 'Extending', 'time',
                              'IIIIIIIIIIIIIIIIIIIIIEEEIIIIIIIIIIIIIIIIIIIII']

network_connectivity_checking = ['checking', 'netwurk', 'cunnectivitg...', 'descriptiun:', 'running',
                                 'a', 'cummand', 'items', 'haximum', 'time', 'cumpleted', 'tutal',
                                 'remaining']

undetermined_error = ['failed', 'undetermined', 'errur', 'refer', 'install', 'lug', 'fur', 'further', 'infurmatiun']

database_install_in_progress = ['t]', 'cumpuueut', 'install', 'installing', 'database', 'cumpuuent', 'item',
                                'descriptiuu',
                                'running', 'a', 'script', 'items', 'time', 'remaining', 'hbgtes']

floppy_invalid = ['this', 'is', 'nut', 'huutahle', 'disk', 'insert', 'press', 'any', 'again', 'please', '{luppy']

install_successful = ['the', 'iustallatiuu', 'installatiun', 'uf', 'ciscu', 'unified', 'cummunicatiuus',
                      'cummnuicatiuns',
                      'hauager', 'hg.', 'has', 'dumpleted', 'cumpleted', 'snccessfu', 'ciscu',
                      'successfu', 'unified', 'cummnuicatiuns', 'hauager', 'hanager', 'lugin:']

cuc_install_succesful = ['the', 'iustallatiun', 'installatiun', 'uf', 'ciscu', 'uuitg', 'cuuuecuuu', 'cunnectiun',
                         'hung',
                         'has', 'cumpleted', 'successfully.', 'unity', 'successfullg'
                                                                       'cunnectiun', 'lugiu:', 'lugin:', 'installatiun',
                         'dumpleted', 'lugin', 'successfullg.']

cup_install_successful = ['the', 'iustallatiuu', 'installatiun', 'uf', 'ciscu', 'unified', 'cummunicatiuus',
                          'cummnuicatiuns',
                          'hauager', 'hg.', 'has', 'dumpleted', 'cumpleted', 'snccessfu', 'ciscu', 'successfu',
                          'unified',
                          'cummnuicatiuns', 'hauager', 'hanager', 'lugin:']

subscriber_error = ['errur', 'cunfiguratiuu', 'with', 'failed', 'cuufigured', 'bust', 'name', 'dues', 'uut', 'match',
                    'the', 'at', 'currectlg?', 'current']


def return_ucm_install_error(value):
    return ucm_install_errors.keys()[ucm_install_errors.values().index(value)]
