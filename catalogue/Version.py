__author__ = 'gogasca'

from conf import settings
import yaml

def get_default_iso(application,version):
    """

    :param application:
    :param version:
    :return:
    """
    try:
        with open(settings.versionFile) as f:
            yaml_document = yaml.load(f)
        try:
            return yaml_document[application][version]
        except KeyError:
            print 'get_default_iso() Invalid key application[' + application + '] version['+ version + ']'
        except Exception,e:
            print e
        #print doc['esx']['vnc'].
    except Exception,e:
        print(e)