__author__ = 'gogasca'

import logging
from conf import logging_conf
from conf import settings
from utils import yaml_processor

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Application():
    def __init__(self, type, application_params):
        """

        :param type:
        :param application_params:
        :return:
        """
        self.type = type
        self.application_params = application_params
        self.virtualmachine = None
        log.info('Application() Instance created')

    def __repr__(self):
        """

        :return:
        """
        return "<Application Instance: {} >".format(self.type)

    def set_params(self, parameters):
        """

        :param parameters:
        :return:
        """
        self.application_params = parameters

    def is_valid(self):
        """

        :return:
        """
        try:
            # We read the YAML application for valid applications and determine if type is there we just return True
            valid_applications = yaml_processor.YamlProcessor(settings.application_catalogue).read_file('application')
            if self.type in valid_applications:
                return True
            else:
                return False
        except Exception, e:
            log.error(str(e))

    def set_virtualmachine(self, vm):
        """

        :param vm:
        :return:
        """
        self.virtualmachine = vm

    def get_virtualmachine(self):
        """

        :return:
        """
        return self.virtualmachine
