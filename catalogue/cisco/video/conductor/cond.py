__author__ = 'gogasca'

import logging

from catalogue.cisco.common.ova import ova_handler
from conf import logging_conf
from hypervisor.esxi import file_operations
from hypervisor.esxi import general

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Conductor(ova_handler.OVAManager):
    """

    """
    def __init__(self,app):
        """

        :rtype : object
        """
        self.app = app
        log.info('cond.__init__() Telepresence Conductor Instance created')

    def deploy_ova(self, directory, datastore, name, annotation, deployment, ova_file, network, params, powerOn, job):
        """
        /usr/lib/vmware-ovftool/ovftool --noSSLVerify --acceptAllEulas -dm=thin -ds=datastore1 --powerOn --name=vcs
        --annotation=vcs --prop:ip4.address=110.10.0.201 --prop:ip4.netmask=255.255.255.0
        --prop:ip4.gateway=110.10.0.254 --prop:system.hostname=vcs --deploymentOption=VCS_Small
        /vmfs/volumes/datastore1/OVA-ISO/s42700x8_7_1.ova "vi://root:password@localhost"

        :param directory:
        :param datastore:
        :param name:
        :param annotation:
        :param deployment:
        :param ova_file:
        :param network:
        :param params:
        :param powerOn:
        :return:
        """

        # Check Network length
        if len(network) == 0:
            network = "VM Network"

        # powerOn
        if powerOn:
            powerOn = " --powerOn "
        else:
            powerOn = " "

        # Update Annotation
        annotation += '_{}'.format(job)

        # TPS deployment type
        if deployment == "10":
            deployment = "small"
        elif deployment == "100":
            deployment = "medium"
        else:
            log.warn('tp_.deploy_ova() Using default Conductor size. Size received {}'.format(deployment))
            deployment = "small"

        log.info('cond.deploy_ova() Deployment size: {}'.format(deployment))
        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName=ova_file):
            log.info('cond.deploy_ova() OVA file exists')
        else:
            log.error('cond.deploy_ova() OVA file doesnt exists')
            return False

        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName='/usr/lib/vmware-ovftool',
                                                    softlink=True):
            log.info('cond.deploy_ova() Link File already exists')
        else:
            log.info('cond.deploy_ova() Link File doesnt exist. Will proceed to create it')
            command_list = list()
            command_list.append('ln -s ' + directory + '/vmware-ovftool/ /usr/lib/vmware-ovftool' + ' ; echo $? ')
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)
            if '0' in res[0].encode('utf-8'):
                log.info('cond.deploy_ova() Link file created')
            else:
                log.info('cond.deploy_ova() Link file creation failed')
                return False

        command_list = list()
        command_list.append(
            'cd ' + directory + ' && /usr/lib/vmware-ovftool/ovftool --noSSLVerify --acceptAllEulas' + powerOn + '-dm=thin -ds=' + datastore +
            ' --name=' + name + ' --annotation="' + annotation + '" --network="' + network + '" --prop:ip4.address=' +
            params[
                'ip'] + ' --prop:ip4.netmask=' + params['mask'] + ' --prop:ip4.gateway=' + params[
                'gw'] + ' --deploymentOption=' + deployment +
            ' ' + ova_file + ' "vi://' + self.get_server().username + ':' + self.get_server().password + '@localhost"')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)
        log.info(res)
        if len(res) > 0:
            if 'Completed successfully' in res[-1].encode('utf-8'):
                log.info('cond.deploy_ova() OVA deployment completed. Deleting softlink...')
                general.send_command(commandList=['rm -rf /usr/lib/vmware-ovftool'],
                                     server=self.get_server().server,
                                     username=self.get_server().username,
                                     password=self.get_server().password,
                                     port=self.get_server().sshPort,
                                     display=True)
                return True

        log.error('cond.deploy_ova() OVA deployment failed')
        return False