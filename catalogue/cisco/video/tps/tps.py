import json
import logging
from subprocess import call
from time import sleep

from catalogue.cisco.common.ova import ova_handler
from conf import logging_conf
from conf import settings
from hypervisor.esxi import file_operations
from hypervisor.esxi import general
from hypervisor.esxi import vm_operations
from utils import helper
from utils import validator
from virtualization import VirtualMachine

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class TelepresenceServer(ova_handler.OVAManager):
    """

    """

    def __init__(self, app):
        """

        :rtype : object
        """
        self.app = app
        log.info('tps.__init__() Telepresence Server Instance created')

    def send_sequence(self, esx, params):
        """
        static 192.168.1.2 255.255.255.0 192.168.1.1
        :param esx:
        :param params:
        :return:
        """
        try:
            log.info('telepresence.send_sequence() Sending install sequence...waiting for VM to boot up')
            sleep(settings.ova_wait_deployment * 2)
            log.info(
                'telepresence.send_sequence() Starting to send VNC commands {} {}'.format(esx.server,
                                                                                          params['vnc_port']))
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type static'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key space'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['ip']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key space'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['mask']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key space'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['gw']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            return True
        except Exception, exception:
            log.exception('telepresence.send_sequence() {}'.format(exception))

    def deploy_ova(self, directory, datastore, name, annotation, deployment, ova_file, network, params, powerOn, job):
        """
        /usr/lib/vmware-ovftool/ovftool --noSSLVerify --acceptAllEulas -dm=thin -ds=datastore1 --powerOn --name=vcs
        --annotation=vcs --prop:ip4.address=110.10.0.201 --prop:ip4.netmask=255.255.255.0
        --prop:ip4.gateway=110.10.0.254 --prop:system.hostname=vcs --deploymentOption=VCS_Small
        /vmfs/volumes/datastore1/OVA-ISO/s42700x8_7_1.ova "vi://root:password@localhost"

        :param directory:
        :param datastore:
        :param name:
        :param annotation:
        :param deployment:
        :param ova_file:
        :param network:
        :param params:
        :param powerOn:
        :return:
        """

        # Check Network length
        if len(network) == 0:
            network = "VM Network"

        # powerOn
        if powerOn:
            powerOn = " --powerOn "
        else:
            powerOn = " "

        # Update Annotation
        annotation += '_{}'.format(job)

        # TPS deployment type
        if deployment == "10":
            deployment = "VTS_8"
        if deployment == "100":
            deployment = "VTS_3X"
        if deployment == "1000":
            deployment = "VTS_46"
        else:
            log.warn('tps.deploy_ova() Using default TP size. Size received {}'.format(deployment))
            deployment = "VTS_8"

        log.info('tps.deploy_ova() Deployment size: {}'.format(deployment))
        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName=ova_file):
            log.info('tps.deploy_ova() OVA file exists')
        else:
            log.error('tps.deploy_ova() OVA file doesnt exists')
            return False

        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName='/usr/lib/vmware-ovftool',
                                                    softlink=True):
            log.info('tps.deploy_ova() Link File already exists')
        else:
            log.info('tps.deploy_ova() Link File doesnt exist. Will proceed to create it')
            command_list = list()
            command_list.append('ln -s ' + directory + '/vmware-ovftool/ /usr/lib/vmware-ovftool' + ' ; echo $? ')
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)
            if '0' in res[0].encode('utf-8'):
                log.info('tps.deploy_ova() Link file created')
            else:
                log.info('tps.deploy_ova() Link file creation failed')
                return False

        command_list = list()
        command_list.append(
            'cd ' + directory + ' && /usr/lib/vmware-ovftool/ovftool --allowExtraConfig --noSSLVerify --acceptAllEulas' + powerOn + '-dm=thin -ds=' + datastore +
            ' --name=' + name + ' --annotation="' + annotation + '" --network="' + network + '" --deploymentOption=' + deployment + ' ' + ova_file + ' "vi://' + self.get_server().username + ':' + self.get_server().password + '@localhost"')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)
        log.info(res)
        if len(res) > 0:
            if 'Completed successfully' in res[-1].encode('utf-8'):
                log.info('tps.deploy_ova() OVA deployment completed. Deleting softlink...')
                general.send_command(commandList=['rm -rf /usr/lib/vmware-ovftool'],
                                     server=self.get_server().server,
                                     username=self.get_server().username,
                                     password=self.get_server().password,
                                     port=self.get_server().sshPort,
                                     display=True)
                return True

        log.error('tps.deploy_ova() OVA deployment failed')
        return False

    def install(self, app_params, params, power, job, esx):
        """

        :param app_params:
        :param params:
        :param power:
        :param job:
        :param esx:
        :return:
        """

        if self.get_ovf_file():
            log.info('tps.install() OVF file was obtained successfully')
        else:
            log.error('tps.install() OVF file failed')
            return -1

        if self.deploy_ovf_file(app_params['location']):
            log.info('tps.install() deployed OVF file successfully')
        else:
            log.error('tps.install() deployed OVF file failed')
            return -1

        if self.deploy_ova(app_params['location'],
                           app_params['datastore'],
                           app_params['hostname'],
                           app_params['annotation'],
                           app_params['size'],
                           app_params['ova_file'],
                           app_params['network_name'],
                           params, power, job.reference):
            log.info('tps.install() deployed OVA successfully')
            self.clean_ovf_file(app_params['location'])

        else:
            log.error('tps.install() deployed OVA failed')
            return -1

        # Will edit VMX file.
        log.info('tps.install() VMX file: {}'.format(app_params['esx_vmx_file']))

        # =========================================================
        # VNC Port definition
        # =========================================================

        try:

            log.info('tps.install() Getting Monitor port from DB...')
            # esx.vnc_ports is the DB load from VNC information
            res = validator.check_vnc_port(esx.vnc_ports)

            if res == 1:  # Return 1 when Single port defined in server
                # Sync VNC ports.
                # Login to ESXi via SSH and allocate VNC ports in use.
                vnc_ports_inuse = esx.allocate_vnc_ports(returnList=True)
                for vnc_port_inuse in vnc_ports_inuse:
                    if esx.add_vnc_port(vnc_port_inuse):
                        app_params['vnc_port'] = str(int(esx.vnc_ports))

            elif res == 2:  # Range of Ports
                esx.allocate_vnc_ports()  # Login via SSH and allocate VNC ports in use.
                port_range = range(int(esx.vnc_ports.split(':')[0]), 1 + int(esx.vnc_ports.split(':')[1]))
                port_found = False
                log.info('tps.install() Monitor ports: ')
                port_range = sorted(port_range, key=int)
                for port in port_range:
                    if esx.add_vnc_port(port):
                        app_params['vnc_port'] = str(port)
                        port_found = True
                        break
                if not port_found:
                    log.warn('tps.install() DB Invalid port defined in range. Using default Monitor port.')
                    app_params['vnc_port'] = settings.defaultVNCPort
            else:
                log.warn('tps.install() DB Invalid port defined in range. Using default Monitor port.')
                app_params['vnc_port'] = settings.defaultVNCPort

            message = 'Monitor port: ' + app_params['vnc_port']
            log.info('tps.install() {}'.format(message))
            log.info('tps.install() VMX File: {}'.format(app_params['esx_vmx_file']))
            command_list = list()
            command_list.append('echo \'RemoteDisplay.vnc.enabled = \"TRUE\"\' >> ' + app_params['esx_vmx_file'])
            command_list.append('echo \'RemoteDisplay.vnc.port = \"' + str(app_params['vnc_port']) + '\"\' >> ' +
                                app_params['esx_vmx_file'])
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)

            log.info(res)
            if res is None:
                log.info('tps.install() VMX file updated')
            else:
                log.error('tps.install() VMX file update failed')
                return -2

        except Exception as exception:
            message = 'ERROR Unable to get Monitor port'
            log.error('tps.install() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            log.exception(
                'tps.install() Unable to get Monitor port: ' + str(exception))
            return -1

        log.info('tps.install() Proceeding to start VM...')
        command_list = list()
        command_list.append('vim-cmd vmsvc/getallvms | grep ' + job.reference + ' | awk \'{print $1}\'')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)

        vmId = res[0].encode('utf-8')
        vmId = vmId.rstrip()

        if vmId:
            vm = VirtualMachine.VirtualMachine(app_params['hostname'], vmId)
            self.set_virtualmachine(vm)

            # Associate VM to parent ESXi Server
            vm.set_server(self.get_server())

            if vm.get_status():
                log.info('tps.install VM status is OK')
            else:
                log.error('tps.install() VM status is error')
                return -3

            if vm.power_on():
                log.info('tps.install VM power on successfully')
            else:
                log.error('tps.install() VM power on failed')
                return -3
        else:
            log.error('tps.install() Unable to get vmId')

        if vm_operations.insert_vm(
                vmId=vmId,
                vm_hostname=app_params['hostname'],
                status=1,
                description=job.reference,
                ip_information=params['ip'],
                uuid='',
                app_id=200006,
                power_state=True,
                host_id=app_params['host_id'],
                directory=app_params['location']):
            log.info('tps.install() Virtual machine DB insertion was successful')
            log.info('tps.install() Waiting for VM machine to start')

            #############################################
            # Update job infrastructure
            #############################################

            job.update_info(self.get_server().id, vm.vmId)
            server_json = json.dumps(job.server_info)
            sqlquery = """UPDATE job SET host_information='""" + server_json + """' WHERE job.id=""" + str(job.id)
            helper.update_database(sqlquery)

        else:
            message = 'ERROR: VM insertion failed'
            log.error('tps.install ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return -4

        log.info('tps.install() Proceeding to pass configuration parameters...')
        if self.send_sequence(esx, app_params):
            log.info('tps.install() Deploy IP information successfully')
            log.info('tps.install() Removing monitor port: ' + str(app_params['vnc_port']))
            esx.remove_vnc_port(app_params['vnc_port'])
            log.info('tps.install() Waiting for VM to deploy')
            sleep(180)

        else:
            log.info('tps.install() Unable to set parameters')
            return -5

        # =========================================================
        # get screen URL
        # =========================================================

        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + vm.vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + vm.vmId

        log.info('tps.install() Screen: ' + url)
        log.info('tps.install() Waiting for virtual machine to start...')
        self.app.application_params['screen_url'] = url

        log.info('tps.install() Instance is starting...')
        self.app.application_params['vmid'] = vmId
        return vmId