import json
import logging
from subprocess import call
from time import sleep

from catalogue.cisco.common.ova import ova_handler
from conf import logging_conf
from conf import settings
from hypervisor.esxi import file_operations
from hypervisor.esxi import general
from hypervisor.esxi import vm_operations
from utils import helper
from utils import validator
from virtualization import VirtualMachine

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Cspc(ova_handler.OVAManager):
    """
    -Deploy OVA
    -Press Any Key to continue <send enter>
    -Send admin <send enter>/Admin!23M<send enter>
    -Send IP information: conf ip <interface> <IP address> <Netmask> <Default Gateway>
    -Send DNS:  conf dns -a <DNS IP address 1> <DNS IP address 2>
    -pwdreset collectorlogin 180
        Capture Image
    -pwdreset root 180
        Capture Image
    -Reboot
    """

    def __init__(self, app):
        """

        :rtype : object
        """
        self.app = app
        log.info('cspc.__init__() Cisco CSPC Instance created')

    def deploy_ova(self, directory, datastore, name, annotation, ova_file, network, params, powerOn, job):
        """
        /usr/lib/vmware-ovftool/ovftool --noSSLVerify --acceptAllEulas -dm=thin -ds=datastore1 --powerOn --name=vcs
        --annotation=vcs --prop:ip4.address=110.10.0.201 --prop:ip4.netmask=255.255.255.0
        --prop:ip4.gateway=110.10.0.254 --prop:system.hostname=vcs
        /vmfs/volumes/datastore1/OVA-ISO/s42700x8_7_1.ova "vi://root:password@localhost"

        :param directory:
        :param datastore:
        :param name:
        :param annotation:
        :param ova_file:
        :param network:
        :param params:
        :param powerOn:
        :return:
        """

        # Check Network definition, if empty use default VLAN
        if len(network) == 0:
            network = "VM Network"

        # powerOn
        if powerOn:
            log.info('cspc.deploy_ova() Power On')
            powerOn = " --powerOn "
        else:
            powerOn = " --powerOffTarget "

        # Update Annotation
        annotation += '_{}'.format(job)

        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName=ova_file):
            log.info('cspc.deploy_ova() OVA file exists')
        else:
            log.error('cspc.deploy_ova() OVA file doesnt exists')
            return False

        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName='/usr/lib/vmware-ovftool',
                                                    softlink=True):
            log.info('cspc.deploy_ova() Link File already exists')
        else:
            log.info('cspc.deploy_ova() Link File doesnt exist. Will proceed to create it')
            command_list = list()
            # Default directory  ln -s /vmfs/volumes/datastore1/vmware-ovftool/  /usr/lib/vmware-ovftool
            # If link exists: rm -rf /usr/lib/vmware-ovftool
            command_list.append('ln -s ' + directory + '/vmware-ovftool/ /usr/lib/vmware-ovftool' + ' ; echo $? ')
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)
            if '0' in res[0].encode('utf-8'):
                log.info('cspc.deploy_ova() Link file created')
            else:
                log.info('cspc.deploy_ova() Link file creation failed')
                return False

        command_list = list()
        command_list.append(
            'cd ' + directory + ' && /usr/lib/vmware-ovftool/ovftool --noSSLVerify --acceptAllEulas' +
            powerOn + '-dm=thin -ds=' + datastore + ' --name=' + name + ' --annotation="' + annotation +
            '" --network="' + network + '" ' + ova_file + ' "vi://' + self.get_server().username + ':' +
            self.get_server().password + '@localhost"')

        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)
        log.info(res)
        if isinstance(res, bool) and res is False:
            log.error('cspc.deploy_ova() OVF Tool error')
            return False

        if len(res) > 0:
            if 'Completed successfully' in res[-1].encode('utf-8'):
                log.info('cspc.deploy_ova() OVA deployment completed. Deleting softlink...')
                general.send_command(commandList=['rm -rf /usr/lib/vmware-ovftool'],
                                     server=self.get_server().server,
                                     username=self.get_server().username,
                                     password=self.get_server().password,
                                     port=self.get_server().sshPort,
                                     display=True)
                return True

        log.error('cspc.deploy_ova() OVA deployment failed')
        return False

    def install(self, app_params, params, power, job, esx):
        """

        :param app_params:
        :param params:
        :param power:
        :param job:
        :param esx:
        :return:
        """
        """
        if self.get_ovf_file():
            log.info('cspc.install() OVF file was obtained successfully')
        else:
            log.error('cspc.install() OVF file failed')
            return -1

        if self.deploy_ovf_file(app_params['location']):
            log.info('cspc.install() deployed OVF file successfully')
        else:
            log.error('cspc.install() deployed OVF file failed')
            return -1
        """

        if self.deploy_ova(app_params['location'],
                           app_params['datastore'],
                           app_params['hostname'],
                           app_params['annotation'],
                           app_params['ova_file'],
                           app_params['network_name'],
                           params, power, job.reference):
            log.info('cspc.install() deployed OVA successfully')
            # self.clean_ovf_file(app_params['location'])

        else:
            log.error('cspc.install() deployed OVA failed')
            return -1

        # Will edit VMX file.
        log.info('cspc.install() VMX file: {}'.format(app_params['esx_vmx_file']))

        # =========================================================
        # VNC Port definition
        # =========================================================

        try:

            log.info('cspc.install() Getting Monitor port from DB...')
            # esx.vnc_ports is the DB load from VNC information
            res = validator.check_vnc_port(esx.vnc_ports)

            if res == 1:  # Return 1 when Single port defined in server
                # Sync VNC ports.
                # Login to ESXi via SSH and allocate VNC ports in use.
                vnc_ports_inuse = esx.allocate_vnc_ports(returnList=True)
                for vnc_port_inuse in vnc_ports_inuse:
                    if esx.add_vnc_port(vnc_port_inuse):
                        app_params['vnc_port'] = str(int(esx.vnc_ports))

            elif res == 2:  # Range of Ports
                esx.allocate_vnc_ports()  # Login via SSH and allocate VNC ports in use.
                port_range = range(int(esx.vnc_ports.split(':')[0]), 1 + int(esx.vnc_ports.split(':')[1]))
                port_found = False
                log.info('cspc.install() Monitor ports: ')
                port_range = sorted(port_range, key=int)
                for port in port_range:
                    if esx.add_vnc_port(port):
                        app_params['vnc_port'] = str(port)
                        port_found = True
                        break
                if not port_found:
                    log.warn('cspc.install() DB Invalid port defined in range. Using default Monitor port.')
                    app_params['vnc_port'] = settings.defaultVNCPort
            else:
                log.warn('cspc.install() DB Invalid port defined in range. Using default Monitor port.')
                app_params['vnc_port'] = settings.defaultVNCPort

            message = 'Monitor port: ' + app_params['vnc_port']
            log.info('cspc.install() {}'.format(message))
            log.info('cspc.install() VMX File: {}'.format(app_params['esx_vmx_file']))
            command_list = list()
            command_list.append('echo \'RemoteDisplay.vnc.enabled = \"TRUE\"\' >> ' + app_params['esx_vmx_file'])
            command_list.append('echo \'RemoteDisplay.vnc.port = \"' + str(app_params['vnc_port']) + '\"\' >> ' +
                                app_params['esx_vmx_file'])
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)

            if res is None:
                log.info('cspc.install() VMX file updated')
            else:
                log.info(res)
                log.error('cspc.install() VMX file update failed')
                return -2

        except Exception as exception:
            message = 'ERROR Unable to get Monitor port'
            log.error('tps.install() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            log.exception(
                'cspc.install() Unable to get Monitor port: ' + str(exception))
            return -1

        log.info('cspc.install() Proceeding to start VM...')
        command_list = list()
        command_list.append('vim-cmd vmsvc/getallvms | grep ' + job.reference + ' | awk \'{print $1}\'')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)

        vmId = res[0].encode('utf-8')
        vmId = vmId.rstrip()

        if vmId:
            vm = VirtualMachine.VirtualMachine(app_params['hostname'], vmId)
            self.set_virtualmachine(vm)

            # Associate VM to parent ESXi Server
            vm.set_server(self.get_server())

            if vm.get_status():
                log.info('cspc.install VM status is OK')
            else:
                log.error('cspc.install() VM status is error')
                return -3

            if vm.power_on():
                log.info('cspc.install VM power on successfully')
            else:
                log.error('cspc.install() VM power on failed')
                return -3
        else:
            log.error('cspc.install() Unable to get vmId')

        if vm_operations.insert_vm(
                vmId=vmId,
                vm_hostname=app_params['hostname'],
                status=1,
                description=job.reference,
                ip_information=params['ip'],
                uuid='',
                app_id=200006,
                power_state=True,
                host_id=app_params['host_id'],
                directory=app_params['location']):
            log.info('cspc.install() Virtual machine DB insertion was successful')
            log.info('cspc.install() Waiting for VM machine to start')

            #############################################
            # Update job infrastructure
            #############################################

            job.update_info(self.get_server().id, vm.vmId)
            server_json = json.dumps(job.server_info)
            sqlquery = """UPDATE job SET host_information='""" + server_json + """' WHERE job.id=""" + str(job.id)
            helper.update_database(sqlquery)

        else:
            message = 'ERROR: VM insertion failed'
            log.error('cspc.install ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return -4

        log.info('cspc.install() Proceeding to pass configuration parameters...')
        if self.send_sequence(esx, app_params):
            log.info('cspc.install() Deploy IP information successfully')
            log.info('cspc.install() Removing monitor port: ' + str(app_params['vnc_port']))
            esx.remove_vnc_port(app_params['vnc_port'])
            log.info('cspc.install() Waiting for VM to deploy...')
            sleep(180)

        else:
            log.info('cspc.install() Unable to set parameters')
            return -5

        # =========================================================
        # get screen URL
        # =========================================================

        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + vm.vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + vm.vmId

        log.info('cspc.install() Screen: ' + url)
        log.info('cspc.install() Waiting for virtual machine to start...')
        self.app.application_params['screen_url'] = url

        log.info('cspc.install() Instance is starting...')
        self.app.application_params['vmid'] = vmId
        return vmId

    def send_sequence(self, esx, params):
        """

        :param esx:
        :param params:
        :return:

        -Press Any Key to continue <send enter>
        -Send admin <send enter>/Admin!23<send enter>
        -Send IP information: conf ip <interface> <IP address> <Netmask> <Default Gateway>
        -pwdreset collectorlogin 180
            Capture Image
        -pwdreset root 180
            Capture Image
        -Reboot

        """
        try:
            log.info('cspc.send_sequence() Waiting for VM to boot up {0} seconds'.format(
                settings.ova_wait_deployment))
            sleep(settings.ova_wait_deployment)
            log.info(
                'cspc.send_sequence() Waiting {0} seconds for CSPC software to start'.format(settings.cspc_wait_time))
            sleep(settings.cspc_wait_time)
            log.info(
                'cspc.send_sequence() Sending VNC commands {} {}'.format(esx.server, params['vnc_port']))

            ##################################
            # Login default username: admin
            ################# ################
            # Press any key to continue
            #call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            #sleep(5)

            log.info('cspc.send_sequence() Send username')
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type admin'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)

            ##################################
            # Use default password
            ##################################

            log.info('cspc.send_sequence() Send default password')
            call(['vncdo --force-caps -s ' + esx.server + '::' + params[
                'vnc_port'] + ' type ' + settings.cspc_default_password], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(5)

            ##################################
            # Password reset: Asks for old password
            ##################################

            call(['vncdo --force-caps -s ' + esx.server + '::' + params[
                'vnc_port'] + ' type ' + settings.cspc_default_password], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)

            if settings.cspc_dev_mode:
                log.warn('cspc.send_sequence() Using CSPC Dev mode')
                log.info('cspc.send_sequence() Using CSPC new default password')

                ##################################
                # New password
                ##################################

                call(['vncdo --force-caps -s ' + esx.server + '::' + params[
                    'vnc_port'] + ' type ' + settings.cspc_dev_password],
                     shell=True)
                sleep(2)
                call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
                sleep(2)
                call(['vncdo --force-caps -s ' + esx.server + '::' + params[
                    'vnc_port'] + ' type ' + settings.cspc_dev_password],
                     shell=True)
                sleep(2)
                call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
                sleep(5)
                ############################
                # Says password is expired
                ############################
                log.info('cspc.send_sequence() Password set to expired')
                # Old password
                call(['vncdo --force-caps -s ' + esx.server + '::' + params[
                    'vnc_port'] + ' type ' + settings.cspc_dev_password],
                     shell=True)
                sleep(2)
                call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
                sleep(5)

            ##################################
            # New password
            ##################################
            call(['vncdo --force-caps -s ' + esx.server + '::' + params['vnc_port'] + ' type "' + params[
                'admin_password'] + '"'],
                 shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo --force-caps -s ' + esx.server + '::' + params['vnc_port'] + ' type "' + params[
                'admin_password'] + '"'],
                 shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(10)

            ##################################
            # IP Address
            ##################################
            log.info('cspc.send_sequence() Configuring IP settings')
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)

            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type "conf ip eth0 ' + params['ip'] + ' ' +
                  params['mask'] + ' ' + params['gw'] + '"'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            log.info('cspc.send_sequence() Configuring pwdreset collectorlogin')
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type "pwdreset collectorlogin 180"'],
                 shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(5)
            # Read Password in screen
            self.read_screen_information()
            log.info('cspc.send_sequence() Configuring pwdreset root')
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type "pwdreset root 180"'],
                 shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(5)
            # Read Password in screen
            self.read_screen_information()
            log.info('cspc.send_sequence() Rebooting...')
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type reboot'],
                 shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type y'],
                 shell=True)
            sleep(1)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            return True

        except KeyError, e:
            log.exception(e)

    def read_screen_information(self):
        """

        :return:
        """

        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + \
                  self.get_virtualmachine().vmId

        log.info('cspc.read_screen_information() URL ' + url)
        img = self.read_screen_information_exponential_backoff(url=url,
                                                               username=self.get_server().username,
                                                               password=self.get_server().password,
                                                               httpsPort=self.get_server().httpsPort,
                                                               downloadPath=self.app.application_params['screen'])

        if not img:
            self.set_install_failed()
            log.info('cspc.read_screen_information() Unable to get image')
