__author__ = 'gogasca'

import time
import warnings

import psycopg2

warnings.filterwarnings("ignore")
import logging
from alarms import send_sms
from billiard.exceptions import Terminated
from catalogue.cisco.common import install_uc
from celery.task import task
from celery.result import AsyncResult
from conf import logging_conf
from conf import settings
from hypervisor.esxi import general
from shared import Report
from utils import db_utils
from utils import helper
from utils import host_utils
from utils import validator


@task(bind=True, default_retry_delay=300, max_retries=3, throws=(Terminated,))
def imbue_cluster(
        self,
        configuration,
        job=None,
        **kwargs):
    """

    :param configuration:
    :param job:
    :param job_id:
    :param cli:
    :param api_call:
    :param log_file:
    :param esx:
    :param tools:
    :param kwargs:
    :return:
    """
    # ********************************** Process Tools server ***********
    log = logging_conf.LoggerManager().getLogger("__app___")
    log.setLevel(level=logging.DEBUG)
    log.info(
        '---------------------------------------Initializing IMBUE cluster service---------------------------------------')
    try:
        start = time.time()
        error_detected = False

        if self.request.id:
            log.info('deploy_cluster() Celery task id: {}'.format(self.request.id))

        # =========================================================
        # API request
        # =========================================================

        log.info('deploy_cluster() API call request: (' + job.reference + ') | ' + str(configuration))

        # =========================================================
        # Create log file directory
        # =========================================================

        log.info('deploy_cluster() Creating log directory: ' + job.reference)
        log_folder = None
        command = 'mkdir ' + settings.log_folder + job.reference
        res = general.run_command(command)
        if res == 0:  # Check command was executed successfully
            log_folder = job.reference + '/'
            log_folder = settings.log_folder + log_folder
        else:
            log.warn('deploy_cluster() Unable to create log folder')
            error_detected = True
            return

        # =========================================================
        # Creating Report object
        # =========================================================

        report = Report.Report(settings.report_path, job.reference)
        if report.initialize():
            log.info('Report initialized successfully')
        else:
            log.error('Report initialized failed')

        message = 'Cluster installation has started'
        if report:
            report.add_log(message)

        # =========================================================
        # Initialize job in database
        # =========================================================

        if job.id:

            # =========================================================
            # Update existing job in database
            # =========================================================

            sqlquery = """UPDATE job SET description='Cluster task started',is_cluster=true WHERE job.id=""" + \
                       str(job.id)
            db_utils.update_database(sqlquery)

        else:
            log.exception('deploy_cluster() No job')
            return False

        log.info('deploy_cluster() Cluster application install will start...')
        if self.request.id:
            self.update_state(state='PROGRESS', meta={'status': 'Task Started'})

        # =========================================================
        #  Check cluster configuration
        # =========================================================

        if validator.check_cluster_configuration(configuration):
            log.info(
                'deploy_cluster() Valid cluster configuration found. Checking cluster information details')
        else:
            error_detected = True
            log.error('deploy_cluster() Invalid cluster configuration found')
            return

        # =========================================================
        #  Check cluster mode
        # =========================================================

        log.info('deploy_cluster() Checking cluster combination mode...')
        mode = validator.get_cluster_configuration_mode(configuration)

        log.info('deploy_cluster() Cluster configuration: {} Mode: {}'.format(configuration, mode))
        if validator.get_imbue_cluster_installation(configuration, mode):
            # We need to obtain the host information
            log.info('deploy_cluster() Valid cluster install combination. Install will continue')
        else:
            error_detected = True
            log.error('deploy_cluster() Invalid cluster combination: {}'.format(mode))
            return

        log.info('deploy_cluster() Mode {0}'.format(mode))

        # =========================================================
        # Check UC instance
        # =========================================================

        cluster_instances = [instance for instance in configuration["cluster"]]
        log.info(cluster_instances)

        # =========================================================
        # Update install_information for job
        # =========================================================

        log.info('deploy_cluster() Updating job install information')
        if host_utils.get_install_information(configuration=cluster_instances, job_reference=job.reference, type=2):
            log.info('deploy_cluster() Job information updated successfully')
        else:
            log.error('deploy_cluster() Job information update failed')

        host_id = cluster_instances[0]['instance']['host']['id']
        log.info(
            'deploy_cluster() Installing Publisher in host: {0}'.format(host_id))
        esx_instance = helper.get_server_instance(host_id)
        log.info('deploy_cluster() Application type: {}'.format(
            cluster_instances[0]['instance']['application']['type']))

        # =========================================================
        # Install Publisher
        # =========================================================

        if len(cluster_instances) > 1:
            publisher_info = cluster_instances[0]  # Information required to update cluster file
            subscriber_info = cluster_instances[1]  # Information required to update cluster file
        else:
            subscriber_info = None  # No subscriber information

        pub_instance = install_uc.imbue_uc(
            configuration=cluster_instances[0],
            job=job,
            cluster=True,
            log_folder=log_folder,
            report=report,
            esx=esx_instance,
            additional=subscriber_info)

        if pub_instance:

            # =========================================================
            # Successful publisher installation
            # =========================================================

            if self.request.id:
                job_status = AsyncResult(self.request.id).state
                log.info('deploy_cluster() Job status: {} Task: {}'.format(job_status, self.request.id))
                if job_status == 'REVOKED':
                    message = 'Cluster Installation was cancelled during Publisher installation'
                    log.info('deploy_cluster() ' + message)
                    return

            log.info('deploy_cluster() Cluster: Publisher install completed successfully')
            end = time.time()
            total = end - start
            log.info('deploy_cluster() Publisher elapsed time: ' + str(int(total / 60)) + ' seconds')

            # =========================================================
            # Subscriber installation
            # =========================================================

            # Continue with subscriber installation. This is a sequential install.
            log.info('deploy_cluster() Cluster: Proceeding with subscriber installation')
            log.info('deploy_cluster() Cluster: Will install ' +
                     str(len(cluster_instances[1:])) + ' subscribers')

            # Check subscriber instances exists:
            if cluster_instances[1:] is not None:
                if len(cluster_instances[1:]) >= 1:
                    log.info('deploy_cluster() Cluster proceeding cluster install')
                else:
                    log.warn(
                        'deploy_cluster() Cluster: CUCM cluster installed successfully')
                    log.warn('deploy_cluster() No subscribers defined')
                    send_sms.send_sms_alert(
                        body='Cluster: CUCM cluster installed successfully: ' + str(job.reference))
            else:
                log.warn('deploy_cluster() Cluster: CUCM cluster installed successfully')
                log.warn('deploy_cluster() No subscribers defined')
                send_sms.send_sms_alert(
                    body='Cluster: CUCM cluster installed successfully: ' + str(job.reference))

            for cluster_instance in cluster_instances[1:]:

                # =========================================================
                # Launch subscriber installation serially
                # =========================================================

                log.info(
                    'deploy_cluster() Installing subscriber: {}'.format(cluster_instance))

                # =========================================================
                # Normalize configurations
                # =========================================================

                host_id = cluster_instance['instance']['host']['id']
                esx_instance = helper.get_server_instance(host_id)

                # =========================================================
                # Launch UCM installation for publisher
                # =========================================================

                sub_instance = install_uc.imbue_uc(
                    configuration=cluster_instance,
                    cluster=True,
                    log_folder=log_folder,
                    job=job,
                    report=report,
                    esx=esx_instance,
                    additional=publisher_info)

                if sub_instance:
                    end = time.time()
                    total = end - start
                    message = 'Cluster install process took: ' + str(int(total)) + ' seconds'
                    if report:
                        report.add_log(message)
                    log.info('deploy_cluster() ' + message)
                else:

                    # =========================================================
                    # Subscriber failed
                    # =========================================================

                    error_detected = True
                    message = 'ERROR Cluster Subscriber deployment failed'
                    if report:
                        report.add_log(message)
                    log.error('deploy_cluster() ' + message)
                    helper.generate_alert(job.id, message, job.reference)
                    return

            log.info('deploy_cluster() Cluster installed succesfully')
            send_sms.send_sms_alert(
                body='Cluster installed successfully: ' + str(job.reference))

        else:
            # =========================================================
            # Install Publisher failed
            # =========================================================
            error_detected = True
            message = 'ERROR Cluster Publisher deployment failed'
            if report:
                report.add_log(message)
            log.error('deploy_cluster() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return

    except psycopg2.ProgrammingError as exception:
        log.exception('deploy_cluster() Database error' + str(exception))

    except Exception as exception:
        log.exception('deploy_cluster() ' + str(exception))
        error_detected = True
        return

    finally:

        # =========================================================
        # Proceed if ESXi connectivity is successful...exit if not
        # =========================================================

        log.info('deploy_cluster() Job: ' + job.reference + ' has been completed')
        if report:
            report.add_log('Job [' + job.reference + '] is terminating')

        # =========================================================
        # Job was cancelled
        # =========================================================

        if self.request.id:
            log.info('deploy_cluster() Job information: {}'.format(self.request.id))
            job_status = AsyncResult(self.request.id).state
            log.info('deploy_cluster() Job status: {} Task: {}'.format(job_status, self.request.id))
            if job_status == 'REVOKED':
                # Update job final status
                message = 'Cluster Installation was cancelled'
                log.info('deploy_cluster() ' + message)
                if report:
                    report.add_log('Cluster Installation ' + job.reference + ' has been cancelled')
                    report.generate_report(video_supported=True)

                self.update_state(state='CANCELLED', meta={'status': message})
                job_status = AsyncResult(self.request.id).state
                log.info(
                    'install_uc() Job status after cancel request: {} Task: {}'.format(job_status, self.request.id))
                if job.id:
                    sqlquery = """UPDATE job SET status=1,is_cancelled=true,job_end='now()' WHERE job.id=""" + str(
                        job.id)
                    db_utils.update_database(sqlquery)
                return {'status': message, 'result': 1}

        # =========================================================
        # Job has been completed
        # =========================================================

        if error_detected:
            if job.id:
                sqlquery = """UPDATE job SET status=-1,description='Cluster install failed',job_end='now()' WHERE job.id=""" + \
                           str(job.id)
            message = 'ERROR Cluster install failed'
            if report:
                report.add_log(message)
            result = -1
            if self.request.id:
                self.update_state(state='FAILURE', meta={'status': message})

        else:
            if job.id:
                sqlquery = """UPDATE job SET status=1,description='Cluster install completed',job_end='now()' WHERE job.id=""" + \
                           str(job.id)
            message = 'Cluster install was successful'
            if report:
                report.add_log(message)
            result = 1

        # =========================================================
        # Generate report
        # =========================================================

        if report:
            report.generate_report(video_supported=True)

        if job.id:
            helper.update_database(sqlquery)

        if self.request.id:
            self.update_state(state='COMPLETED', meta={'status': 1})
            return {'status': message, 'result': result}
        else:
            if error_detected:
                return False
            else:
                return True
