__author__ = 'gogasca'

import gc
import json
import logging
import random
from subprocess import call
from time import sleep

from catalogue.cisco.common.xml import xml_processor, answer_file_processor
from conf import logging_conf, settings
from database import Db
from error import app_exceptions
from error import ucm_errors
from hypervisor.esxi import general
from hypervisor.esxi import network_operations
from hypervisor.esxi import vm_operations
from img import image_recognition
from utils import helper
from virtualization import VirtualMachine

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class cup():
    def __init__(self, app):
        """

        :rtype : object
        """
        self.app = app
        log.info('cup.__init__() Instance created')

    def clean_up(self, remote_directory):
        """

        :param remote_directory:
        :return:
        """
        #delete_directory
        log.info('cup.clean_up')
        if self.get_server():

            if self.get_server().delete_working_directory(remote_directory=remote_directory):
                log.info('Directory deleted successfully {}'.format(remote_directory))
                return True
            else:
                log.info('Directory deletion failed {}'.format(remote_directory))
                return False

    def revert_installation(self):
        """

        :return:
        """
        try:

            # =========================================================
            # Get VM status
            # =========================================================

            log.info('revert_installation() Get current VM state')
            # Verify VM is on/off/doesnt exist
            command_list = []

            command_list.append(
                'vim-cmd vmsvc/power.getstate ' + str(self.get_virtualmachine().vmId) + ' | grep \'Power\'')
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)

            if res:
                result = res[0].encode('utf-8').rstrip()
                if result == 'Powered on':
                    log.warn('revert_installation() Machine is power on')

                    # =========================================================
                    # Power off Virtual machine
                    # =========================================================

                    log.info('ucm.revert_installation power off VM')
                    # Check for vm status ip address/hostname/toolsRunningStatus
                    res = vm_operations.power_off(server=self.get_server().server,
                                                  username=self.get_server().username,
                                                  password=self.get_server().password,
                                                  port=self.get_server().sshPort,
                                                  vmId=self.get_virtualmachine().vmId)
                    if res:
                        sleep(15)
                        # Sleep to let the machine be off
                    else:
                        log.error(
                            'cup.revert_installation power off failed.')
                        return False
            else:
                log.info('cup.revert_installation Unable to get status. Probably doesnt exist anymore')
                return True

            # =========================================================
            # Destroy Virtual machine
            # =========================================================

            log.info('cup.revert_installation() destroying VM')

            if settings.destroy_vm:
                res = vm_operations.destroy(server=self.get_server().server,
                                            username=self.get_server().username,
                                            password=self.get_server().password,
                                            port=self.get_server().sshPort,
                                            vmId=self.get_virtualmachine().vmId)

                if not res:
                    log.error('cup.revert_installation destroy failed')
                    return False

                # =========================================================
                # Delete from Database
                # =========================================================


                sqlquery = """DELETE FROM virtualmachine WHERE fk_server=""" + \
                           str(self.get_server().id) + """ AND vmid=""" + \
                           str(self.get_virtualmachine().vmId)
                helper.update_database(sqlquery)

                log.info('cup.revert_installation() Deletion was successful')
                return True

            else:
                log.warn(
                    'cup.revert_installation() Don t destroy VM. Just power it off')
                return True

        except Exception, exception:
            log.exception('cup.revert_installation() '.format(exception))
            return False

    def deploy_floppy(self, app_server, esx_server):
        """

        :param appServer:
        :param esxServer:
        :return:
        """
        try:
            log.info('cup.deploy_floppy()')
            # Move answerfile to Tools, create Floppy drive and put it into
            # ESXi
            if answer_file_processor.deploy_answer_file(app_server, esx_server, self.app.application_params):
                return True
            else:
                log.error('Unable to deploy floppy with answer file')
                return False
        except Exception, e:
            log.exception(str(e))
            return False

    def skip_media_check(self, attempts=0, **kwargs):
        """

        :param attempts:
        :param kwargs:
        :return:
        """
        log.info('cup.skip_media_check() Media check started')
        try:

            if attempts > 3:
                log.info(
                    'cup.install() Failed Exceeded max number of attempts: ' + str(attempts))
                self.set_install_failed()
                return False

            # get_screenshot(url='https://110.10.0.144/screen?id=' + vmId,username='root',password='password',
            # httpsPort='443',
            # downloadPath=image_file)
            if self.get_server().httpsPort is not None:
                url = 'https://' + self.get_server().server + ':' + str(
                    self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
            else:
                url = 'https://' + self.get_server().server + '/screen?id=' + \
                      self.get_virtualmachine().vmId

            log.info('cup.skip_media_check() url ' + url)
            img = image_recognition.get_screenshot(url=url,
                                                   username=self.get_server().username,
                                                   password=self.get_server().password,
                                                   httpsPort=self.get_server().httpsPort,
                                                   downloadPath=self.app.application_params['screen'])

            # ****** We were able to download an image
            if img:
                log.info('Image downloaded successfully')
            else:
                # Retry later
                self.set_install_failed()
                return False

            res = image_recognition.processImage(
                self.app.application_params['screen'])

            if res:

                # ******* Check we are already at the Media check prompt
                if image_recognition.is_prompt_ready(list=ucm_errors.media_check_words, image_to_text=res.lower(),
                                                     coefficient=5):
                    log.info('cup.skip_media_check() Media check screen is ready')

                    # ********** Verify VNC port is opened
                    # ********** Send VNC Commands: vncdo -s 110.10.0.144::5901 key tab
                    # ********** Send VNC Commands: vncdo -s 110.10.0.144::5901 key enter


                    if general.check_network_connectivity(server=self.get_server().server,
                                                          vncPort=self.app.application_params['vnc_port'], mode='vnc'):
                        log.info(
                            'cup.skip_media_check() Media check screen sending tab...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key tab'], shell=True)
                        log.info(
                            'cup.skip_media_check() Media check screen sending Enter...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key enter'], shell=True)
                        log.info(
                            'cup.skip_media_check() Media check screen was skipped successfully...')
                        return True
                    else:
                        from error import app_exceptions
                        raise app_exceptions.UcmVirtualMachineException(
                            'Machine has not started...')
                else:
                    log.error(
                        'cup.skip_media_check() Other. Media check prompt is not available Retrying...')

                    sleep(30)
                    self.skip_media_check(attempts=attempts + 1)
                    return True
            else:
                log.error('cup.skip_media_check() No text available')
                return False

        except Exception, e:
            log.exception(str(e))
            return False

    def overwrite_harddisk(self, attempts=0, **kwargs):
        """
        :param attempts:
        :return: :raise app_exceptions.UcmVirtualMachineException:
        """

        # =========================================================
        # Overwrite Harddisk
        # =========================================================

        if attempts <= 3:
            log.info('ucm.overwrite_harddisk() Attempt: {0}'.format(attempts))
        else:
            log.error(
                'cup.overwrite_harddisk() Exceeded max number of attempts: ' + str(attempts))
            self.set_install_failed()
            return False

        from error import ucm_errors
        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + \
                  self.get_virtualmachine().vmId

        try:
            log.info('cup.overwrite_harddisk() URL ' + url)
            img = image_recognition.get_screenshot(url=url,
                                                   username=self.get_server().username,
                                                   password=self.get_server().password,
                                                   httpsPort=self.get_server().httpsPort,
                                                   downloadPath=self.app.application_params['screen'])

        except Exception, e:
            log.exception('cup.overwrite_harddisk() ' + str(e))
            sleep(3)
            img = image_recognition.get_screenshot(url=url,
                                                   username=self.get_server().username,
                                                   password=self.get_server().password,
                                                   httpsPort=self.get_server().httpsPort,
                                                   downloadPath=self.app.application_params['screen'])

        if img:
            log.info('cup.overwrite_harddisk() Image obtained successfully')
        else:
            log.error('cup.overwrite_harddisk() Unable to retrieve image')
            self.set_install_failed()
            return False

        res = image_recognition.processImage(
            self.app.application_params['screen'])

        if res:
            # Send res.lower case
            if image_recognition.is_prompt_ready(list=ucm_errors.install_words, image_to_text=res.lower(),
                                                 coefficient=5):
                log.info('cup.overwrite_harddisk() Install software is ready')

                # Verify port is opened
                if general.check_network_connectivity(server=self.get_server().server,
                                                      vncPort=self.app.application_params['vnc_port'], mode='vnc'):
                    # vncdo -s 110.10.0.144::5901 key enter
                    log.info('cup.overwrite_harddisk() sending Enter...')
                    call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                        'vnc_port'] + ' key enter'], shell=True)

                    log.info('ucm.overwrite_harddisk() Waiting for machine to bootup...{0}'.
                             format(settings.bootup_time_wait))
                    sleep(settings.bootup_time_wait)
                    return True

                else:
                    from error import app_exceptions
                    raise app_exceptions.UcmVirtualMachineException(
                        'cup.overwrite_harddisk() Machine has not started...')
            else:
                log.info(
                    'cup.overwrite_harddisk() Software install is not available yet. Retrying...')
                sleep(30)
                res = self.overwrite_harddisk(attempts=attempts + 1)
                if res:
                    log.info(
                        'cup.overwrite_harddisk() Waiting for machine to bootup...')
                    sleep(settings.media_check_wait)
                    return True
                else:
                    return False
                    # ucm.install_application() Error during software install
        else:
            log.error('cup.overwrite_harddisk() No text available')
            return False

    def reinitialize_harddisk(self):
        """

        :return:
        """
        try:
            if self.get_server().httpsPort is not None:
                url = 'https://' + self.get_server().server + ':' + str(
                    self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
            else:
                url = 'https://' + self.get_server().server + '/screen?id=' + \
                      self.get_virtualmachine().vmId

            log.info('cup.reinitialize_harddisk() URL: ' + url)
            img = image_recognition.get_screenshot(url=url,
                                                   username=self.get_server().username,
                                                   password=self.get_server().password,
                                                   httpsPort=self.get_server().httpsPort,
                                                   downloadPath=self.app.application_params['screen'])

            if img:
                log.info('cup.reinitialize_harddisk() Image obtained successfully')
            else:
                log.error('cup.reinitialize_harddisk() Unable to retrieve image')
                self.set_install_failed()
                return False

            res = image_recognition.processImage(
                self.app.application_params['screen'])
            if res:
                if image_recognition.is_prompt_ready(list=ucm_errors.reinitialize_error, image_to_text=res.lower(),
                                                     coefficient=10):
                    log.info(
                        'cuc.reinitialize_harddisk() Successful reinitialize disk')
                    # Verify port is opened
                    if general.check_network_connectivity(server=self.get_server().server,
                                                          vncPort=self.app.application_params['vnc_port'], mode='vnc'):
                        from subprocess import call
                        # vncdo -s 110.10.0.144::5901 key enter
                        log.info(
                            'cup.reinitialize_harddisk() Media check screen sending tab...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key tab'], shell=True)
                        log.info(
                            'cup.reinitialize_harddisk() Media check screen sending tab...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key tab'], shell=True)
                        log.info(
                            'cup.reinitialize_harddisk() Media check screen sending tab...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key tab'], shell=True)
                        log.info('cup.reinitialize_harddisk() sending Enter...')
                        call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                            'vnc_port'] + ' key enter'], shell=True)
                        return True

            log.warn('cup.reinitialize_harddisk() Image not found')
            return False

        except Exception, e:
            log.exception('cup.reinitialize_harddisk()' + str(e))
            return False

    def get_ip_information(self, ip_only=False, **kwargs):
        """

        :return:
        """

        log.info('cup.get_ip_information()')
        ip_info = []
        try:
            log.info('cup.get_ip_information() Answer File: ' +
                     self.app.application_params['answer_file'])
            if self.app.application_params['answer_file']:
                filename = self.app.application_params['answer_file']
                # Hostname
                if xml_processor.get_param_value(filename, 'LocalHostDHCP') == 'no':
                    # Get Hostname and IP Information from XML answerfile
                    if ip_only:
                        ip_info.append(xml_processor.get_param_value(
                            filename, 'LocalHostIP0'))
                    else:
                        ip_info.append(xml_processor.get_param_value(
                            filename, 'LocalHostName'))
                        ip_info.append(xml_processor.get_param_value(
                            filename, 'LocalHostIP0'))
                else:
                    log.error(
                        'cup.get_ip_information() DHCP set to Yes. Unable to get IP Address via Answer File')
            else:
                log.error('cup.get_ip_information() No answer file defined')

            # Return [hostname,ip_address]
            return ip_info

        except Exception, e:
            log.exception(e)

    def check_install_status(self):
        """
        # We will kick off installation, for 70 minutes
        # check for errors check Install time, we will wait 70 minutes
        :return:
        """
        log.info('cup.check_install_status()')
        skip_check = False
        elapsed_time = 0
        # 70 minutes to complete installation
        while elapsed_time < settings.install_timeout:
            log.info(
                'cup.check_install_status() ******************************************************')
            log.info('cup.check_install_status() Elapsed time: ' +
                     str(elapsed_time / 60) + ' minutes')
            res = self.check_install_progress(skip_check_tools=skip_check)

            """
                Returns  1 - vmware tools installed and ip address found| no errors found
                Returns  0 - vmware tools not installed in progress
                Returns -1 - vmware tools installed and ip address| errors found

            """
            if res == 2:
                log.info(
                    'cup.check_install_status() Installation in progress. VM tools installed. Time remaining: ' + str(
                        (settings.install_timeout - elapsed_time) / 60) + ' minutes')
                elapsed_time += 60
                sleep(60)
                # Fix issue #40
                gc.collect()
                skip_check = True

            elif res == 1:
                # HTTPS.
                log.info(
                    'cup.check_install_status() Installation has been successful No errors found')
                return True

            # Installation is in progress. Tools not running
            elif res == 0:
                log.info('cup.check_install_status() Installation in progress. Time remaining: ' + str(
                    (settings.install_timeout - elapsed_time) / 60) + ' minutes')
                elapsed_time += 60
                sleep(60)
                # Fix issue #40
                gc.collect()

            else:
                # < 0 Error
                log.error(
                    'cup.check_install_status() Installation error found. Terminating: ' + str(res))
                return False

        # Finished with Error
        log.warn(
            'cup.check_install_status() Install timeout exceeded allowed time. Check VM manually')
        return False

    def install_application(self):
        """

        :return:
        """

        # =========================================================
        # Disable Firewall to be able to send VNC commands
        # =========================================================

        log.info('cup.install_application Enabling gdbserver connections in firewall')
        if self.get_server().update_firewall(operation='true'):
            log.info(
                'cup.install_application Enabling gdbserver connections in firewall was successful')
        else:
            log.error(
                'cup.install_application() Enabling gdbserver connections in firewall failed')
            log.warn('cup.install_application() Continue installation...')

            # =========================================================
            # We will continue BEST EFFORT. And fail if we detect error
            # =========================================================

        log.info('cup.install_application() started. Checking VNC Connectivity')

        if self.get_virtualmachine().check_vnc_connectivity():
            log.info('cup.install_application() Success VNC connectivity verified.')

            # =========================================================
            # Skip media checks and overwrite hard disk
            # =========================================================

            if self.skip_media_check():

                # =========================================================
                # Skip Media check was successful. Wait to recognize hard disks
                # =========================================================

                from time import sleep
                from conf import settings
                log.info('ucm.install_application() Media check skipped. Please wait {0} seconds'.format(
                    settings.media_check_wait))
                sleep(settings.media_check_wait)

                # =========================================================
                # Overwrite hard disk
                # =========================================================

                if self.overwrite_harddisk(attempts=1):
                    log.info(
                        'cup.install_application() Software installation started...')
                    return True
                else:
                    log.error(
                        'cup.install_application() Error during software install')
                    self.set_install_failed()
                    return False

            else:
                log.error('cup.install_application() Error during media check')
                self.set_install_failed()
                return False
        else:
            log.error('cup.install_application() failed')
            self.set_install_failed()
            return False

    def read_screen_information_exponential_backoff(self, url):
        """

        :return:
        """

        for n in range(0, 5):
            try:
                return image_recognition.get_screenshot(
                    url=url,
                    username=self.get_server().username,
                    password=self.get_server().password,
                    httpsPort=self.get_server().httpsPort,
                    downloadPath=self.app.application_params['screen'])

            except IOError:
                log.exception("cup.read_screen_information_exponential_backoff() Cant read file/directory")
                return False

            except Exception:
                log.exception("cup.read_screen_information_exponential_backoff() Retrying {} times".format(n))
                sleep((2 ** n) + random.random())

        log.exception("cup.read_screen_information_exponential_backoff() The request never succeeded.")
        return False

    # read_screen_information
    def read_screen_information(self):
        """

        :return:
        """
        try:
            # Import errors known
            if self.get_server().httpsPort is not None:
                url = 'https://' + self.get_server().server + ':' + str(
                    self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
            else:
                url = 'https://' + self.get_server().server + '/screen?id=' + \
                      self.get_virtualmachine().vmId

            log.info('cup.read_screen_information() URL ' + url)
            img = self.read_screen_information_exponential_backoff(url=url)

            if not img:
                self.set_install_failed()
                log.info('cup.read_screen_information() Unable to get image')
                return -1

            res = image_recognition.processImage(
                self.app.application_params['screen'])

            # Return Error code when install fails
            log.info(
                'cup.read_screen_information() Checking if any errors during install')
            if image_recognition.is_prompt_ready(list=ucm_errors.cup_install_successful, image_to_text=res.lower(),
                                                 coefficient=10):
                log.info('cup.read_screen_information() Successful installation')
                return 1

            elif image_recognition.is_prompt_ready(list=ucm_errors.default_gateway_error, image_to_text=res.lower(),
                                                   coefficient=10):
                log.error('cup.read_screen_information() Error -1')
                return -1

            elif image_recognition.is_prompt_ready(list=ucm_errors.network_connectivity_error,
                                                   image_to_text=res.lower(),
                                                   coefficient=10):
                log.error('cup.read_screen_information() Error -2')
                return -2
            elif image_recognition.is_prompt_ready(list=ucm_errors.ntp_error, image_to_text=res.lower(),
                                                   coefficient=10):
                log.error('cup.read_screen_information() Error -3')
                return -3

            elif image_recognition.is_prompt_ready(list=ucm_errors.subscriber_error, image_to_text=res.lower(),
                                                   coefficient=10):
                log.info('cup.read_screen_information() Error -4')
                return -4

            elif image_recognition.is_prompt_ready(list=ucm_errors.undetermined_error, image_to_text=res.lower(),
                                                   coefficient=8):
                log.error('ucm.read_screen_information() Error -5')
                return -5

            # ***************  In progress....****************
            elif image_recognition.is_prompt_ready(list=ucm_errors.database_install_in_progress,
                                                   image_to_text=res.lower(),
                                                   coefficient=10):
                log.info(
                    'cup.read_screen_information() Application installation is in progress')
                return 0
            elif image_recognition.is_prompt_ready(list=ucm_errors.network_connectivity_checking,
                                                   image_to_text=res.lower(),
                                                   coefficient=10):
                log.info(
                    'cup.read_screen_information() Warning connectivity check still in progress...')
                return 0

            # =========================================================
            # No match found. Unknown Error/Message
            # =========================================================

            else:
                log.info('cup.read_screen_information No errors found continue...')
                if general.check_network_connectivity(server=self.get_server().server,
                                                      vncPort=self.app.application_params['vnc_port'], mode='vnc'):
                    from subprocess import call
                    log.info(
                        'cup.read_screen_information() Send VNC ctrl-C key to refresh image')
                    call(['vncdo -s ' + self.get_server().server + '::' + self.app.application_params[
                        'vnc_port'] + ' key ctrl-C'], shell=True)
                    return 0
                else:
                    log.warn(
                        'cup.read_screen_information VNC problem installation may continue')
                    return 0
            return 0

        except Exception, e:
            log.exception(e)
            return -4

    def check_duplicate_ip(self):
        """

        :return:
        """
        try:
            log.info('cup.check_duplicate_ip() Find all vm')
            vm_ids = self.get_server().find_vms(poweronOnly=True)

            if vm_ids is not None:
                for vm_id in vm_ids:
                    try:
                        log.info('cup.check_duplicate_ip() vmId: ' + str(vm_id))
                        from hypervisor.esxi import vm_operations
                        # Check for vm status ip
                        # address/hostname/toolsRunningStatus
                        cup_info = vm_operations.get_summary(server=self.get_server().server,
                                                             username=self.get_server().username,
                                                             password=self.get_server().password,
                                                             port=self.get_server().sshPort,
                                                             vmId=vm_id)

                        # Read UCM information
                        if cup_info is not None:
                            log.info(
                                'cup.check_duplicate_ip() Processing VM: ' + vm_id)
                            log.info(cup_info)
                            try:
                                # Verify guestToolsRunning
                                if 'guestToolsRunning' in cup_info[0].encode('utf-8'):
                                    log.info(
                                        'cup.check_duplicate_ip() guestToolsRunning gathering IP Information')
                                    ip_info = self.get_ip_information()
                                    log.info(ip_info)

                                    if len(ip_info) == 2 and len(cup_info) == 3:
                                        # ip_info: ['pub', '110.10.0.201'] cucm_info [u'guestToolsRunning', u'pub', u'110.10.0.201']
                                        # We match hostnames and IP information
                                        if ip_info[0] == cup_info[1] and ip_info[1] == cup_info[2]:
                                            log.error(
                                                'cup.check_duplicate_ip() VM Already configured using hostname and ip address vmId: ' + vm_id)
                                            return -1
                                        elif ip_info[1] == cup_info[2]:
                                            log.error(
                                                'cup.check_duplicate_ip() VM Already configured using ip address vmId: ' + vm_id)
                                            return -2
                                        elif ip_info[1] == cup_info[2]:
                                            log.error(
                                                'cup.check_duplicate_ip() VM Already configured using hostname vmId: ' + vm_id)
                                            return -3

                            except Exception, e:
                                log.exception(e)
                        else:
                            log.warn('cup.check_duplicate_ip() No VMs in ESXi')
                            return 0

                    except Exception, e:
                        log.exception(e)

            # Return my own IP information
            return self.get_ip_information()[1]

        except Exception, e:
            print e

    def check_install_progress(self, skip_check_tools=False, **kwargs):
        """

        :return:

        """

        try:
            log.info('cup.check_install_progress()')
            if skip_check_tools:
                res = self.read_screen_information()
                # ********* Error found terminate script *********
                if res < 0:
                    log.error(
                        'cup.check_install_progress() Install error detected')
                    self.set_install_failed()
                    return -1
                    # ****** Installation in progress...
                elif res == 0:
                    # Fix Issue #32
                    log.warn(
                        'cup.check_install_progress() Installation is in progress.')
                    return 2
                else:
                    log.info('cup.check_install_progress() No errors found.')
                    return 1

            cup_info = vm_operations.get_summary(server=self.get_server().server,
                                                 username=self.get_server().username,
                                                 password=self.get_server().password,
                                                 port=self.get_server().sshPort,
                                                 vmId=self.get_virtualmachine().vmId)
            # Read CUP information
            if cup_info is not None:
                log.info('cup.check_install_progress() Displaying info:')
                log.info(cup_info)
                try:
                    # Verify guestToolsRunning
                    from conf import settings
                    if 'guestToolsRunning' in cup_info[0].encode('utf-8'):
                        log.info(
                            'cup.check_install_progress() guestToolsRunning gathering IP Information')
                        # DHCP Support
                        if settings.dhcp_support:
                            # Hostname and IP are ready
                            if len(cup_info) == 3:
                                # Validate each field is populated
                                # [u'guestToolsRunning', u'pub', u'110.10.0.202']
                                if cup_info[0] and cup_info[1] and cup_info[2]:
                                    return 2
                            else:
                                log.info(
                                    'cup.check_install_progress() VMWare Tools are running. VM instance is not ready')
                                return 0

                        # Static IP Address
                        else:
                            # Get IP information from XML file
                            ip_info = self.get_ip_information()
                            if not ip_info:
                                log.info(
                                    'ucm.check_install_progress() No IP information')
                                return 0

                            if len(ip_info) == 2 and len(cup_info) == 3:
                                # We match hostnames and IP information
                                if ip_info[0] == cup_info[1] and ip_info[1] == cup_info[2]:

                                    # =========================================================
                                    # Info is ready
                                    # =========================================================
                                    # ip_info: ['pub', '110.10.0.201']
                                    # cucm_info [u'guestToolsRunning', u'pub', u'110.10.0.201']

                                    return 2

                                elif ip_info[0] == cup_info[1] and len(cup_info[2]) > 0:

                                    # =========================================================
                                    # Add IPv6 support in new CUCM 105
                                    # =========================================================
                                    return 2

                                else:

                                    # =========================================================
                                    # Info is not ready
                                    # =========================================================
                                    # [u'guestToolsRunning', u'', u'192.168.1.10']

                                    log.warn(
                                        'cup.check_install_progress() VMWare Tools are running. IP Info is not ready')
                                    log.warn(
                                        'Answer file information: ' + str(ip_info) + ' VM instance: ' + str(cup_info))
                                    return 0

                            # Check IP information
                            else:
                                log.info(
                                    'cup.check_install_progress() VMWare Tools are running. VM instance is not ready')
                                return 0

                    else:
                        log.info(
                            'cup.check_install_progress() VMWare Tools are not running. Check later...')
                        return 0

                except KeyError:
                    return 0
                except Exception, e:
                    log.exception(e)
                    return -1
            else:
                log.warn(
                    'cup.check_install_progress() VM information is not available. Checking ESXi host')
                if general.send_command(commandList=['ls -alh'],
                                        server=self.get_server().server,
                                        username=self.get_server().username,
                                        password=self.get_server().password,
                                        port=self.get_server().sshPort):
                    log.error(
                        'cup.check_install_progress() ESXi host is available. VM is not responding')
                    return -1
                else:
                    log.exception(
                        'cup.check_install_progress() ESXi host is not available.')
                    return -1

                    # VM is active

        except Exception, e:
            log.exception(str(e))
            return -1

    def add_single_quotes(self, string):
        """

        :param string:
        :return:
        """
        return '\'' + str(string) + '\''

    def add_double_quotes(self, string):
        """

        :param string:
        :return:
        """
        return '"' + str(string) + '"'

    def update_firewall(self, allow=False, **kkwargs):
        """

        :param allow:
        :param kkwargs:
        :return:
        """
        if self.get_server() is not None:
            log.info('cup.update_firewall disabling firewall')

            if allow:
                log.info('cup.update_firewall enabling firewall')
                if network_operations.update_firewall(server=self.server.server,
                                                      username=self.server.username,
                                                      password=self.server.password,
                                                      port=self.server.sshPort,
                                                      set='true'):
                    return True
            else:
                if network_operations.update_firewall(server=self.server.server,
                                                      username=self.server.username,
                                                      password=self.server.password,
                                                      port=self.server.sshPort):
                    return True
        else:
            log.error(
                'cup.update_firewall() disabling firewall failed. Not a valid ESXi')
            self.set_install_failed()
            return False

    def complete_installation(self):
        """

        :return:
        """
        try:
            log.info("cup.complete_installation() Post install tasks started...")

            # =========================================================
            # Updating VMX files
            # =========================================================

            log.info('cup.complete_installation() power off VM')
            # Check for vm status ip address/hostname/toolsRunningStatus
            res = vm_operations.power_off(server=self.get_server().server,
                                          username=self.get_server().username,
                                          password=self.get_server().password,
                                          port=self.get_server().sshPort,
                                          vmId=self.get_virtualmachine().vmId)
            if res:
                sleep(15)
                log.info('cup.complete_installation() Power off succeeded.')
                # Sleep to let the machine be off
            else:
                log.error(
                    'cup.complete_installation() Power off failed.')

            log.info("cup.complete_installation() Removing floppy drive and disabling VNC")

            esx_vmx_file = self.app.application_params['esx_vmx_file']

            command_list = []
            command_list.append('rm -rf ' + self.app.application_params['esx_floppy_file'])
            command_list.append('sed -i \'/RemoteDisplay.vnc.port/d\' ' + esx_vmx_file)
            command_list.append('sed -i \'/RemoteDisplay.vnc.enabled/d\' ' + esx_vmx_file)
            command_list.append('sed -i \'/floppy0.fileType/d\' ' + esx_vmx_file)
            command_list.append('sed -i \'/floppy0.fileName/d\' ' + esx_vmx_file)
            command_list.append('sed -i \'/floppy0.startConnected/d\' ' + esx_vmx_file)
            command_list.append('echo \'floppy0.startConnected = \"FALSE\"\' >> ' + esx_vmx_file)
            command_list.append('echo \'floppy0.fileType = \"device\" \' >> ' + esx_vmx_file)

            res = general.send_command(commandList=command_list,
                                    server=self.get_server().server,
                                    username=self.get_server().username,
                                    password=self.get_server().password,
                                    port=self.get_server().sshPort)
            if res is None:
                log.info(
                    'cup.complete_installation() VMX file updated succesfully')

            else:
                log.error(
                    'cup.complete_installation() VMX file updated failed')

            res = vm_operations.power_on(server=self.get_server().server,
                                         username=self.get_server().username,
                                         password=self.get_server().password,
                                         port=self.get_server().sshPort,
                                         vmId=self.get_virtualmachine().vmId)

            if res is None:
                log.info('cup.complete_installation Power on succeeded.')
            else:
                log.error(
                    'cup.complete_installation Power on failed.')
                return False

            from hypervisor.esxi import network_operations, service_operations

            if self.server.status == 0:
                log.info('cuc.complete_installation Disabling SSH service...')
                service_operations.ssh_service_activation(
                    server=self.server.server,
                    username=self.server.username,
                    password=self.server.password,
                    port=self.server.sshPort,
                    operation=0)

            log.info('cup.complete_installation disabling firewall')
            network_operations.update_firewall(server=self.server.server,
                                               username=self.server.username,
                                               password=self.server.password,
                                               port=self.server.sshPort)

            log.info('cup.complete_installation Removing Monitor port: ' +
                     str(self.app.application_params['vnc_port']))
            self.server.remove_vnc_port(
                self.app.application_params['vnc_port'])

            json_db = json.dumps(self.app.application_params)
            log.info('cup.complete_installation: JSON information:' + json_db)

            sqlquery = """
                    INSERT INTO installation_completed (job, is_completed, application_params, owner_id, fk_server, timestamp)"""

            content = "\'" + self.app.application_params['job'] + \
                      '\',True,\'' + \
                      json_db + "\'," + \
                      str(self.app.application_params['owner_id']) + ',' + str(self.server.id) + ',' + 'now()'

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()
            res = db_instance.insert(sqlquery, content)

            if res:

                # =========================================================
                # Install successful
                # =========================================================

                log.info(
                    'cup.complete_installation() Installation successfully completed')
                return True
            else:

                # =========================================================
                # Install failed
                # =========================================================

                log.exception(
                    'cup.complete_installation() Failed to insert record in database')
                return False

        except Exception, e:
            self.set_install_failed()
            log.exception(
                'cup.complete_installation() Failed to insert record in database' + e.message)
            return False

    def set_install_failed(self):
        """

        :return:
        """
        log.error('cup.set_install_failed Flag to True')
        self.install_failed = True

    def cancel_install(self):
        """

        :return:
        """
        log.info('cup.cancel_install() will be cancelled')

    def get_params(self):
        """

        :return:
        """
        return self.app.application_params

    def deploy_virtualmachine(self):
        """

        :return:
        """

        log.info('cup.deploy_virtualmachine()')
        try:
            # Create a new VirtualMachine
            if self.get_server() is not None:
                # Issue #37

                if self.get_server().find_duplicate_vm(self.app.application_params['hostname']):
                    log.error(
                        'cup.deploy_virtualmachine() Machine exists with same hostname')
                    return -2

                # Create new VirtualMachine instance
                log.info('cup.deploy_virtualmachine() Creating Object: ' +
                         str(self.app.application_params['hostname']))
                vm = VirtualMachine.VirtualMachine(
                    self.app.application_params['hostname'], '')

                # Define VM
                self.set_virtualmachine(vm)

                # Assign VNC Port
                log.info(self.app.application_params)

                if self.app.application_params['vnc_port']:
                    vm.vncPort = self.app.application_params['vnc_port']

                # Associate VM to parent ESXi Server
                vm.set_server(self.get_server())

                # Create VMX file
                vm_operations.create_vmx_file(self.get_server().get_hypervisor(),
                                              self.app.application_params[
                                                  'tools_vmx_file'],
                                              self.app.application_params['app_id'], self.app.application_params)
                # Transfer vmxFile to ESXi Server
                vm.load_vmx_file(self.app.application_params['tools_vmx_file'],
                                 self.app.application_params['esx_vmx_file'])
                # Create VMK Disk, this operation may take sometime
                vm.create_vmk_disk(self.app.application_params[
                                       'vmdk_path'], '80', 1)
                # Register VM
                vm.register_vm()
                # Get VM Id
                log.info('cup.deploy_virtualmachine() vmId: ' + str(vm.vmId))
                self.app.application_params['vmid'] = vm.vmId

                # Power on VM
                if vm.get_status():
                    res = vm.power_on()
                    log.info(
                        'cup.deploy_virtualmachine() Waiting for virtual machine to start...')
                    if res:
                        if self.app.application_params['job']:
                            job = self.app.application_params['job']
                            log.info('cup.deploy_virtualmachine() Updating host infrastructure')
                            job.update_info(self.get_server().id, vm.vmId)
                            server_json = json.dumps(job.server_info)
                        else:
                            server_json = '{}'
                            log.warn('cup.deploy_virtualmachine() Job not defined')

                        sqlquery = """UPDATE job SET host_information='""" + server_json + """' WHERE job.id=""" + str(
                            job.id)
                        helper.update_database(sqlquery)

                        # =========================================================
                        # get screen URL
                        # =========================================================

                        if self.get_server().httpsPort is not None:
                            url = 'https://' + self.get_server().server + ':' + str(
                                self.get_server().httpsPort) + '/screen?id=' + vm.vmId
                        else:
                            url = 'https://' + self.get_server().server + '/screen?id=' + vm.vmId

                        log.info('cup.deploy_virtualmachine() Screen ' + url)
                        log.info('cup.deploy_virtualmachine() Waiting for virtual machine to start...')
                        self.app.application_params['screen_url'] = url

                        # =========================================================
                        # Return vmId
                        # =========================================================

                        return int(vm.vmId)
                    else:
                        return -3
                else:
                    raise app_exceptions.UcmVirtualMachineException(
                        'cup.launch_virtualmachine() Unable to launch VM')

            else:
                log.error(
                    'cup.deploy_virtualmachine() Need to associate ESXi server')

            return -1

        except Exception, e:
            log.exception(str(e))
            raise

    def set_virtualmachine(self, vm):
        """

        :param vm:
        :return:
        """
        self.app.set_virtualmachine(vm)

    def get_virtualmachine(self):
        """

        :return:
        """
        return self.app.get_virtualmachine()

    def set_server(self, server):
        """

        :param server:
        :return:
        """
        self.server = server

    def get_server(self):
        """

        :return:
        """
        return self.server
