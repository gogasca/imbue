import warnings
import logging
import os
from catalogue.cisco.common.esxi import initialize_server
import re
from time import sleep
from alarms import send_sms
from billiard.exceptions import Terminated
from conf import logging_conf
from conf import settings
from catalogue.cisco.common.xml import xml_processor
from catalogue.application import Application
from celery.result import AsyncResult
from celery.task import task
from hypervisor.esxi import general
from hypervisor.esxi.file_operations import check_local_file
from hypervisor.esxi.file_operations import get_image_folder
from hypervisor.esxi import vm_operations
from shared import Lock
from shared import Report
from utils import db_utils
from utils import file_utils
from utils import helper
from utils import history
from utils import host_utils
from utils import validator

warnings.filterwarnings("ignore")


@task(bind=True, default_retry_delay=150, max_retries=3, throws=(Terminated,))
def imbue_uc(
        self,
        configuration=None,
        cluster=None,
        log_folder=None,
        log_file=None,
        report=None,
        job=None,
        esx=None,
        additional=None,
        **kwargs):
    """
    This module will select which application you want to install based on configuration type field
        and call proper module. After terminating will notify the user via SMS if SMS is enabled in user profile

    :param self:
    :param configuration:
    :param cluster:
    :param log_folder:
    :param log_file:
    :param report:
    :param job:
    :param esx:
    :param additional:
    :param kwargs:
    :return:
    """
    # =========================================================
    # Process UC server
    # =========================================================

    log = logging_conf.LoggerManager().getLogger("__app___", logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '---------------------------------------Initializing IMBUE Application ---------------------------------------')

    try:
        if self.request.id:
            log.info('install_uc() Active celery task id: ({})'.format(self.request.id))

        application_instance_exists = False
        error_detected = False
        lock = None
        remote_directory = None
        uc_application = None
        vm_response = 0

        # =========================================================
        # API request
        # =========================================================

        log.info('install_uc() API request: (' + job.reference + ') | {}'.format(configuration))

        if log_folder is None:
            log.info('install_uc() Creating local log directory: ' + settings.log_folder + job.reference)
            command = 'mkdir ' + settings.log_folder + job.reference

            # =========================================================
            # Check command was executed successfully
            # =========================================================

            res = general.run_command(command)
            if res == 0:
                log_folder = job.reference + '/'
                log_folder = settings.log_folder + log_folder
            else:
                log.warn(
                    'install_uc() Unable to create local log folder')
                return

        if report is None:
            report = Report.Report(settings.report_path, job.reference)
            if report.initialize():
                log.info('install_uc() Report initialized successfully')
                report.job_path = log_folder
            else:
                log.error('install_uc() Report initialization failed')
                report = None

        if not cluster:
            log.info('install_uc() Updating job install information')
            if host_utils.get_install_information(configuration, job.reference, 1):
                log.info('install_uc() Job information updated successfully')
            else:
                log.error('install_uc() Job information update failed')

        # =========================================================
        # Connect to Server
        # =========================================================

        # Change Task status. This will help to update cancellation process.
        if self.request.id:
            self.update_state(state='SERVER_VERIFY', meta={'status': 'Verifying server'})

        # Check Server network connectivity
        # TODO Validate Hard Disk Resources based on Application requirements.
        res = initialize_server.prepare_server(self, job, configuration, log_file, report)
        log.info('install_uc() Prepare server: {}'.format(res))
        if res:
            if res['type'] == 'OK':
                log.info('install_uc() Server connectivity was successful. Ready to install application')
                esx = res['esx']
                tools = res['tools']
                host_id = res['host_id']
                owner_id = res['owner_id']
            else:
                error_detected = True
                log.error('install_uc()  Install failed')
                if cluster:
                    return False
                else:
                    return {'status': res['status'], 'result': res['result']}
        else:
            error_detected = True
            log.error('install_uc()  Install failed')
            if cluster:
                return False
            else:
                return {'status': 'ERROR', 'result': -1}

        # =========================================================
        #  Creating lock
        # =========================================================

        if settings.check_install_in_progress:
            log.info('install_uc() Install started(). Acquiring lock...')
            lock = Lock.Lock(esx.name)
            if owner_id:
                lock.owner_id = owner_id
            if not lock.aquire():
                message = 'Max installs exceeded. Existing server install in progress. ' \
                          'Stop other installations or synchronize server'
                log.error('install_uc() ' + message)
                if report:
                    report.add_log(message)
                helper.generate_alert(job.id, message, job.reference)
                error_detected = True
                return

        # =========================================================
        #  Install UC application
        # =========================================================

        message = 'Application installation starting...'

        log.info('install_uc() ' + message)
        if report:
            report.add_log(message)

        # =========================================================
        #  Vendor Table
        #  cisco: 200 +
        #  ucm    001
        #  cuc    002
        #  cup    003
        #
        #  Example:   CUCM = 200001
        # =========================================================

        app_id = configuration['instance']['application']['type']
        app_name = settings.cisco_list[app_id]
        log.info('install_uc() Deploying application...')

        # Change Task status. This will help to update cancellation process.
        if self.request.id:
            self.update_state(state='PROGRESS', meta={'status': message})

        # =========================================================
        # Check UC instance request
        # =========================================================

        log.info('install_uc() Validating request: {0}'.format(configuration['instance']['application']))
        if 200001 <= app_id <= 200005:
            if validator.check_imbue_uc(
                    configuration['instance']['application'],
                    cluster,
                    configuration['instance']['application']['publisher'],
                    additional):

                log.info('install_uc() Valid UC server configuration')

            else:
                error_detected = True
                message = 'ERROR Invalid UC server configuration'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                return

        elif 200006 <= app_id <= 200008:
            log.info('install_uc() TelePresence type found')
        elif 200009:
            log.info('install_uc() CSPC type found')
        else:
            error_detected = True
            log.error('install_uc() Invalid type')
            message = 'ERROR Invalid UC server configuration'
            if report:
                report.add_log(message)
            log.error('install_uc() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Check if suffix is defined
        # =========================================================

        if app_id == 200001:

            # =========================================================
            # This is a CUCM
            # =========================================================
            message = 'Application [UCM] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'ucm_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.ucm_suffix + job.reference + '/'
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/ucm/' + job.reference + '/'

        elif app_id == 200002:

            # =========================================================
            # This is a CUC
            # =========================================================
            message = 'Application [CUC] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'cuc_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.cuc_suffix + job.reference + '/'
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/cuc/' + job.reference + '/'

        elif app_id == 200003:

            # =========================================================
            # This is a CUP
            # =========================================================
            message = 'Application [CUP] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'cup_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.cup_suffix + job.reference + '/'
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/cup/' + job.reference + '/'

        elif app_id == 200006:

            # =========================================================
            # This is a VCS
            # =========================================================
            message = 'Application [VCS] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'vcs_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.vcs_suffix
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/vcs/' + job.reference + '/'

        elif app_id == 200007:

            # =========================================================
            # This is a TPS
            # =========================================================
            message = 'Application [TPS] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'tps_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.tps_suffix
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/tps/' + job.reference + '/'

        elif app_id == 200008:

            # =========================================================
            # This is a Conductor
            # =========================================================
            message = 'Application [CONDUCTOR] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'cond_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.cond_suffix
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/cond/' + job.reference + '/'

        elif app_id == 200009:

            # =========================================================
            # This is a CSPC
            # =========================================================
            message = 'Application [CSPC] will be installed'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)
            if hasattr(settings, 'cspc_suffix'):
                remote_directory = configuration['instance']['application'][
                                       'directory'] + settings.cspc_suffix
            else:
                remote_directory = configuration['instance']['application'][
                                       'directory'] + '/uc/cspc/' + job.reference + '/'

        else:
            log.error('install_uc() Invalid application')
            error_detected = True
            return

        # =========================================================
        # Check remote install directory is valid
        # =========================================================

        if not validator.check_folder(remote_directory):
            error_detected = True
            return

        # =========================================================
        # Verify API request and ESX server is defined.
        # =========================================================

        if esx:
            # =========================================================
            # Connect using VMware API and obtain datastore information
            # =========================================================
            log.info(
                'install_uc() Valid Server object. Gathering datastore details')
            datastore_list = esx.discover_datastores()
            if report:
                message = 'Server name: ' + esx.name
                report.add_log(message)

            if datastore_list:
                log.info('install_uc() Datastores connected to ESXi server:')
                log.info(datastore_list)
                esx.set_datastore(datastore_list)
                esx.get_datastores_details()
                esx.get_uuid()
            else:
                error_detected = True
                message = 'ERROR Unable to obtain datastore list'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                return

        else:
            error_detected = True
            message = 'ERROR No valid Server'
            if report:
                report.add_log(message)
            log.error('install_uc() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Verify we can read ESXi file system
        # =========================================================

        if esx.initialize_filesystem():
            log.info('install_uc() SSH Access to server is available')
        else:
            error_detected = True
            message = 'ERROR Unable to access server via SSH'
            if report:
                report.add_log(message)
            log.error('install_uc() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Check if install flag is enabled.
        # =========================================================

        if 'is_installed' in configuration['instance']['application']['params']:
            if configuration['instance']['application']['params']['is_installed']:
                message = 'Application already installed.'
                log.warn(
                    'install_uc() ' + message + 'is_installed=True')
                sqlquery = """UPDATE job SET status=1,description='[""" + settings.cisco_list[app_id] + \
                           """] Skip install. Server already installed' WHERE job.id=""" + \
                           str(job.id)
                db_utils.update_database(sqlquery)
                if report:
                    report.add_log(message)
                return
            else:
                log.info('install_uc() Application will be installed: is_installed=False')

        # =========================================================
        # Create Remote Directory in ESXi server and perform work
        # =========================================================

        sqlquery = "UPDATE job SET working_directory='" + remote_directory + "' WHERE job.id=" + str(job.id)
        db_utils.update_database(sqlquery)
        if esx.create_working_directory(esx.datastore, remote_directory):
            log.info('install_uc() Remote directory was created successfully')

        # =========================================================
        # Unable to create Directory
        # =========================================================

        else:
            error_detected = True
            message = 'ERROR Unable to create remote directory'
            if report:
                report.add_log(message)
            log.error('install_uc() ' + message)

            helper.generate_alert(job.id, message, job.reference)
            sqlquery = """UPDATE server SET status=-1 WHERE server.name='""" + \
                       str(esx.name) + "'"
            db_utils.update_database(sqlquery)
            return

        # =========================================================
        # Start installation. Populate common parameters
        # =========================================================

        app_params = dict()
        app_params['app_id'] = app_id
        app_params['name'] = app_name
        app_params['vendor'] = 'Cisco Systems, Inc'
        app_params['hypervisor'] = esx.get_hypervisor()
        app_params['location'] = remote_directory
        app_params['host_id'] = host_id
        app_params['hostname'] = configuration['instance']['application']['params']['hostname']
        app_params['ip'] = configuration['instance']['application']['params']['ip']
        app_params['mask'] = configuration['instance']['application']['params']['mask']
        app_params['gw'] = configuration['instance']['application']['params']['gw']
        # TODO provide template to install different UC sizes.
        app_params['size'] = settings.default_ova_size.__str__()
        app_params['owner_id'] = owner_id

        # Assign Default VM Network. Fix for Issue #35
        if 'esx_network' in configuration['instance']['application']:
            app_params['network_name'] = configuration['instance']['application']['esx_network']
        else:
            app_params['network_name'] = ''

        if job:
            log.info('install_uc() Job exists')
            app_params['job'] = job
        else:
            log.warn('install_uc() No job assigned')

        log.info(configuration)

        # =========================================================
        # Define Size
        # =========================================================

        if 'size' in configuration['instance']['application']:
            ova = configuration['instance']['application']['size']
            if isinstance(ova, int):
                app_params['size'] = str(configuration['instance']['application']['size'])
            else:
                log.error('install_uc() Invalid size parameter. Must be integer value')

        log.info('install_uc() OVA size {0}'.format(app_params['size']))

        if 200001 <= app_id <= 200005:

            # =========================================================
            # Copy files to log folder
            # =========================================================

            app_params['publisher'] = configuration['instance']['application']['publisher']
            app_params['version'] = configuration['instance']['application']['version']

            message = "Application details: " + app_params['hostname'] + "|" + app_params['ip'] + '|' + \
                      app_params['version']
            if report:
                report.add_log(message)

            if log_folder:

                # =========================================================
                # We need to move Answer file to log folder
                # If clusterFile, we move it too
                # =========================================================

                if check_local_file(configuration['instance']['application']['answer_file']):

                    log.info('install_uc() Looking for existing Platform config file:')
                    head, tail = os.path.split(configuration['instance']['application']['answer_file'])
                    old_file = log_folder + tail
                    new_file = log_folder + tail + '.pub'
                    if check_local_file(old_file):
                        log.info('install_uc() Back up platform config file:')
                        general.run_command('mv ' + old_file + ' ' + new_file)

                    log.info('install_uc() Copy answer file to log folder: ')
                    res = general.run_command(
                        'cp ' +
                        configuration['instance']['application']['answer_file'] +
                        ' ' +
                        log_folder)

                    if res == 0:
                        log.info('install_uc() Answer file copied to log folder')
                        head, tail = os.path.split(
                            configuration['instance']['application']['answer_file'])
                        app_params['answer_file'] = log_folder + tail
                        log.info(
                            'install_uc() Answer file path: ' + app_params['answer_file'])
                    else:
                        log.error('install_uc() Unable to copy answer file')
                        error_detected = True
                        return
                else:
                    message = 'Critical System error. Please try again later or contact Parzee'
                    log.error('install_uc() Answer file does not exist in specified path. Will clean up folder')
                    if esx.delete_working_directory(remote_directory):
                        log.warn('install_uc() Remote directory was deleted successfully')
                    error_detected = True
                    helper.generate_alert(job.id, message, job.reference)
                    return

                if cluster:
                    if 'cluster_file' in configuration['instance']['application']:
                        if check_local_file(configuration['instance']['application']['cluster_file']):
                            res = general.run_command(
                                'cp ' +
                                configuration['instance']['application']['cluster_file'] +
                                ' ' +
                                log_folder)
                            if res == 0:
                                head, tail = os.path.split(
                                    configuration['instance']['application']['cluster_file'])
                                app_params['cluster_file'] = log_folder + tail
                                log.info(
                                    'install_uc() Cluster file path: ' +
                                    app_params['cluster_file'])
                            else:
                                log.error(
                                    'install_uc() Unable to copy cluster file')
                                error_detected = True
                                return
                        else:
                            log.error('install_uc() Invalid path in cluster file')
                            error_detected = True
                            return

                    else:
                        log.warn(
                            'install_uc() Subscriber in cluster. Cluster file not in configuration')

            else:
                log.error('install_uc() No log folder is present')
                error_detected = True
                return

            # =========================================================
            # Validate version file
            # =========================================================

            if xml_processor.validate_xml_version(
                    app_params['answer_file'],
                    app_params['version']):
                log.info(
                    'install_uc() Version request {0} match answer file'.format(
                        app_params['version']))
            else:
                log.error(
                    'install_uc() Version request error. Request doesnt match Answer file')

            # =========================================================
            # Update Answer file with parameters
            # =========================================================

            if helper.update_xml(
                    configuration['instance']['application']['params'],
                    app_params):
                log.info('install_uc() Answer file updated successfully')
            else:
                message = 'Answer file update failed'
                log.error('install_uc() {}'.format(message))
                helper.generate_alert(job.id, message, job.reference)
                error_detected = True
                return

            # =========================================================
            # Update Cluster file #Fix Issue 165
            # =========================================================

            if cluster:
                if additional:
                    log.info('install_uc() Updating answer file for subscribers')
                    if app_id == 200001 == additional['instance']['application']['type']:
                        if app_params['publisher']:
                            log.info('install_uc() CUCM cluster. Publisher')
                            if helper.update_cluster_xml(
                                    configuration['instance']['application']['params'],
                                    additional['instance']['application']['params'],
                                    app_params['cluster_file']):
                                log.info('install_uc() Cluster file updated successfully')

                            else:
                                log.error('install_uc() Cluster file update failed')
                                sqlquery = """UPDATE job SET description='Cluster file update failed' WHERE job.id=""" + \
                                           str(job.id)
                                db_utils.update_database(sqlquery)
                                error_detected = True
                                return
                        else:
                            log.info('install_uc() CUCM cluster. Subscriber')
                            if helper.add_publisher_information(
                                    additional['instance']['application']['params'],
                                    app_params):
                                log.info('install_uc() Subscriber answer file updated successfully')
                            else:
                                log.error('install_uc() CUCM Subscriber answer file update failed')
                                sqlquery = """UPDATE job SET description='Subscriber configuration update failed' WHERE job.id=""" + \
                                           str(job.id)
                                db_utils.update_database(sqlquery)
                                error_detected = True
                                return

                    # CUCM + CUC | CUCM + CUP.
                    # All are publishers. Just update CUC and CUP with CUCM information.
                    if (app_id == 200002 or app_id == 200003) and (
                                additional['instance']['application']['type'] == 200001):
                        if app_params['publisher']:
                            log.info('install_uc() CUCM+CUC/CUCM+CUP')
                            if helper.add_publisher_information(
                                    additional['instance']['application']['params'],
                                    app_params):
                                log.info('install_uc() Subscriber answer file updated successfully')
                            else:
                                log.error('install_uc() CUCM + CUC | CUCM + CUP Subscriber answer file update failed')
                                sqlquery = """UPDATE job SET description='Subscriber configuration update failed' WHERE job.id=""" + \
                                           str(job.id)
                                db_utils.update_database(sqlquery)
                                error_detected = True
                                return

                else:  # Just me, will install subs in the future
                    if helper.update_cluster_xml_single(
                            configuration['instance']['application']['params'],
                            app_params['cluster_file']):
                        log.info('install_uc() Cluster file updated successfully')

                    else:
                        log.error('install_uc() Cluster file update failed')
                        sqlquery = """UPDATE job SET description='Cluster file update failed' WHERE job.id=""" + \
                                   str(job.id)
                        db_utils.update_database(sqlquery)
                        error_detected = True
                        return

            else:
                log.info('install_uc() Not a cluster installation')

            # =========================================================
            # Floppy image processing. # We move the file to this ESX location
            # =========================================================

            app_params['floppy_image'] = job.reference + '.img'
            app_params['esx_floppy_file'] = app_params['location'] + settings.floppy_image_name

            # =========================================================
            # Tools server information
            #  =========================================================

            app_params['tools_floppy_folder'] = settings.tools_floppy_folder
            app_params['tools_floppy_file'] = settings.tools_floppy_folder + job.reference + '.img'

            # =========================================================
            # Temporary log repository
            # =========================================================

            if log_folder:
                app_params['local_floppy_file'] = log_folder + 'floppy.img'
            else:
                log.warn(
                    'install_uc() No log folder defined. Using default settings: local_floppy_file')
                app_params['local_floppy_file'] = settings.local_floppy_image

            app_params['vmdk'] = app_params['hostname'] + '.vmdk'
            app_params['vmdk_path'] = app_params['location'] + app_params['vmdk']

            # =========================================================
            # Find correct ISO based on CUCM version
            # =========================================================

            try:
                # =========================================================
                # Get a list of ISO files, folder and remote_iso files
                # =========================================================
                iso_pattern = None
                iso_image = file_utils.get_iso_file(configuration, app_params)
                image_dir = get_image_folder(app_params['host_id'])
                image_files_found = esx.get_image_files(directory=image_dir, return_list=True)

                log.info('install_uc() iso_image file: {0}'.format(iso_image))
                log.info('install_uc() image_directory: {0}'.format(image_dir))
                log.info('install_uc() iso_files_found: {0}'.format(image_files_found))

                if iso_image:
                    log.info('install_uc() Image: {0}'.format(iso_image))
                    if iso_image in image_files_found:
                        app_params['bootable_iso'] = image_dir + iso_image

                    else:
                        error_detected = True
                        message = 'ERROR Not able to found image file {0} in folder: {1}'.format(
                            iso_image, image_dir)
                        if report:
                            report.add_log(message)
                        log.error('install_uc() ' + message)
                        helper.generate_alert(job.id, message, job.reference)
                        return

                else:
                    log.info('install_uc() Trying to find latest ISO file in server')
                    if app_id == 200001:

                        # =========================================================
                        # CUCM
                        # =========================================================

                        if settings.ucm_version_pattern[configuration['instance']['application']['version']]:
                            iso_pattern = settings.ucm_version_pattern[
                                configuration['instance']['application']['version']]

                    elif app_id == 200002:

                        # =========================================================
                        # CUC
                        # =========================================================

                        if settings.cuc_version_pattern[configuration['instance']['application']['version']]:
                            iso_pattern = settings.cuc_version_pattern[
                                configuration['instance']['application']['version']]

                    elif app_id == 200003:

                        # =========================================================
                        # CUP
                        # =========================================================

                        if settings.cup_version_pattern[configuration['instance']['application']['version']]:
                            iso_pattern = settings.cuc_version_pattern[
                                configuration['instance']['application']['version']]
                    else:
                        iso_pattern = None

                    # Try to find pattern in Settings directory. If we can find version we proceed
                    if iso_pattern:
                        log.info(
                            'install_uc() Trying to find latest image file: {0}'.format(iso_pattern))
                        valid_files = []
                        for iso_file_found in image_files_found:
                            if re.match(iso_pattern, iso_file_found):
                                valid_files.append(iso_file_found)

                        if len(valid_files) > 0:
                            app_params['bootable_iso'] = image_dir + \
                                                         sorted(valid_files, reverse=True)[0]
                            log.info('install_uc() ISO file found: {0}'.format(
                                app_params['bootable_iso']))
                            message = 'ISO file: ' + app_params['bootable_iso']
                            if report:
                                report.add_log(message)
                        else:
                            error_detected = True
                            message = 'ERROR Not Valid ISO Files matched pattern: ' + \
                                      configuration['instance'][
                                          'application']['version']
                            if report:
                                report.add_log(message)
                            log.error('install_uc() ' + message)
                            helper.generate_alert(job.id, message, job.reference)
                            return

                    else:
                        error_detected = True
                        message = 'ERROR Not Valid ISO Files pattern found for version: ' + \
                                  configuration['instance']['application']['version']
                        log.error('install_uc() ' + message)
                        if report:
                            report.add_log(message)
                        helper.generate_alert(job.id, message, job.reference)

                        return

            except Exception as exception:
                error_detected = True
                log.exception(str(exception))

            # =========================================================
            # Update configuration to use log folder.
            # =========================================================

            if log_folder:
                app_params['tools_vmx_file'] = log_folder + \
                                               app_params['hostname'] + '.vmx'
            else:
                log.warn(
                    'install_uc() No log folder defined. Using default settings: tools_vmx_file')
                app_params['tools_vmx_file'] = settings.vmxPath + \
                                               app_params['hostname'] + '.vmx'

            app_params['esx_vmx_file'] = app_params[
                                             'location'] + app_params['hostname'] + '.vmx'

            if log_folder:
                app_params['screen'] = log_folder + 'screen'
            else:
                log.warn(
                    'install_uc() No log folder defined. Using default settings: screen')
                app_params['screen'] = settings.screen

            # =========================================================
            # VNC Port definition
            # =========================================================

            try:

                log.info('install_uc() Getting Monitor port from DB: ')
                # esx.vnc_ports is the DB load from VNC information
                res = validator.check_vnc_port(esx.vnc_ports)

                if res == 1:  # Return 1 when Single port defined in server
                    # Sync VNC ports.
                    # Login to ESXi via SSH and allocate VNC ports in use.
                    vnc_ports_inuse = esx.allocate_vnc_ports(returnList=True)
                    for vnc_port_inuse in vnc_ports_inuse:
                        if esx.add_vnc_port(vnc_port_inuse):
                            app_params['vnc_port'] = str(int(esx.vnc_ports))

                elif res == 2:  # Range of Ports
                    esx.allocate_vnc_ports()  # Login via SSH and allocate VNC ports in use.
                    port_range = range(int(esx.vnc_ports.split(':')[0]), 1 + int(esx.vnc_ports.split(':')[1]))
                    port_found = False
                    log.info('install_uc() Monitor ports: ')
                    port_range = sorted(port_range, key=int)
                    for port in port_range:
                        if esx.add_vnc_port(port):
                            app_params['vnc_port'] = str(port)
                            port_found = True
                            break
                    if not port_found:
                        log.warn(
                            'install_uc() DB Invalid port defined in range. Using default Monitor port.')
                        app_params[
                            'vnc_port'] = settings.defaultVNCPort
                else:
                    log.warn(
                        'install_uc() DB Invalid port defined in range. Using default Monitor port.')
                    app_params['vnc_port'] = settings.defaultVNCPort

                message = 'Monitor port: ' + app_params['vnc_port']
                if report:
                    report.add_log(message)

            except Exception as exception:
                error_detected = True
                message = 'ERROR Unable to get Monitor port'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                log.exception(
                    'install_uc() Unable to get Monitor port: ' + str(exception))

                return

            # =========================================================
            # Create new Application logical instance
            # =========================================================

            application_instance = Application.Application(app_name, app_params)
            application_instance_exists = True

            if app_id == 200001:

                # =========================================================
                # Create Cisco CallManager instance
                # =========================================================

                from catalogue.cisco.voice.ucm import ucm as uc_app
                uc_application = uc_app.ucm(application_instance)

            elif app_id == 200002:

                # =========================================================
                # Create CUC instance
                # =========================================================

                from catalogue.cisco.voice.cuc import cuc as uc_app
                uc_application = uc_app.cuc(application_instance)

            elif app_id == 200003:

                # =========================================================
                # Create CUP instance
                # =========================================================

                from catalogue.cisco.voice.cup import cup as uc_app
                uc_application = uc_app.cup(application_instance)
            else:
                error_detected = True
                return

            log.info('install_uc() Creating application {0}'.format(
                settings.cisco_list[app_id]))

            # =========================================================
            # Define ESX server
            # =========================================================

            uc_application.set_server(esx)

            # =========================================================
            # Check duplicate IP before installing  Issue #42
            # =========================================================

            ip_information = uc_application.check_duplicate_ip()
            log.info('install_uc() VM IP address result: {}'.format(ip_information))

            if ip_information < 0:
                error_detected = True
                message = 'ERROR Invalid network information. Existing VM using configured IP info'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                return

            # =========================================================
            # Deploy floppy drive
            # =========================================================

            if uc_application.deploy_floppy(tools, esx):  # Fix Issue 44
                log.info('install_uc() Floppy drive deployed successfully')
            else:
                error_detected = True
                message = 'ERROR Unable to deploy Floppy drive'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                return

            message = 'Virtual Machine deployment is in progress...'
            log.info('install_uc() ' + message)
            if report:
                report.add_log(message)

            # =========================================================
            # VM is being created.
            # =========================================================

            if self.request.id:
                self.update_state(state='SERVER_BUSY', meta={'status': 1})

            vm_response = uc_application.deploy_virtualmachine()

            # Returns VM
            if vm_response > 0:

                log.info('install_uc() Virtual machine started: {}'.format(vm_response))

                # =========================================================
                # Machine was created. Insert it into Database
                # =========================================================

                try:
                    if self.request.id:
                        self.update_state(state='PROGRESS', meta={'status': message})

                    log.info(
                        'install_uc() Inserting VirtualMachine. Application id {0} User {1} '.format(app_id,
                                                                                                     app_params[
                                                                                                         'owner_id']))
                    if 'screen_url' in uc_application.app.application_params:
                        log.info('install_uc() VM Monitor URL: ' + uc_application.app.application_params['screen_url'])
                        screen_url = uc_application.app.application_params['screen_url']

                    # =========================================================
                    # Update table request
                    # =========================================================

                    res = history.save_history(app_params, return_id=True)
                    if res:
                        uc_application.app.application_params['installation_request'] = res
                        log.info('install_uc() Installation saved in installation requests...')

                        # Add vmid
                        history.add_vm(uc_application.app.application_params)

                    else:
                        error_detected = True
                        message = 'ERROR Installation not saved in installation requests'
                        if report:
                            report.add_log(message)
                        log.error('install_uc() ' + message)
                        helper.generate_alert(job.id, message, job.reference)
                        # Reverting installation
                        uc_application.revert_installation()
                        return

                    if vm_operations.insert_vm(
                            vmId=vm_response,
                            vm_hostname=app_params['hostname'],
                            status=1,
                            description=job.reference,
                            ip_information=ip_information,
                            uuid='',
                            app_id=app_id,
                            power_state=True,
                            host_id=app_params['host_id'],
                            directory=app_params['location'],
                            url=screen_url):
                        log.info('install_uc() Virtual machine DB insertion was successful')

                        # =========================================================
                        # Wait for VM to bootup
                        # =========================================================

                        log.info('install_uc() Waiting for VM machine to start')
                        sleep(settings.vm_startup)

                    else:
                        error_detected = True
                        message = 'ERROR VM insertion failed'
                        if report:
                            report.add_log(message)
                        log.error('install_uc() ' + message)
                        helper.generate_alert(job.id, message, job.reference)
                        return

                except Exception as exception:
                    log.exception(str(exception))
                    error_detected = True
                    return

            elif vm_response == -1:
                error_detected = True
                message = 'ERROR VM deployment failed'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                # Delete folder and clean up
                uc_application.revert_installation()
                if self.request.id:
                    self.update_state(state='SERVER_FAILED', meta={'status': -1})
                return

            elif vm_response == -2:
                error_detected = True
                message = 'ERROR VM already exists'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                esx.delete_working_directory(remote_directory)
                if self.request.id:
                    self.update_state(state='SERVER_FAILED', meta={'status': -1})
                return

            elif vm_response == -3:
                error_detected = True
                message = 'ERROR Unable to deploy virtual machine. Power on failed'
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                uc_application.revert_installation()
                if self.request.id:
                    self.update_state(state='SERVER_FAILED', meta={'status': -1})
                return
            else:

                error_detected = True
                message = 'ERROR Unable to deploy VM. Unknown Error'
                if report:
                    report.add_log(message)
                log.error('install_uc() Invalid error vm_id: {}'.format(vm_response))
                esx.delete_working_directory(remote_directory)
                if self.request.id:
                    self.update_state(state='SERVER_FAILED', meta={'status': -1})

                return

            # =========================================================
            # Install UC application
            # =========================================================

            try:

                # =========================================================
                # Skip media check and overwrite harddisk
                # =========================================================
                message = 'Installation started'
                if report:
                    report.add_log(message)
                sqlquery = """UPDATE job SET description='""" + \
                           message + """' WHERE job.id=""" + str(job.id)
                db_utils.update_database(sqlquery)

                if uc_application.install_application():
                    log.info('Installation is in progress. Please wait...')
                else:
                    error_detected = True
                    message = 'ERROR Installation error'
                    if report:
                        report.add_log(message)
                    log.error('install_uc() ' + message)
                    helper.generate_alert(job.id, message, job.reference)
                    # Reverting installation
                    uc_application.revert_installation()
                    return

                # =========================================================
                # Reinitialize Hard Disk in Version 11+.
                # =========================================================

                log.info(
                    'install_uc() UC version {0} Check if reinitialize is required.'.format(
                        app_params['version']))

                if uc_application.reinitialize_harddisk():
                    log.info('install_uc() Reinitialize was required. Please wait...')
                    sleep(60)
                else:
                    log.warn('install_uc() Reinitialize was not required')

                log.info('deploy_uc UC version {0}'.format(app_params['version']))

                # =========================================================
                # Verify install is in progress # DO NOT SHARE
                # =========================================================

                install_result = uc_application.check_install_status()
                log.info('install_uc() Install result: ' + str(install_result))

                if install_result:
                    log.info(
                        'install_uc() Installation completed. Performing post install tasks')
                    if uc_application.complete_installation():
                        log.info('install_uc() Post install task completed successfully')
                    else:
                        log.warn(
                            'install_uc() An error may have occurred during Post install task')
                        error_detected = True

                    # =========================================================
                    # Successful installation
                    # =========================================================
                    message = 'Installation has been completed successfully'
                    log.info('install_uc() ' + message)
                    if report:
                        report.add_log(message)
                    cellphone = helper.get_user_information(owner_id=owner_id)
                    send_sms.send_sms_alert(
                        destination_number=cellphone,
                        body='[' + settings.cisco_list[app_id].upper() +
                             '] UC Installation has been completed successfully: ' +
                             str(job.reference))

                else:

                    # =========================================================
                    # Failed installation
                    # =========================================================

                    error_detected = True
                    message = 'ERROR Installation failed'
                    if report:
                        report.add_log(message)
                    log.error('install_uc() ' + message)
                    helper.generate_alert(job.id, message, job.reference)

                    # =========================================================
                    # Reverting installation
                    # =========================================================

                    uc_application.revert_installation()
                    return

            except Exception as exception:
                log.exception(exception.__repr__())
                error_detected = True
                return

        # =========================================================
        # OVA based installations. Install Video device/ CSPC
        # =========================================================

        if 200006 <= app_id <= 200009:

            app_params['annotation'] = configuration['instance']['application']['params']['annotation']
            app_params['datastore'] = configuration['instance']['application']['datastore']
            app_params['ova_file'] = configuration['instance']['application']['ova_file']
            app_params['vm_path'] = esx.get_datastore_path(app_params['datastore']) + '/' + app_params['hostname']
            app_params['esx_vmx_file'] = app_params['vm_path'] + '/' + app_params['hostname'] + '.vmx'

            message = "Application details: " \
                      + app_params['hostname'] \
                      + "|" + app_params['ip'] \
                      + '|' + app_params['ova_file']

            if report:
                report.add_log(message)

            # Capture screen
            if log_folder:
                app_params['screen'] = log_folder + 'screen'
            else:
                log.warn(
                    'install_uc() No log folder defined. Using default settings: screen')
                app_params['screen'] = settings.screen

            # =========================================================
            # Create application instance
            # =========================================================

            application_instance = Application.Application(app_name, app_params)
            application_instance_exists = True

            # =========================================================
            # Save installation_request
            # =========================================================

            if app_id == 200006:
                log.info('install_uc() Install Cisco Telepresence instance')
                from catalogue.cisco.video.vcs import vcs as uc_app
                uc_application = uc_app.Vcs(application_instance)

            if app_id == 200007:
                log.info('install_uc() Install Cisco Telepresence Server instance')
                from catalogue.cisco.video.tps import tps as uc_app
                uc_application = uc_app.TelepresenceServer(application_instance)

            if app_id == 200008:
                log.info('install_uc() Install Cisco Telepresence Conductor instance')
                from catalogue.cisco.video.conductor import cond as uc_app
                uc_application = uc_app.Conductor(application_instance)

            if app_id == 200009:
                log.info('install_uc() Install Cisco Services Platform Collector instance')
                from catalogue.cisco.platform.cspc import cspc as uc_app
                uc_application = uc_app.Cspc(application_instance)
                # CSPC requires admin_password support
                app_params['admin_password'] = configuration['instance']['application']['params']['admin_password']

            # =========================================================
            # Define ESXi Server and then Deploy OVA
            # =========================================================

            uc_application.set_server(esx)
            install_result = uc_application.install(app_params,
                                                    configuration['instance']['application']['params'],
                                                    False,
                                                    job,
                                                    esx)
            vm_response = install_result

            # =========================================================
            # Check install result:
            #
            # -1 Unable to deploy OVF
            # -2 VMX file update
            # -3 VM status error
            # -4 Unable to insert VM in database
            # -5 Unable to send sequences
            # =========================================================

            if install_result > 0:

                if self.request.id:
                    self.update_state(state='PROGRESS', meta={'status': message})

                log.info('install_uc() Inserting VM. Application id {0} User {1} '.format(app_id,
                                                                                          app_params['owner_id']))

                if 'screen_url' in uc_application.app.application_params:
                    log.info('install_uc() VM Monitor URL: ' + uc_application.app.application_params['screen_url'])

                res = history.save_history(app_params, return_id=True)
                if res:
                    log.info('install_uc() Installation saved in installation requests...')
                    app_params['installation_request'] = res
                else:
                    error_detected = True
                    message = 'ERROR Installation not saved in installation requests...'
                    if report:
                        report.add_log(message)
                    log.error('install_uc() ' + message)
                    helper.generate_alert(job.id, message, job.reference)
                    return

                if uc_application.complete_installation():
                    log.info(
                        'install_uc() Post install task completed successfully')
                    cellphone = helper.get_user_information(owner_id=owner_id)
                    send_sms.send_sms_alert(destination_number=cellphone,
                                            body='[' + settings.cisco_list[
                                                app_id].upper() + '] OVA deployment completed successfully: ' + str(
                                                job.reference))
                    message = 'OVA deployment completed'
                    sqlquery = """UPDATE job SET description='""" + message + """' WHERE job.id=""" + str(job.id)
                    db_utils.update_database(sqlquery)
                    log.info('install_uc() OVA deployment completed successfully')
                else:
                    message = 'Post-Installation error'
                    sqlquery = """UPDATE job SET description='""" + message + """' WHERE job.id=""" + str(job.id)
                    db_utils.update_database(sqlquery)
                    log.warn(
                        'install_uc() An error may have occurred during Post install task')
                    error_detected = True

                history.add_vm(uc_application.app.application_params)

            else:
                log.error('install_uc() OVA deployment failed')
                error_detected = True
                message = 'ERROR OVA deployment failed'
                sqlquery = """UPDATE job SET description='""" + message + """' WHERE job.id=""" + str(job.id)
                db_utils.update_database(sqlquery)
                if report:
                    report.add_log(message)
                log.error('install_uc() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                cellphone = helper.get_user_information(owner_id=owner_id)
                send_sms.send_sms_alert(destination_number=cellphone,
                                        body='[' + settings.cisco_list[
                                            app_id].upper() + '] OVA deployment failed: ' + str(
                                            job.reference))
                if install_result == -1:
                    uc_application.revert_installation(power_off=False,
                                                       destroy=False,
                                                       delete_directory=app_params['vm_path'])
                elif install_result == -2:
                    uc_application.revert_installation(power_off=False,
                                                       destroy=False,
                                                       delete_directory=app_params['vm_path'])
                elif install_result == -3:
                    uc_application.revert_installation(power_off=False)
                elif install_result == -4:
                    pass
                elif install_result == -5:
                    pass

                else:
                    uc_application.revert_installation()
                return

    except KeyError as exception:
        log.exception('Key Error: {}'.format(exception))
        error_detected = True
        return

    except Exception as exception:
        log.exception(exception.__repr__())
        error_detected = True
        return

    finally:

        # =========================================================
        # Update installations in progress counter
        # =========================================================
        log.info('install_uc() Installation is finalizing')

        if settings.check_install_in_progress:
            log.info('install_uc() Check install in progress. Lock release')
            if lock:
                lock.release()

        # =========================================================
        # Job has been cancelled
        # =========================================================

        if cluster:
            log.info('install_uc() Cluster Job [' + job.reference + '] is terminating')
        else:
            log.info('install_uc() Job [' + job.reference + '] is terminating.')
            sleep(settings.cancel_task_wait)
            job_status = AsyncResult(self.request.id).state
            log.info('install_uc() Job status: [{}] Task: {}'.format(job_status, self.request.id))
            if report:
                report.add_log('Job [' + job.reference + '] is terminating')

            if job_status == 'REVOKED':
                message = 'Installation was cancelled'
                log.info('install_uc() ' + message)

                # =========================================================
                # Generate Report
                # =========================================================
                if report:
                    report.add_log(message + ': ' + job.reference)
                    report.generate_report(video_supported=True)

                # =========================================================
                # VM was created
                # =========================================================
                if vm_response > 0:
                    self.update_state(state='CANCELLED', meta={'status': message})
                    job_status = AsyncResult(self.request.id).state
                    log.info('install_uc() Job status after cancel request: {} Job: {}'.format(job_status,
                                                                                               self.request.id))
                    return {'status': message, 'result': 1}

                # =========================================================
                # Safe termination
                # =========================================================
                sqlquery = """UPDATE job SET status=1,is_cancelled=true, job_end='now()' WHERE job.id=""" + str(job.id)
                db_utils.update_database(sqlquery)
                return {'status': message, 'result': 1}

        log.info('install_uc() Terminating process. Install exit response: {} '.format(vm_response))

        # =========================================================
        # Handling Errors
        # =========================================================

        if error_detected:
            log.error('install_uc() Terminating process. Error detected. Updating status information')
            if vm_response > 0:
                sqlquery = """UPDATE virtualmachine SET status=-1 WHERE virtualmachine.fk_server=""" + \
                           str(esx.id) + """ AND vmid=""" + str(vm_response)
                db_utils.update_database(sqlquery)
                log.warn('install_uc() Virtual machine was created with errors in this transaction')

            if cluster:
                sqlquery = """UPDATE job SET status=-1 WHERE job.id=""" + str(job.id)
            else:
                sqlquery = """UPDATE job SET status=-1, job_end='now()' WHERE job.id=""" + str(job.id)
            if self.request.id:
                self.update_state(state='FAILURE', meta={'status': -1})
            # Update job final status
            db_utils.update_database(sqlquery)
            message = 'Application install failed'
            if report:
                report.add_log(message)
            result = -1

        else:
            log.info('install_uc() Terminating process. No error found')
            if vm_response > 0:
                sqlquery = """UPDATE virtualmachine SET status=0 WHERE virtualmachine.fk_server=""" + \
                           str(esx.id) + """ AND vmid=""" + str(vm_response)
                db_utils.update_database(sqlquery)
                log.info('install_uc() Virtual machine was created in this transaction')

            if cluster:
                sqlquery = """UPDATE job SET status=0 WHERE job.id=""" + str(job.id)
            else:
                sqlquery = """UPDATE job SET status=1, job_end='now()', description='[""" + settings.cisco_list[
                    app_id].upper() + """] Installation completed' WHERE job.id=""" + str(job.id)

            # Update job final status
            db_utils.update_database(sqlquery)
            message = 'Application install completed successfully'
            if report:
                report.add_log(message)
            result = 1

        if application_instance_exists:
            # =========================================================
            # Installation finished successfully
            # =========================================================
            log.info('install_uc() Application installation completed')
            history.complete_request(uc_application.app.application_params)

        # =========================================================
        # Return result
        # =========================================================

        if cluster:
            if error_detected:
                return False
            else:
                return True
        else:
            # =========================================================
            # Generate report
            # =========================================================
            if report:
                report.generate_report(video_supported=True)
            if self.request.id:
                self.update_state(state='COMPLETED', meta={'status': 1})
                return {'status': message, 'result': result}
