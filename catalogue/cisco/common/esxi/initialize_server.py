import warnings

warnings.filterwarnings("ignore")
import logging
import json
from alarms import send_sms
from celery.result import AsyncResult
from conf import logging_conf
from conf import settings
from error.exceptions import DiscoveryException
from tools import deploy_tools_lib
from utils import helper
from utils import db_utils


def prepare_server(self, job, configuration, log_file, report):
    """

    :param self:
    :param job:
    :param configuration:
    :param log_file:
    :param report:
    :return:
    """

    # =========================================================
    # DB access. Settings will be using Database information
    # =========================================================
    try:

        log = logging_conf.LoggerManager().getLogger("__app___", logging_file=log_file)
        log.setLevel(level=logging.DEBUG)
        log.info(
            '---------------------------------------Initializing Server---------------------------------------')

        error_detected = False
        host_id = None
        log.info('prepare_server() Connecting to database...')

        if job.id:

            # =========================================================
            # Update existing job in database
            # =========================================================

            sqlquery = """UPDATE job SET description='Task started' WHERE job.id=""" + \
                       str(job.id)
            db_utils.update_database(sqlquery)
            esx = None

        else:

            # =========================================================
            # If no job_id Insert job in database
            # =========================================================
            log.error('prepare_server() No Job found')
            error_detected = True

        # =========================================================
        # Update job id with owner_id information
        # =========================================================

        log.info('prepare_server() Updating job with associated owner_id')
        owner_id = helper.get_owner_id(configuration=configuration)
        if owner_id:
            sqlquery = """UPDATE job SET owner_id=""" + \
                       str(owner_id) + """ WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)
            log.info('prepare_server() Added owner_id successfully')
        else:
            error_detected = True
            message = 'Job doesnt contain owner_id. Unable to get it from parent Server'
            log.exception('prepare_server() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Use default Tools server.
        # =========================================================

        log.warn(
            'prepare_server() Using default Tools server in settings.tools')
        if hasattr(settings, 'tools_primary'):
            tools_name = settings.tools_primary
        else:
            error_detected = True
            message = 'ERROR No Tools primary server found'
            report.add_log(message)
            log.exception('deploy_uc() ' + message)

            # =========================================================
            # Update Celery TASK
            # =========================================================

            if self.request.id:
                self.update_state(state='FAILURE', meta={'status': message})

            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Get Tools server information and Initialize
        # =========================================================

        log.info('prepare_server() Connecting to primary Tools server: ' + tools_name)
        tools = deploy_tools_lib.get_tools_instance(tools_name, True)

        # =========================================================
        # Discover Tools server
        # =========================================================

        if tools:
            log.info(
                'prepare_server() Tools primary server discovered successfully')
            sqlquery = """UPDATE job SET description='Manual Tools server found'
                                    WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)

        else:
            sqlquery = """UPDATE tools SET status=-1 WHERE tools.hostname='""" + str(tools_name) + "'"
            db_utils.update_database(sqlquery)
            message = 'Tools primary server is unreachable. Trying secondary: {}'.format(settings.tools_secondary)
            log.warn('prepare_server() ' + message)
            report.add_log(message)

            tools = deploy_tools_lib.get_tools_instance(settings.tools_secondary, True)

            if tools:
                message = 'Tools secondary server is reachable'
                log.warn('prepare_server() ' + message)
                report.add_log(message)

            else:
                message = 'Tools secondary server is down: {}'.format(settings.tools_secondary)
                report.add_log(message)
                log.error('prepare_server() ' + message)
                error_detected = True

                if self.request.id:
                    self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)
                return

        # =========================================================
        # Initialize ESXi server
        # =========================================================

        log.info(
            'prepare_server() Host id information: {}'.format(
                configuration['instance']['host']['id']))

        if 'id' in configuration['instance']['host']:

            host_id = configuration['instance']['host']['id']
            log.info('prepare_server() Installing server in host: {0}'.format(host_id))
            esx = helper.get_server_instance(host_id)

            if esx:
                log.info('prepare_server() Server name in DB: ' + esx.name)
                log.info('prepare_server() Initializing Server: ' + esx.name)

            else:
                error_detected = True
                message = 'Host not found'
                log.error('prepare_server() ' + message)
                helper.generate_alert(
                    job.id, message, job.reference)
                return

        else:
            error_detected = True
            return

        # =========================================================
        # Start Initialization ESXi server. Get Server information
        # =========================================================
        log.info('prepare_server() Will start discovery...')

        try:

            # =========================================================
            # Update server information
            # =========================================================

            if job:
                log.info(
                    'prepare_server() Updating Host infrastructure in job')
                if 'is_installed' in configuration['instance']['application']['params']:
                    if configuration['instance']['application']['params']['is_installed']:
                        job.update_info(esx.id, -1)
                    else:
                        job.update_info(esx.id, None)
                else:
                    job.update_info(esx.id, None)
                server_json = json.dumps(job.server_info)
            else:
                server_json = '{}'
                log.warn('prepare_server() Job not defined')

            sqlquery = """UPDATE job SET host_information='""" + \
                       server_json + """' WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)

            # =========================================================
            # Discover server
            # =========================================================

            res = esx.discover(init=True, get_status_code=True)

            if res:
                # =========================================================
                # Discovery succeeded. Send init=True to open firewall via HTTPS
                # =========================================================
                message = 'Server discovery was successful'
                log.info(
                    'prepare_server() Valid Server: ' + message)
                report.add_log(message)
                sqlquery = """UPDATE server SET discovered=True WHERE server.id=""" + str(
                    configuration['instance']['host']['id'])

                db_utils.update_database(sqlquery)
                sqlquery = """UPDATE job SET description='Server discovery was successful'
                                        WHERE job.id=""" + str(job.id)
                db_utils.update_database(sqlquery)

            else:

                # =========================================================
                # Discovery failed
                # =========================================================

                error_detected = True
                message = 'ERROR Server connectivity error'
                log.error('prepare_server() ' + message)
                report.add_log(message)
                if self.request.id:
                    self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)

                # =========================================================
                # Update server status -1
                # =========================================================

                sqlquery = """UPDATE server SET status=-1 WHERE server.id=""" + str(
                    configuration['instance']['host']['id'])
                db_utils.update_database(sqlquery)
                return

        except DiscoveryException:
            error_detected = True
            message = 'ERROR Server discovery failed. Please verify HTTPS/SSH ports'
            report.add_log(message)
            log.exception('prepare_server() ' + message)

            # =========================================================
            # Send ALERT
            # =========================================================
            helper.generate_alert(job.id, message, job.reference)
            sqlquery = """UPDATE server SET discovered=False, status=-1 WHERE server.id=""" + str(
                configuration['instance']['host']['id'])
            db_utils.update_database(sqlquery)

            if self.request.id:
                self.update_state(state='FAILURE', meta={'status': message})
            return

        except Exception as exception:
            log.exception(str(exception))
            error_detected = True
            return

    except Exception as exception:
        log.exception('prepare_server() {} '.format(exception))
        error_detected = True
        return

    finally:
        # =========================================================
        # Proceed if ESXi connectivity is ok, otherwise exit
        # =========================================================
        if self.request.id:
            job_status = AsyncResult(self.request.id).state
            log.info('prepare_server() Job status: [{}] Exiting prepare_server()...'.format(job_status))

        if error_detected:
            message = 'ERROR Installation failed'
            log.warn('prepare_server() Error detected')
            sqlquery = """UPDATE job SET status=-1, job_end='now()' WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)
            report.add_log(message)
            cellphone = helper.get_user_information(owner_id=owner_id)
            send_sms.send_sms_alert(body=message + ' ' + str(job.reference), destination_number=cellphone)
            if self.request.id:
                return {'type': 'ERROR', 'status': message, 'result': -1}
        else:
            message = 'Installation in progress'
            report.add_log(message)
            sqlquery = """UPDATE job SET description='""" + message + """' WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)
            return {'type': 'OK', 'esx': esx, 'host_id': host_id, 'tools': tools, 'owner_id': owner_id, 'result': 1}
