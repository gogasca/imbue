import logging
import random
import json
from time import sleep
from subprocess import call
from conf import logging_conf
from conf import settings
from database import Db
from error import ucm_errors
from error import app_exceptions
from hypervisor.esxi import general
from hypervisor.esxi import vm_operations
from hypervisor.esxi import file_operations
from hypervisor.esxi import network_operations
from hypervisor.esxi import service_operations
from img import image_recognition
from utils import helper, validator
from virtualization import VirtualMachine

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

__author__ = 'gogasca'


class OVAManager(object):
    def __init__(self):
        """

        :rtype : object
        """
        self.app = None
        self.install_failed = False
        log.info('ovamanager.__init__() Instance created')

    def add_single_quotes(self, string):
        """

        :param string:
        :return:
        """
        return '\'' + str(string) + '\''

    def add_double_quotes(self, string):
        """

        :param string:
        :return:
        """
        return '"' + str(string) + '"'

    def cancel_install(self):
        """

        :return:
        """
        log.info('ovamanager.cancel_install() will be cancelled')

    def check_duplicate_ip(self):
        try:
            log.info('ovamanager.check_duplicate_ip() Find all vm')
            vm_ids = self.get_server().find_vms(poweronOnly=True)

            if vm_ids is not None:
                for vm_id in vm_ids:
                    try:

                        log.info(
                            'ovamanager.check_duplicate_ip() Virtual machine vmid( ' + str(vm_id) + ')')

                        # =========================================================
                        # Check for vm status ip address/hostname/toolsRunningStatus
                        # =========================================================

                        cucm_info = vm_operations.get_summary(
                            server=self.get_server().server,
                            username=self.get_server().username,
                            password=self.get_server().password,
                            port=self.get_server().sshPort,
                            vmId=vm_id)

                        # =========================================================
                        # Check cucm information
                        # =========================================================

                        if cucm_info is not None:
                            log.info(
                                'ovamanager.check_duplicate_ip() Processing VM: ' + vm_id)
                            log.info(cucm_info)
                            try:
                                # Verify guestToolsRunning
                                if 'guestToolsRunning' in cucm_info[
                                    0].encode('utf-8'):
                                    log.info(
                                        'ovamanager.check_duplicate_ip() guestToolsRunning gathering IP Information')
                                    ip_info = self.get_ip_information()
                                    log.info(ip_info)
                                    if not ip_info:
                                        log.info(
                                            'ovamanager.check_duplicate_ip() No IP information')
                                        return 0

                                    if len(ip_info) == 2 and len(
                                            cucm_info) == 3:

                                        # =========================================================
                                        # We match hostnames and IP information
                                        # =========================================================
                                        # ip_info: ['pub', '110.10.0.201']
                                        # cucm_info [u'guestToolsRunning', u'pub', u'110.10.0.201']

                                        if ip_info[0] == cucm_info[
                                            1] and ip_info[1] == cucm_info[2]:
                                            log.error(
                                                'ovamanager.check_duplicate_ip() VM Already configured using '
                                                'hostname and ip address vmId: ' + vm_id)
                                            return -1
                                        elif ip_info[1] == cucm_info[2]:
                                            log.error(
                                                'ovamanager.check_duplicate_ip() VM Already configured using '
                                                'ip address vmId: ' + vm_id)
                                            return -2
                                        elif ip_info[1] == cucm_info[2]:
                                            log.error(
                                                'ovamanager.check_duplicate_ip() VM Already configured using '
                                                'hostname vmId: ' + vm_id)
                                            return -3

                            except Exception as e:
                                log.exception(e)
                        else:
                            log.warn('ovamanager.check_duplicate_ip() No VMs in ESXi')
                            return 0

                    except Exception as e:
                        log.exception(e)

            # =========================================================
            # Return my own IP information
            # =========================================================

            return self.get_ip_information()[1]

        except Exception as e:
            print e

    def complete_installation(self):
        """

        :return:
        """
        try:

            log.info('ovamanager.complete_installation() Post install tasks started...')

            # =========================================================
            # Updating VMX files
            # =========================================================

            log.info('ovamanager.complete_installation() power off VM')
            # Check for vm status ip address/hostname/toolsRunningStatus
            res = vm_operations.power_off(server=self.get_server().server,
                                          username=self.get_server().username,
                                          password=self.get_server().password,
                                          port=self.get_server().sshPort,
                                          vmId=self.get_virtualmachine().vmId)
            if res:
                sleep(15)
                log.info('ovamanager.complete_installation() Power off succeeded.')
                # Sleep to let the machine be off
            else:
                log.error(
                    'ovamanager.complete_installation() Power off failed.')

            log.info('ovamanager.complete_installation() Removing floppy drive and disabling VNC')

            esx_vmx_file = self.app.application_params['esx_vmx_file']

            command_list = []
            command_list.append('sed -i \'/RemoteDisplay.vnc.port/d\' ' + esx_vmx_file)
            command_list.append('sed -i \'/RemoteDisplay.vnc.enabled/d\' ' + esx_vmx_file)

            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort)
            if res is None:
                log.info(
                    'ovamanager.complete_installation() VMX file updated succesfully')

            else:
                log.error(
                    'ovamanager.complete_installation() VMX file updated failed')

            res = vm_operations.power_on(server=self.get_server().server,
                                         username=self.get_server().username,
                                         password=self.get_server().password,
                                         port=self.get_server().sshPort,
                                         vmId=self.get_virtualmachine().vmId)

            if res is None:
                log.info('ovamanager.complete_installation Power on succeeded.')
            else:
                log.error(
                    'ovamanager.complete_installation Power on failed.')
                return False

            # Check no other installations are in progress.
            if self.server.status == 0:
                log.info('ovamanager.complete_installation Disabling SSH service...')
                service_operations.ssh_service_activation(
                    server=self.server.server,
                    username=self.server.username,
                    password=self.server.password,
                    port=self.server.sshPort,
                    operation=0)

            log.info(
                'ovamanager.complete_installation disabling firewall. False by default')
            network_operations.update_firewall(server=self.server.server,
                                               username=self.server.username,
                                               password=self.server.password,
                                               port=self.server.sshPort)

            json_db = json.dumps(self.app.application_params)
            log.info('ovamanager.complete_installation: JSON information:' + json_db)
            sqlquery = """
                    INSERT INTO installation_completed (job, is_completed, application_params, owner_id, fk_server, timestamp)"""

            content = "\'" + self.app.application_params['job'] + \
                      '\',True,\'' + \
                      json_db + "\'," + \
                      str(self.app.application_params['owner_id']) + ',' + str(self.server.id) + ',' + 'now()'

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()
            res = db_instance.insert(sqlquery, content)

            if res:

                # =========================================================
                # Install successful
                # =========================================================

                log.info(
                    'ovamanager.complete_installation() Installation successfully completed')
                return True
            else:

                # =========================================================
                # Install failed
                # =========================================================

                log.exception(
                    'ovamanager.complete_installation() Failed to insert record in database')
                return False

        except Exception as e:
            log.exception(
                'ovamanager.complete_installation() Failed to insert record in database' +
                e.message)
            self.set_install_failed()
            return False

    def clean_ovf_file(self, directory):
        """
        Once ovf file has been downloaded, cp to datastore1 (Default), delete softlink
        :param directory:
        :return:
        """
        log.info('ovamanager.clean_ovf_file() Deleting ovftool.tar.gz and vmware-ovftool')
        if file_operations.check_remote_directory_exists(server=self.get_server().server,
                                                         username=self.get_server().username,
                                                         password=self.get_server().password,
                                                         port=self.get_server().sshPort,
                                                         directory=directory):
            log.info('ovamanager.clean_ovf_file() Directory exists')
        else:
            log.error('ovamanager.clean_ovf_file() Directory doesnt exist')

        command_list = list()
        command_list.append('cd ' + directory + ' && rm -rf * ; echo $? ')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)

        log.info(res)
        if '0' in res[0].encode('utf-8'):
            log.info('ovamanager.clean_ovf_file() File cleaned completed')
            return True
        else:
            log.error('ovamanager.clean_ovf_file() File cleaned failed')
            return False

    def deploy_ovf_file(self, directory):
        """
        Once ovf file has been downloaded, cp to datastore1 (Default), extract it, create softlink and validate it works
        cp vmware-ovftool.tar.gz to /vmfs/volumes/datastore1/
        :param directory:
        :return:
        """

        if file_operations.check_remote_directory_exists(server=self.get_server().server,
                                                         username=self.get_server().username,
                                                         password=self.get_server().password,
                                                         port=self.get_server().sshPort,
                                                         directory=directory):
            log.info('ovamanager.deploy_ovf_file() Directory exists')
        else:
            log.error('ovamanager.deploy_ovf_file() Directory doesnt exist')

        command_list = list()
        if directory != '/':
            command_list.append('cp ' + settings.ovf_filename + ' ' + directory)

        command_list.append('cd ' + directory + ' && tar -zxf ' + settings.ovf_filename + ' ; echo $? ')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)

        log.info(res)
        if '0' in res[0].encode('utf-8'):
            log.info('ovamanager.get_ovf_file() File deployment completed')
            return True
        else:
            log.error('ovamanager.get_ovf_file() File deployment failed')
            return False

    def deploy_ova(self):
        """

        :return:
        """
        log.info('ovamanager.deploy_ova()')

    def deploy_virtualmachine(self):
        """

        :param jobtracker:
        :return:
        """

        log.info('ovamanager.deploy_virtualmachine()')
        try:
            # Create a new VirtualMachine
            if self.get_server():
                # Issue #37

                if self.get_server().find_duplicate_vm(
                        self.app.application_params['hostname']):
                    log.error(
                        'ovamanager.deploy_virtualmachine() Machine exists with same hostname')
                    return -2

                # Create new VirtualMachine instance
                log.info('ovamanager.deploy_virtualmachine() Creating Object: ' +
                         str(self.app.application_params['hostname']))
                vm = VirtualMachine.VirtualMachine(
                    self.app.application_params['hostname'], '')

                # Define VM
                self.set_virtualmachine(vm)

                # Assign VNC Port
                log.info(self.app.application_params)

                if self.app.application_params['vnc_port']:
                    vm.vncPort = self.app.application_params['vnc_port']

                # Associate VM to parent ESXi Server
                vm.set_server(self.get_server())

                # Create VMX file
                vm_operations.create_vmx_file(
                    self.get_server().get_hypervisor(),
                    self.app.application_params['tools_vmx_file'],
                    self.app.application_params['app_id'],
                    self.app.application_params)

                # Transfer vmxFile to ESXi Server
                vm.load_vmx_file(self.app.application_params['tools_vmx_file'],
                                 self.app.application_params['esx_vmx_file'])
                # Create VMK Disk, this operation may take sometime
                vm.create_vmk_disk(self.app.application_params[
                                       'vmdk_path'], '80', 1)
                # Register VM
                vm.register_vm()
                # Get VM Id
                log.info('ovamanager.deploy_virtualmachine() vmId: ' + str(vm.vmId))
                self.app.application_params['vmid'] = vm.vmId

                # Power on VM
                if vm.get_status():
                    res = vm.power_on()
                    if res:

                        # =========================================================
                        # Job
                        # =========================================================

                        if self.app.application_params['job']:
                            job = self.app.application_params['job']
                            log.info(
                                'ovamanager.deploy_virtualmachine() Updating host infrastructure in job')
                            job.update_info(self.get_server().id, vm.vmId)
                            server_json = json.dumps(job.server_info)
                        else:
                            server_json = '{}'
                            log.warn(
                                'ovamanager.deploy_virtualmachine() Job not defined')

                        sqlquery = """UPDATE job SET host_information='""" + \
                                   server_json + """' WHERE job.id=""" + str(job.id)
                        helper.update_database(sqlquery)

                        # =========================================================
                        # get screen URL
                        # =========================================================

                        if self.get_server().httpsPort is not None:
                            url = 'https://' + self.get_server().server + ':' + str(
                                self.get_server().httpsPort) + '/screen?id=' + vm.vmId
                        else:
                            url = 'https://' + self.get_server().server + '/screen?id=' + vm.vmId

                        log.info('ovamanager.deploy_virtualmachine() Screen ' + url)
                        log.info(
                            'ovamanager.deploy_virtualmachine() Waiting for virtual machine to start...')
                        self.app.application_params['screen_url'] = url

                        # =========================================================
                        # Return vmId
                        # =========================================================

                        return int(vm.vmId)
                    else:
                        return -3
                else:
                    raise app_exceptions.UcmVirtualMachineException(
                        'ovamanager.launch_virtualmachine() Unable to launch VM')

            else:
                log.error(
                    'ovamanager.deploy_virtualmachine() Need to associate ESXi server')

            return -1

        except Exception as e:
            log.exception(str(e))
            raise

    def get_params(self):
        """

        :return:
        """
        return self.app.application_params

    def get_ovf_file(self):
        """
        Download OVF file from parzee repository
        wget http://www.parzee.com/utils/vmware-ovftool.tar.gz
        :return:
        """
        if self.get_server().initialize_filesystem():
            log.info('ovamanager.get_ovf_file() SSH Access to server is available')
        else:
            log.error('ovamanager.get_ovf_file() SSH Access server is not available')
            return False

        if file_operations.check_remote_file_exists(server=self.get_server().server,
                                                    username=self.get_server().username,
                                                    password=self.get_server().password,
                                                    port=self.get_server().sshPort,
                                                    fileName=settings.ovf_filename):
            log.info('ovamanager.get_ovf_file() File already exists')
            return True
        else:
            log.info('ovamanager.get_ovf_file() File doesnt exists will proceed to download')
            command_list = list()
            command_list.append('wget -q ' + settings.ovf_url + ' ; echo $? ')
            # Get ovftool to deploy OVF wget -q http://198.199.110.24:8083/vmware/ovftool.tar.gz
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)

            if '0' in res[0].encode('utf-8'):
                log.info('ovamanager.get_ovf_file() File download completed')
            else:
                log.info('ovamanager.get_ovf_file() File download failed')

            log.info('ovamanager.get_ovf_file() Checking file download integrity')

            ############################################################################################
            # ls -al ovftool.tar.gz | awk '{print $5}' File size is 28,233,406 bytes ~ 27MB
            ############################################################################################

            res = general.send_command(commandList=['ls -al ' + settings.ovf_filename + ' | awk \'{print $5}\''],
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)
            log.info(res)
            if '28233406' in res[0].encode('utf-8'):  # OVFtool size is always the same.
                log.info('ovamanager.get_ovf_file() File size is correct')
                return True
            else:
                # Wait in case link connection is slow...
                sleep(45)
                res = general.send_command(commandList=['ls -al ' + settings.ovf_filename + ' | awk \'{print $5}\''],
                                           server=self.get_server().server,
                                           username=self.get_server().username,
                                           password=self.get_server().password,
                                           port=self.get_server().sshPort,
                                           display=True)

                if '28233406' in res[0].encode('utf-8'):
                    log.info('ovamanager.get_ovf_file()  File size is correct')
                    return True
                else:
                    log.error('ovamanager.get_ovf_file() File download failed')
                    return False

    def get_server(self):
        """

        :return:
        """
        return self.server

    def get_virtualmachine(self):
        """

        :return:
        """
        return self.app.get_virtualmachine()

    def install(self, app_params, params, power, job, esx):
        """

        :return:
        """

        if self.get_ovf_file():
            log.info('ovamanager.install() OVF file was obtained successfully')
        else:
            log.error('ovamanager.install() OVF file failed')
            return -1

        if self.deploy_ovf_file(app_params['location']):
            log.info('ovamanager.install() deployed OVF file successfully')
        else:
            log.error('ovamanager.install() deployed OVF file failed')
            return -1

        if self.deploy_ova(app_params['location'],
                           app_params['datastore'],
                           app_params['hostname'],
                           app_params['annotation'],
                           app_params['size'],
                           app_params['ova_file'],
                           app_params['network_name'],
                           params, power, job.reference):
            log.info('ovamanager.install() deployed OVA successfully')
            self.clean_ovf_file(app_params['location'])

        else:
            log.error('ovamanager.install() deployed OVA failed')
            return -1

        # Will edit VMX file.
        log.info('ovamanager.install() VMX file: {}'.format(app_params['esx_vmx_file']))

        # =========================================================
        # VNC Port definition
        # =========================================================

        try:

            log.info('ovamanager.install() Getting Monitor port from DB...')
            # esx.vnc_ports is the DB load from VNC information
            res = validator.check_vnc_port(esx.vnc_ports)

            if res == 1:  # Return 1 when Single port defined in server
                # Sync VNC ports.
                # Login to ESXi via SSH and allocate VNC ports in use.
                vnc_ports_inuse = esx.allocate_vnc_ports(returnList=True)
                for vnc_port_inuse in vnc_ports_inuse:
                    if esx.add_vnc_port(vnc_port_inuse):
                        app_params['vnc_port'] = str(int(esx.vnc_ports))

            elif res == 2:  # Range of Ports
                esx.allocate_vnc_ports()  # Login via SSH and allocate VNC ports in use.
                port_range = range(int(esx.vnc_ports.split(':')[0]), 1 + int(esx.vnc_ports.split(':')[1]))
                port_found = False
                log.info('ovamanager.install() Monitor ports: ')
                port_range = sorted(port_range, key=int)
                for port in port_range:
                    if esx.add_vnc_port(port):
                        app_params['vnc_port'] = str(port)
                        port_found = True
                        break
                if not port_found:
                    log.warn('ovamanager.install() DB Invalid port defined in range. Using default Monitor port.')
                    app_params['vnc_port'] = settings.defaultVNCPort
            else:
                log.warn('ovamanager.install() DB Invalid port defined in range. Using default Monitor port.')
                app_params['vnc_port'] = settings.defaultVNCPort

            message = 'Monitor port: ' + app_params['vnc_port']
            log.info('ovamanager.install() {}'.format(message))
            log.info('ovamanager.install() VMX File: {}'.format(app_params['esx_vmx_file']))
            command_list = list()
            command_list.append('echo \'RemoteDisplay.vnc.enabled = \"TRUE\"\' >> ' + app_params['esx_vmx_file'])
            command_list.append('echo \'RemoteDisplay.vnc.port = \"' + str(app_params['vnc_port']) + '\"\' >> ' +
                                app_params['esx_vmx_file'])
            res = general.send_command(commandList=command_list,
                                       server=self.get_server().server,
                                       username=self.get_server().username,
                                       password=self.get_server().password,
                                       port=self.get_server().sshPort,
                                       display=True)

            log.info(res)
            if res is None:
                log.info('ovamanager.install() VMX file updated')
            else:
                log.error('ovamanager.install() VMX file update failed')
                return -2

        except Exception as exception:
            message = 'ERROR Unable to get Monitor port'
            log.error('ovamanager.install() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            log.exception(
                'ovamanager.install() Unable to get Monitor port: ' + str(exception))
            return -1

        log.info('ovamanager.install() Proceeding to start VM...')
        command_list = list()
        command_list.append('vim-cmd vmsvc/getallvms | grep ' + job.reference + ' | awk \'{print $1}\'')
        res = general.send_command(commandList=command_list,
                                   server=self.get_server().server,
                                   username=self.get_server().username,
                                   password=self.get_server().password,
                                   port=self.get_server().sshPort,
                                   display=True)

        vmId = res[0].encode('utf-8')
        vmId = vmId.rstrip()

        if vmId:
            vm = VirtualMachine.VirtualMachine(app_params['hostname'], vmId)
            self.set_virtualmachine(vm)

            # Associate VM to parent ESXi Server
            vm.set_server(self.get_server())

            if vm.get_status():
                log.info('ovamanager.install VM status is OK')
            else:
                log.error('ovamanager.install() VM status is error')
                return -3

            if vm.power_on():
                log.info('ovamanager.install VM power on successfully')
            else:
                log.error('ovamanager.install() VM power on failed')
                return -3
        else:
            log.error('ovamanager.install() Unable to get vmId')

        if vm_operations.insert_vm(
                vmId=vmId,
                vm_hostname=app_params['hostname'],
                status=1,
                description=job.reference,
                ip_information=params['ip'],
                uuid='',
                app_id=200006,
                power_state=True,
                host_id=app_params['host_id'],
                directory=app_params['location']):
            log.info('ovamanager.install() Virtual machine DB insertion was successful')
            log.info('ovamanager.install() Waiting for VM machine to start')

            #############################################
            # Update job infrastructure
            #############################################

            job.update_info(self.get_server().id, vm.vmId)
            server_json = json.dumps(job.server_info)
            sqlquery = """UPDATE job SET host_information='""" + server_json + """' WHERE job.id=""" + str(job.id)
            helper.update_database(sqlquery)

        else:
            message = 'ERROR: VM insertion failed'
            log.error('ovamanager.install ' + message)
            helper.generate_alert(job.id, message, job.reference)
            return -4

        log.info('ovamanager.install() Proceeding to pass configuration parameters...')
        if self.send_sequence(esx, app_params):
            log.info('ovamanager.install() Deploy IP information successfully')
            log.info('ovamanager.install() Removing monitor port: ' + str(app_params['vnc_port']))
            esx.remove_vnc_port(app_params['vnc_port'])
            log.info('ovamanager.install() Waiting for VM to deploy')
            sleep(180)

        else:
            log.info('ovamanager.install() Unable to set parameters')
            return -5

        # =========================================================
        # get screen URL
        # =========================================================

        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + vm.vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + vm.vmId

        log.info('ovamanager.install() Screen: ' + url)
        log.info('ovamanager.install() Waiting for virtual machine to start...')
        self.app.application_params['screen_url'] = url

        log.info('ovamanager.install() Instance is starting...')
        self.app.application_params['vmid'] = vmId
        return vmId

    def revert_installation(self, power_off=True, destroy=True, delete_directory=None, **kwargs):
        """

        :param power_off:
        :param destroy:
        :param kwargs:
        :return:
        """
        try:

            # =========================================================
            # Get VM status
            # =========================================================

            if power_off:
                log.info('revert_installation() Get current VM state')
                # Verify VM is on/off/doesnt exist
                command_list = []
                command_list.append('vim-cmd vmsvc/power.getstate ' +
                                    str(self.get_virtualmachine().vmId) +
                                    ' | grep \'Power\'')
                res = general.send_command(commandList=command_list,
                                           server=self.get_server().server,
                                           username=self.get_server().username,
                                           password=self.get_server().password,
                                           port=self.get_server().sshPort,
                                           display=True)

                if res:
                    result = res[0].encode('utf-8').rstrip()
                    if result == 'Powered on':
                        log.warn('revert_installation() Machine is power on')

                        # =========================================================
                        # Power off Virtual machine
                        # =========================================================

                        log.info('revert_installation power off VM')
                        # Check for vm status ip
                        # address/hostname/toolsRunningStatus
                        res = vm_operations.power_off(
                            server=self.get_server().server,
                            username=self.get_server().username,
                            password=self.get_server().password,
                            port=self.get_server().sshPort,
                            vmId=self.get_virtualmachine().vmId)
                        if res:
                            sleep(15)
                            # Sleep to let the machine be off
                        else:
                            log.error(
                                'revert_installation power off failed.')
                            return False
                else:
                    log.info(
                        'revert_installation Unable to get status. Probably doesnt exist anymore')
                    return True

            if destroy:
                # =========================================================
                # Destroy Virtual machine
                # =========================================================

                log.info('revert_installation() destroying VM')

                if settings.destroy_vm:
                    res = vm_operations.destroy(
                        server=self.get_server().server,
                        username=self.get_server().username,
                        password=self.get_server().password,
                        port=self.get_server().sshPort,
                        vmId=self.get_virtualmachine().vmId)

                    if not res:
                        log.error('revert_installation destroy failed')
                        return False

                    # =========================================================
                    # Delete from Database
                    # =========================================================

                    sqlquery = """DELETE FROM virtualmachine WHERE fk_server=""" + \
                               str(self.get_server().id) + """ AND vmid=""" + \
                               str(self.get_virtualmachine().vmId)
                    helper.update_database(sqlquery)

                    log.info('revert_installation() Deletion was successful')
                    return True

                else:
                    log.warn(
                        'revert_installation() Don t destroy VM. Just power it off')
                    return True

            if delete_directory:
                log.info('revert_installation() Delete directory')
                if self.get_server().delete_working_directory(delete_directory):
                    return True
                else:
                    return False

        except Exception as exception:
            log.exception('revert_installation() '.format(exception))
            return False

    def read_screen_information_exponential_backoff(self, url, username, password, httpsPort, downloadPath):
        """

        :return:
        """

        for n in range(0, 5):
            try:
                return image_recognition.get_screenshot(
                    url=url,
                    username=self.get_server().username,
                    password=self.get_server().password,
                    httpsPort=self.get_server().httpsPort,
                    downloadPath=self.app.application_params['screen'])

            except Exception:
                log.exception('ovamanager.read_screen_information_exponential_backoff() Retrying {} times'.format(n))
                sleep((2 ** n) + random.random())

        log.exception('ovamanager.read_screen_information_exponential_backoff() The request never succeeded.')
        return False

    def read_screen_information(self):
        """

        :return:
        """

        if self.get_server().httpsPort is not None:
            url = 'https://' + self.get_server().server + ':' + str(
                self.get_server().httpsPort) + '/screen?id=' + self.get_virtualmachine().vmId
        else:
            url = 'https://' + self.get_server().server + '/screen?id=' + \
                  self.get_virtualmachine().vmId

        log.info('ovamanager.read_screen_information() URL ' + url)
        img = self.read_screen_information_exponential_backoff(url=url,
                                                               username=self.get_server().username,
                                                               password=self.get_server().password,
                                                               httpsPort=self.get_server().httpsPort,
                                                               downloadPath=self.app.application_params['screen'])

        if not img:
            self.set_install_failed()
            log.info('ovamanager.read_screen_information() Unable to get image')
            return -1

        res = image_recognition.processImage(
            self.app.application_params['screen'])

        # =========================================================
        # Return Error code when install fails
        # =========================================================

        log.info(
            'ovamanager.read_screen_information() Checking if any errors during install')
        if image_recognition.is_prompt_ready(
                list=ucm_errors.install_successful,
                image_to_text=res.lower(),
                coefficient=10):
            log.info('ovamanager.read_screen_information() Successful installation')
            return 1
        elif image_recognition.is_prompt_ready(list=ucm_errors.default_gateway_error, image_to_text=res.lower(),
                                               coefficient=10):
            log.error('ovamanager.read_screen_information() Error -1')
            return -1

        elif image_recognition.is_prompt_ready(list=ucm_errors.network_connectivity_error,
                                               image_to_text=res.lower(),
                                               coefficient=10):
            log.error('ovamanager.read_screen_information() Error -2')
            return -2
        elif image_recognition.is_prompt_ready(list=ucm_errors.ntp_error, image_to_text=res.lower(),
                                               coefficient=10):
            log.error('ovamanager.read_screen_information() Error -3')
            return -3

        elif image_recognition.is_prompt_ready(list=ucm_errors.subscriber_error, image_to_text=res.lower(),
                                               coefficient=10):
            log.info('ovamanager.read_screen_information() Error -4')
            return -4

        elif image_recognition.is_prompt_ready(list=ucm_errors.undetermined_error, image_to_text=res.lower(),
                                               coefficient=8):
            log.error('ovamanager.read_screen_information() Error -5')
            return -5
        # =========================================================
        # In progress
        # =========================================================

        elif image_recognition.is_prompt_ready(list=ucm_errors.database_install_in_progress,
                                               image_to_text=res.lower(),
                                               coefficient=10):
            log.info(
                'ovamanager.read_screen_information() Application installation is in progress')
            return 0
        elif image_recognition.is_prompt_ready(list=ucm_errors.network_connectivity_checking,
                                               image_to_text=res.lower(),
                                               coefficient=10):
            log.info(
                'ovamanager.read_screen_information() Warning connectivity check still in progress...')
            return 0

        # =========================================================
        # No match found. Unknown Error/Message
        # =========================================================

        else:
            log.info('ovamanager.read_screen_information No errors found continue...')

            if general.check_network_connectivity(
                    server=self.get_server().server,
                    vncPort=self.app.application_params['vnc_port'],
                    mode='vnc'):
                log.info(
                    'ovamanager.read_screen_information() Send VNC CTRL-C key to refresh image')
                call(['vncdo -s ' +
                      self.get_server().server +
                      '::' +
                      self.app.application_params['vnc_port'] +
                      ' key ctrl-C'], shell=True)
                return 0
            else:
                log.warn(
                    'ovamanager.read_screen_information VNC problem installation may continue')
                return 0

        return 0

    def set_install_failed(self):
        """

        :return:
        """
        log.error('ovamanager.set_install_failed Flag to True')
        self.install_failed = True

    def set_virtualmachine(self, vm):
        """

        :param vm:
        :return:
        """
        self.app.set_virtualmachine(vm)

    def set_server(self, server):
        """

        :param server:
        :return:
        """
        self.server = server

    def send_sequence(self, esx, params):
        """

        :param esx:
        :param params:
        :return:

        /cr
        admin /cr
        TANDBERG /cr
        y /cr
        /cr
        /cr
        $<ip> /cr
        $<subnet> /cr
        $<gateway> /cr
        /cr
        /cr
        y /cr

        """
        try:
            log.info(
                'ovamanager.send_sequence() Sending install sequence...waiting for VM to boot up {} seconds'.format(
                    settings.ova_wait_deployment))
            sleep(settings.ova_wait_deployment)
            log.info(
                'ovamanager.send_sequence() Starting to send VNC commands {} {}'.format(esx.server, params['vnc_port']))
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type admin'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            # Send CAPS
            call(['vncdo --force-caps -s ' + esx.server + '::' + params['vnc_port'] + ' type TANDBERG'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type y'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['ip']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['mask']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' type ' + params['gw']], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            # Restart services to take effect
            call(['vncdo -s ' + esx.server + '::' + params['vnc_port'] + ' key enter'], shell=True)
            sleep(2)
            return True

        except Exception:
            log.exception('')

    def update_firewall(self, allow=False, **kkwargs):
        """

        :param allow:
        :param kkwargs:
        :return:
        """
        if self.get_server():

            # =========================================================
            # Disable firewall
            # =========================================================

            log.info('ovamanager.update_firewall disabling firewall')
            if allow:
                log.info('ovamanager.update_firewall enabling firewall')
                if network_operations.update_firewall(
                        server=self.server.server,
                        username=self.server.username,
                        password=self.server.password,
                        port=self.server.sshPort,
                        set='true'):
                    return True
            else:
                if network_operations.update_firewall(
                        server=self.server.server,
                        username=self.server.username,
                        password=self.server.password,
                        port=self.server.sshPort):
                    return True
        else:
            log.error(
                'ovamanager.update_firewall() disabling firewall failed. Not a valid ESXi')
            self.set_install_failed()
            return False
