from catalogue.cisco.common.xml import xml_processor

__author__ = 'gogasca'

import os, pathlib, shutil
from conf import settings
from utils import validator
from hypervisor.esxi import general, file_operations
from conf import logging_conf
import logging

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def deploy_answer_file(app_server, esx_server, params):
    # This will transfer local answerfile to Tools server
    """
    params['hypervisor'] = 1
    params['app_id'] = 1
    params['answer_file'] = settings.answerFile
    params['remote_floppy'] = '/usr/local/src/utils/customers/test/floppy.img'
    params['local_floppy']  = '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/backup/floppy.img'
    params['esx_floppy'] = remoteDir + '/' + 'floppy.img'

    """
    try:
        # Issue 44
        cluster = False
        if xml_processor.check_valid_xmlfile(params['answer_file']):
            log.info('Valid XML answer file')
            if xml_processor.get_param_value(params['answer_file'], 'LocalHostDHCP') == 'yes':
                if settings.dhcp_support:
                    pass
                else:
                    log.error('Answer file is configured with DHCP. DCHP not supported')
                    return False
        else:
            log.error('Invalid XML answer file')
            return False

        try:
            if params['cluster_file']:
                log.info('deploy_answer_file() Cluster option: ')
                if xml_processor.check_valid_xmlfile(params['cluster_file']):
                    log.info('deploy_answer_file() Valid XML cluster file')
                    cluster = True
                else:
                    log.error('deploy_answer_file() Invalid XML cluster file')
                    return False
        except KeyError:
            log.info('deploy_answer_file() Not a cluster deployment.')

        # =========================================================
        # Move local XML file to Tools server
        # =========================================================

        answer_file = transfer_xmlfile_to_remote(app_server, params['tools_floppy_folder'], params['answer_file'],
                                                 params['app_id'])

        if cluster:
            cluster_file = transfer_xmlfile_to_remote(app_server, params['tools_floppy_folder'], params['cluster_file'],
                                                      params['app_id'])

        # =========================================================
        # Obtain name from Answer File
        # =========================================================

        if answer_file:

            # =========================================================
            # Create floppy drive in Tools server
            # =========================================================

            if cluster:
                if create_floppy_drive(params['hypervisor'], app_server, params['tools_floppy_folder'],
                                       params['floppy_image'], answer_file, cluster_file):
                    pass
                else:
                    return False
            else:
                if create_floppy_drive(params['hypervisor'], app_server, params['tools_floppy_folder'],
                                       params['floppy_image'], answer_file, None):
                    pass
                else:
                    return False

            # =========================================================
            # Transfer floppy drive to ESXi (Tools > Local Dir > ESXi)
            # =========================================================
            transfer_floppy_drive_to_esx(app_server, params['tools_floppy_file'], params['local_floppy_file'],
                                         esx_server,
                                         params['esx_floppy_file'])
            return True

        log.error('deploy_answer_file() Invalid answer file')
        return False

    except Exception, e:
        log.exception(str(e))
        return False


def create_floppy_drive(hypervisor, app_server, floppyImageDir, floppyImageName, answerFile, clusterFile):
    """


    :rtype : object
    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param floppyImageDir:
    :param floppyImageName:
    :param answerFile:
    :return:
    """
    try:
        log.info('application.create_floppy_drive()')

        if hypervisor == 1:

            """
                Logic:
                    Validate app server connectivity
                    Validate credentials
                    Validate path
                        If folder exists
                            Check if file exists
                                If true overwrite it
                                else create new
                        If folder doesnt exist
                            Create folder
                    Check answer file exists remotely
                    Create floppy drive
            """

            if clusterFile:
                res = file_operations.create_floppy(server=app_server.server,
                                                    username=app_server.username,
                                                    password=app_server.password,
                                                    port=app_server.sshPort,
                                                    floppyImageDir=floppyImageDir,
                                                    floppyImageName=floppyImageName,
                                                    answerFile=answerFile,
                                                    clusterFile=clusterFile)
                if res:
                    return True
                else:
                    return False

            else:

                # =========================================================
                # ./create_floppy.sh -d /usr/local/src/utils/ -f floppy.img -p /usr/local/src/utils/platformConfig.xml
                # =========================================================

                res = file_operations.create_floppy(server=app_server.server,
                                                    username=app_server.username,
                                                    password=app_server.password,
                                                    port=app_server.sshPort,
                                                    floppyImageDir=floppyImageDir,
                                                    floppyImageName=floppyImageName,
                                                    answerFile=answerFile)
                if res:
                    return True
                else:
                    return False

        else:
            from error import exceptions
            log.exception('application.create_floppy_drive() Hypervisor model not available yet')
            raise exceptions.UnknownHypervisor('application.create_floppy_drive() Hypervisor model not available yet')
    except Exception, e:
        log.exception(str(e))
        return False


def transfer_xmlfile_to_remote(tools_server, remoteDirectory, answer_file, appId):
    """

    :param tools_server:
    :param remoteDirectory:
    :param answer_file:
    :param appId:
    :return:
    """

    try:
        """
            appId
            value   app
            -----   ------
                1   cucm
                2   cuc
                3   cup
            """
        if appId == 200001:
            log.info('transfer_xmlfile_to_remote() ucm')
            log.info('transfer_xmlfile_to_remote() Moving ' + answer_file + ' in ' + remoteDirectory)
            if general.transfer_file_scp(tools_server.server,
                                         tools_server.username,
                                         tools_server.password,
                                         tools_server.sshPort,
                                         remoteFilePath=remoteDirectory,
                                         localFile=answer_file):
                import os.path
                head, tail = os.path.split(answer_file)
                if remoteDirectory.endswith('/'):
                    answerFileInServer = remoteDirectory + tail
                else:
                    answerFileInServer = remoteDirectory + '/' + tail
                return answerFileInServer

            else:
                return False

        elif appId == 200002:
            log.info('transfer_xmlfile_to_remote() cuc')
            log.info('transfer_xmlfile_to_remote() Moving ' + answer_file + ' in ' + remoteDirectory)
            if general.transfer_file_scp(tools_server.server,
                                         tools_server.username,
                                         tools_server.password,
                                         tools_server.sshPort,
                                         remoteFilePath=remoteDirectory,
                                         localFile=answer_file):
                import os.path
                head, tail = os.path.split(answer_file)
                if remoteDirectory.endswith('/'):
                    answerFileInServer = remoteDirectory + tail
                else:
                    answerFileInServer = remoteDirectory + '/' + tail
                return answerFileInServer

            else:
                return False

        elif appId == 200003:
            log.info('transfer_xmlfile_to_remote() cup')
            log.info('transfer_xmlfile_to_remote() Moving ' + answer_file + ' in ' + remoteDirectory)
            if general.transfer_file_scp(tools_server.server,
                                         tools_server.username,
                                         tools_server.password,
                                         tools_server.sshPort,
                                         remoteFilePath=remoteDirectory,
                                         localFile=answer_file):
                import os.path
                head, tail = os.path.split(answer_file)
                if remoteDirectory.endswith('/'):
                    answerFileInServer = remoteDirectory + tail
                else:
                    answerFileInServer = remoteDirectory + '/' + tail
                return answerFileInServer

            else:
                return False

        else:
            log.error('transfer_xmlfile_to_remote() Unknown AppId')
            return False

    except Exception, e:
        log.exception(str(e))
        return False


def transfer_floppy_drive_to_esx(appServer,
                                 remote_floppy_file,
                                 local_floppy_file,
                                 esxServer,
                                 esx_floppy_file_esx):
    """

    :param appIp:
    :param appUsername:
    :param appPassword:
    :param appSSHPort:
    :param remote_floppy_file:
    :param local_floppy_file:
    :param esxServer:
    :param esxUsername:
    :param esxPassword:
    :param esxSSHPort:
    :param esx_floppy_file_esx:
    :return:
    """
    try:
        res = general.get_file_scp(appServer.server,
                                   appServer.username,
                                   appServer.password,
                                   appServer.sshPort,
                                   remote_floppy_file,
                                   local_floppy_file)
        if res:
            log.info('transfer_floppy_drive_to_esx() Transfer file to ESXi server')
            res = general.transfer_file_scp(esxServer.server,
                                            esxServer.username,
                                            esxServer.password,
                                            esxServer.sshPort,
                                            remoteFilePath=esx_floppy_file_esx,
                                            localFile=local_floppy_file)
            if res:
                return True

        return False
    except Exception, e:
        log.exception(str(e))
        return False


def read_file(filename):
    try:
        p = pathlib.Path(filename)
        if p.exists():
            # Open file descriptor
            if os.path.isfile(filename):
                return True
        else:
            return False
    except Exception, e:
        log.exception(str(e))
        return False


def readAnswerFile(filePath):
    if isinstance(filePath, (str, unicode)):
        fd = read_file(filePath)
        if fd:
            return True
    else:
        return False


def duplicate_file(source, destination):
    try:
        shutil.copyfile(source, destination)
    except Exception, e:
        log.exception(str(e))


def generate_new_answerfile(model=None, ver=None, type='pub', customerId=None, hostname=None, **kwargs):
    """
    Generate a new AnswerFile for a new Cisco UC Application based on a template and pre-defined parameters:
    :param model:
    :param ver:
    :param type:
    :param customerId:
    :param hostname:
    :param kwargs:
    :return:
    """
    # Models CUCM, CUC, CUP
    # Path /cisco/templates/<model>/<ver>/<type>/platformConfig.xml
    try:
        # Verify templates sources and customer destination is configured
        if hasattr(settings, 'answerFileTemplateDir') and hasattr(settings, 'answerFileCustomerDir'):
            if validator.check_folder(model) and validator.check_folder(ver):
                # Obtain Source file from settings file
                source = settings.answerFileTemplates + model + '/' + ver + '/'
                # Obtain destination from settings file
                destination = settings.answerFileCustomerDir + '/'

                if hasattr(settings, 'answerFile'):
                    if type == 'pub':
                        source = source + 'pub/'
                        duplicate_file(source, destination)
                    elif type == 'sub':
                        source = source + 'sub/'
                        duplicate_file(source, destination)
                    else:
                        return False
            else:
                log.error('generate_new_answerfile() Invalid model')
                return False
        else:
            log.error('generate_new_answerfile() Settings value configured incorrectly')
            return False

    except Exception, e:
        log.exception(str(e))
        return False


"""
if __name__ == '__main__':

    #filename = '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/catalogue/cisco/ucm/templates/90/pub/platformConfig.xml'
    filename = '/Users/gogasca/platformConfig.xml'
    if xml_processor.check_valid_xmlfile(filename):
        print 'Valid'
    else:
        print 'Invalid'

    if readAnswerFile(filename):
        xml_processor.print_contents(filename)
        xml_processor.update_hostname(filename, 'pub')
        print xml_processor.get_param_value(filename,'LocalHostName')
        #LocalHostDHCP
        if xml_processor.get_param_value(filename, 'LocalHostDHCP') == 'no':
            print 'Static IP Address'
        else:
            if xml_processor.get_param_value(filename, 'LocalHostDHCP') == 'yes':
                print 'DHCP'
        print xml_processor.get_param_value(filename, 'InstallType')
    else:
        print 'Invalid file'
"""
__version__ = '0.1'
