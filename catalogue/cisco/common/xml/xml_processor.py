__author__ = 'gogasca'

try:
    import xml.etree.cElementTree as ET
    from utils import utils
except ImportError:
    import xml.etree.ElementTree as ET
    # https://docs.python.org/2/library/xml.etree.elementtree.html
import logging
import re
from conf import logging_conf
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def indent(elem, level=0, more_sibs=False):
    """

    :param elem:
    :param level:
    :param more_sibs:
    :return:
    """
    i = "\n"
    if level:
        i += (level-1) * '  '
    num_kids = len(elem)
    if num_kids:
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
            if level:
                elem.text += '  '
        count = 0
        for kid in elem:
            indent(kid, level+1, count < num_kids - 1)
            count += 1
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
            if more_sibs:
                elem.tail += '  '
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
            if more_sibs:
                elem.tail += '  '

def check_valid_xmlfile(filename):
    try:
        tree = ET.parse(filename)
        root = tree.getroot()
        log.info('check_valid_xmlfile() Valid XML file')
        return True
    except Exception, e:
        log.error('check_valid_xmlfile() Invalid XML file ' + str(e))
        return False


def get_xml_version(filename):
    try:
        log.info('print_contents() Printing contents')
        tree = ET.parse(filename)
        root = tree.getroot()
        for child_of_root in root.iter('Version'):
            if child_of_root.text:
                log.info('Template version: ' + child_of_root.text)
                return child_of_root.text
    except Exception, exception:
        log.exception(str(exception))


def insert_element(filename, key_value=None, param_text=None, param_default=None, parameter_value=None):
    """
    <key>
        <ParamNameText>description</ParamNameText>
        <ParamDefaultValue>none</ParamDefaultValue>
        <ParamValue>parameter_value</ParamValue>
   </key>

   <NtpServer>
        <ParamNameText>Address Range for NTP server</ParamNameText>
        <ParamDefaultValue>none</ParamDefaultValue>
        <ParamValue>192.5.41.41</ParamValue>
   </NtpServer>
    :param filename:
    :param parameter:
    :param parameter_value:
    :return:
    """
    try:
        # tree = ET.parse(filename)
        tree = ET.parse(filename)
        root = tree.getroot()

        ####################################
        # Create node
        ####################################

        if root is not None:
            node = ET.SubElement(root, key_value)
            ET.SubElement(node, 'ParamNameText').text = param_text
            ET.SubElement(node, 'ParamDefaultValue').text = param_default
            ET.SubElement(node, 'ParamValue').text = parameter_value
            ET.dump(node)
            log.info('insert_node() Node created successfully')
            indent(root)
            tree.write(filename)
        else:
            log.error('insert_element() No root')
    except Exception, exception:
        log.exception('insert_node() {}'.format(exception))




def get_param_value(filename, parameter, returnDefault=False, **kwargs):
    """
    :param filename:
    :param parameter:
    :return:
    """
    try:
        log.info('xml_processor.get_param_value() Get parameter from XML file: ' + parameter)
        tree = ET.parse(filename.encode("utf-8"))
        root = tree.getroot()

        log.info('xml_processor.get_param_value() Root tag: ' + root.tag)
        if parameter and isinstance(parameter, (str, unicode)):
            log.info('xml_processor.get_param_value() Searching XML File: ' + parameter)
            for param in root.iter(parameter):
                paramValue = param.find('ParamValue')

        if paramValue.text:
            return paramValue.text
        else:
            return None

    except Exception, exception:
        log.exception(str(exception))
        raise


def set_param_value_subtree(filename, parameters=None, parameters_value=None, subtree=None):
    """

    :param filename:
    :param parameters:
    :param parameters_value:
    :param subtree:
    :return:
    """
    try:
        log.info('xml_processor.set_param_value_subtree() Set parameter from XML file: ' + subtree)
        tree = ET.parse(filename)
        root = tree.getroot()
        if len(parameters) != len(parameters_value):
            log.info('xml_processor.set_param_value_subtree() Invalid values: {} {}'.format(parameters,
                                                                                            parameters_value))
            return False
        index = 0
        xml_option = root.find(subtree)
        for param in parameters:
            paramValue = xml_option.find(param)
            paramValue.find('ParamValue').text = parameters_value[index]
            index += 1
            tree.write(filename)

        return True

    except Exception, exception:
        log.exception(str(exception))
        return False


def set_param_value(filename, parameter, newParamValue):
    """
    :param filename:
    :param parameter:
    :return:
    """

    try:
        log.info('xml_processor.set_param_value() XML file: {0}|{1}|{2} '.format(filename,
                                                                                 parameter,
                                                                                 newParamValue))
        tree = ET.parse(filename)
        root = tree.getroot()

        if parameter and isinstance(parameter, (str, unicode)):
            for param in root.iter(parameter):
                paramValue = param.find('ParamValue')

        # Issue #182
        if paramValue is not None and newParamValue:
            log.info('xml_processor.set_param_value() Parameter value: ' + paramValue.text)
            paramValue.text = newParamValue
            tree.write(filename)
            log.info('xml_processor.set_param_value() Update successful')
            log.info('xml_processor.set_param_value() New parameter value: ' + newParamValue)
            return True
        else:
            log.error('xml_processor.set_param_value() Update failed, Invalid parameter: {}'.format(parameter))
            return False

    except Exception, exception:
        log.exception(str(exception))


def update_hostname(filename, hostname=''):
    """
    LocalHostName
    :return:
    """
    try:
        print 'update_hostname() Updating LocalHostName'
        tree = ET.parse(filename)
        root = tree.getroot()
        log.info('Root tag: ' + root.tag)
        for param in root.iter('LocalHostName'):
            paramValue = param.find('ParamValue')

        log.info('Updating ParamValue: ' + paramValue.text + ' to ' + hostname)

        if utils.isValidHostName(hostname):
            paramValue.text = hostname
            log.info(paramValue.text)
            tree.write(filename)
            log.info('update_hostname() Update successful')
            return True
        else:
            log.error('update_hostname() Update failed, invalid Hostname')
            return False
    except Exception, exception:
        log.exception('update_hostname() Update failed...' + str(exception))


def validate_xml_version(filename, version_request):
    """

    :param filename:
    :param version_request:
    :return:
    """
    try:
        file_version = get_xml_version(filename)
        # 9.0.1  = 90
        # 10.5.1 = 105
        # 11.0.1 = 11
        file_version = ''.join(file_version.split('.'))
        print file_version
        if re.match(version_request, file_version):
            log.info('validate_version() Valid version found in XML')
            return True

        return False
    except Exception, exception:
        log.exception(str(exception))


def update_cluster_hostname(filename, is_publisher, hostname):
    """

    :param filename:
    :param is_publisher:
    :param hostname:
    :return:
    """
    try:
        tree = ET.parse(filename.encode("utf-8"))
        root = tree.getroot()
        if is_publisher:
            for param in root[1].iter('Hostname'):
                paramValue = param.find('ParamValue')
            log.info('update_cluster_hostname() Updating Publisher ParamValue: ' + paramValue.text + ' to ' + hostname)
            paramValue.text = hostname
            tree.write(filename)
        else:
            for param in root[2].iter('Hostname'):
                paramValue = param.find('ParamValue')
            log.info('update_cluster_hostname() Updating Subscriber ParamValue: ' + paramValue.text + ' to ' + hostname)
            paramValue.text = hostname
            tree.write(filename)

    except Exception, exception:
        log.exception(str(exception))


def update_cluster_ip(filename, is_publisher, hostname):
    """

    :param filename:
    :param is_publisher:
    :param hostname:
    :return:
    """
    try:
        tree = ET.parse(filename.encode("utf-8"))
        root = tree.getroot()
        if is_publisher:
            for param in root[1].iter('IpAddress'):
                paramValue = param.find('ParamValue')
            log.info('update_cluster_ip() Updating Publisher ParamValue: ' + paramValue.text + ' to ' + hostname)
            paramValue.text = hostname
            tree.write(filename)
        else:
            for param in root[2].iter('IpAddress'):
                paramValue = param.find('ParamValue')
            log.info('update_cluster_ip() Updating Subscriber ParamValue: ' + paramValue.text + ' to ' + hostname)
            paramValue.text = hostname
            tree.write(filename)

    except Exception, exception:
        log.exception(str(exception))

