__author__ = 'gogasca'

import platform
import sys
from kombu import Exchange, Queue
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')

ADMINS = (
    ('Gonzalo Gasca Meza', 'gonzalo@parzee.com'),
    ('Viraj Raut', 'viraj@parzee.com'),
)

from conf import settings

CELERYD_CHDIR = settings.filepath
CELERY_ACCEPT_CONTENT = ['json', 'pickle', 'yaml']
CELERY_IGNORE_RESULT = False
CELERY_RESULT_BACKEND = "amqp"
CELERY_TASK_RESULT_EXPIRES = 18000
CELERY_RESULT_PERSISTENT = True
BROKER_URL = 'amqp://imbue:imbue@rabbitmq:5672'
CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = "US/Eastern"
CELERY_IMPORTS = ("catalogue.cisco.voice.cluster.deploy_cluster",
                  "catalogue.cisco.common.install_uc",
                  "hypervisor.esxi.deploy_esx",
                  "hypervisor.esxi.vm_operations",
                  "shared.Report",
                  "tools.deploy_tools",)

CELERY_DEFAULT_QUEUE = "default"
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    Queue('gold', Exchange('imbue'), routing_key='imbue.gold'),
    Queue('silver', Exchange('imbue'), routing_key='imbue.silver'),
    Queue('bronze', Exchange('imbue'), routing_key='imbue.bronze'),
)
CELERY_DEFAULT_EXCHANGE = "default"
CELERY_DEFAULT_EXCHANGE_TYPE = "topic"
CELERY_DEFAULT_ROUTING_KEY = "default"

CELERY_ROUTES = {
    'catalogue.cisco.voice.cluster.deploy_cluster': {
        'queue': 'gold',
        'routing_key': 'imbue.gold',
    },
    'catalogue.cisco.common.install_uc': {
        'queue': 'gold',
        'routing_key': 'imbue.gold',
    },
    'hypervisor.esxi.deploy_esx': {
        'queue': 'silver',
        'routing_key': 'imbue.silver',
    },
    'hypervisor.esxi.vm_operations': {
        'queue': 'silver',
        'routing_key': 'imbue.silver',
    },
    'shared.Report': {
        'queue': 'silver',
        'routing_key': 'imbue.silver',
    },
}