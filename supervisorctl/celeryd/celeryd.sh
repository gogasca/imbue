#!/bin/bash
source ~/.profile
CELERY_LOGFILE=/usr/local/src/imbue/application/imbue/log/%n.log
CELERY_PROCESSES=25
CELERY_PID_FILE=/usr/local/src/imbue/application/imbue/log/%n.pid
CELERYD_OPTS="-P processes -c $CELERY_PROCESSES --loglevel=DEBUG --pidfile=$CELERY_PID_FILE"
cd /usr/local/src/imbue/application/imbue/conf
exec celery worker -n celeryd$1@%h -f $CELERY_LOGFILE $CELERYD_OPTS