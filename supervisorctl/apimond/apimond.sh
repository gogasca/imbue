#!/bin/bash
#
# Program: Test API
# Author: Gonzalo Gasca Meza <gonzalo@parzee.com>
# Parzee
# This program is distributed under the terms of the GNU Public License V2
trap "rm .f 2> /dev/null; exit" 0 1 3

API_USERNAME="ACda38cb38f1654200b439724edec211c1"
API_PASSWORD="43b6e91ca96217e0be09c6f3fe94ec31"
BASE_URL="https://loadbalancer:8443/api/1.0"
HEADERS="Content-Type: application/json"
LOG_FILE=/usr/local/src/imbue/application/imbue/log/apimond.log
#LOCK=/var/tmp/apimonlock
SLEEP_TIMER=10
ENVIRONMENT="PRODUCTION"
ENABLE_ALERTS=1
ERROR_COUNTER=0
ALARMS_SENT=1
PHONE_NUMBER="+14082186575"
REQUEST_COUNTER=1

sendAlarm() {
    echo "Send SMS alarm $1 $2"
    python /usr/local/src/imbue/application/imbue/alarms/send_sms.py $1 $2
}

processHttpStatus() {

    if [ "$#" -ne 2 ]; then
        echo "Usage: $0 processHttpStatus HTTP_CODE HTTP_URL " >&2
        exit 1
    fi


    MONITOR_LOG=$(date)
    # HTTP 200 OK Response

     if [ $2 -eq 200 ] || [ $2 -eq 202 ]; then

        ERROR_COUNTER=0
        ALARMS_SENT=0
        if [ -f $LOG_FILE ];  then
            echo "$MONITOR_LOG APIMOND_REQUEST $1 HTTP Code: $2" >> $LOG_FILE
        else
            touch $LOG_FILE
            echo "$MONITOR_LOG APIMOND_REQUEST $1 HTTP Code: $2" >> $LOG_FILE
        fi
    else
        echo "imbue() Request HTTP code: "$2" Request failed"
        # For each Non 200 OK we increase error counter
        ERROR_COUNTER=`expr $ERROR_COUNTER + 1`;
            # Non 200 OK Response
        echo "Alarm counter: $ERROR_COUNTER"
        if [ $ERROR_COUNTER -eq 6 ] && [ $ENABLE_ALERTS -eq 1 ]; then
            echo "$MONITOR_LOG APIMOND_REQUEST_FAILED $1 $2" >> $LOG_FILE
             # Send SMS to notify API is down!
            sendAlarm "APIMOND_REQUEST_FAILED|"$1"|"$ENVIRONMENT $PHONE_NUMBER
            echo "$MONITOR_LOG ERROR SMS Alarm sent" >> $LOG_FILE
            ALARMS_SENT=`expr $ALARMS_SENT + 1`;
            ERROR_COUNTER=`expr -6 \\* $ALARMS_SENT`
        fi


        if [ -f $LOG_FILE ];  then
            echo "$MONITOR_LOG URL $1 HTTP Code: $2" >> $LOG_FILE
        else
            touch $LOG_FILE
            echo "$MONITOR_LOG URL $1 HTTP Code: $2" >> $LOG_FILE
        fi
    fi
}

testPostUrl() {
    API_BASE_URL=$BASE_URL$1
    PARAMS=" | cut -d ':' -f2-3"
    start=$(date +"%T")
    HTTP_RESULT="$(curl -k -A "apimond" -s -o /dev/null -w %{http_code}:%{time_total} -u ${API_USERNAME}:${API_PASSWORD} -H "${HEADERS}" -X POST ${API_BASE_URL})";
    HTTP_STATUS=`echo $HTTP_RESULT | cut -d ':' -f 1`
    TOTAL_TIME=`echo $HTTP_RESULT | cut -d ':' -f 2`
    end=$(date +"%T")
    echo "Response: HTTP Code: ${HTTP_STATUS} | Start time: $start End time: $end |  Total time: ${TOTAL_TIME} | ${API_BASE_URL} | Request number: "$REQUEST_COUNTER
    REQUEST_COUNTER=`expr $REQUEST_COUNTER + 1`;
    # Monitor HTTP Response
    processHttpStatus $API_BASE_URL $HTTP_STATUS
}

testGetUrl() {
    API_BASE_URL=$BASE_URL$1
    PARAMS=" | cut -d ':' -f2-3"
    start=$(date +"%T")
    HTTP_RESULT="$(curl -k -A "apimond" -s -o /dev/null -w %{http_code}:%{time_total} -u ${API_USERNAME}:${API_PASSWORD} -H "${HEADERS}" ${API_BASE_URL})";
    HTTP_STATUS=`echo $HTTP_RESULT | cut -d ':' -f 1`
    TOTAL_TIME=`echo $HTTP_RESULT | cut -d ':' -f 2`
    end=$(date +"%T")
    echo "Response: HTTP Code: ${HTTP_STATUS} | Start time: $start End time: $end |  Total time: ${TOTAL_TIME} | ${API_BASE_URL} | Request number: "$REQUEST_COUNTER
    REQUEST_COUNTER=`expr $REQUEST_COUNTER + 1`;
    # Monitor HTTP Response
    processHttpStatus $API_BASE_URL $HTTP_STATUS
}


while true; do
    echo 'imbue() Echo()'
    testGetUrl "/echo/"
    echo 'imbue() Hosts()'
    testGetUrl "/host/"
    echo 'imbue() Jobs()'
    testGetUrl "/job/"
    echo 'imbue() Infrastructure()'
    testGetUrl "/infrastructure/"
    echo 'imbue() Echo Database()'
    #testPostUrl "/echo/"
    echo "Waiting..."
    sleep $SLEEP_TIMER;
done