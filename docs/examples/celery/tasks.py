__author__ = 'gogasca'

from celery.task import task

@task
def multiply(x, y):
        multiplication = x * y
        return "The result is " + str(multiplication)
@task
def addition(op1=1, op2=1, **kwars):
        result = op1 + op2
        return  "The result is " + str(result)