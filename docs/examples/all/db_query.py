__author__ = 'gogasca'
def get_server(hostname=None):
        try:
            from conf import settings
            from database import Db

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)
            sqlquery = """
                            SELECT COUNT(*)
                            FROM server
                            WHERE server"""

            db_instance.initialize()
            # TODO Check query returns more than 1 row
            # https://pypi.python.org/pypi/sqlparse

            if hostname:
                sqlquery = sqlquery + ".hostname=" + "'" + str(hostname) + "'"

            server_info = db_instance.query(sqlquery)
            if len(server_info)>0:
                if list(server_info[0])[0] == 0:
                    return False
                elif list(server_info[0])[0] == 1:
                    return True
                else:
                    return False
            if server_info:
                return True
            else:
                return False
        except Exception, e:
            from error import exceptions
            raise exceptions.DBReadException('Server.initialization failed. Db query error')

from server import Server
esx1 = Server.Server(hostname='esx-ziro1')
esx1.update(hostname='esx-ziro1',configuration=None)