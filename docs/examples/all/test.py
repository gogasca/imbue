__author__ = 'gogasca'

# import paramiko
# from pyVim import connect
import platform
import sys
import time
import warnings

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')
from scp import SCPClient
# import psycopg2
# from hypervisor.esxi import datastore_operations, file_operations, general, template_handler, vm_operations
from hypervisor.esxi import general


def initialize(hostname="1.1.1.1", **kwargs):
    for key, value in kwargs.items():
        if key == 'username':
            print value
        print key

    print hostname


def delete_servers():
    query = """
            TRUNCATE server RESTART IDENTITY CASCADE;
            """
    try:
        # print "delete_servers()"
        execute_query(connect(), query, 2)
    except Exception as e:
        print str(e)


def count_servers():
    query = """
                SELECT COUNT(*) FROM server;
                """
    try:
        # print "count_servers()"
        num_servers = execute_query(connect(), query, 1)
        return num_servers

    except Exception as e:
        print str(e)


def connect():
    """Connect to the PostgreSQL database.  Returns a database connection."""
    host = "198.199.110.24"
    dbname = "imbuedb"
    user = "imbue"
    password = "imbue"
    port = 5432

    conn_string = "host='%s' dbname='%s' user='%s' password='%s' port='%i'" \
                  % (host, dbname, user, password, port)

    return psycopg2.connect(conn_string)


def execute_query(conn=None, query="", operation=0, content="", **kwargs):
    """
    Execute
    """
    try:
        if conn:
            cur = conn.cursor()
            if operation == 0:
                # Query All
                cur.execute(query)
                rows = cur.fetchall()
                return rows
            elif operation == 1:
                # Query One Record value
                cur.execute(query)
                rows = cur.fetchone()[0]
                return rows
            elif operation == 2:
                # Delete Records
                cur.execute(query)
                conn.commit()
            else:
                # Anything else...
                print 'Unknow option'
                cur.execute(query)
                conn.commit()
                # conn.close()
    except Exception, e:
        print str(e)
        raise


def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if port:
        client.connect(server, port, user, password)
    else:
        client.connect(server, username=user, password=password)
    return client


def transfer_file_scp(remotePath=".", file='', host='', port='', username='', password='', **kwargs):
    if remotePath == '' or remotePath == None:
        print('Invalid directory path')
        return False
    if file == '' or file == None:
        print('Invalid file')
        return False
    # Connectivity check
    if host == '' or host == None:
        print('Invalid host')
        return False
    if username == '' or username == None:
        print('Invalid username')
        return False
    if password == '' or password == None:
        print('Invalid password')
        return False

    print 'Transferring file...'
    ssh = createSSHClient(host, port, username, password)
    scp = SCPClient(ssh.get_transport())
    scp.put(remote_path=remotePath, files=file)


def clientSSH(host=None, port=None, username=None, password=None, **kwargs):
    print "Creating SSH client"
    from paramiko import SSHClient

    client = SSHClient()
    client.load_system_host_keys()
    client.connect(hostname=host, port=port, username=username, password=password, timeout=15)
    remote_conn = client.invoke_shell()
    stdin, stdout, stderr = client.exec_command('test -d /vmfs/volumes/5010037f-81726ac0-2df6-a4934caa3e21 ; echo $?')
    err = stderr.readlines()
    out = stdout.readlines()
    if out:
        # Valid no error
        for line in out:
            print line.rstrip()
        return True


def check_https_connectivity(server=None, port=None, username=None, password=None, **kwargs):
    print('check_https_connectivity()')
    service_instance = None
    try:
        if port:
            service_instance = connect.SmartConnect(host=server,
                                                    port=int(port),
                                                    user=username,
                                                    pwd=password)
            print "HTTPS Test connectivity is successful..."
            return True

        else:
            service_instance = connect.SmartConnect(host=server,
                                                    port=443,
                                                    user=username,
                                                    pwd=password)
            print "HTTPS Test connectivity is successful..."
            return True

    except IOError as e:
        pass
        if not service_instance:
            print("Could not connect to the specified host using specified "
                  "username and password")
            return False

    except Exception, e:
        import sys

        exc_type, exc_value, exc_traceback = sys.exc_info()
        import traceback, os.path

        print str(e)
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print "HTTPS Test failed..."
        return False


def add_single_quotes(string):
    return '\'' + str(string) + '\''


def add_double_quotes(string):
    return '"' + str(string) + '"'


def send_command(commandList=None, server=None, port=None, username=None, password=None, timeout=10, toolsServer=0,
                 delay=2, **kwargs):
    """

    :param commandList:
    :param server:
    :param port:
    :param username:
    :param password:
    :param toolsServer:
    :param delay:
    :param timeout:
    :param kwargs:
    :return:
    """
    # Default port is None which is default used in libraries
    # we would not check it unless is defined in settings.py
    # Verify all parameters are populated or valid.
    # TODO need to check server is resolvable or valid IP address

    if commandList == None:
        commandList = settings.cmd

    print('general.send_command() ' + str(commandList))

    try:
        from paramiko import SSHClient

        client = SSHClient()
        # client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if port:
            client.connect(server, port=int(port), username=username, password=password, timeout=timeout)
        else:
            client.connect(server, username=username, password=password, timeout=timeout)

        print "SSH connection established to %s" % server
        for command in commandList:
            print "Executing command: " + command
            stdin, stdout, stderr = client.exec_command(command)
            err = stderr.readlines()
            out = stdout.readlines()
            if err:
                print "stderr: ", stderr.readlines()
                print "Error executing command: " + command
                for line in err:
                    print line.rstrip()
                return False
            if out:
                # Valid no error
                for line in out:
                    print line.rstrip()
            return True

    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        return False


def test_host():
    print 'Test 1'
    configuration = """{
          "db": true,
            "hypervisor":{
                "type": "esxi",
                "name":"esx-milpitas-2",
                "ip":"110.10.0.145",
                "username":"root",
                "password":"password",
                "ssh": 22,
                "https": 443,
                "vnc":"5900:5909" }
        }"""

    import json
    configuration = json.loads(configuration)
    from utils import validator
    if validator.check_host_request(configuration):
        print 'True'
    else:
        print 'False'


def get_network_services():
    hostname = 'esx-milpitas'
    from database import Db
    from conf import settings

    db_instance = Db.Db(server=settings.dbhost,
                        username=settings.dbusername,
                        password=settings.dbpassword,
                        database=settings.dbname,
                        port=settings.dbport)

    db_instance.initialize()

    sqlquery = """SELECT fk_network_services FROM server WHERE server"""
    if hostname:
        sqlquery = sqlquery + ".hostname=" + "'" + str(hostname) + "'"
        fk_network_services = db_instance.query(sqlquery)

        if len(fk_network_services) > 0:
            if list(fk_network_services[0])[0]:
                fk_network_services = list(fk_network_services[0])[0]
                print fk_network_services

    sqlquery = """SELECT properties FROM network_services WHERE id=""" + str(fk_network_services)
    properties = db_instance.query(sqlquery)
    l = list(properties[0])[0]
    print type(l)
    print l['ssh']['port']
    print l['https']
    print l['vnc']


def test_cucm_configuration():
    """
    The actual test.
    Any method which starts with ``test_`` will considered as a test case.
    """
    configuration = """{
              "type": "cucm",
              "hostname": "pub90",
              "publisher": true,
              "version": "90",
              "esx_network": "VM Network",
              "directory": "/vmfs/volumes/datastore1/parzee",
              "answer_file": "/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/catalogue/cisco/ucm/templates/90/pub/platformConfig.xml"
        }"""

    import json
    from utils import validator
    configuration = json.loads(configuration)
    print configuration
    if validator.check_imbue_cucm(configuration):
        print 'True'
    else:
        print 'False'


def get_vm_info(vm, depth=1, max_depth=10):
    """
    Print information for a particular virtual machine or recurse into a
    folder with depth protection
    """

    summary = vm.summary
    print "Name       : " + summary.config.name
    print "Guest      : " + summary.config.guestFullName

    annotation = summary.config.annotation
    if annotation:
        print "Annotation : " + annotation
    print "State      : " + summary.runtime.powerState
    if summary.guest is not None:
        ip = summary.guest.ipAddress
        if ip:
            print "IP         : " + ip


def print_vm_info(vm, depth=1, max_depth=10):
    """
    Print information for a particular virtual machine or recurse into a
    folder with depth protection
    """

    summary = vm.summary
    print "Name       : " + summary.config.name
    print "Path       : " + summary.config.vmPathName
    print "Guest      : " + summary.config.guestFullName
    print "UUID       : " + summary.config.instanceUuid
    annotation = summary.config.annotation
    if annotation:
        print "Annotation : " + annotation
    print "State      : " + summary.runtime.powerState
    if summary.guest is not None:
        ip = summary.guest.ipAddress
        if ip:
            print "IP         : " + ip
    if summary.runtime.question is not None:
        print "Question  : " + summary.runtime.question.text


def discover_datastores():
    from hypervisor.esxi import datastore_operations
    datastore_list = datastore_operations.get_datastore_list(
        server='110.10.0.144',
        username='root',
        password='password',
        httpsPort=443)  # Default datastore array

    return datastore_list


def get_datastores_path():

    datastores = discover_datastores()
    datastores_fullpath = []
    active_datastore = ''
    if datastores:
        for datastore in datastores:
            command = 'esxcli storage filesystem list | grep ' + datastore + ' | awk \'{ print $1}\''
            res = general.send_command(
                commandList=[command],
                server='110.10.0.144',
                username='root',
                password='password',
                port=22,
                display=True)

            if res:
                if res[0].encode('utf-8').rstrip():
                    print res
                    active_datastore = res[0].encode('utf-8').rstrip() + ":" + datastore
                    print active_datastore
                    datastores_fullpath.append(active_datastore)

        datastore_names = ",".join(str(x) for x in datastores_fullpath)
        print datastore_names

    else:
        print 'No datastores found'
        datastore_names = ""


def test_vmid():
    from pyVmomi import vim
    from pyVim import connect
    import ssl
    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context

    si = connect.SmartConnect(host='ziro1.noip.me', user='root', pwd='password', port=8026)
    content = si.RetrieveContent()
    children = content.rootFolder.childEntity
    for child in children:
        print child

    print 'Datacenter:'
    print children[0].name

    content = si.RetrieveContent()
    object_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                          [], True)
    for obj in object_view.view:
        if isinstance(obj, vim.VirtualMachine):
            print "Virtual machine: ",
            print obj.summary.config.name,
            print obj.summary.quickStats.uptimeSeconds

            #print obj
        if isinstance(obj, vim.Datastore):
            print obj
        if isinstance(obj, vim.Network):
            print 'Network:',
            print obj.name

    object_view.Destroy()


def get_datastores():
    from hypervisor.esxi import datastore_operations
    res = datastore_operations.get_datastores_details("110.10.0.144", 'root', 'password', 443, True)
    print res


def get_vm():
    hostname = 'esx-milpitas'
    from database import Db
    from conf import settings
    from utils import helper

    db_instance = Db.Db(server=settings.dbhost,
                        username=settings.dbusername,
                        password=settings.dbpassword,
                        database=settings.dbname,
                        port=settings.dbport)

    db_instance.initialize()

    # helper.execute_query()
    sqlquery = """SELECT vmid FROM virtualmachine WHERE virtualmachine.fk_server=87 ORDER BY vmid ASC"""
    # properties = db_instance.query(sqlquery)
    properties = helper.execute_query(sqlquery)
    properties = [item[0] for item in properties]
    print properties
    lista = [4, 34, 74, 84]
    for p in properties:
        if p not in lista:
            print p


if __name__ == '__main__':
    #
    # transfer_file_scp(remotePath="/vmfs/",file="templates/ucm.vmx",host="110.10.0.144",username="root",password='password')
    print "Testing...."
    #get_datastores_path()
    # get_vm()
    # test_cucm_configuration()
    # get_network_services()
    test_vmid()
    # get_datastores()



    """from utils.security import encrypt
    from conf import settings
    l = encrypt.encrypt("test",settings.url_encryption)
    print l
    from hypervisor.esxi import general
    res = general.send_command(commandList=['esxcfg-info | grep -A1 \'Serial Number\' | awk \'{print $2}\' |  head -n 1'],
    #res = general.send_command(commandList=['esxcli network ip connection list | grep vmx-mks | awk \'{print $4}\' | awk -F\':\' \'{print $2}\''],
                               server='110.10.0.144',
                               username='root',
                               password='password',
                               port='22',
                               display=True)

    try:
        if res:
            import re
            serial_number = re.search(r'(\w+)(\.+)(\w+)', res[0])
            serial_number = serial_number.group(3)
            print serial_number
        else:
            print 'Not found'
    except Exception,e:
        print e
    """

    """from conf import settings
    db = DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
    """
    # result = db.execute("""SELECT COUNT(*) FROM auth_user WHERE auth_user.id=1""")
    """for r in result:
        s = (list(r))[0]
    if s==1:
        print 'yes'
    """

    """
    from conf import settings
    res = general.run_command('mkdir ' + settings.log_folder + '123456789')
    if res == 0:
        print 'Success'
    else:
        print 'Failure'
    """
    """
    try:
        res = general.send_command(commandList=['vim-cmd vmsvc/getallvms | sed -e \'1d\' -e \'s/ \[.*$//\' | awk \'$1 ~ /^[0-9]+$/ {print $1":"substr($0,8,80)}\''],
                               server='ziro1.noip.me',
                               username='root',
                               password='password',
                               port='8126',
                               display=True)
        vm_list = {}

        if res:
            for vm in res:
                try:
                    vm.strip()
                    vmId = vm.split(':')[0]
                    if vmId.isdigit():
                        vm_list[vmId] = vm.split(':')[1].strip()
                except Exception:
                    pass
            import pprint
            #pprint.pprint(vm_list)

            from hypervisor.esxi import vm_operations
            vm_ipinfo = vm_operations.get_info(server='ziro1.noip.me',username='root',password='password',port='8026',getIpInfo=True)
            #pprint.pprint(vm_ipinfo)

            final_list = {}
            for key, value in vm_list.iteritems() :
                for element in vm_ipinfo:
                    if element['name'] == value:
                        final_list[key] = element

            pprint.pprint(final_list)




    except Exception, e:
        print str(e)
        raise

    """
    """#delete_servers()
    #print count_servers()
    #file = '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/conf/apps/ucm.yaml'
    file = '/Users/gogasca/Downloads/demo/ucm90/ucm.yaml'
    import yaml
    with open(file, 'r') as f:
        config = yaml.load(f)
    try:
        from server import Server
        esx1 = Server.Server(hostname='test')
        esx1.initialize(db=False, yaml_configuration=config)
        from utils import validator
        if validator.check_imbue_config(config):
            print 'Ok'
        else:
            print 'Error'

        if validator.check_imbue_esx(config['esx']):
            print 'Ok'
        else:
            print 'Error'
        #esx1.discover('all')
        #esx1.get_esx_version()
        #esx1.read_files()
        #esx1.read_network_interfaces()
        #esx1.read_network()
        #esx1.read_storage()
        #print esx1.find_all_vmids()
        #esx1.find_duplicate_vm('pub')
        #x = vm_operations.get_summary(server='110.10.0.144',username='root',password='password',port='22',vmId='32')
        #file_operations.delete_directory(server='110.10.0.144',username='root',password='password', sshPort='22',directory='/vmfs/volumes/datastore1/parzee/uc/ucm/WW8038S7')

    except KeyError:
        print 'Invalid key'
    except Exception,e:
        print e

    """

__version__ = '0.1'
