application_params = dict()
application_params['job'] = 'DEADBEEF'
application_params['owner_id'] = 1
id = 4

json_db = """{
    "glossary": {
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }
}"""

content = "\'" + application_params['job'] + '\',True,\'' + json_db + "\'," + str(application_params['owner_id']) + ',' + str(id)

print content