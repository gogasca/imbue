__author__ = 'gogasca'

import logging

import requests
import pytesseract
from PIL import Image
from PIL import ImageFilter
from conf import logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

def processImage(image_path):
    try:
        import os

        log.info('image_recognition.processImage() Processing image: ' + image_path)
        if os.path.isfile(image_path) and os.access(image_path, os.R_OK) and image_path:
            log.info('image_recognition.processImage() Image File exists and is readable: ' + image_path)
        else:
            log.info('image_recognition.processImage() Image File doesnt exist or is not readable: ' + image_path)
            return None
        # https://pypi.python.org/pypi/pytesseract
        im = Image.open(image_path)
        im.filter(ImageFilter.SHARPEN)
        im.filter(ImageFilter.EDGE_ENHANCE_MORE)
        # Defaul Language is English
        text = pytesseract.image_to_string(im, lang='eng')
        return text

    except Exception, e:
        log.exception(str(e))


if __name__ == '__main__':
    print processImage('/Users/gogasca/Downloads/vmdisk.png')