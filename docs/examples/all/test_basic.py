__author__ = 'gogasca'

# ##############################################################################################################################
# This function check for server connectivity via SSH and/or HTTPS This is specified in mode parameter.
# ##############################################################################################################################

from error import exceptions, app_exceptions
from hypervisor.esxi import datastore_operations, file_operations, general


def discoverServer(hypervisor, server, username, password, sshPort, httpsPort, mode, **kwargs):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param httpsPort:
    :param mode:
    :param kwargs:
    :return:
    """
    # Factory pattern
    # TODO Create Test cases

    try:
        print "application.discoverServer()"
        # Hypervisor ESXi VMWare 5.5
        if hypervisor == 1:
            # Check if I can connect to ESXi via SSH and HTTPS
            if (general.check_network_connectivity(server=server,
                                                   username=username,
                                                   sshPort=sshPort,
                                                   httpsPort=httpsPort,
                                                   password=password,
                                                   mode=mode)):

                pass
                # "Print log connectivity was succesful"
            else:
                return False
                raise exceptions.ConnectivityError('discoverServer() Unable to connect to host')
        else:
            print 'discoverServer() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor('application.discoverServer() Hypervisor model not available yet')
    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


# ##############################################################################################################################
# Check available datastores in specific server .This returns an array with Stores, default one is <datastore1>
# ##############################################################################################################################

def discoverServerDatastores(hypervisor, server, username, password, httpsPort, **kwargs):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param kwargs:
    :return:
    """
    try:
        print "application.discoverServerDatastore()"
        if hypervisor == 1:
            dsList = datastore_operations.getDS(server=server, username=username, password=password,
                                                httpsPort=httpsPort);
            # Default datastore array
            print 'application.discoverServerDatastore() Default datastore: ' + str(dsList)
            return dsList
        else:
            print 'application.discoverServerDatastore() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor(
                'application.discoverServerDatastore() Hypervisor model not available yet')
    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


# ##############################################################################################################################
# Check available datastores in specific server. This returns default Datastore, default one is <datastore1>
# ##############################################################################################################################

def selectDefaultDataStore(hypervisor, server, username, password, httpsPort, datastoreList, **kwargs):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastoreList:
    :param kwargs:
    :return:
    """
    try:
        if hypervisor == 1:
            if datastoreList:
                # Extract Datastores. Default datastore1
                defaultDatastore = file_operations.get_default_datastore(datastoreList)
                if defaultDatastore != None:
                    print 'application.selectDataStore() defaultDatastore: ' + defaultDatastore
                    return defaultDatastore

            raise exceptions.NoDataStoreDefined('application.selectDataStore() No DataStoreList defined')
            return
        else:
            print 'application.selectDataStore() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor('application.selectDataStore() Hypervisor model not available yet')
    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


# ##############################################################################################################################
# Verify datastore is accesible
# ##############################################################################################################################

def findDatastore(hypervisor, server, username, password, httpsPort, datastore, **kwargs):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastore:
    :param kwargs:
    :return:
    """
    try:
        print 'application.checkServerResources()'
        if hypervisor == 1:
            if datastore:
                datastore_operations.find_datastore_list(server=server, username=username, password=password, httpsPort=httpsPort,
                                            datastoreName=datastore)
                return True

            raise exceptions.NoDataStoreDefined('application.checkServerResources() Invalid datastore' + datastore)
            return
        else:
            print 'application.checkServerResources() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor('application.checkServerResources() Hypervisor model not available yet')
    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


# ##############################################################################################################################
# Print ESXi Server resources
# ##############################################################################################################################

def getServerDetails(hypervisor, server, username, password, httpsPort, datastore, **kwargs):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastore:
    :param kwargs:
    :return:
    """
    try:


        if hypervisor == 1:
            # getServerDetails from ESXi Server
            datastore_operations.printDSDirectory(server, username, password, httpsPort, datastore)
        else:
            print 'application.getServerDetails() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor('application.getServerDetails() Hypervisor model not available yet')

    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


# ##############################################################################################################################
# Obtain ESXi UUID for default datastore
# ##############################################################################################################################

def getServer_UUID(hypervisor, server, username, password, httpsPort, datastoreName):
    """

    :param hypervisor:
    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastoreName:
    :return:
    """
    try:
        print 'application.getServer_UUID()'
        if hypervisor == 1:
            # get_UUID from ESXi Server

            UUID = datastore_operations.get_UUID(server, username, password, httpsPort, datastoreName)
            return UUID
        else:
            print 'application.getServer_UUID() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor('application.getServer_UUID() Hypervisor model not available yet')
    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise


#
def transfer_floppy_drive(appIp,
                          appUsername,
                          appPassword,
                          appSSHPort,
                          remote_floppy_file,
                          local_floppy_file,
                          esxServer,
                          esxUsername,
                          esxPassword,
                          esxSSHPort,
                          esx_floppy_file_esx):
    """

    :param appIp:
    :param appUsername:
    :param appPassword:
    :param appSSHPort:
    :param remote_floppy_file:
    :param local_floppy_file:
    :param esxServer:
    :param esxUsername:
    :param esxPassword:
    :param esxSSHPort:
    :param esx_floppy_file_esx:
    :return:
    """
    general.get_file_scp(appIp, appUsername, appPassword, appSSHPort, remote_floppy_file, local_floppy_file)
    general.transfer_file_scp(esxServer,
                              esxUsername,
                              esxPassword,
                              esxSSHPort,
                              remoteFilePath=esx_floppy_file_esx,
                              localFile=local_floppy_file)


def create_working_directory(hypervisor, server, username, password, sshPort, httpsPort, datastore, remoteDirectory,
                             appId):
    try:
        print 'application.create_working_directory()'

        if hypervisor == 1:
            """
            appId
            value   app
            -----   ------
                1   cucm
                2   cuc
                3   cup
            """
            if appId == 1:
                remoteDirectory = remoteDirectory + 'uc/cucm'
                print 'application.create_working_directory() CUCM'
                print 'application.create_working_directory() Creating directory layout ' + remoteDirectory
                # Create Remote Directory in ESXi host. If its the first time create it, otherwise use it
                if file_operations.create_directory_layout(server, username, password, sshPort, httpsPort, datastore,
                                                           remoteDirectory):
                    return remoteDirectory
                else:
                    return None

            elif appId == 2:
                print 'CUC'
            elif appId == 3:
                print 'CUP'
            else:
                print 'create_working_directory() Unknown AppId'
                raise app_exceptions.AppNotFound('create_working_directory() Unknown AppId')
        else:
            print 'application.create_working_directory() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor(
                'application.create_working_directory() Hypervisor model not available yet')

    except Exception, e:
        print e
        raise


def create_vmk_file(hypervisor, server, username, password, sshPort, remoteDiskName, diskSize,
                    appId, params):
    try:
        print 'application.create_vmk_file()'

        if hypervisor == 1:
            """
            appId
            value   app
            -----   ------
                1   cucm
                2   cuc
                3   cup
            """
            if appId == 1:
                print 'application.create_vmk_file() CUCM'
                print 'application.create_vmk_file() Creating disk remotely ' + remoteDiskName
                # Create Remote Hard Disk in ESXi host. If its the first time create it, otherwise overwrite it!
                print remoteDiskName
                print diskSize
                print params

                if file_operations.create_harddisk(server, username, password, port=sshPort, remoteName=remoteDiskName,
                                                   size=diskSize):
                    return True
                else:
                    raise exceptions.createVMKException(
                        'application.create_vmk_file() Unable to create vmk file remotely')
                    return False

            elif appId == 2:
                print 'CUC'
            elif appId == 3:
                print 'CUP'
            else:
                print 'create_working_directory() Unknown AppId'
                raise app_exceptions.AppNotFound('create_working_directory() Unknown AppId')
        else:
            print 'application.create_working_directory() Hypervisor model not available yet'
            raise exceptions.UnknownHypervisor(
                'application.create_working_directory() Hypervisor model not available yet')

    except Exception, e:
        import sys
        import traceback, os.path

        exc_type, exc_value, exc_traceback = sys.exc_info()
        top = traceback.extract_stack()[-1]
        print ', '.join([type(e).__name__, os.path.basename(top[0]), str(top[1])])
        print e
        raise

    """

            #TODO Browse ISO Images in remote folder and select the best suitable
            # general.browseBootableISO(ds)

             # Create VM Hard Disk
            # def create_harddisk(diskSize='80G', path='/', diskName='disk.vmdk', server=None, port=None, username='', password='', **kwargs)
            #TODO Create Test cases
            general.create_harddisk(diskSize=settings.diskSize,
                                    path=DsPath,
                                    diskName=settings.hostname + '.vmdk',
                                    server=settings.esxIp,
                                    port=settings.esxPort,
                                    username=settings.esxUsername,
                                    password=settings.esxPassword)

            #TODO Create Test cases
            #pre_deployment_verification(vmxPath=None,vmxFile=None,server=None, port=None, username=None, password=None, **kwargs)
            general.pre_deployment_verification()


            #TODO Create Test cases
            # Initialize VM in Esxi
            # def register_vm(vmxpath=None, vmxFile=None, server=None, port=None, username=None, password=None, **kwargs)
            general.register_vm(vmxpath=DsPath,
                                vmxFile=customerVmxFile,
                                host=settings.esxIp,
                                port=settings.esxPort,
                                username=settings.esxUsername,
                                password=settings.esxPassword)

            # TODO Create Test cases
            #update_firewall(service='gdbserver', set='true',server=None, port=None, username=None, password=None, **kwargs):
            general.update_firewall(service='gdbserver',
                                    server=settings.esxIp,
                                    port=settings.esxPort,
                                    username=settings.esxUsername,
                                    password=settings.esxPassword)



        """


def initializeApp():
    pass
    # TODO Starts timer

    # TODO Create Test cases
    # Send VNC Commands
    # TODO
    # Check installation is completed!
