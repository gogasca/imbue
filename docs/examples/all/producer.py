__author__ = 'gogasca'

import logging

import pika


logging.basicConfig()

# http://stackoverflow.com/questions/6742938/deleting-queues-in-rabbitmq

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()

#durable=True When RabbitMQ quits or crashes it will forget the queues and messages unless you tell it not to.

channel.queue_declare(queue='esxi', durable=True)
channel.basic_publish(exchange='',
                      routing_key='esxi',
                      body='update',
                      properties=pika.BasicProperties(
                          delivery_mode=2,  # make message persistent
                      ))
print " [x] Sent 'database update'"
connection.close()
