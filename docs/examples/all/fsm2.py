__author__ = 'gogasca'
from random import randint
from time import clock

class Transition(object):
    def __init__(self,toState):
        self.toState = toState
    def Execute(self):
        print "Transitioning"

class State(object):
    """

    """
    def __init__(self,FSM):
        self.FSM = FSM
        self.timer = 0
        self.startTime = 0

    def Enter(self):
        self.timer = randint(0,5)
        self.startTime = int(clock())

    def Execute(self):
        pass

    def Exit(self):
        pass

class Sleep(State):
    def __init__(self,FSM):
        super(Sleep,self).__init__(FSM)

    def Enter(self):
        print "Preparing to clean the dishes"
        super(Sleep,self).Enter()

    def Execute(self):
        print "Sleeping"
        if (self.startTime + self.timer <= clock()):
            if not (randint(1,3)%2):
                self.FSM.ToTransition("toVacuum")
            else:
                self.FSM.ToTransition("toCleanDishes")

    def Exit(self):
        print "Waking up from Sleep"

class CleanDishes(State):
    def __init__(self,FSM):
        super(CleanDishes,self).__init__(FSM)

    def Enter(self):
        print "Preparing to clean the dishes"
        super(CleanDishes,self).Enter()

    def Execute(self):
        print "Cleaning Dishes"
        if (self.startTime + self.timer <= clock()):
            if not (randint(1,3)%2):
                self.FSM.ToTransition("toVacuum")
            else:
                self.FSM.ToTransition("toSleep")

    def Exit(self):
        print "Finished cleaning dishes"

class Vacuum(State):
    def __init__(self,FSM):
        super(Vacuum,self).__init__(FSM)

    def Enter(self):
        print "Preparing to vacuum"
        super(Vacuum,self).Enter()

    def Execute(self):
        print "Vacuuming now..."
        if (self.startTime + self.timer <= clock()):
            if not (randint(1,3)%2):
                self.FSM.ToTransition("toSleep")
            else:
                self.FSM.ToTransition("toCleanDishes")

    def Exit(self):
        print "Finished Vacuuming"

class FSM(object):
    """

    """
    def __init__(self,character):
        self.char = character
        self.states = {}
        self.transitions = {}
        self.curState = None
        self.prevState = None # To prevent Looping
        self.trans = None

    # Add transitions
    def AddTranstion(self,transName, transition):
        self.transitions[transName] = transition

    # Add States
    def AddState(self,stateName, state):
        self.states[stateName] = state

    def SetState(self,stateName):
        self.prevState = self.curState
        self.curState = self.states[stateName]

    def ToTransition(self,toTrans):
        self.trans = self.transitions[toTrans]

    def Execute(self):
        if (self.trans):
            self.curState.Exit()
            self.trans.Execute()
            self.SetState(self.trans.toState)
            self.curState.Enter()
            self.trans = None   # Reset transition
        # Update current state
        self.curState.Execute()

Char = type("Char",(object,),{})

class RobotMaid(Char):
    """

    """
    def __init__(self):
        self.FSM =  FSM(self)
        ## STATES
        self.FSM.AddState("Sleep",Sleep(self.FSM))
        self.FSM.AddState("CleanDishes",CleanDishes(self.FSM))
        self.FSM.AddState("Vacuum",Vacuum(self.FSM))

        ## Transitions
        self.FSM.AddTranstion("toSleep",Transition("Sleep"))
        self.FSM.AddTranstion("toVacuum",Transition("Vacuum"))
        self.FSM.AddTranstion("toCleanDishes",Transition("CleanDishes"))

    def Execute(self):
        self.FSM.Execute()

if __name__ == "__main__":
    r = RobotMaid()
    r.FSM.SetState("Sleep")

    for i in xrange(20):
        startTime = clock()
        timeInterval = 1
        while(startTime + timeInterval > clock()):
            pass

        print '...'
        r.Execute()