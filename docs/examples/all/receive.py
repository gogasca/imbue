__author__ = 'gogasca'

import logging

import pika


logging.basicConfig()

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='esxi')

print ' [*] Waiting for database updates. To exit press CTRL+C'


def callback(ch, method, properties, body):
    print " [x] Received %r" % (body,)
    if body == 'update':
        print 'Database has been updated'
    elif body == 'insert':
        print 'Database contains a new record'
    elif body == 'insert':
        print 'Database record has been deleted'
    else:
        print 'Unknown message'
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='esxi',
                      no_ack=True)

channel.start_consuming()