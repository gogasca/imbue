__author__ = 'gogasca'

import time
import logging, sys

sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')
from conf import logging_conf, settings
from utils import Generator
# Generate Job id
import time
# # dd/mm/yyyy format
print (time.strftime("%d%m%Y"))

id_ = Generator.Generator()
job_id = id_.generate_job(8)
log_file = 'trace_' + (time.strftime("%d%m%Y")) + '_'

log = logging_conf.LoggerManager().getLogger("__app___", cli=True, logging_file=log_file + job_id + '.log')
log.setLevel(level=logging.DEBUG)
log.info(
    '----------------------------------------Initializing IMBUEapp CLI----------------------------------------')
# Check time
start = time.time()

from database import db_connection

if db_connection.dbInitialization():
    job = '12345678'
    # psql -h <HOSTNAME> -d <DATABASE_NAME> -U <USERNAME> -W
    #\dt
    #\l
    print 'imbue_cli|DB|Database connection is succesful'
    log.info('imbue_cli|DB|Database connection is succesful')
    from conf import settings
    from database import Db

    db_instance = Db.Db(server=settings.dbhost,
                        username=settings.dbusername,
                        password=settings.dbpassword,
                        database=settings.dbname,
                        port=settings.dbport)

    db_instance.initialize()
    sqlquery = """INSERT INTO job (status,description,reference)"""
    content = "'1','" + 'Initialized' + "','" + job + "'"
    job_id = db_instance.insert(sqlquery, content)
    print 'Job id: ' + str(job_id)
    log.info('imbue_cli.insert() New Job Id: ' + str(job_id))
else:
    log.exception('imbue_cli|DB|Unable to connect to database')
    print 'imbue_cli|DB|Unable to connect to database'

sqlquery = """UPDATE job SET status='-1',description='Tools server found' WHERE job.id=""" + str(job_id)
db_instance.update(sqlquery)
