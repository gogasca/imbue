__author__ = 'gogasca'

import time, sys, getopt
import warnings
warnings.filterwarnings("ignore")
import platform
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')

from utils import helper
from conf import logging_conf, settings

import logging
def test(config_file=None, log_file=None, job='1', interactive=False, **kwargs):
    """
    IMBUE APP CLI application
    python -W ignore imbue_cli -i <settings.yaml>
    :return:
    """

    log = logging_conf.LoggerManager().getLogger("__app___", logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '----------------------------------------Initializing IMBUEapp CLI----------------------------------------')
    # Check time
    start = time.time()
    log.info(start)


def main(argv):
    try:
        configfile = ''
        supervised = False
        logfile = ''
        from utils import banner
        print '\n***********************************************************************************************************************'
        banner.horizontal('Parzee INC test')
        print '***********************************************************************************************************************'
        opts, args = getopt.getopt(argv, "hc:l:i", ["configfile=", "log=", "interactive="])
    except getopt.GetoptError:
        print 'Usage: imbue_cli.py -c <configfile> -l <logfile> -i'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print 'imbue_cli.py -c <configfile> -l <outputfile> -i'
            sys.exit()
        elif opt in ("-c", "--config"):
            configfile = arg

        elif opt in ("-l", "--log"):
            logfile = arg

        elif opt in ("-i", "--interactive"):
        # Supervised installation
            supervised = True

    # Generate Job id
    from utils import Generator

    id = Generator.Generator()
    job_id = id.generate_job(8)

    if configfile:

        print helper.print_time() + ' |imbue_cli| INFO | Config file is: ', configfile
        if logfile:
            print helper.print_time() + ' |imbue_cli| INFO | Log file is: ', logfile
        else:
            # Use log_folder
            logfile = settings.log_folder + 'trace_' + (time.strftime("%d%m%Y")) + '_' + job_id + '.log'
            print helper.print_time() + ' |imbue_cli| INFO | Log file is: ', logfile
        print helper.print_time() + ' |imbue_cli| INFO | Job id: ', str(job_id)

        test(config_file=configfile, log_file=logfile, job=job_id)
    else:
        print helper.print_time() + ' |imbue_cli| ERROR | Please enter valid input file'

if __name__ == "__main__":
    main(sys.argv[1:])