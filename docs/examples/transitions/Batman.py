__author__ = 'gogasca'

from transitions import Machine
from transitions import core

class IMBUE(object):

    # Define some states. Most of the time, narcoleptic superheroes are just like
    # everyone else. Except for...
    states = ['starting', 'reading_image', 'skipping_check','formatting', 'initializing', 'error', 'completed', 'exit']

    def __init__(self, name):

        # No anonymous superheroes on my watch! Every narcoleptic superhero gets
        # a name. Any name at all. SleepyMan. SlumberGirl. You get the idea.
        self.name = name

        self.version = '11'
        # What have we accomplished today?
        self.install_started = False

        # Initialize the state machine
        self.machine = Machine(model=self, states=IMBUE.states, initial='starting')

        # add some transitions. We could also define these using a static list of
        # dictionaries, as we did with states above, and then pass the list to
        # the Machine initializer as the transitions= argument.

        # =========================================================
        # When VNC is open and VM is power on, initialize
        # =========================================================

        self.machine.add_transition('execute',
                                    'starting',
                                    'reading_image',
                                    after='read_image')


        # =========================================================
        # Media check to format hard disk
        # =========================================================

        self.machine.add_transition(trigger='found_mediacheck_screen',
                                    source='skipping_check',
                                    dest='formatting',
                                    after='read_image')

        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # =========================================================
        # Found hard disk message
        # =========================================================

        self.machine.add_transition(trigger='found_harddisk_screen',
                                    source='formatting',
                                    dest='initializing',
                                    conditions=['is_initialization_needed'],
                                    after='read_image')

        # =========================================================
        # Found hard disk message when we need to reinitialize HD
        # =========================================================

        self.machine.add_transition(trigger='found_harddisk_screen',
                                    source='formatting',
                                    dest='completed')

        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # =========================================================
        # Found initialize message
        # =========================================================

        self.machine.add_transition(trigger='found_initialize_harddisk_screen',
                                    source='initializing',
                                    dest='completed')

        # =========================================================
        # Found initialize message
        # =========================================================

        self.machine.add_transition(trigger='didnt_find_initialize_harddisk_screen',
                                    source='initializing',
                                    dest='completed')

        # =========================================================
        # Our IMBUE can go into error state at any point
        # =========================================================

        self.machine.add_transition('error_detected', '*', 'error')



        # When they get off work, they're all sweaty and disgusting. But before
        # they do anything else, they have to meticulously log their latest
        # escapades. Because the legal department says so.
        self.machine.add_transition('start_install',
                                    'completed',
                                    'exit',
                                    after='start_installation')


    # =========================================================
    # Read image
    # =========================================================

    def read_image(self):
        print 'CALL: read_image() Reading image'
        print '...'
        import time
        time.sleep(3)
        image = 1
        if image == 1:
            print 'read_image() Found skip media check'
            self.sendvnc_skipmediacheck()
        elif image == 2:
            self.sendvnc_formatharddisk()
        elif image == 3:
            self.sendvnc_initialize()
        else:
            return -1


    def sendvnc_skipmediacheck(self):
        print 'CALL: sendvnc|skipmediacheck() Trying to send VNC keys for media check'
        mediacheckscreen = True
        if mediacheckscreen:
            print 'CALL: sendvnc|skipmediacheck() Media check screen found successfully'
            print "State: " + self.state
            print '...'
            import time
            time.sleep(3)
            self.found_mediacheck_screen()

        else:
            self.error_detected()

    def sendvnc_formatharddisk(self):
        print 'CALL: sendvnc|formatharddisk() Trying to send VNC keys for format hard disk'
        formatharddisk = True
        if formatharddisk:
            print 'CALL: sendvnc|formatharddisk() Format Hard disk screen found'
            print "State: " + self.state
            print '...'
            import time
            time.sleep(3)
            self.found_harddisk_screen()
        else:
            self.error_detected()

    def sendvnc_initialize(self):
        print 'CALL: sendvnc|initialize() Trying to send VNC keys to initialize hard disk'
        initializeharddisk = True
        if initializeharddisk:
            print 'CALL: sendvnc|initialize() Hard disk initialized correctly'
            print "State: " + self.state
            print '...'
            import time
            time.sleep(3)
            self.found_initialize_harddisk_screen()
        else:
            print 'CALL: sendvnc|initialize() Hard disk was not initialized. Screen not found'
            print "State: " + self.state
            self.didnt_find_initialize_harddisk_screen()

    def start_installation(self):
        self.install_started = True

    def is_initialization_needed(self):
        if self.version == "11":
            print 'CALL: is_initialization_needed(): True Version: ' + self.version
            return True
        return False



if __name__== "__main__":
    imbue = IMBUE("IMBUE")
    print "State: " + imbue.state
    elapsed_time = 0
    try:
        imbue.execute()
    except core.MachineError:
        print 'ERROR: Invalid state'
        print "State: " + imbue.state

    if imbue.state == 'completed':
        print 'INFO: Completed'
        imbue.start_install()
        print "State: " + imbue.state
    else:
        print 'ERROR: Terminated with errors'
