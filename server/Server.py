import logging
import json
import re
from conf import logging_conf, settings
from database import Db
from error.exceptions import InvalidCredentials
from error.exceptions import DiscoveryException
from error import exceptions
from hypervisor.esxi import general, datastore_operations, file_operations, network_operations, vm_operations
from utils import validator
from utils.security import common
from utils import db_utils

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

__author__ = 'gogasca'


# =========================================================
# Main Server class
# =========================================================

class Server(object):
    """
        Main server class which helps manage ESXi server
    """
    INIT = 'init'
    BOOTING = 'booting'
    CONFIGURING = 'configuring'
    RUNNING = 'running'
    SHUTTING_DOWN = 'shutting_down'
    SHUTDOWN = 'shutdown'

    def __init__(self, name=None, hypervisor=settings.hypervisor_id, **kwargs):
        """
        Constructor method for Server

        During Initialization HTTPS by default is the only service active
        If active:
                We will attempt to initialize SSH service.
                If both HTTPS and SSH are succesful we set discovered to True.
        Status:
            -1 - SSH|HTTPS not accessible           | Discovered False - Initialization failed
            0 - Not in use                          | Discovery  True
            1 - SSH/HTTPS accessible                | Discovered True - Installation in progress

        :param name:
        :param kwargs:
        :return:
        """

        if name:
            if general.validator.isValidString(name, True):
                self.name = name
            else:
                raise exceptions.InvalidParameter('server.__init__() Invalid Server name')

        else:
            self.name = 'server_default'  # Default name

        self.state = self.INIT
        self.id = None
        self.description = None
        self.hypervisor = hypervisor  # Default hypervisor is 1.
        self.status = 0
        self.serial_number = settings.serial_number
        self.discovered = False
        self.datastore = None
        self.datastore_list = None

        # Foreign keys
        self.model = settings.esx_model_index
        self.ip_settings = None
        self.network_interface = None
        self.network_services = None
        self.server_credentials = None
        self.license = 'DEMO'
        self.version = None

        # Network information. Server object, username and password is the same
        # for ssh and https
        self.server = None
        self.username = None
        self.password = None
        self.sshPort = None
        self.httpsPort = None
        self.vnc_ports = None
        self.vncports_in_use = []

        log.info('server.__init__() Instance created')

    # =========================================================
    # Add VNC port in use. Return True/False
    # =========================================================

    def add_vnc_port(self, vnc_port):
        """

        :param vnc_port:
        :return:
        """
        if vnc_port in self.vncports_in_use:
            log.warn(
                'server.add_vnc_port() add_vnc_port Monitor port is in use already: ' +
                str(vnc_port))
            return False
        else:
            log.info(
                'server.add_vnc_port() add_vnc_port Allocating Monitor port: ' +
                str(vnc_port))
            self.vncports_in_use.append(vnc_port)
            return True

    # =========================================================
    # Get VNC ports and check which ones are in use via SSH
    # =========================================================

    def allocate_vnc_ports(self, returnList=False):
        """

        :return:
        """

        try:
            log.info(
                'server.allocate_vnc_ports() Checking active Monitor ports in server')
            res = general.send_command(
                commandList=[
                    'esxcli network ip connection list | grep vmx-mks | awk \'{print $4}\' | awk -F\':\' \'{print $2}\''],
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                display=True,
                close=True)

            vnc_port_list = []

            if res is not None:
                for vnc_port in res:
                    vnc_port = vnc_port.rstrip('\n')
                    if vnc_port.isdigit():
                        vnc_port_list.append(int(vnc_port))
                        log.info('server.allocate_vnc_ports() Allocate Monitor ports: ' + str(vnc_port))

                for port in vnc_port_list:
                    if self.add_vnc_port(port):
                        pass

                if returnList:
                    return vnc_port_list

            if returnList:
                return

        except Exception, exception:
            log.exception(exception.__repr__())
            if returnList:
                return None

    # =========================================================
    # Get ESXi OS version. Return True/False
    # =========================================================

    def create_working_directory(self, datastore, remote_directory):
        """

        :param datastore:
        :param remote_directory:
        :param appId:
        :param params:
        :return:
        """
        try:

            log.info('server.create_working_directory() Create_working_directory()')
            overwrite_check = False  # Issue 12
            # =========================================================
            # Check if we will overwrite directory using conf.settings
            # =========================================================
            try:
                if hasattr(settings, 'overwrite_check'):  # Default TRUE
                    overwrite_check = settings.overwrite_check
                else:
                    overwrite_check = False

            except Exception, exception:
                log.exception(exception.__repr__())

            log.info(
                'server.create_working_directory() Base install: ' +
                remote_directory)

            # =========================================================
            # Check if we will overwrite directory
            # =========================================================

            if overwrite_check:  # Issue #12
                log.info(
                    'server.create_working_directory() Verifying directory: ' +
                    remote_directory)
                if file_operations.check_remote_directory_exists(
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        port=self.sshPort,
                        directory=remote_directory):

                    log.info(
                        'server.create_working_directory() Directory exists working on existing directory')
                    return True

                else:
                    log.info(
                        'server.create_working_directory() Directory doesnt exist will create new one')

                    # Create Remote Directory in ESXi host. If its the
                    # first time create it, otherwise use it

                    if file_operations.create_directory_layout(
                            server=self.server,
                            username=self.username,
                            password=self.password,
                            sshPort=self.sshPort,
                            httpsPort=self.httpsPort,
                            datastoreName=datastore,
                            directory=remote_directory):
                        log.info(
                            'server.create_working_directory() Directory created successfully returning')
                        return True

                    else:
                        return False
            else:
                # Create Remote Directory in ESXi host. If its the first time create it, otherwise use it.
                # This may create an exception
                if file_operations.create_directory_layout(
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        sshPort=self.sshPort,
                        httpsPort=self.httpsPort,
                        datastoreName=datastore,
                        directory=remote_directory):
                    log.info(
                        'server.create_working_directory() Directory created successfully returning')
                    return True

                else:
                    return False

        except Exception as exception:
            log.exception(exception.__repr__())
            return False

    # =========================================================
    # Establish network connectivity to Server
    # =========================================================

    def discover(self, mode='all', init=False, get_status_code=False, **kwargs):
        """

        :param mode:
        :param init:
        :param get_status_code:
        :param kwargs:
        :return:
        """

        log.info("server.discover()")
        log.info('server.discover() Username: {} SSH: {} HTTPS: {}'.format(self.username,
                                                                           self.sshPort,
                                                                           self.httpsPort))
        # Hypervisor ESXi VMWare 5.5

        # Check if I can connect to ESXi via SSH and HTTPS
        check = general.check_network_connectivity(
            server=self.server,
            username=self.username,
            sshPort=self.sshPort,
            httpsPort=self.httpsPort,
            password=self.password,
            mode=mode,
            init=init,
            get_status_code=get_status_code)

        # =========================================================
        # Update network services
        # =========================================================

        if get_status_code:
            log.info('server.discover() Server processing status code. Updating network services')

            if check < 0:
                if check == -1:
                    log.error('server.discover() Invalid HTTPS credentials')
                else:
                    log.error('server.discover() No HTTPS/SSH connectivity')

                network_services = -1
                self.discovered = False

            elif check == 0:
                log.info('server.discover() Server initializing')
                network_services = 0

            elif check == 1:
                log.info('server.discover() HTTPS/SSH connectivity')
                network_services = 1

            else:
                log.warn('server.discover() Invalid status')
                network_services = 0

        # =========================================================
        # Check discovery status
        # =========================================================

        if check >= 0:

            # "Print log connectivity was successful"
            log.info('server.discover() Server discovery connectivity was successful')

            if init:
                log.info(
                    'server.discover() Server discovery was initialized and ssh service was enabled')
                self.status = 1
            else:
                log.info(
                    'server.discover() Server discovery was initialized and ssh service was enabled')
                self.status = 2

            self.discovered = True
            self.state = self.RUNNING

            # =========================================================
            # Update network services ONLINE HTTP/SSH
            # =========================================================

            if self.name and get_status_code:
                self.update_network_status(
                    hostname=self.name,
                    status=network_services)
            return True

        else:
            log.error(
                'server.discover() Unable to connect to ESXi host')
            self.status = 0
            self.discovered = False

            # =========================================================
            # Update network services OFFLINE HTTPS/SSH
            # =========================================================

            if self.name and get_status_code:
                self.update_network_status(hostname=self.name,
                                           status=network_services)

            # =========================================================
            # Throw exceptions
            # =========================================================

            if check == -1:
                raise InvalidCredentials('server.discover() Invalid HTTPS credentials')
            else:
                raise DiscoveryException('server.discover() Unable to discover server')

    # =========================================================
    # Obtain ESXi server datastores using ESXi API
    # =========================================================

    def discover_datastores(self):
        """

        :return:
        """
        try:
            log.info("server.discover_datastores()")
            datastore_list = datastore_operations.get_datastore_list(
                server=self.server,
                username=self.username,
                password=self.password,
                httpsPort=self.httpsPort)

            # Default datastore array
            log.info('server.discover_datastores() Datastores found: {0}'.format(datastore_list))
            return datastore_list

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Delete Remote directory
    # =========================================================

    def delete_working_directory(self, remote_directory):
        """

        :param datastore:
        :param remote_directory:
        :return:
        """
        log.info('server.delete_working_directory() Delete working directory()')
        if remote_directory:
            if file_operations.delete_directory(server=self.server,
                                                username=self.username,
                                                password=self.password,
                                                sshPort=self.sshPort,
                                                directory=remote_directory):
                log.info('server.delete_working_directory() Directory {} was deleted '.format(remote_directory))
                return True

        log.error('server.delete_working_directory() Directory {} was not deleted '.format(remote_directory))
        return False


    # =========================================================
    # Find ESXi server datastore
    # =========================================================

    def find_datastore(self, datastore):
        """

        :param datastore:
        :return:
        """
        try:
            log.info('server.find_datastore()')
            if datastore:
                datastore_operations.find_datastore_list(
                    server=self.server,
                    username=self.username,
                    password=self.password,
                    httpsPort=self.httpsPort,
                    datastoreName=datastore)
                return True

            raise exceptions.NoDataStoreDefined(
                'server.find_datastore() Invalid datastore' + datastore)

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Find VMs in server
    # =========================================================

    def find_vms(self, returnVmId=False, returnIpInformation=False, poweronOnly=False, **kwargs):  # Fix issue 42
        """

        :param returnVmId:
        :param returnIpInformation:
        :param poweronOnly:
        :param kwargs:
        :return:
        """

        try:
            log.info('server.find_vms()')

            res = general.send_command(commandList=[
                'vim-cmd vmsvc/getallvms '
                '| sed -e \'1d\' -e \'s/ \[.*$//\' | '
                'awk \'$1 ~ /^[0-9]+$/ {print $1":"substr($0,8,80)}\''],
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                display=True,
                close=True)

            vm_list = {}
            vmid_list = []

            log.info('server.find_vms Result: ' + str(res))
            if res is not None:

                for virtual_machine in res:
                    try:
                        virtual_machine.strip()
                        vmId = virtual_machine.split(':')[0]
                        if vmId.isdigit():
                            vm_list[vmId] = virtual_machine.split(':')[
                                1].strip()
                            if returnVmId:
                                vmid_list.append(vmId)
                    except Exception:
                        log.error(
                            'server.find_vms Error found parsing VM information')
                log.info('server.find_vms VM list in server: ' + str(vm_list))

                if returnIpInformation:
                    vm_ip = vm_operations.get_info(
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        port=self.httpsPort,
                        getIpInfo=True)
                    final_list = {}
                    for key, value in vm_list.iteritems():
                        for element in vm_ip:
                            if element['name'] == value:
                                final_list[key] = element
                    return final_list

                if poweronOnly:
                    vm_ip = vm_operations.get_info(
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        port=self.httpsPort,
                        getIpInfo=True)
                    vmid_list = []
                    for key, value in vm_list.iteritems():
                        for element in vm_ip:
                            if element['name'] == value and len(
                                    element['ip']) > 0:
                                vmid_list.append(key)
                    return vmid_list

                # Return VMid only
                if returnVmId:
                    return vmid_list
                else:
                    return vm_list

            # No VM found
            log.warn('server.find_vms No VM found')
            return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Find duplicated VMs in server
    # =========================================================

    def find_duplicate_vm(self, hostname):
        """

        :param hostname:
        :return:
        """
        # esxcfg-vmknic -l
        try:

            res = self.find_vms()
            log.info(
                'server.find_duplicate_vm() Looking for VM using: ' +
                hostname +
                ' in ' +
                str(res))
            if res:
                for vmId, vmName in res.iteritems():
                    if vmName.strip() == hostname:
                        return True
            else:
                return False

            # No duplicated VM found
            return False

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Get Server UUID
    # =========================================================

    def get_uuid(self):
        """

        :return:
        """
        try:
            log.info('server.get_uuid()')  # get_UUID from ESXi Server
            if self.is_https_opened():
                uuid = datastore_operations.get_uuid(self.server,
                                                     self.username,
                                                     self.password,
                                                     self.httpsPort)
                return uuid
            else:
                raise exceptions.HTTPSConnectivityError(
                    'server.findDatastore() HTTPS port is not opened')

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Get Server ID
    # =========================================================

    def get_server(self, name=None, owner_id=None, get_id=False, **kwargs):
        """

        :param name:
        :param owner_id:
        :param get_id:
        :param kwargs:
        :return:
        """

        log.info('server.get_server()')

        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        sqlquery = """SELECT COUNT(*) FROM server WHERE server"""
        db_instance.initialize()

        if name and owner_id:
            log.info('server.get_server() name and owner_id defined')
            sqlquery = sqlquery + ".name='" + str(name) + "' AND server.owner_id=" + str(owner_id)
        elif name:
            log.warn('server.get_server() name defined')
            sqlquery = sqlquery + ".name='" + str(name) + "'"
        else:
            log.error('server.get_server() Server not defined')
            return False

        log.info('server.get_server() Find server info SQL Query: {}'.format(sqlquery))
        server_info = db_instance.query(sqlquery)

        if server_info:
            if list(server_info[0])[0] == 0:
                log.error('server.get_server() Server not found')
                if get_id:
                    return
                return False

            elif list(server_info[0])[0] == 1:
                if name and owner_id:
                    log.info('server.get_server() Get id: name and owner_id defined')
                    sqlquery = """SELECT id FROM server WHERE server.name='""" + str(
                        name) + "' AND server.owner_id=" + str(owner_id)
                elif name:
                    log.info('server.get_server() Get id: name')
                    sqlquery = """SELECT id FROM server WHERE server.name='""" + str(name) + "'"
                else:
                    log.exception('server.get_server() Get id not defined')
                    return False

                log.info('server.get_server() Find server id SQL Query: {}'.format(sqlquery))
                server_id = db_instance.query(sqlquery)
                self.id = str(list(server_id[0])[0])
                log.info('server.get_server() Server id:' + str(self.id))
                if get_id:
                    return self.id
                return True
            else:
                log.exception('server.get_server() More than 1 Server found')
                raise RuntimeError
        else:
            return False

    # =========================================================

    # Read Server files
    # =========================================================

    def get_image_files(
            self,
            directory=None,
            patterns=['*.iso'],
            return_list=False,
            insert_db=False,
            **kwargs):
        """

        :param directory:
        :param pattern:
        :return:
        """
        if not directory:
            log.warn('server.get_image_files() Directory not defined. Using configured value')
            directory = file_operations.get_image_folder(self.id)

        directory_exists = file_operations.check_remote_directory_exists(
            directory=directory,
            server=self.server,
            username=self.username,
            password=self.password,
            port=self.sshPort)

        # Validate remote directory exists. If it doesnt, create a new one,
        # otherwise exit with error
        if not directory_exists:
            log.error(
                'server.get_image_files() Directory doesnt exist')
            logging.info('No ISO files in directory: ' + directory)
            return False

        log.info('server.get_image_files() Using directory: {} '.format(directory))
        try:
            file_list = []
            for pattern in patterns:
                res = general.send_command(
                    commandList=[
                        'ls -A1 ' +
                        directory +
                        pattern +
                        ' | sed -r \'s/^.+\///\''],
                    server=self.server,
                    username=self.username,
                    password=self.password,
                    port=self.sshPort,
                    display=True)

                if res:
                    for iso_file in res:
                        if return_list:
                            file_list.append(iso_file.strip())

            if return_list:
                if insert_db:
                    log.info('server.get_image_files() Updating database')
                    if self.id:
                        if file_list:
                            log.info('server.get_image_files() Files found')
                            iso_files = ",".join(str(x) for x in file_list)

                        else:
                            log.warn(
                                'server.get_image_files() No Files found')
                            iso_files = ""

                        log.info(iso_files)
                        sqlquery = """UPDATE server SET iso_files='""" + str(
                            iso_files) + """' WHERE server.id=""" + str(self.id)
                        log.info(sqlquery)
                        db_utils.update_database(sqlquery)

                return file_list

            else:
                return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Get hypervisor type
    # =========================================================

    def get_hypervisor(self):
        """

        :return:
        """
        return self.hypervisor

    # =========================================================
    # Get all server datastores
    # =========================================================

    def get_datastores(self):
        """

        :return:
        """
        log.info('server.get_datastores() Updating database')

        if self.id:
            datastores = self.discover_datastores()
            if datastores:
                log.info('server.get_datastores() Datastores found')
                datastore_names = ",".join(str(x) for x in datastores)

            else:
                log.warn(
                    'server.get_datastores() No Datastores found')
                datastore_names = ""

            log.info(datastore_names)
            sqlquery = """UPDATE server SET datastores='""" + str(datastore_names) + """' WHERE server.id=""" + str(
                self.id)
            log.info(sqlquery)
            db_utils.update_database(sqlquery)

    def get_datastore_path(self, datastore):
        """

        :param datastore:
        :return:
        """
        log.info('server.get_datastore_path() Updating database path')

        if self.id:
            datastores = self.discover_datastores()
            active_datastore = None

            if datastores:
                log.info('server.get_datastore_path() Datastores found')

                if datastore:
                    command = 'esxcli storage filesystem list | grep ' + datastore + ' | awk \'{ print $1}\''
                    log.info('server.get_datastore_path() Datastores found')
                    res = general.send_command(
                        commandList=[command],
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        port=self.sshPort,
                        display=True)

                    if res:
                        if res[0].encode('utf-8').rstrip():
                            # Full path format is: /vmfs/volumes/54a4195b-3938f170-e90e-0026b9f0f5dc:datastore1
                            active_datastore = res[0].encode('utf-8').rstrip()

                return active_datastore
            else:
                log.warn(
                    'server.get_datastore_path() No datastores found')
                datastore_names = ""

            log.info(datastore_names)
            sqlquery = """UPDATE server SET datastores_path='""" + str(
                datastore_names) + """' WHERE server.id=""" + str(
                self.id)
            log.info(sqlquery)
            db_utils.update_database(sqlquery)

        else:
            log.error('server.get_datastore_path() Server id not found')
            return

    # =========================================================
    # Print full datastores paths
    # =========================================================

    def get_datastores_path(self):
        """

        :return:
        """
        log.info('server.get_datastores_path() Updating database path')

        if self.id:
            datastores = self.discover_datastores()
            datastores_fullpath = []

            if datastores:
                log.info('server.get_datastores_path() Datastores found')

                for datastore in datastores:
                    command = 'esxcli storage filesystem list | grep ' + datastore + ' | awk \'{ print $1}\''
                    log.info('server.get_datastores_path() Datastores found')
                    res = general.send_command(
                        commandList=[command],
                        server=self.server,
                        username=self.username,
                        password=self.password,
                        port=self.sshPort,
                        display=True)

                    if res:
                        if res[0].encode('utf-8').rstrip():
                            # Full path format is: /vmfs/volumes/54a4195b-3938f170-e90e-0026b9f0f5dc:datastore1
                            active_datastore = res[0].encode('utf-8').rstrip() + ":" + datastore
                            datastores_fullpath.append(active_datastore)

                datastore_names = ",".join(str(x) for x in datastores_fullpath)

            else:
                log.warn(
                    'server.get_datastores_path() No datastores found')
                datastore_names = ""

            log.info(datastore_names)
            sqlquery = """UPDATE server SET datastores_path='""" + str(
                datastore_names) + """' WHERE server.id=""" + str(
                self.id)
            log.info(sqlquery)
            db_utils.update_database(sqlquery)

        else:
            log.error('server.get_datastores_path() Server id not found')
            return



    # =========================================================
    # Print ESXi server datastore
    # =========================================================

    def get_datastore_details(self, datastore):
        """

        :param datastore:
        :return:
        """
        try:
            log.info('server.get_server_details()')  # getServerDetails from ESXi Server
            datastore_operations.print_datastore_directories(
                self.server, self.username, self.password, self.httpsPort, datastore)
            return True

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Get all server information
    # =========================================================

    def get_datastores_details(self):
        """

        :return:
        """
        try:
            log.info('server.get_datastores_details() Getting Datastore JSON object')
            self.datastore_list = datastore_operations.get_datastores_details(server=self.server,
                                                                              username=self.username,
                                                                              password=self.password,
                                                                              https_port=self.httpsPort,
                                                                              json_format=True)

            log.info('server.get_datastores_details() {0}'.format(self.datastore_list))
            return self.datastore_list

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Read Network information
    # =========================================================

    def get_networks(self):
        """

        :return:
        """
        # esxcli network ip route ipv4 list
        try:
            log.info('server.get_networks()')

            networks = network_operations.get_networks(server=self.server,
                                                       username=self.username,
                                                       password=self.password,
                                                       port=self.httpsPort)
            if networks:
                network_list = ",".join(str(x) for x in networks)
                log.info(network_list)
                sqlquery = """UPDATE server SET networks='""" + str(
                    network_list) + """' WHERE server.id=""" + str(self.id)
                log.info(sqlquery)
                db_utils.update_database(sqlquery)
                return True
            else:
                return False

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Insert Server in database.
    # =========================================================

    def insert(
            self,
            ip_address=None,
            name='server_default',
            interface_name='eth0',
            description='Server',
            properties=None,
            ssh_username=None,
            ssh_password=None,
            **kwargs):
        """

        :param ip_address:
        :param name:
        :param interface_name:
        :param description:
        :param properties:
        :param ssh_username:
        :param ssh_password:
        :param kwargs:
        :return:
         Insert Server into Database:

         Add ip_settings
         ip, mask, gateway
         Get return id (fk_ip_settings)
         INSERT INTO ip_settings (ip) values ('1.1.1.1') RETURNING id;

         Add network interfaces
         name, status
         Get return id (fk_network_interfaces)
         INSERT INTO network_interfaces(name,status,fk_ip_settings) values ('eth0',True,1) RETURNING id;

         Add network services
         Properties: {"ssh": { "active": 1, "port": 22 }, "https": { "active": 1, "port": 443 },
         "vnc": { "active": 1, "port": 5901 }}
         Get return id (fk_network_services)
         INSERT INTO network_services (properties) VALUES ('{"ssh": { "active": 1, "port": 22 },
         "https": { "active": 1, "port": 443 }, "vnc": { "active": 1, "ports": "5900:5910" } }') RETURNING id;

         Add licensing
         Default: DEMO
         INSERT INTO licensing (key,type,active) values ('DEMO',1,TRUE) RETURNING id;

         Add server_credentials
         ssh_username, ssh_password, https_username, https_password
         Get return id (fk_server_credentials)
         INSERT INTO server_credentials (ssh_username,ssh_password,https_username,https_password)
         VALUES ('root','M1Nub3','root','M1Nub3') RETURNING id;

         Add server
         discovered = False
         status = 0
         fk_server_model = 13
         created = 'now()'

         INSERT INTO server (hostname,serialnumber,description,discovered,status,created,fk_server_model,
         fk_licensing,fk_network_interfaces,fk_server_credentials,fk_network_services)

        """
        try:

            log.info('server.insert()')
            # Set state
            self.state = self.CONFIGURING

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()

            ssh_password = common.encrypt(ssh_password)
            if not ssh_password:
                log.exception('tools.update() Unable to encrypt password')

            # =========================================================
            # Insert ip_settings values.
            # =========================================================

            sqlquery = """INSERT INTO ip_settings (ip)"""
            # In case user enters a no-ip like field
            log.info(
                'server.insert() Validating hostname|ip address: ' +
                str(ip_address))
            if validator.is_valid_ipv4_address(
                    str(ip_address)) or validator.isValidHostName(
                str(ip_address)):
                log.info(
                    'server.insert() Valid hostname|ip address: ' +
                    str(ip_address))
                content = "'" + ip_address + "'"
                fk_ip_settings = db_instance.insert(sqlquery, content)
                log.info(
                    'server.insert() fk_ip_settings: ' +
                    str(fk_ip_settings))
                self.ip_settings = str(fk_ip_settings)
            else:
                log.exception(
                    'server.insert() Invalid fk_ip_settings: ' +
                    ip_address)
                # from error import exceptions
                # raise exceptions.InvalidHostName('server.insert() fk_ip_settings invalid ip address')

            # =========================================================
            # Insert interfaces
            # =========================================================

            sqlquery = """INSERT INTO network_interfaces (name,status,fk_ip_settings)"""
            if validator.isValidString(interface_name):
                content = "'" + interface_name + \
                          "',True,'" + str(self.ip_settings) + "'"
                fk_network_interface = db_instance.insert(sqlquery, content)
                log.info(
                    'server.insert() fk_network_interface: ' +
                    str(fk_network_interface))
                self.network_interface = str(fk_network_interface)
            else:
                raise exceptions.InvalidHostName(
                    'server.insert() fk_ip_settings invalid ip address')

            # =========================================================
            # Insert network_services
            # =========================================================

            sqlquery = """INSERT INTO network_services (properties)"""
            if validator.is_json(properties):
                content = "'" + properties + "'"
                fk_network_services = db_instance.insert(sqlquery, content)
                log.info(
                    'server.insert() fk_network_services: ' +
                    str(fk_network_services))
                self.network_services = str(fk_network_services)
            else:
                raise exceptions.InvalidHostName(
                    'server.insert() fk_network_services invalid json')

            # =========================================================
            # Insert server_credentials
            # =========================================================

            sqlquery = """INSERT INTO server_credentials (ssh_username, ssh_password, https_username, https_password)"""
            if validator.isValidString(ssh_username) and validator.isValidString(ssh_password):
                # INSERT server_credentials
                content = "'" + ssh_username + "','" + ssh_password + \
                          "','" + ssh_username + "','" + ssh_password + "'"
                fk_server_credentials = db_instance.insert(sqlquery, content)
                log.info(
                    'server.insert() fk_server_credentials: ' +
                    str(fk_server_credentials))
                self.server_credentials = str(fk_server_credentials)
            else:
                raise exceptions.InvalidParameter(
                    'server.insert() server_credentials invalid credentials')

            # =========================================================
            # Insert licensing
            # =========================================================

            sqlquery = """INSERT INTO licensing (key,type,active)"""
            if validator.isValidString(self.license):
                # INSERT licensing
                content = "'Demo','1','True'"
                fk_licensing = db_instance.insert(sqlquery, content)
                log.info('server.insert() fk_licensing: ' + str(fk_licensing))
                self.license = str(fk_licensing)
            else:
                raise exceptions.InvalidParameter(
                    'server.insert() Invalid license')

            # =========================================================
            # Insert server object
            # =========================================================

            sqlquery = """INSERT INTO server (name, serialnumber, description, discovered, status, created,
            fk_server_model,fk_licensing,fk_network_interfaces,fk_server_credentials,fk_network_services)"""
            # Validate description
            if self.description is None:
                self.description = description

                # Validate description
            if self.name is None:
                self.name = name

            if validator.isValidString(self.name, True) and validator.isValidString(self.description):
                log.info(
                    'server.insert(): ' +
                    self.name +
                    '|' +
                    self.description +
                    '|' +
                    self.ip_settings +
                    '|' +
                    self.network_interface +
                    '|' +
                    self.server_credentials +
                    '|' +
                    self.network_services)

                content = "'" + self.name + "','" + self.serial_number + "','" + self.description + "','" + str(
                    self.discovered) + "','" + str(self.status) + "','now()','" + str(self.model) + "','" + str(
                    self.license) + "','" + str(self.network_interface) + "','" + str(
                    self.server_credentials) + "','" + str(self.network_services) + "'"

                self.id = db_instance.insert(sqlquery, content)
                log.info('server.insert() New Server server Id: ' + str(self.id))
            else:
                log.error('server.insert() Invalid server name')
                return False

        except Exception as exception:
            log.exception(str(exception))
            return False

    # =========================================================
    # Connect via SSH and check of we can read file system. Return True/False
    # =========================================================

    def initialize_filesystem(self):
        """

        :return:
        """
        try:
            log.info(
                'server.initialize_filesystem() application.create_working_directory()')

            res = file_operations.check_file_system_access(
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort)
            if res:
                return True

            return False

        except Exception, exception:
            log.exception(exception.__repr__())
            return False

    # =========================================================
    # Read database values and assign values to server
    # =========================================================

    def initialize(
            self,
            id=None,
            name=None,
            serial_number=None,
            owner_id=None,
            **kwargs):
        """

        :return:
        """

        if self:

            try:
                log.info('server.initialize() Started')

                self.state = self.BOOTING

                db_instance = Db.Db(server=settings.dbhost,
                                    username=settings.dbusername,
                                    password=settings.dbpassword,
                                    database=settings.dbname,
                                    port=settings.dbport)
                sqlquery = """
                            SELECT
                            server.name,
                            ip_settings.ip,
                            server.discovered,
                            server.status,
                            server_credentials.ssh_username,
                            server_credentials.ssh_password,
                            network_services.properties,
                            server.id
                            FROM server
                            INNER JOIN network_interfaces ON network_interfaces.id = server.fk_network_interfaces
                            INNER JOIN ip_settings ON ip_settings.id = network_interfaces.fk_ip_settings
                            INNER JOIN network_services ON network_services.id = server.fk_network_services
                            INNER JOIN server_credentials ON server_credentials.id = server.fk_server_credentials
                            WHERE server"""

                db_instance.initialize()

                # Fix multitenancy
                if name and owner_id:
                    log.info('server.initialize() name and owner_id defined')
                    sqlquery = sqlquery + ".name='" + str(name) + "' AND server.owner_id=" + str(owner_id)

                elif name:
                    sqlquery = sqlquery + ".name=" + \
                               "'" + str(name) + "'"
                elif id:
                    sqlquery = sqlquery + ".id=" + str(id)
                    self.id = id

                elif serial_number:
                    sqlquery = sqlquery + \
                               ".serialnumber" + str(serial_number)
                else:
                    sqlquery = sqlquery + ".name=" + \
                               "'" + str(self.name) + "'"

                server_info = db_instance.query(sqlquery)

                if server_info:
                    server_hostname = server_info[0][0]
                    server_ip = server_info[0][1]
                    if self:
                        self.discovered = server_info[0][2]
                        self.status = server_info[0][3]
                    server_username = server_info[0][4]
                    server_password = server_info[0][5]

                    server_password = common.decrypt(server_password)
                    if server_password:
                        log.info('server.initialize() Password decrypted')

                    if isinstance(server_info[0][6], dict):
                        log.info('server.initialize() Dictionary detected')
                        log.info(
                            'server.initialize() Server network services:  ' +
                            str(server_info))
                        server_ssh_port = server_info[0][6]['ssh']['port']
                        server_https_port = server_info[
                            0][6]['https']['port']
                        try:
                            self.vnc_ports = server_info[
                                0][6]['vnc']['port']
                        except KeyError:
                            log.warn('server.initialize() No Monitor port defined')
                        # Assign Server Id
                        log.info(server_info)
                        self.id = server_info[0][7]
                    else:
                        log.info('server.initialize() Non-Dictionary detected')
                        import json
                        log.info(
                            'server.initialize() Server network services:  ' +
                            str(server_info))
                        j = json.loads(server_info[0][6])
                        server_ssh_port = j['ssh']['port']
                        server_https_port = j['https']['port']
                        try:
                            self.vnc_ports = j['vnc']['port']
                        except KeyError:
                            log.warn('server.initialize() No Monitor port defined')
                        # Assign Server Id
                        log.info(server_info)
                        self.id = server_info[0][7]

                    self.set_network_info(
                        server_ip,
                        server_username,
                        server_password,
                        server_ssh_port,
                        server_https_port)

                    self.name = server_hostname
                    return True

                elif server_info is None:
                    log.warn('server.initialize() No server found')
                    return False

                else:
                    log.exception(
                        'server.initialize() Failed. DB query error no rows')
                    raise exceptions.DBReadException(
                        'server.initialize() failed. DB query error')

            except Exception, exception:
                log.exception(exception.__repr__())
                return False

    # =========================================================
    # Is SSH opened
    # =========================================================

    def is_ssh_opened(self):
        """

        :return:
        """
        return general.check_ssh_connectivity(
            self.server, self.username, self.password, self.sshPort)

    # =========================================================
    # Is HTTPS opened
    # =========================================================

    def is_https_opened(self):
        """

        :return:
        """
        return general.check_https_connectivity(
            self.server, self.username, self.password, self.httpsPort)

    # =========================================================
    # Read Network information
    # =========================================================

    def read_network(self):
        """

        :return:
        """
        # esxcli network ip route ipv4 list
        try:
            log.info('server.read_network()')
            res = general.send_command(
                commandList=['esxcli network ip route ipv4 list'],
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                display=True)

            if res:
                for filesystem in res:
                    log.info(filesystem.strip())
            else:
                return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Read Network interfaces
    # =========================================================

    def read_network_interfaces(self):
        """

        :return:
        """
        # esxcfg-vmknic -l
        try:
            log.info('server.read_network_interfaces() Reading network interface information')
            res = general.send_command(commandList=['esxcli network ip interface ipv4 get'],
                                       server=self.server,
                                       username=self.username,
                                       password=self.password,
                                       port=self.sshPort,
                                       display=True)

            if res:
                for interface in res:
                    log.info(interface.strip())
            return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Read Storage
    # =========================================================

    def read_storage(self):
        """

        :return:
        """
        # esxcli storage filesystem list
        log.info('server.read_storage()')
        try:
            res = general.send_command(
                commandList=['esxcli storage filesystem list'],
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                display=True)

            return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Remove VNC port in use. Return True/False
    # =========================================================

    def remove_vnc_port(self, vnc_port):
        """

        :param vncport:
        :return:
        """
        try:
            self.vncports_in_use.remove(vnc_port)
            log.info(
                'server.remove_vnc_port() remove_vnc_port Removing Monitor port: ' +
                str(vnc_port))
            return True
        except ValueError:
            log.info('server.remove_vnc_port() remove_vnc_port Monitor port not in use')
            return False

    # =========================================================
    # Select ESXi server datastore
    # =========================================================

    def set_datastore(self, datastore_list, datastore=None, **kwargs):
        """

        :param datastoreList:
        :return:
        """
        try:
            log.info('server.set_datastore() Check datastore_list() ')
            if datastore_list:
                # Extract Datastores. Default datastore1
                self.datastore = file_operations.get_default_datastore(datastore_list,
                                                                       datastore)
                if self.datastore:
                    log.info(
                        'server.set_datastore() Default datastore: ' +
                        self.datastore)
                    return self.datastore
            else:
                raise exceptions.NoDataStoreDefined(
                    'server.set_datastore() No datastore list defined')

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Define hypervisor type
    # =========================================================

    def set_hypervisor(self, value):
        """
        Define hypervisor value
        Version 1.0 Supports only 1 which is ESXi 5.5
        :param value:
        :return:
        """
        self.hypervisor = value

    # =========================================================
    # Defined network information found in database
    # =========================================================

    def set_network_info(self, server, username, password, sshPort, httpsPort):
        """
        Enter network information for Server access
        :param server:
        :param username:
        :param password:
        :param sshPort:
        :param httpsPort:
        :return:
        """

        # =========================================================
        # We assume username and password for ssh and https are the same.
        # =========================================================
        log.info('server.set_network_info()')

        if validator.check_authentication_parameters(
                server=server,
                username=username,
                password=password,
                sshPort=sshPort,
                httpsPort=httpsPort):

            log.info('server.set_network_info() Setting network information')
            self.server = server
            self.username = username
            self.password = password
            self.sshPort = sshPort
            self.httpsPort = httpsPort

        else:
            log.exception('server.set_network_info() Invalid parameters')
            raise exceptions.ConnectivityError(
                'server.set_network_info() Invalid parameters')

    # =========================================================
    # Set ESXi OS version. Return True/False
    # =========================================================

    def get_version(self):
        """

        :return:
        """
        return self.version

    # =========================================================
    # Set ESXi OS version. Return True/False
    # =========================================================

    def set_version(self):
        """

        :return:
        """
        try:
            # esxcli system version get | grep Version
            log.info('server.set_version()')
            version = network_operations.get_esx_version(server=self.server,
                                                         username=self.username,
                                                         password=self.password,
                                                         port=self.sshPort)
            if version:
                sqlquery = """UPDATE server SET version='""" + version + """' WHERE server.id=""" + str(self.id)
                db_utils.update_database(sqlquery)
                self.version = version
            return

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Set image folder
    # =========================================================

    def set_image_folder(self, image_folder):
        """

        :param image_folder:
        :return:
        """

        if image_folder:
            if file_operations.check_remote_directory_exists(
                    server=self.server,
                    username=self.username,
                    password=self.password,
                    port=self.sshPort,
                    directory=image_folder):

                log.info('server.set_image_folder() Remote directory exists')
                sqlquery = """UPDATE server SET image_folder='""" + image_folder + """' WHERE server.id=""" + str(
                    self.id)
                db_utils.update_database(sqlquery)
                log.info('server.set_image_folder() Image folder correctly updated: {}'.format(image_folder))
                return True
            else:
                log.error('server.set_image_folder() Image folder {} doesnt exist'.format(image_folder))
                return False

        return False

    # =========================================================
    # Set install folder
    # =========================================================

    def set_install_folder(self, install_folder):
        """

        :param install_folder:
        :return:
        """

        if install_folder:
            if file_operations.check_remote_directory_exists(
                    server=self.server,
                    username=self.username,
                    password=self.password,
                    port=self.sshPort,
                    directory=install_folder):

                log.info('server.set_install_folder() Remote directory exists')
                sqlquery = """UPDATE server SET install_folder='""" + install_folder + """' WHERE server.id=""" + str(
                    self.id)
                db_utils.update_database(sqlquery)
                log.info('server.set_install_folder() Install folder correctly updated: {}'.format(install_folder))
                return True
            else:
                log.error('server.set_install_folder() Install folder {} doesnt exist'.format(install_folder))
                return False

        return False

    # =========================================================
    # Read ESXi VM
    # =========================================================

    def synchronize(self, force=False, reset=False, **kwargs):
        """
        Read all VM from Database
        If successful delete existing VMs
        Insert new VM
        :param force:
        :param kwargs:
        :return:
        """

        # Get VM list
        try:

            log.info('server.synchronize() Started')

            # =========================================================
            # Return a Dictionary
            # =========================================================
            # {u'116': {'ip': '192.168.1.45','name': 'cpca1','uuid': '52be2710-0b7d-7967-f182-9ae816fe701b'}
            # vm_list = self.find_vms(returnIpInformation=True)

            if self.is_https_opened() and self.is_ssh_opened():
                log.info('server.synchronize() HTTPS and SSH ports are opened')
            else:
                log.info('server.synchronize() Unable to connect via HTTPS/SSH')
                sqlquery = """UPDATE server SET status=-1, discovered=False WHERE server.id=""" + str(self.id)
                log.info('server.synchronize() SQL Query: {}'.format(sqlquery))
                db_utils.update_database(sqlquery)
                return False

            log.info('server.synchronize() Getting Virtual machines...')

            vm_list = vm_operations.get_virtualmachines(
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.httpsPort)

            if vm_list:

                # =========================================================
                # Delete VM which are in DB
                # =========================================================

                log.warn(
                    'server.synchronize() Updating information: ' + str(self.id))
                if force:
                    log.info('server.synchronize() Force synchronization')
                    sqlquery = """DELETE FROM virtualmachine WHERE virtualmachine.fk_server=""" + str(self.id)

                else:
                    log.warn('server.synchronize() Force synchronization is False. Will update only VMs not active')
                    sqlquery = """SELECT vmid FROM virtualmachine WHERE status=1 AND virtualmachine.fk_server=""" + str(
                        self.id)
                    active_vm = db_utils.execute_query(sqlquery)
                    active_vm = [item[0] for item in active_vm]
                    sqlquery = """DELETE FROM virtualmachine WHERE status != 1 AND virtualmachine.fk_server=""" + str(
                        self.id)
                    log.info('server.synchronize() Active VirtualMachines {} '.format(active_vm))

                log.info('server.synchronize() SQL Query: {}'.format(sqlquery))
                db_utils.update_database(sqlquery)
                log.info('server.synchronize() Virtual machines table updated')

                # =========================================================
                # Re-insert VMs not in active installation
                # =========================================================

                for vmId, vmInfo in vm_list.iteritems():
                    # vmId, vmName
                    if force:
                        vm_operations.insert_vm(
                            vmId=vmId,
                            vm_hostname=vmInfo['name'],
                            status=0,
                            description=vmInfo['annotation'],
                            ip_information=vmInfo['ip'],
                            uuid=vmInfo['uuid'],
                            app_id=vmInfo['app_id'],
                            uptime=vmInfo['uptime'],
                            power_state=vmInfo['power_state'],
                            host_id=self.id)
                        log.info('server.synchronize() VM inserted {0} {1} '.format(vmId, vmInfo))
                    else:

                        if int(vmId) not in active_vm:
                            vm_operations.insert_vm(
                                vmId=vmId,
                                vm_hostname=vmInfo['name'],
                                status=0,
                                description=vmInfo['annotation'],
                                ip_information=vmInfo['ip'],
                                uuid=vmInfo['uuid'],
                                app_id=vmInfo['app_id'],
                                power_state=vmInfo['power_state'],
                                host_id=self.id)
                            log.info('server.synchronize() VM inserted {0} {1} '.format(vmId, vmInfo))
                        else:
                            log.info('server.synchronize() VM active installation {0} {1} '.format(vmId, vmInfo))
            else:
                # =========================================================
                # Delete All VM
                # =========================================================
                log.warn(
                    'server.synchronize() No VMs found in ESXi server. Updating Database')
                sqlquery = """DELETE FROM virtualmachine WHERE virtualmachine.fk_server=""" + str(self.id)
                db_utils.update_database(sqlquery)

            log.info('server.synchronize() Updating Serial Number')
            self.update_serialnumber()
            log.info('server.synchronize() Reading image files. Patterns: [*iso,*.ova]')
            self.get_image_files(return_list=True, patterns=['*.iso', '*.ova'], insert_db=True)
            log.info('server.synchronize() Reading datastore information')
            self.get_datastores()
            self.get_datastores_path()
            log.info('server.synchronize() Reading network information')
            self.get_networks()
            log.info('server.synchronize() Define ESX version')
            self.set_version()
            log.info('server.synchronize() Set last synch time')
            sqlquery = """UPDATE server SET discovered=True, last_synchronized=now() WHERE server.id=""" + str(self.id)
            db_utils.update_database(sqlquery)
            if reset:
                log.info('server.synchronize() Reset status to 0')
                sqlquery = """UPDATE server SET status=0 WHERE server.id=""" + str(self.id)
                db_utils.update_database(sqlquery)
            return True

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Update user Id
    # =========================================================

    def update_user_id(self, owner_id):
        """

        :param owner_id:
        :return:
        """
        try:
            log.info('server.update_user_id()')
            import numbers
            if isinstance(owner_id, numbers.Integral):
                from database import database_session
                log.info(
                    'server.update_user_id() Updating information for server id: {0}'.format(self.id))
                db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)

                result = db.execute(
                    """SELECT COUNT(*) FROM auth_user WHERE auth_user.id=""" + str(owner_id))

                for res in result:
                    count_result = (list(res))[0]

                # User id is found
                if count_result == 1:
                    log.info('server.update_user_id(): {}'.format(owner_id))

                else:
                    if count_result == 0:
                        log.warn(
                            'server.update_user_id() User id defined in owner_id field doesnt exist in DB')
                    log.error('server.update_user_id() Value: {0} not able to be updated'.format(owner_id))
                    return False

                db.execute("""UPDATE server SET owner_id='""" +
                           str(owner_id) + """' WHERE server.id=""" + str(self.id))

                return True
            else:
                log.error('server.update_user_id() Invalid owner_id')
                return False

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Update Server network in database.
    # =========================================================
    def update_network_status(self, hostname=None, port_option=0, status=0, **kwargs):
        """

        :param hostname:
        :param port_option: -1, 0, HTTPS_SSH, 1 HTTPS, 2 SSH, 3 VNC
        :param status:      -1 CLOSE, 0 NOT DISCOVERED, 1 OPENED
        :param kwargs:
        :return:
        """
        try:
            log.info('server.update_network_status()')
            self.state = self.CONFIGURING
            from database import Db
            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()

            sqlquery = """SELECT fk_network_services FROM server WHERE server"""
            if hostname:
                sqlquery = sqlquery + ".name='" + str(hostname) + "'"
                fk_network_services = db_instance.query(sqlquery)
                if len(fk_network_services) > 0:
                    if list(fk_network_services[0])[0]:
                        log.info('server.change_status() Server fk_network_services: ' +
                                 str(list(fk_network_services[0])[0]))
            else:
                return False

            sqlquery = """SELECT fk_network_services FROM server WHERE server"""
            if hostname:
                sqlquery = sqlquery + ".name='" + str(hostname) + "'"
                fk_network_services = db_instance.query(sqlquery)

                if len(fk_network_services) > 0:
                    if list(fk_network_services[0])[0]:
                        fk_network_services = list(fk_network_services[0])[0]
                        log.info(
                            'server.update() Server fk_network_services: ' + str(fk_network_services))
                else:
                    log.error('server.change_port_status() Error')

            sqlquery = """SELECT properties FROM network_services WHERE id=""" + \
                       str(fk_network_services)
            properties = db_instance.query(sqlquery)

            if isinstance(list(properties[0])[0], dict):
                properties = list(properties[0])[0]
                log.info(
                    'server.change_port_status() dictionary detected ' + str(properties))

                ssh_port = properties['ssh']['port']
                ssh_port_status = properties['ssh']['active']
                https_port = properties['https']['port']
                https_port_status = properties['https']['active']
                try:
                    vnc_port = properties['vnc']['port']
                    vnc_port_status = properties['vnc']['active']
                except KeyError:
                    pass

            else:
                log.info('change_port_status() non-dictionary detected' +
                         str(list(properties[0])[0]))
                j = json.loads(list(properties[0])[0])
                ssh_port = j['ssh']['port']
                https_port = j['https']['port']
                ssh_port_status = j['https']['active']
                https_port_status = j['https']['active']
                try:
                    vnc_port = j['vnc']['port']
                    vnc_port_status = j['vnc']['active']
                except KeyError:
                    pass

            # =========================================================
            # Update port status
            # =========================================================

            if status == 1:
                port_status = "1"
            elif status < 0:
                port_status = "-1"
            else:
                port_status = "0"

            # =========================================================
            # Update port status
            # =========================================================

            if port_option == 0:
                https_port_status = port_status
                ssh_port_status = port_status
            elif port_option == 1:
                https_port_status = port_status
            elif port_option == 2:
                ssh_port_status = port_status
            elif port_option == 3:
                vnc_port_status = port_status

            # =========================================================
            # Update database values
            # =========================================================

            if vnc_port:
                properties = '{"ssh": { "active": ' + str(ssh_port_status) + \
                             ', "port": ' + str(ssh_port) + ' },' + \
                             '"https": { "active": ' + str(https_port_status) + \
                             ', "port": ' + str(https_port) + ' },' + \
                             '"vnc": { "active": ' + str(vnc_port_status) + \
                             ', "port": ' + '"' + str(vnc_port) + '"' + ' }}'

            else:
                properties = '{ "ssh": { "active": ' + str(ssh_port_status) + \
                             ', "port": ' + str(ssh_port) + ' },' + \
                             '"https": { "active": ' + str(https_port_status) + \
                             ', "port": ' + str(https_port) + ' } }'

            sqlquery = """UPDATE network_services SET properties=""" + "'" + properties + "'" + \
                       """ WHERE network_services""" + ".id='" + \
                       str(fk_network_services) + "'"
            log.info(sqlquery)
            db_instance.update(sqlquery)
            return True

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Update firewall
    # =========================================================

    def update_firewall(self, operation='false', **kwargs):
        """

        :param flag:
        :param params:
        :param kwargs:
        :return:
        """
        log.info('server.update_firewall()')
        if network_operations.update_firewall(
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                service='gdbserver',
                set=operation):
            return True
        else:
            return False

    # =========================================================
    # Update Server in database.
    # =========================================================

    def update(self, id=id, configuration=None, **kwargs):
        """

        :param name:
        :param configuration:
        :param kwargs:
        :return:
        """
        try:
            log.info('server.update()')
            self.state = self.CONFIGURING
            from database import Db

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()

            # =========================================================
            # Check fk_network_interfaces
            # =========================================================

            sqlquery = """SELECT fk_network_interfaces FROM server WHERE server"""  # Issue 49
            if id:
                sqlquery = sqlquery + ".id=" + str(id)
                fk_network_interfaces = db_instance.query(sqlquery)
                if len(fk_network_interfaces) > 0:
                    if list(fk_network_interfaces[0])[0]:
                        log.info('server.update() Server fk_network_services: ' +
                                 str(list(fk_network_interfaces[0])[0]))
                else:
                    return False

            # =========================================================
            # Update description
            # =========================================================
            log.info('server() Update server description')
            description = configuration['host']['hypervisor']['description']
            sqlquery = """UPDATE server SET description='""" + description + """' WHERE server.id=""" + str(id)

            db_instance.update(sqlquery)

            # =========================================================
            # Check fk_ip_settings
            # =========================================================

            sqlquery = """SELECT fk_ip_settings FROM network_interfaces WHERE network_interfaces"""
            if fk_network_interfaces:
                sqlquery = sqlquery + ".id='" + \
                           str(list(fk_network_interfaces[0])[0]) + "'"
                fk_ip_settings = db_instance.query(sqlquery)
                if len(fk_ip_settings) > 0:
                    if list(fk_ip_settings[0])[0]:
                        log.info('server.update() Server fk_ip_settings: ' +
                                 str(list(fk_ip_settings[0])[0]))
                else:
                    return False

            # =========================================================
            # Check fk_network_services
            # =========================================================

            sqlquery = """SELECT fk_network_services FROM server WHERE server"""
            if id:
                sqlquery = sqlquery + ".id=" + str(id)
                fk_network_services = db_instance.query(sqlquery)
                if len(fk_network_services) > 0:
                    if list(fk_network_services[0])[0]:
                        log.info('server.update() Server fk_network_services: ' +
                                 str(list(fk_network_services[0])[0]))
                else:
                    return False

            # =========================================================
            # Check fk_server_credentials
            # =========================================================

            sqlquery = """SELECT fk_server_credentials FROM server WHERE server.id=""" + str(id)
            fk_server_credentials = db_instance.query(sqlquery)
            if len(fk_server_credentials) > 0:
                if list(fk_network_services[0])[0]:
                    log.info('server.update() Server fk_server_credentials: ' +
                             str(list(fk_server_credentials[0])[0]))
            else:
                return False

            if fk_server_credentials and fk_network_services and fk_ip_settings:
                username = configuration['host']['hypervisor']['username']
                # Add encryption
                if 'password' in configuration['host']['hypervisor']:
                    log.info('server.update() Password credentials will be updated')
                    password = common.encrypt(configuration['host']['hypervisor']['password'])
                    sqlquery = """UPDATE server_credentials SET ssh_username='""" + username + """',ssh_password='""" + \
                               password + """',https_username='""" + username + """',https_password='""" + password + "'" \
                               + """ WHERE server_credentials.id='""" + str(list(fk_server_credentials[0])[0]) \
                               + "'"
                else:
                    log.info('server.update() Password not updated')
                    sqlquery = """UPDATE server_credentials SET ssh_username='""" + username + """',https_username='""" + \
                               username + """' WHERE server_credentials.id='""" + str(
                        list(fk_server_credentials[0])[0]) + "'"
                db_instance.update(sqlquery)

                # =========================================================
                # Update network services values.
                # =========================================================
                properties = '{"ssh": { "active": 0, "port": ' + str(
                    configuration['host']['hypervisor']['ssh']) + ' }, "https": { "active": 0, "port": ' + str(
                    configuration['host']['hypervisor']['https']) + ' }, "vnc": { "active": 1, "port": ' + '"' + str(
                    configuration['host']['hypervisor']['vnc']) + '"' + ' }}'

                sqlquery = """UPDATE network_services SET properties='""" + properties + "'" + \
                           """ WHERE network_services.id='""" + \
                           str(list(fk_network_services[0])[0]) + "'"
                db_instance.update(sqlquery)

                sqlquery = """UPDATE ip_settings SET ip='""" + configuration['host']['hypervisor'][
                    'ip'] + """' WHERE ip_settings.id='""" + str(list(fk_ip_settings[0])[0]) + "'"
                db_instance.update(sqlquery)

                log.info('server.update() Server update successful')
                return True

            else:
                log.error('server.update() failed')
                return False

        except Exception, exception:
            log.exception(exception.__repr__())

    # =========================================================
    # Read Server Serial number
    # =========================================================

    def update_serialnumber(self):
        """

        :return:
        """
        # esxcli network ip route ipv4 list
        try:
            log.info('server.update_serialnumber()')

            res = general.send_command(
                commandList=[
                    'esxcfg-info | grep -A1 \'Serial Number\' | awk \'{print $2}\' |  head -n 1'],
                server=self.server,
                username=self.username,
                password=self.password,
                port=self.sshPort,
                display=True)

            if res:

                serial_number = re.search(r'(\w+)(\.+)(\w+)', res[0])
                if serial_number:
                    serial_number = serial_number.group(3)

                if serial_number:
                    log.info(
                        'server.update_serialnumber() Serial number found: {}'.format(serial_number))
                    self.serial_number = serial_number

                # Get VM list
                try:
                    log.info(
                        'server.update_serialnumber() Updating information: ' + str(self.id))
                    sqlquery = """UPDATE server SET serialnumber='""" + str(self.serial_number) \
                               + """' WHERE server.id=""" + str(self.id)
                    db_utils.update_database(sqlquery)
                    return True
                except Exception as exception:
                    log.exception(str(exception))

            else:
                log.error('server.update_serialnumber() Serial number not found')

        except Exception, exception:
            log.exception(exception.__repr__())
            self.serial_number = 'N/A'
