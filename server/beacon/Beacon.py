__author__ = 'gogasca'

import logging

from conf import logging_conf
from hypervisor.esxi import general


log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

# beacon_parameters = {}
# beacon_parameters['app_id']   = 1
# beacon_parameters['version']  = '1.0'
# beacon_parameters['md5checksum']  = '83fc0beee5769f55cbdae0259dfc003c'
# beacon_parameters['tools_location'] = '/usr/local/src/beacon/v1.0/'
# beacon_parameters['esx_location'] = '/vmfs/volumes/datastore1/'
# beacon_parameters['filename'] = 'beacon.zip'

class Beacon():
    def __init__(self):
        self.beacon_parameters = {}
        self.app_server = None
        self.remote_server = None
        self.virtual_machine = None

    """
    Beacon is stored in Tools server
    It has predefined values defined in beacon_parameters{}
    Remote script can modify .vmx file in both Tools server or ESXi
        VNC Default Port
        VNC Enable
        Name
        Network


    1. Transfer Beacon from Tools server to ESXi
        Unzip
        Edit params if necessary
        Zip
    2. Register VM
    3. Power on VM
    4. Verify VM is on
    5. Validate via VNC/Image recognition Beacon is up

    1)  Validate remote App server connectivity
            check_app_server_connectivity()
        Read Beacon file from remote App server
            check_remote_file_exists()
        Check md5checksum
        Create remote folder in Esxi
        Move Beacon to new folder
        Edit remote vmx file.
        Zip
        Transfer file to Esxi (Remote folder path)
        Unzip
        Check md5checksum

    """

    def set_server(self, server):
        self.remote_server = server

    def get_server(self):
        return self.remote_server

    def set_vm(self, vm):
        self.virtual_machine = vm

    def get_vm(self):
        return self.virtual_machine

    def upload_beacon(self, app_server):
        log.info('beacon.transfer_beacon()')
        # Move answerfile to Tools, create Floppy drive and put it into ESXi
        upload_beacon_to_app(app_server, self.beacon_parameters)

    # We verify beacon exists
    def check_beacon_file_exists(self):
        log.info('beacon.check_beacon_file_exists()')
        return check_remote_file_exists(self.app_server, self.beacon_parameters['tools_location'] +
                                        self.beacon_parameters['filename'])

    # Create new folder remotely
    def create_new_remote_folder(self, server, directory):
        """

        :param server:
        :param directory:
        :return:
        """

        try:
            from hypervisor.esxi import file_operations

            directoryExists = file_operations.check_remote_directory_exists(directory=directory,
                                                                            server=server.server,
                                                                            username=server.username,
                                                                            password=server.password,
                                                                            port=server.sshPort)

            # Validate remote directory exists. If it doesnt, create a new one, otherwise exit with error
            if not directoryExists:
                log.info(
                    'beacon.create_new_remote_folder() New directory. Trying to create remote directory remotely: '
                    + directory)
                general.send_command(commandList=['mkdir -p ' + directory],
                                     server=server.server,
                                     username=server.username,
                                     password=server.password,
                                     port=server.sshPort)
            else:
                from error import exceptions

                raise exceptions.InvalidDirectoryPath('beacon.create_new_remote_folder() Directory already exists')

            # Validate directory exists after creation
            directoryExists = file_operations.check_remote_directory_exists(directory=directory,
                                                                            server=server.server,
                                                                            username=server.username,
                                                                            password=server.password,
                                                                            port=server.sshPort)

            if directoryExists:
                log.info('beacon.create_new_remote_folder() Directory created successfully: ' + directory)
                return True
            else:
                from error import exceptions

                raise exceptions.CreateDirectoryFailure('beacon.create_new_remote_folder() Unable to create directory')
                return False

        except Exception, e:
            log.exception(str(e))
            raise


    def check_app_server_connectivity(self, app_server):
        """

        :param app_server:
        :return:
        """
        check = general.check_network_connectivity(server=app_server.server, username=app_server.username,
                                                   password=app_server.password, sshPort=app_server.sshPort, mode='ssh')

        if check:
            pass
            # "Print log connectivity was succesful"
            log.info('beacon.discover() Server discovery was succesful')
            return True
        else:
            from error import exceptions

            log.exception('beacon.discover() Unable to connect to host')
            raise exceptions.ConnectivityError('beacon.discover() Unable to connect to host')

    def check_esxi_connectivity(self, remote_server):
        """

        :param remote_server:
        :return:
        """
        check = general.check_network_connectivity(server=remote_server.server, username=remote_server.username,
                                                   password=remote_server.password, sshPort=remote_server.sshPort,
                                                   mode='ssh')

        if check:
            pass
            # "Print log connectivity was succesful"
            log.info('beacon.discover() Server discovery was succesful')
            return True
        else:
            from error import exceptions

            log.exception('beacon.discover() Unable to connect to host')
            raise exceptions.ConnectivityError('beacon.discover() Unable to connect to host')


    def generate_copy(self, server=None, **kwargs):
        """
        Copy original beacon.zip to customer directory
        app_server,beacon_parameters['tools_location']+beacon_parameters['filename'],beacon_parameters['beacon_zip_folder']
        """
        try:
            if general.send_command(commandList=['cp -v ' + self.beacon_parameters['tools_location']
                                                         + self.beacon_parameters['filename'] + '  '
                                                         + self.beacon_parameters['beacon_zip_folder']],

                                    server=self.app_server.server,
                                    username=self.app_server.username,
                                    password=self.app_server.password,
                                    port=self.app_server.sshPort):

                log.info('beacon.generate_copy() Copied beacon successfully')
                return True
            else:
                from error import exceptions

                log.exception('beacon.generate_copy() Unable to copy beacon')
                raise exceptions.BeaconCopyException('beacon.generate_copy() Unable to create floppy drive')


        except Exception, e:
            log.exception(str(e))
            raise

    def edit_vmx_file(self, property=None, value=None, **kwargs):
        """
            mv test/beacon.vmx test/beacon.vmx.bak
            cat test/beacon.vmx.bak | grep -v "answer.msg.uuid.altered" > test/beacon.vmx
            echo answer.msg.uuid.altered=\"I move it\" >> test/beacon.vmx
        """
        try:

            if general.send_command(commandList=['mv ' + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['vmx']
                                                         + '  '
                                                         + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['vmx']
                                                         + '.bak',
                                                 'cat ' + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['vmx']
                                                         + '.bak'
                                                         + ' | grep -v ' + property + ' > '
                                                         + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['vmx'],
                                                 'echo ' + property + ' = ' + '\\"' + value + '\\"' + ' >> '
                                                         + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['vmx']],
                                    server=self.app_server.server,
                                    username=self.app_server.username,
                                    password=self.app_server.password,
                                    port=self.app_server.sshPort):

                log.info('beacon.edit_vmx_file() edit vmx beacon successfully')
            else:
                log.error('beacon.edit_vmx_file() edit vmx beacon failed')
                return False

        except Exception, e:
            log.exception(str(e))
            raise


    def unzip(self):
        try:

            if general.send_command(commandList=['unzip ' + self.beacon_parameters['beacon_zip_folder']
                                                         + self.beacon_parameters['filename']
                                                         + ' -d ' + self.beacon_parameters['beacon_zip_folder']],
                                    server=self.app_server.server,
                                    username=self.app_server.username,
                                    password=self.app_server.password,
                                    port=self.app_server.sshPort):

                log.info('beacon.unzip() unzip beacon successfully')
            else:
                log.error('beacon.unzip() unzip beacon failed')
                return False

        except Exception, e:
            log.exception(str(e))
            raise

    # zip beacon.zip *
    def zip(self, delete=False, **kwargs):
        try:
            # rm -rf /usr/local/src/imbue/customers/test/beacon.zip

            # Use zip -j to not unzip the file structure
            # zip -j /usr/local/src/imbue/customers/test/beacon.zip /usr/local/src/imbue/customers/test/*

            if delete:
                if general.send_command(commandList=['rm -rf ' + self.beacon_parameters['beacon_zip_folder']
                                                             + self.beacon_parameters['filename'],
                                                     'zip -j ' + self.beacon_parameters['beacon_zip_folder']
                                                             + self.beacon_parameters['filename']
                                                             + ' '
                                                             + self.beacon_parameters['beacon_zip_folder']
                                                             + '*'],
                                        server=self.app_server.server,
                                        username=self.app_server.username,
                                        password=self.app_server.password,
                                        port=self.app_server.sshPort):

                    log.info('beacon.zip() zip beacon files successfully. Original file was deleted')
                else:
                    log.error('beacon.zip() zip beacon failed')
                    return False
            else:
                if general.send_command(commandList=['unzip ' + self.beacon_parameters['beacon_zip_folder']
                                                             + self.beacon_parameters['filename']
                                                             + ' -d ' + self.beacon_parameters['beacon_zip_folder']],
                                        server=self.app_server.server,
                                        username=self.app_server.username,
                                        password=self.app_server.password,
                                        port=self.app_server.sshPort):

                    log.info('beacon.zip() zip beacon successfully')
                else:
                    log.error('beacon.zip() zip beacon failed')
                    return False
        except Exception, e:
            log.exception(str(e))
            raise


    def get_md5(self, server, file='', **kwargs):
        # md5sum <filename> | awk '{print $1}'
        try:

            res = general.send_command(commandList=['md5sum ' + file + ' | awk \'{print $1}\''],
                                       server=server.server,
                                       username=server.username,
                                       password=server.password,
                                       port=server.sshPort,
                                       display=True)

            log.info('beacon.get_md5() Copied beacon successfully')
            if res[0].encode('utf-8').rstrip():
                log.info('beacon.get_md5() md5: ' + res[0].encode('utf-8').rstrip())
                return res[0].encode('utf-8').rstrip()
            else:
                log.error('beacon.get_md5() md5 does not exist')
                return None

        except Exception, e:
            log.exception(str(e))
            raise


    # remoteFile=".", localPath='.'

    def transfer_beacon_copy(self):
        try:
            log.info('beacon.transfer_beacon_copy()')
            # Get zip file to local App. Once in App will transfer it to ESXi
            general.get_file_scp(server=self.app_server.server,
                                 username=self.app_server.username,
                                 password=self.app_server.password,
                                 port=self.app_server.sshPort,
                                 remoteFile=self.beacon_parameters['beacon_zip_folder'] + self.beacon_parameters[
                                     'filename'],
                                 localPath=self.beacon_parameters['app_location'])

            # remoteFilePath=None, localFile=None
            general.transfer_file_scp(server=self.remote_server.server,
                                      username=self.remote_server.username,
                                      password=self.remote_server.password,
                                      port=self.remote_server.sshPort,
                                      remoteFilePath=self.beacon_parameters['esx_location'],
                                      localFile=self.beacon_parameters['app_location'] + self.beacon_parameters[
                                          'filename'])

            # Unzip file once it has been transferred
            if general.send_command(commandList=['unzip ' + self.beacon_parameters['esx_location']
                                                         + self.beacon_parameters['filename']
                                                         + ' -d '
                                                         + self.beacon_parameters['esx_location']],
                                    server=self.remote_server.server,
                                    username=self.remote_server.username,
                                    password=self.remote_server.password,
                                    port=self.remote_server.sshPort):

                log.info('beacon.zip() Unzip and transferred beacon file successfully')
            else:
                log.error('beacon.zip() Unzip and transferred beacon file failed')
                return False

            return True
        except Exception, e:
            log.exception(str(e))
            raise

    def activate(self):
        pass


# Functions to read file information

def check_remote_file_exists(app_server, remote_file):
    """

    :param app_server:
    :param remote_file:
    :return:
    """
    try:
        from hypervisor.esxi import file_operations

        if file_operations.check_remote_file_exists(server=app_server.server, username=app_server.username,
                                                    password=app_server.password, port=app_server.sshPort,
                                                    fileName=remote_file):

            log.info('beacon.check_remote_file_exists() beacon.zip exists')
            return True
        else:
            log.error('beacon.check_remote_file_exists() beacon.zip doesnt exists')
            return False
    except Exception, e:
        log.exception(str(e))
        raise


def upload_beacon_to_app(app_server, beacon_parameters):
    """

    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param remoteDirectory:
    :param answerFileName:
    :param appId:
    :return:
    """
    try:
        """
            appId
            value   app
            -----   ------
                1   cucm
                2   cuc
                3   cup
            """
        if beacon_parameters['app_id'] == 1:

            log.info('transfer_answer_file() CUCM')
            log.info(
                'transfer_answer_file() Moving Beacon' + beacon_parameters['filename'] + ' in ' + beacon_parameters[
                    'tools_location'])

            if general.transfer_file_scp(app_server.server, app_server.username, app_server.password,
                                         app_server.sshPort,
                                         remoteFilePath=beacon_parameters['tools_location'],
                                         localFile=beacon_parameters['filename']):
                import os.path

                head, tail = os.path.split(beacon_parameters['filename'])
                if beacon_parameters['tools_location'].endswith('/'):
                    beaconInServer = beacon_parameters['tools_location'] + tail
                else:
                    beacon_parameters['tools_location'] = beacon_parameters['tools_location'] + '/' + tail
                return beaconInServer

            else:
                return False

        elif beacon_parameters['app_id'] == 2:

            log.info('CUC')

        elif beacon_parameters['app_id'] == 3:
            log.info('CUP')

        else:
            log.error('transfer_answer_file() Unknown AppId')
    except Exception, e:
        log.exception(str(e))
        raise


if __name__ == '__main__':
    try:
        from tools import Tools

        beacon = Beacon()
        app_server = Tools.Tools('parzee-dev-tools-sf-1', 'Digital Ocean')
        app_server.initialize()
        beacon.app_server = app_server

        beacon_parameters = {}
        beacon_parameters['app_id'] = 1
        beacon_parameters['version'] = '1.0'
        beacon_parameters['customer'] = 'test'
        beacon_parameters['md5checksum'] = '83fc0beee5769f55cbdae0259dfc003c'
        beacon_parameters['tools_location'] = '/usr/local/src/imbue/beacon/version/10/'
        beacon_parameters['esx_location'] = '/vmfs/volumes/datastore1/test/beacon/'
        beacon_parameters[
            'app_location'] = '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/backup/beacon/'
        # beacon_parameters['app_location'] = '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/backup/beacon/'
        beacon_parameters['filename'] = 'beacon.zip'
        beacon_parameters['vmx'] = 'beacon.vmx'
        beacon_parameters['beacon_zip_folder'] = '/usr/local/src/imbue/customers/test/'

        beacon.beacon_parameters = beacon_parameters
        beacon.check_app_server_connectivity(app_server)
        beacon.create_new_remote_folder(app_server, beacon_parameters['beacon_zip_folder'])
        beacon.check_beacon_file_exists()
        beacon.generate_copy()
        beacon.beacon_parameters['md5'] = beacon.get_md5(app_server,
                                                         beacon_parameters['beacon_zip_folder'] + beacon_parameters[
                                                             'filename'])
        beacon.unzip()
        beacon.edit_vmx_file('answer.msg.uuid.altered', 'I copied it')
        beacon.zip(delete=True)

        from virtualization import VirtualMachine
        from server import Server

        # Create new instance of server
        esx1 = Server.Server('parzee-dev-1', 'Milpitas')
        esx1.initialize()
        esx1.set_hypervisor(1)

        beacon.set_server(esx1)
        beacon.transfer_beacon_copy()
        if beacon.get_md5(beacon.get_server(), beacon_parameters['esx_location'] + beacon_parameters['filename']) == \
                beacon.beacon_parameters['md5']:
            pass
        else:
            from error import exceptions

            raise exceptions.VirtualMachineDeploymentException('Invalid md5 checksum in ESXi')

        vm1 = VirtualMachine.VirtualMachine('beacon', '', 1)
        beacon.virtual_machine = vm1
        beacon.virtual_machine.set_server(beacon.get_server())
        beacon.virtual_machine.set_vmxfile(beacon_parameters['esx_location'] + beacon_parameters['vmx'])
        beacon.virtual_machine.register_vm()

        if beacon.virtual_machine.get_status():
            # Power on VM
            beacon.virtual_machine.power_on()
            beacon.activate()
        else:
            from error import exceptions

            raise exceptions.VirtualMachineDeploymentException('Failed to deploy VM')

    except Exception, e:
        log.exception(str(e))
        raise





