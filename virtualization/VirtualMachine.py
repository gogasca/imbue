__author__ = 'gogasca'

import logging
from utils import validator
from error import exceptions
from hypervisor.esxi import general, file_operations
from conf import logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class VirtualMachine(object):
    """
    Virtual machine object
    """
    INIT = 'init'
    BOOTING = 'booting'
    CONFIGURING = 'configuring'
    RUNNING = 'running'
    SHUTTING_DOWN = 'shutting_down'
    SHUTDOWN = 'shutdown'

    def __init__(self, hostname, vmId):
        """

        :rtype : object
        """
        self.state = VirtualMachine.INIT

        self.hostname = hostname
        self.vmId = vmId
        # Params
        self.id = None
        self.hostserver = None
        self.uuid = None
        self.vmxFile = None
        self.vncPort = None
        self.vncPassword = None
        self.screen = None
        log.info('VirtualMachine() Instance created')

    def set_server(self, Server):
        """
        # Define parent physical server

        :param Server:
        :return:
        """
        self.hostserver = Server

    def get_server(self):
        """
        # Get server information
        :return:
        """
        return self.hostserver

    def set_vmxfile(self, vmx):
        """

        :param vmx:
        :return:
        """
        self.vmxFile = vmx

    def register_vm(self):
        """
        # Register VM if ESXi = 1
        :return:
        """

        self.state = VirtualMachine.BOOTING
        log.info('VirtualMachine.register_vm()')
        if self.hostserver:
            log.info('VirtualMachine.register_vm() Server exists')
        else:
            log.exception('VirtualMachine.register_vm() Not ESXi server associated')
            raise exceptions.SSHConnectivityError('VirtualMachine.register_vm() Not ESXi server associated')
        try:
            if (file_operations.check_remote_file_exists(server=self.hostserver.server,
                                                         username=self.hostserver.username,
                                                         password=self.hostserver.password,
                                                         port=self.hostserver.sshPort,
                                                         fileName=self.vmxFile)):
                pass
            else:
                from error import exceptions
                log.exception('register_vm() VMX file doesnt exist')
                raise exceptions.SSHConnectivityError('register_vm() VMX file doesnt exist')

        except Exception, e:
            log.exception(str(e))

        try:
            log.info('VirtualMachine.register_vm()')
            res = general.send_command(
                commandList=['vim-cmd solo/registervm ' + self.vmxFile],
                server=self.hostserver.server,
                username=self.hostserver.username,
                password=self.hostserver.password,
                port=self.hostserver.sshPort,
                display=True)

            vmId = res[0].encode('utf-8')
            vmId = vmId.rstrip()
            # res = True if directory exists. 1 directory doesnt exists
            if vmId:
                log.info('VirtualMachine.register_vm() New vmId created ' + vmId)
                self.vmId = vmId
                return vmId
            else:
                from error import exceptions
                log.exception('register_vm() Unable to create vmId')
                raise exceptions.VirtualMachineStatus('register_vm() Unable to create vmId')
                return None

            log.info('VirtualMachine.register_vm() vmk disk created successfully')

        except Exception, e:
            log.exception(str(e))
            raise

    def power_on(self):
        """
        # Power on VM via cli
        :return:
        """
        if self.hostserver and validator.check_authentication_parameters(server=self.hostserver.server,
                                                                         username=self.hostserver.username,
                                                                         password=self.hostserver.password,
                                                                         sshPort=self.hostserver.sshPort):
            pass
        else:
            log.exception('VirtualMachine.power_on() Check login options')
            raise exceptions.SSHConnectivityError('VirtualMachine.power_on() Check login options')

        try:
            log.info('power_on()')
            self.state = VirtualMachine.BOOTING
            res = general.send_command(commandList=['vim-cmd vmsvc/power.on ' + self.vmId],
                                       server=self.hostserver.server,
                                       username=self.hostserver.username,
                                       password=self.hostserver.password,
                                       port=self.hostserver.sshPort,
                                       display=True)

            if res:
                log.info('VirtualMachine.power_on() VM power on successfully')
                return True
            else:
                log.error('VirtualMachine.power_on() VM power on failed')
                return False

        except Exception, e:
            log.exception(str(e))

    # Power off VM via cli
    def power_off(self):
        """

         :return:
        """
        if self.hostserver and validator.check_authentication_parameters(server=self.hostserver.server,
                                                                         username=self.hostserver.username,
                                                                         password=self.hostserver.password,
                                                                         sshPort=self.hostserver.sshPort):
            pass
        else:
            log.exception('VirtualMachine.power_off() Check login options')
            raise exceptions.SSHConnectivityError('VirtualMachine.power_off() Check login options')

        try:
            log.info('VirtualMachine.power_off()')
            self.state = VirtualMachine.SHUTTING_DOWN
            res = general.send_command(commandList=['vim-cmd vmsvc/power.off ' + self.vmId],
                                       server=self.hostserver.server,
                                       username=self.hostserver.username,
                                       password=self.hostserver.password,
                                       port=self.hostserver.sshPort,
                                       display=True)

            if res:
                log.info('VirtualMachine.power_off() VM power off successfully')
                self.state = VirtualMachine.SHUTDOWN
            else:
                log.error('VirtualMachine.power_off() VM power on failed')

        except Exception, e:
            log.exception(str(e))

    def get_status(self):
        """
        # Get VM status
        :return:
        """
        log.info('VirtualMachine.get_status() Check server information')
        if self.hostserver and validator.check_authentication_parameters(server=self.hostserver.server,
                                                                         username=self.hostserver.username,
                                                                         password=self.hostserver.password,
                                                                         sshPort=self.hostserver.sshPort):
            log.info('VirtualMachine.get_status() Server exists')
        else:
            log.exception('VirtualMachine.get_status() Check login options')
            raise exceptions.SSHConnectivityError('VirtualMachine.get_status() Check login options')

        try:
            log.info('VirtualMachine.get_status()')
            res = general.send_command(commandList=['vim-cmd vmsvc/get.summary ' + self.vmId + ' | grep overallStatus'],
                                       server=self.hostserver.server,
                                       username=self.hostserver.username,
                                       password=self.hostserver.password,
                                       port=self.hostserver.sshPort,
                                       display=True)

            if 'green' in res[0].encode('utf-8'):
                log.info('VirtualMachine.get_status() Status Ok')
                self.state = VirtualMachine.RUNNING
                return True
            else:
                from error import exceptions
                log.exception('VirtualMachine.get_status() Unknown status')
                raise exceptions.VirtualMachineStatus('VirtualMachine.get_status() Unknown status')

        except Exception, e:
            log.exception(str(e))

    def get_param(self, param):
        """
        Get specific parameter
        :param param:
        :return:
        """
        if self.hostserver and validator.check_authentication_parameters(server=self.hostserver.server,
                                                                         username=self.hostserver.username,
                                                                         password=self.hostserver.password,
                                                                         sshPort=self.hostserver.sshPort):
            pass
        else:
            from error import exceptions

            log.exception('VirtualMachine.get_param() Check login options')
            raise exceptions.SSHConnectivityError('VirtualMachine.get_param() Check login options')

        try:
            log.info('VirtualMachine.get_param()')
            res = general.send_command(commandList=['vim-cmd vmsvc/get.summary ' + self.vmId + ' | grep ' + param],
                                       server=self.hostserver.server,
                                       username=self.hostserver.username,
                                       password=self.hostserver.password,
                                       port=self.hostserver.sshPort,
                                       display=True)

            if res:
                return res
            else:
                from error import exceptions

                log.exception('VirtualMachine.get_param() Error processing param.')
                raise exceptions.VirtualMachineStatus('VirtualMachine.get_param() Error processing param.')
                return False

        except Exception, e:
            log.exception(str(e))

    def load_vmx_file(self, localVmxFileName, remoteVmxFileName):
        """

        :param localVmxFileName:
        :param remoteVmxFileName:
        :return:
        """
        try:
            if general.transfer_file_scp(self.hostserver.server,
                                         self.hostserver.username,
                                         self.hostserver.password,
                                         self.hostserver.sshPort,
                                         localFile=localVmxFileName,
                                         remoteFilePath=remoteVmxFileName):
                log.info('VirtualMachine.load_vmx_file Loaded succesfully')
                self.vmxFile = remoteVmxFileName
            else:
                from error import exceptions

                log.exception('VirtualMachine.load_vmx_file Failed')
                raise exceptions.LoadVMXFileException('VirtualMachine.load_vmx_file Failed')

        except Exception, e:
            log.exception(str(e))

    def check_vnc_connectivity(self):
        """

        :return:
        """
        log.info('VirtualMachine.check_vnc_connectivity()')
        if self.hostserver:
            if self.get_server().hypervisor == 1:
                if general.check_network_connectivity(server=self.hostserver.server, vncPort=self.vncPort, mode='vnc'):
                    return True
                else:
                    log.error('VirtualMachine.check_vnc_connectivity() No VNC connectivity will try to update Firewall')
                    self.get_server().update_firewall(operation='false')
                    # Check VNC again
                    if general.check_network_connectivity(server=self.get_server().server, vncPort=self.vncPort,
                                                          mode='vnc'):
                        return True
                    else:
                        # Didnt work after updating firewall
                        log.exception('VirtualMachine.check_vnc_connectivity() No VNC connectivity')
                        return False
            else:
                from error import exceptions
                log.exception('VirtualMachine.check_vnc_connectivity() Unknown Hypervisor')
                raise exceptions.UnknownHypervisor('VirtualMachine.check_vnc_connectivity() Unknown Hypervisor')
        else:
            log.error('VirtualMachine.check_vnc_connectivity() Invalid state, no Server associated')

    def create_vmk_disk(self, remoteDiskName, diskSize, appId):
        """

        :param remoteDiskName:
        :param diskSize:
        :param appId:
        :param params:
        :return:
        """
        try:
            log.info('VirtualMachine.create_vmk_file()')

            if self.hostserver.hypervisor == 1:
                """
                appId
                value   app
                -----   ------
                    1   cucm
                    2   cuc
                    3   cup
                """
                if appId == 1:
                    log.info('VirtualMachine.create_vmk_disk() UC application')
                    log.info('VirtualMachine.create_vmk_disk() Creating disk remotely ' + remoteDiskName)
                    # Create Remote Hard Disk in ESXi host. If its the first time create it, otherwise overwrite it!
                    log.info(remoteDiskName)
                    # log.info(diskSize)
                    # log.info(params)

                    res = file_operations.create_harddisk(server=self.hostserver.server,
                                                          username=self.hostserver.username,
                                                          password=self.hostserver.password,
                                                          port=self.hostserver.sshPort,
                                                          remoteName=remoteDiskName,
                                                          size=diskSize)

                    log.info('VirtualMachine.create_vmk_disk() Remote command response: ' + str(res))
                    if res:
                        from time import sleep
                        sleep(2)
                        return True
                    else:
                        log.exception('VirtualMachine.create_vmk_disk() Unable to create disk file remotely')
                        from error.exceptions import CreateVmkException
                        raise CreateVmkException(
                            'VirtualMachine.create_vmk_disk() Unable to create disk file remotely')

                elif appId == 2:
                    log.info('VirtualMachine.create_vmk_file() CUC')
                elif appId == 3:
                    log.info('CUP')
                else:
                    from error import app_exceptions
                    log.exception('VirtualMachine.create_vmk_file Unknown AppId')
                    raise app_exceptions.AppNotFound('VirtualMachine.create_vmk_file Unknown AppId')
            else:
                from error import exceptions
                log.exception('VirtualMachine.create_vmk_file() Hypervisor model not available yet')
                raise exceptions.UnknownHypervisor(
                    'VirtualMachine.create_vmk_file() Hypervisor model not available yet')

        except Exception, e:
            log.exception(str(e))
