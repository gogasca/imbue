-- ----------------------------
--  Table structure for hypervisor
-- ----------------------------
DROP TABLE IF EXISTS "public"."hypervisor";
CREATE TABLE "public"."hypervisor" (
	"id" int4 NOT NULL DEFAULT nextval('hypervisor_id_seq'::regclass),
	"name" varchar(250) NOT NULL COLLATE "default",
	"version" varchar(250) COLLATE "default",
	"vendor" varchar(100) NOT NULL COLLATE "default",
	"status" bool NOT NULL,
	"api_version" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."hypervisor" OWNER TO "imbue";

-- ----------------------------
--  Records of hypervisor
-- ----------------------------
BEGIN;
INSERT INTO "public"."hypervisor" VALUES ('1', 'ESXi', '5.5', 'VMWare', 't', '5.5');
INSERT INTO "public"."hypervisor" VALUES ('2', 'Ubuntu', '14.04', 'Canonical', 't', '');
COMMIT;