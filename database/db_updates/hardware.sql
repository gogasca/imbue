/*
 Navicat PostgreSQL Data Transfer

 Source Server         : parzee-db-tools-sfo1
 Source Server Version : 90309
 Source Host           : 198.199.110.24
 Source Database       : imbuedb
 Source Schema         : public

 Target Server Version : 90309
 File Encoding         : utf-8

 Date: 01/05/2016 19:32:21 PM
*/

-- ----------------------------
--  Table structure for hardware
-- ----------------------------
DROP TABLE IF EXISTS "public"."hardware";
CREATE TABLE "public"."hardware" (
	"id" int4 NOT NULL,
	"cpu_speed" float4,
	"cpu_number" int4,
	"memory" float4,
	"disk_size" float4,
	"cpu_model" varchar(200) COLLATE "default",
	"disk_number" int4,
	"raid" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."hardware" OWNER TO "imbue";

-- ----------------------------
--  Records of hardware
-- ----------------------------
BEGIN;
INSERT INTO "public"."hardware" VALUES ('1', '2.4', '8', '32', '2000', 'E5-2609/80W 4C/10 MB Cache/DDR3 1066 MHz', '1', null);
COMMIT;


-- ----------------------------
--  Primary key structure for table hardware
-- ----------------------------
ALTER TABLE "public"."hardware" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table hardware
-- ----------------------------
CREATE UNIQUE INDEX  "hardware_id_key" ON "public"."hardware" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

