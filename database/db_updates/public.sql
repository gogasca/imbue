/*
 Navicat PostgreSQL Data Transfer

 Source Server         : parzee-dev-app-sfo1
 Source Server Version : 90501
 Source Host           : 198.199.110.24
 Source Database       : imbuedb
 Source Schema         : public

 Target Server Version : 90501
 File Encoding         : utf-8

 Date: 04/16/2016 16:27:24 PM
*/

-- ----------------------------
--  Sequence structure for activity_event_list_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."activity_event_list_id_seq";
CREATE SEQUENCE "public"."activity_event_list_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."activity_event_list_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for activity_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."activity_id_seq";
CREATE SEQUENCE "public"."activity_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."activity_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for announce_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."announce_id_seq";
CREATE SEQUENCE "public"."announce_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."announce_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for api_users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."api_users_id_seq";
CREATE SEQUENCE "public"."api_users_id_seq" INCREMENT 1 START 4 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."api_users_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for application_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."application_id_seq";
CREATE SEQUENCE "public"."application_id_seq" INCREMENT 1 START 93 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."application_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for application_id_seq1
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."application_id_seq1";
CREATE SEQUENCE "public"."application_id_seq1" INCREMENT 1 START 66 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."application_id_seq1" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for auth_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_id_seq";
CREATE SEQUENCE "public"."auth_user_id_seq" INCREMENT 1 START 41 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."auth_user_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for beacon_identifier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."beacon_identifier_seq";
CREATE SEQUENCE "public"."beacon_identifier_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."beacon_identifier_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for container_identifier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."container_identifier_seq";
CREATE SEQUENCE "public"."container_identifier_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."container_identifier_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."group_id_seq";
CREATE SEQUENCE "public"."group_id_seq" INCREMENT 1 START 7 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."group_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for hypervisor_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."hypervisor_id_seq";
CREATE SEQUENCE "public"."hypervisor_id_seq" INCREMENT 1 START 4 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."hypervisor_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for ip_settings_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ip_settings_id_seq";
CREATE SEQUENCE "public"."ip_settings_id_seq" INCREMENT 1 START 1454 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."ip_settings_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for job_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."job_id_seq";
CREATE SEQUENCE "public"."job_id_seq" INCREMENT 1 START 3454 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."job_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for licensing_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."licensing_id_seq";
CREATE SEQUENCE "public"."licensing_id_seq" INCREMENT 1 START 568 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."licensing_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for network_interfaces_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."network_interfaces_id_seq";
CREATE SEQUENCE "public"."network_interfaces_id_seq" INCREMENT 1 START 566 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."network_interfaces_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for network_services_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."network_services_id_seq";
CREATE SEQUENCE "public"."network_services_id_seq" INCREMENT 1 START 578 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."network_services_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for payment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."payment_id_seq";
CREATE SEQUENCE "public"."payment_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."payment_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for server_credentials_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."server_credentials_id_seq";
CREATE SEQUENCE "public"."server_credentials_id_seq" INCREMENT 1 START 578 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."server_credentials_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for server_identifier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."server_identifier_seq";
CREATE SEQUENCE "public"."server_identifier_seq" INCREMENT 1 START 579 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."server_identifier_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for server_model_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."server_model_id_seq";
CREATE SEQUENCE "public"."server_model_id_seq" INCREMENT 1 START 14 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."server_model_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for tools_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tools_id_seq";
CREATE SEQUENCE "public"."tools_id_seq" INCREMENT 1 START 11 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."tools_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for virtualmachine_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."virtualmachine_id_seq";
CREATE SEQUENCE "public"."virtualmachine_id_seq" INCREMENT 1 START 5404 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."virtualmachine_id_seq" OWNER TO "imbue";

-- ----------------------------
--  Sequence structure for virtualmachine_identifier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."virtualmachine_identifier_seq";
CREATE SEQUENCE "public"."virtualmachine_identifier_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."virtualmachine_identifier_seq" OWNER TO "imbue";

-- ----------------------------
--  Function structure for public.pg_stat_statements(bool)
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pg_stat_statements"(bool);
CREATE FUNCTION "public"."pg_stat_statements"(IN showtext bool, OUT userid oid, OUT dbid oid, OUT queryid int8, OUT query text, OUT calls int8, OUT total_time float8, OUT min_time float8, OUT max_time float8, OUT mean_time float8, OUT stddev_time float8, OUT "rows" int8, OUT shared_blks_hit int8, OUT shared_blks_read int8, OUT shared_blks_dirtied int8, OUT shared_blks_written int8, OUT local_blks_hit int8, OUT local_blks_read int8, OUT local_blks_dirtied int8, OUT local_blks_written int8, OUT temp_blks_read int8, OUT temp_blks_written int8, OUT blk_read_time float8, OUT blk_write_time float8) RETURNS SETOF "record" 
	AS '$libdir/pg_stat_statements','pg_stat_statements_1_3'
	LANGUAGE c
	COST 1
	ROWS 1000
	STRICT
	SECURITY INVOKER
	VOLATILE;
ALTER FUNCTION "public"."pg_stat_statements"(IN showtext bool, OUT userid oid, OUT dbid oid, OUT queryid int8, OUT query text, OUT calls int8, OUT total_time float8, OUT min_time float8, OUT max_time float8, OUT mean_time float8, OUT stddev_time float8, OUT "rows" int8, OUT shared_blks_hit int8, OUT shared_blks_read int8, OUT shared_blks_dirtied int8, OUT shared_blks_written int8, OUT local_blks_hit int8, OUT local_blks_read int8, OUT local_blks_dirtied int8, OUT local_blks_written int8, OUT temp_blks_read int8, OUT temp_blks_written int8, OUT blk_read_time float8, OUT blk_write_time float8) OWNER TO "imbue";

-- ----------------------------
--  Function structure for public.pg_stat_statements_reset()
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pg_stat_statements_reset"();
CREATE FUNCTION "public"."pg_stat_statements_reset"() RETURNS "void" 
	AS '$libdir/pg_stat_statements','pg_stat_statements_reset'
	LANGUAGE c
	COST 1
	CALLED ON NULL INPUT
	SECURITY INVOKER
	VOLATILE;
ALTER FUNCTION "public"."pg_stat_statements_reset"() OWNER TO "imbue";

-- ----------------------------
--  Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS "public"."activity";
CREATE TABLE "public"."activity" (
	"id" int4 NOT NULL DEFAULT nextval('activity_id_seq'::regclass),
	"user_id" int4 NOT NULL,
	"activity_date" date,
	"activity_event" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."activity" OWNER TO "imbue";

-- ----------------------------
--  Table structure for activity_event_list
-- ----------------------------
DROP TABLE IF EXISTS "public"."activity_event_list";
CREATE TABLE "public"."activity_event_list" (
	"id" int4 NOT NULL DEFAULT nextval('activity_event_list_id_seq'::regclass),
	"Activity_event_list_description" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."activity_event_list" OWNER TO "imbue";

-- ----------------------------
--  Table structure for announce
-- ----------------------------
DROP TABLE IF EXISTS "public"."announce";
CREATE TABLE "public"."announce" (
	"id" int4 NOT NULL DEFAULT nextval('announce_id_seq'::regclass),
	"users_type" int4 DEFAULT 0,
	"announce_date" date,
	"flash" bool,
	"announce_news" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."announce" OWNER TO "imbue";

-- ----------------------------
--  Table structure for hypervisor
-- ----------------------------
DROP TABLE IF EXISTS "public"."hypervisor";
CREATE TABLE "public"."hypervisor" (
	"id" int4 NOT NULL DEFAULT nextval('hypervisor_id_seq'::regclass),
	"name" varchar(250) NOT NULL COLLATE "default",
	"version" varchar(250) COLLATE "default",
	"vendor" varchar(100) NOT NULL COLLATE "default",
	"status" bool NOT NULL,
	"api_version" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."hypervisor" OWNER TO "imbue";

-- ----------------------------
--  Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS "public"."team";
CREATE TABLE "public"."team" (
	"id" int4 NOT NULL DEFAULT nextval('group_id_seq'::regclass),
	"email" varchar(50) COLLATE "default",
	"fk_auth_user" int4,
	"fk_payment" int4
)
WITH (OIDS=TRUE);
ALTER TABLE "public"."team" OWNER TO "imbue";

-- ----------------------------
--  Table structure for ip_settings
-- ----------------------------
DROP TABLE IF EXISTS "public"."ip_settings";
CREATE TABLE "public"."ip_settings" (
	"id" int4 NOT NULL DEFAULT nextval('ip_settings_id_seq'::regclass),
	"ip" varchar(100) NOT NULL COLLATE "default",
	"mask" varchar(100) COLLATE "default",
	"gateway" varchar(100) COLLATE "default",
	"dns1" varchar(100) COLLATE "default",
	"dns2" varchar(100) COLLATE "default",
	"domain" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."ip_settings" OWNER TO "imbue";

-- ----------------------------
--  Table structure for hardware
-- ----------------------------
DROP TABLE IF EXISTS "public"."hardware";
CREATE TABLE "public"."hardware" (
	"id" int4 NOT NULL,
	"cpu_speed" float4,
	"cpu_number" int4,
	"memory" float4,
	"disk_size" float4,
	"cpu_model" varchar(200) COLLATE "default",
	"disk_number" int4,
	"raid" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."hardware" OWNER TO "imbue";

-- ----------------------------
--  Table structure for marketplace
-- ----------------------------
DROP TABLE IF EXISTS "public"."marketplace";
CREATE TABLE "public"."marketplace" (
	"id" int4 NOT NULL,
	"name" varchar(200) NOT NULL COLLATE "default",
	"version" varchar(100) COLLATE "default",
	"vendor" varchar(100) COLLATE "default",
	"is_clustered" bool,
	"application_params" json
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."marketplace" OWNER TO "imbue";

-- ----------------------------
--  Table structure for server_model
-- ----------------------------
DROP TABLE IF EXISTS "public"."server_model";
CREATE TABLE "public"."server_model" (
	"id" int4 NOT NULL DEFAULT nextval('server_model_id_seq'::regclass),
	"model" varchar(250) NOT NULL COLLATE "default",
	"vendor" varchar(250) NOT NULL COLLATE "default",
	"fk_hypervisor" int4,
	"status" bool NOT NULL,
	"fk_hardware" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."server_model" OWNER TO "imbue";

-- ----------------------------
--  Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS "public"."history";
CREATE TABLE "public"."history" (
	"id" int4,
	"timestamp" timestamp(6) NULL,
	"application_params" json,
	"owner_id" int4,
	"fk_server" int4,
	"job" varchar(32) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."history" OWNER TO "imbue";

-- ----------------------------
--  Table structure for server_farm
-- ----------------------------
DROP TABLE IF EXISTS "public"."server_farm";
CREATE TABLE "public"."server_farm" (
	"id" int8 NOT NULL,
	"max_servers" int8 DEFAULT 64
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."server_farm" OWNER TO "imbue";

-- ----------------------------
--  Table structure for virtualmachine
-- ----------------------------
DROP TABLE IF EXISTS "public"."virtualmachine";
CREATE TABLE "public"."virtualmachine" (
	"id" int4 NOT NULL DEFAULT nextval('virtualmachine_id_seq'::regclass),
	"vmid" int4,
	"hostname" varchar(64) NOT NULL COLLATE "default",
	"status" int4,
	"updated" timestamp(6) NOT NULL,
	"fk_server" int4,
	"fk_ip_settings" int4,
	"description" varchar(2048) COLLATE "default",
	"uuid" varchar(128) COLLATE "default",
	"app_id" int4,
	"power_state" bool,
	"directory" varchar(256) COLLATE "default",
	"screen" varchar(128) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."virtualmachine" OWNER TO "imbue";

-- ----------------------------
--  Table structure for network_services
-- ----------------------------
DROP TABLE IF EXISTS "public"."network_services";
CREATE TABLE "public"."network_services" (
	"id" int4 NOT NULL DEFAULT nextval('network_services_id_seq'::regclass),
	"properties" json
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."network_services" OWNER TO "imbue";

-- ----------------------------
--  Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user";
CREATE TABLE "public"."auth_user" (
	"id" int4 NOT NULL DEFAULT nextval('auth_user_id_seq'::regclass),
	"password" varchar(128) NOT NULL COLLATE "default",
	"last_login" timestamp(6) WITH TIME ZONE,
	"is_superuser" bool NOT NULL DEFAULT false,
	"username" varchar(30) COLLATE "default",
	"first_name" varchar(30) NOT NULL COLLATE "default",
	"last_name" varchar(30) NOT NULL COLLATE "default",
	"email" varchar(50) NOT NULL COLLATE "default",
	"is_staff" bool NOT NULL DEFAULT true,
	"is_active" bool NOT NULL,
	"date_joined" timestamp(6) WITH TIME ZONE NOT NULL,
	"is_admin" bool NOT NULL DEFAULT true,
	"is_payment" bool NOT NULL DEFAULT false,
	"company" varchar(40) COLLATE "default",
	"address" varchar(50) COLLATE "default",
	"city" varchar(20) COLLATE "default",
	"state" varchar(20) COLLATE "default",
	"zip_code" varchar(10) COLLATE "default",
	"telephone" varchar(25) COLLATE "default",
	"cellphone" varchar(25) COLLATE "default",
	"is_team" bool DEFAULT false,
	"enable_alerts" bool DEFAULT false
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."auth_user" OWNER TO "imbue";

-- ----------------------------
--  Table structure for network_interfaces
-- ----------------------------
DROP TABLE IF EXISTS "public"."network_interfaces";
CREATE TABLE "public"."network_interfaces" (
	"id" int4 NOT NULL DEFAULT nextval('network_interfaces_id_seq'::regclass),
	"name" varchar(100) COLLATE "default",
	"status" bool,
	"fk_ip_settings" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."network_interfaces" OWNER TO "imbue";

-- ----------------------------
--  Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."payment";
CREATE TABLE "public"."payment" (
	"id" int4 NOT NULL DEFAULT nextval('payment_id_seq'::regclass),
	"is_active" bool NOT NULL,
	"fk_auth_user" int4,
	"fk_group" int4
)
WITH (OIDS=TRUE);
ALTER TABLE "public"."payment" OWNER TO "imbue";

-- ----------------------------
--  Table structure for application_cluster
-- ----------------------------
DROP TABLE IF EXISTS "public"."application_cluster";
CREATE TABLE "public"."application_cluster" (
	"id" int4 NOT NULL,
	"cluster_name" varchar(200) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."application_cluster" OWNER TO "imbue";

-- ----------------------------
--  Table structure for server_credentials
-- ----------------------------
DROP TABLE IF EXISTS "public"."server_credentials";
CREATE TABLE "public"."server_credentials" (
	"id" int4 NOT NULL DEFAULT nextval('server_credentials_id_seq'::regclass),
	"ssh_username" varchar(250) NOT NULL COLLATE "default",
	"ssh_password" varchar(2048) NOT NULL COLLATE "default",
	"https_username" varchar(250) COLLATE "default",
	"https_password" varchar(2048) COLLATE "default",
	"vnc_password" varchar(250) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."server_credentials" OWNER TO "imbue";

-- ----------------------------
--  Table structure for tools
-- ----------------------------
DROP TABLE IF EXISTS "public"."tools";
CREATE TABLE "public"."tools" (
	"id" int4 NOT NULL DEFAULT nextval('tools_id_seq'::regclass),
	"hostname" varchar(250) COLLATE "default",
	"description" varchar(500) COLLATE "default",
	"status" int4 NOT NULL,
	"fk_ip_settings" int4,
	"fk_server_credentials" int4,
	"fk_network_services" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."tools" OWNER TO "imbue";

-- ----------------------------
--  Table structure for api_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."api_user";
CREATE TABLE "public"."api_user" (
	"id" int4 NOT NULL DEFAULT nextval('api_users_id_seq'::regclass),
	"username" varchar(50) COLLATE "default",
	"password_hash" varchar(250) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."api_user" OWNER TO "imbue";

-- ----------------------------
--  Table structure for beacon
-- ----------------------------
DROP TABLE IF EXISTS "public"."beacon";
CREATE TABLE "public"."beacon" (
	"id" int4 NOT NULL DEFAULT nextval('beacon_identifier_seq'::regclass),
	"hostname" varchar(200) NOT NULL COLLATE "default",
	"version" varchar(100) COLLATE "default",
	"remote_path" varchar(200) COLLATE "default",
	"filename" varchar(250) COLLATE "default",
	"description" varchar(250) COLLATE "default",
	"owner_id" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."beacon" OWNER TO "imbue";

-- ----------------------------
--  Table structure for container
-- ----------------------------
DROP TABLE IF EXISTS "public"."container";
CREATE TABLE "public"."container" (
	"id" int4 NOT NULL DEFAULT nextval('container_identifier_seq'::regclass),
	"hostname" varchar(250) COLLATE "default",
	"description" varchar(250) COLLATE "default",
	"owner_id" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."container" OWNER TO "imbue";

-- ----------------------------
--  Table structure for server
-- ----------------------------
DROP TABLE IF EXISTS "public"."server";
CREATE TABLE "public"."server" (
	"id" int4 NOT NULL DEFAULT nextval('server_identifier_seq'::regclass),
	"discovered" bool NOT NULL DEFAULT false,
	"status" int4 NOT NULL,
	"fk_server_model" int4,
	"serialnumber" varchar(128) NOT NULL COLLATE "default",
	"name" varchar(256) NOT NULL COLLATE "default",
	"description" varchar(2048) COLLATE "default",
	"created" timestamp(6) NULL,
	"fk_licensing" int4,
	"fk_network_services" int4,
	"fk_network_interfaces" int4,
	"owner_id" int4,
	"fk_server_credentials" int4,
	"iso_files" varchar(2048) COLLATE "default",
	"datastores" varchar(16384) COLLATE "default",
	"networks" varchar(2048) COLLATE "default",
	"image_folder" varchar(256) COLLATE "default",
	"datastores_path" varchar(16384) COLLATE "default",
	"install_folder" varchar(256) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."server" OWNER TO "imbue";

-- ----------------------------
--  Table structure for licensing
-- ----------------------------
DROP TABLE IF EXISTS "public"."licensing";
CREATE TABLE "public"."licensing" (
	"id" int4 NOT NULL DEFAULT nextval('licensing_id_seq'::regclass),
	"key" varchar(250) COLLATE "default",
	"type" varchar(100) COLLATE "default",
	"active" bool
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."licensing" OWNER TO "imbue";

-- ----------------------------
--  Table structure for job
-- ----------------------------
DROP TABLE IF EXISTS "public"."job";
CREATE TABLE "public"."job" (
	"id" int4 NOT NULL DEFAULT nextval('job_id_seq'::regclass),
	"description" varchar(256) COLLATE "default",
	"reference" varchar(32) NOT NULL COLLATE "default",
	"job_start" timestamp(6) NULL,
	"job_end" timestamp(6) NULL,
	"owner_id" int4,
	"host_information" varchar(1024) COLLATE "default",
	"is_cancelled" bool DEFAULT false,
	"is_cluster" bool DEFAULT false,
	"status" int4,
	"request_data" varchar(4096) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."job" OWNER TO "imbue";

-- ----------------------------
--  Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS "public"."application";
CREATE TABLE "public"."application" (
	"id" int4 NOT NULL DEFAULT nextval('application_id_seq1'::regclass),
	"description" varchar(256) COLLATE "default",
	"application_params" json,
	"is_installed" bool,
	"owner_id" int4,
	"job" varchar(32) COLLATE "default",
	"fk_server" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."application" OWNER TO "imbue";

-- ----------------------------
--  View structure for pg_stat_statements
-- ----------------------------
DROP VIEW IF EXISTS "public"."pg_stat_statements";
CREATE VIEW "public"."pg_stat_statements" AS  SELECT pg_stat_statements.userid,
    pg_stat_statements.dbid,
    pg_stat_statements.queryid,
    pg_stat_statements.query,
    pg_stat_statements.calls,
    pg_stat_statements.total_time,
    pg_stat_statements.min_time,
    pg_stat_statements.max_time,
    pg_stat_statements.mean_time,
    pg_stat_statements.stddev_time,
    pg_stat_statements.rows,
    pg_stat_statements.shared_blks_hit,
    pg_stat_statements.shared_blks_read,
    pg_stat_statements.shared_blks_dirtied,
    pg_stat_statements.shared_blks_written,
    pg_stat_statements.local_blks_hit,
    pg_stat_statements.local_blks_read,
    pg_stat_statements.local_blks_dirtied,
    pg_stat_statements.local_blks_written,
    pg_stat_statements.temp_blks_read,
    pg_stat_statements.temp_blks_written,
    pg_stat_statements.blk_read_time,
    pg_stat_statements.blk_write_time
   FROM pg_stat_statements(true) pg_stat_statements(userid, dbid, queryid, query, calls, total_time, min_time, max_time, mean_time, stddev_time, rows, shared_blks_hit, shared_blks_read, shared_blks_dirtied, shared_blks_written, local_blks_hit, local_blks_read, local_blks_dirtied, local_blks_written, temp_blks_read, temp_blks_written, blk_read_time, blk_write_time);


-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."activity_event_list_id_seq" RESTART 2;
ALTER SEQUENCE "public"."activity_id_seq" RESTART 2;
ALTER SEQUENCE "public"."announce_id_seq" RESTART 2;
ALTER SEQUENCE "public"."api_users_id_seq" RESTART 5;
ALTER SEQUENCE "public"."application_id_seq" RESTART 94;
ALTER SEQUENCE "public"."application_id_seq1" RESTART 67 OWNED BY "application"."id";
ALTER SEQUENCE "public"."auth_user_id_seq" RESTART 42;
ALTER SEQUENCE "public"."beacon_identifier_seq" RESTART 2;
ALTER SEQUENCE "public"."container_identifier_seq" RESTART 3;
ALTER SEQUENCE "public"."group_id_seq" RESTART 8;
ALTER SEQUENCE "public"."hypervisor_id_seq" RESTART 5;
ALTER SEQUENCE "public"."ip_settings_id_seq" RESTART 1455;
ALTER SEQUENCE "public"."job_id_seq" RESTART 3455;
ALTER SEQUENCE "public"."licensing_id_seq" RESTART 569;
ALTER SEQUENCE "public"."network_interfaces_id_seq" RESTART 567;
ALTER SEQUENCE "public"."network_services_id_seq" RESTART 579;
ALTER SEQUENCE "public"."payment_id_seq" RESTART 2;
ALTER SEQUENCE "public"."server_credentials_id_seq" RESTART 579;
ALTER SEQUENCE "public"."server_identifier_seq" RESTART 580;
ALTER SEQUENCE "public"."server_model_id_seq" RESTART 15;
ALTER SEQUENCE "public"."tools_id_seq" RESTART 12;
ALTER SEQUENCE "public"."virtualmachine_id_seq" RESTART 5405;
ALTER SEQUENCE "public"."virtualmachine_identifier_seq" RESTART 2;
-- ----------------------------
--  Primary key structure for table activity
-- ----------------------------
ALTER TABLE "public"."activity" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table activity_event_list
-- ----------------------------
ALTER TABLE "public"."activity_event_list" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table announce
-- ----------------------------
ALTER TABLE "public"."announce" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table hypervisor
-- ----------------------------
ALTER TABLE "public"."hypervisor" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table hypervisor
-- ----------------------------
CREATE UNIQUE INDEX  "hypervisor_id_key" ON "public"."hypervisor" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table team
-- ----------------------------
ALTER TABLE "public"."team" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table team
-- ----------------------------
CREATE INDEX  "fki_auth_user_userid" ON "public"."team" USING btree(fk_auth_user "pg_catalog"."int4_ops" ASC NULLS LAST);
CREATE INDEX  "fki_payment" ON "public"."team" USING btree(fk_payment "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table ip_settings
-- ----------------------------
ALTER TABLE "public"."ip_settings" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table ip_settings
-- ----------------------------
CREATE UNIQUE INDEX  "ip_settings_id_key" ON "public"."ip_settings" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table hardware
-- ----------------------------
ALTER TABLE "public"."hardware" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table hardware
-- ----------------------------
CREATE UNIQUE INDEX  "hardware_id_key" ON "public"."hardware" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table marketplace
-- ----------------------------
ALTER TABLE "public"."marketplace" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table server_model
-- ----------------------------
ALTER TABLE "public"."server_model" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table server_model
-- ----------------------------
CREATE UNIQUE INDEX  "server_model_id_key" ON "public"."server_model" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Uniques structure for table history
-- ----------------------------
ALTER TABLE "public"."history" ADD CONSTRAINT "history_id_key" UNIQUE ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table server_farm
-- ----------------------------
ALTER TABLE "public"."server_farm" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table virtualmachine
-- ----------------------------
ALTER TABLE "public"."virtualmachine" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table network_services
-- ----------------------------
ALTER TABLE "public"."network_services" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table network_services
-- ----------------------------
CREATE UNIQUE INDEX  "network_services_id_key" ON "public"."network_services" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD CONSTRAINT "auth_user_username_key" UNIQUE ("username") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table auth_user
-- ----------------------------
CREATE UNIQUE INDEX  "auth_user_id_key" ON "public"."auth_user" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table network_interfaces
-- ----------------------------
ALTER TABLE "public"."network_interfaces" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table network_interfaces
-- ----------------------------
CREATE UNIQUE INDEX  "network_interfaces_id_key" ON "public"."network_interfaces" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table payment
-- ----------------------------
ALTER TABLE "public"."payment" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table payment
-- ----------------------------
CREATE INDEX  "fki_auth_user" ON "public"."payment" USING btree(fk_auth_user "pg_catalog"."int4_ops" ASC NULLS LAST);
CREATE INDEX  "fki_group" ON "public"."payment" USING btree(fk_group "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table application_cluster
-- ----------------------------
ALTER TABLE "public"."application_cluster" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table server_credentials
-- ----------------------------
ALTER TABLE "public"."server_credentials" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table server_credentials
-- ----------------------------
CREATE UNIQUE INDEX  "server_authentication_id_key" ON "public"."server_credentials" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table tools
-- ----------------------------
ALTER TABLE "public"."tools" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table api_user
-- ----------------------------
ALTER TABLE "public"."api_user" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table beacon
-- ----------------------------
ALTER TABLE "public"."beacon" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table container
-- ----------------------------
ALTER TABLE "public"."container" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table server
-- ----------------------------
ALTER TABLE "public"."server" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table server
-- ----------------------------
CREATE UNIQUE INDEX  "server_id_key" ON "public"."server" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table licensing
-- ----------------------------
ALTER TABLE "public"."licensing" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table licensing
-- ----------------------------
CREATE UNIQUE INDEX  "licensing_id_key" ON "public"."licensing" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table job
-- ----------------------------
ALTER TABLE "public"."job" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table application
-- ----------------------------
ALTER TABLE "public"."application" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table team
-- ----------------------------
ALTER TABLE "public"."team" ADD CONSTRAINT "fk_auth_user_userid" FOREIGN KEY ("fk_auth_user") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."team" ADD CONSTRAINT "fk_payment" FOREIGN KEY ("fk_payment") REFERENCES "public"."payment" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table server_model
-- ----------------------------
ALTER TABLE "public"."server_model" ADD CONSTRAINT "server_model_fk_hardware_fkey" FOREIGN KEY ("fk_hardware") REFERENCES "public"."hardware" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server_model" ADD CONSTRAINT "server_model_fk_hypervisor_fkey" FOREIGN KEY ("fk_hypervisor") REFERENCES "public"."hypervisor" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table history
-- ----------------------------
ALTER TABLE "public"."history" ADD CONSTRAINT "history_fk_server_fkey" FOREIGN KEY ("fk_server") REFERENCES "public"."server" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table virtualmachine
-- ----------------------------
ALTER TABLE "public"."virtualmachine" ADD CONSTRAINT "fk_ip_settings_id" FOREIGN KEY ("fk_ip_settings") REFERENCES "public"."ip_settings" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."virtualmachine" ADD CONSTRAINT "fk_server_id" FOREIGN KEY ("fk_server") REFERENCES "public"."server" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table network_interfaces
-- ----------------------------
ALTER TABLE "public"."network_interfaces" ADD CONSTRAINT "network_interfaces_fk_ip_settings_fkey" FOREIGN KEY ("fk_ip_settings") REFERENCES "public"."ip_settings" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table payment
-- ----------------------------
ALTER TABLE "public"."payment" ADD CONSTRAINT "fk_auth_user_email" FOREIGN KEY ("fk_auth_user") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."payment" ADD CONSTRAINT "fk_group" FOREIGN KEY ("fk_group") REFERENCES "public"."team" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table tools
-- ----------------------------
ALTER TABLE "public"."tools" ADD CONSTRAINT "tools_fk_ip_settings_fkey" FOREIGN KEY ("fk_ip_settings") REFERENCES "public"."ip_settings" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."tools" ADD CONSTRAINT "tools_fk_network_services_fkey" FOREIGN KEY ("fk_network_services") REFERENCES "public"."network_services" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table beacon
-- ----------------------------
ALTER TABLE "public"."beacon" ADD CONSTRAINT "beacon_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table container
-- ----------------------------
ALTER TABLE "public"."container" ADD CONSTRAINT "container_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table server
-- ----------------------------
ALTER TABLE "public"."server" ADD CONSTRAINT "server_fk_license_fkey" FOREIGN KEY ("fk_licensing") REFERENCES "public"."licensing" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server" ADD CONSTRAINT "server_fk_model_fkey" FOREIGN KEY ("fk_server_model") REFERENCES "public"."server_model" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server" ADD CONSTRAINT "server_fk_network_services_fkey" FOREIGN KEY ("fk_network_services") REFERENCES "public"."network_services" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server" ADD CONSTRAINT "server_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server" ADD CONSTRAINT "server_fk_network_interfaces_fkey" FOREIGN KEY ("fk_network_interfaces") REFERENCES "public"."network_interfaces" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server" ADD CONSTRAINT "server_fk_server_credentials_fkey" FOREIGN KEY ("fk_server_credentials") REFERENCES "public"."server_credentials" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table job
-- ----------------------------
ALTER TABLE "public"."job" ADD CONSTRAINT "job_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."auth_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table application
-- ----------------------------
ALTER TABLE "public"."application" ADD CONSTRAINT "fk_server_id" FOREIGN KEY ("fk_server") REFERENCES "public"."server" ("id") ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;

