/*
 Navicat PostgreSQL Data Transfer

 Source Server         : parzee-db-tools-sfo1
 Source Server Version : 90309
 Source Host           : 198.199.110.24
 Source Database       : imbuedb
 Source Schema         : public

 Target Server Version : 90309
 File Encoding         : utf-8

 Date: 01/05/2016 19:32:34 PM
*/

-- ----------------------------
--  Table structure for server_model
-- ----------------------------
DROP TABLE IF EXISTS "public"."server_model";
CREATE TABLE "public"."server_model" (
	"id" int4 NOT NULL DEFAULT nextval('server_model_id_seq'::regclass),
	"model" varchar(250) NOT NULL COLLATE "default",
	"vendor" varchar(250) NOT NULL COLLATE "default",
	"fk_hypervisor" int4,
	"status" bool NOT NULL,
	"fk_hardware" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."server_model" OWNER TO "imbue";

-- ----------------------------
--  Records of server_model
-- ----------------------------
BEGIN;
INSERT INTO "public"."server_model" VALUES ('13', 'Business Edition 6000', 'Cisco Systems, Inc', '1', 't', '1');
INSERT INTO "public"."server_model" VALUES ('14', 'Business Edition 7000', 'Cisco Systems, Inc', '1', 't', '1');
COMMIT;

-- ----------------------------
--  Primary key structure for table server_model
-- ----------------------------
ALTER TABLE "public"."server_model" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table server_model
-- ----------------------------
CREATE UNIQUE INDEX  "server_model_id_key" ON "public"."server_model" USING btree("id" "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Foreign keys structure for table server_model
-- ----------------------------
ALTER TABLE "public"."server_model" ADD CONSTRAINT "server_model_fk_hardware_fkey" FOREIGN KEY ("fk_hardware") REFERENCES "public"."hardware" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "public"."server_model" ADD CONSTRAINT "server_model_fk_hypervisor_fkey" FOREIGN KEY ("fk_hypervisor") REFERENCES "public"."hypervisor" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

