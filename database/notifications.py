__author__ = 'gogasca'

import select

import psycopg2
import psycopg2.extensions
from conf import settings
host = settings.dbhost
dbname = settings.dbname
user = settings.dbusername
password = settings.dbpassword
port = settings.dbport

dsn = 'dbname=%s host=%s user=%s password=%s' % (dbname, host, user, password)
conn = psycopg2.connect(dsn)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

curs = conn.cursor()
curs.execute("LISTEN imbuedb;")
print 'Listening...'

print "Waiting for notifications on channel 'imbuedb'"
while 1:
    if select.select([conn], [], [], 5) == ([], [], []):
        print "Timeout"
    else:
        conn.poll()
        while conn.notifies:
            notify = conn.notifies.pop()
            print "Got NOTIFY:", notify.pid, notify.channel, notify.payload