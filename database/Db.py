__author__ = 'gogasca'

import bleach
import logging
import psycopg2
from conf import logging_conf
from database.db_connection import database_check_parameters
from error import exceptions

# http://initd.org/psycopg/docs/module.html


log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Db():
    """

    """

    def __init__(self, server=None, username=None, password=None, database=None, port=None, **kwargs):
        """

        :param server:
        :param username:
        :param password:
        :param database:
        :param port:
        :param kwargs:
        :return:
        """
        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.dsn = 'dbname=%s host=%s port=%d user=%s password=%s' % (self.database,
                                                                      self.server,
                                                                      self.port,
                                                                      self.username,
                                                                      self.password)
        self.conn = None

    def initialize(self, dsn=None, **kwargs):
        """

        :param dsn:
        :param kwargs:
        :return:
        """

        if database_check_parameters(dbhost=self.server,
                                     dbport=self.port,
                                     dbname=self.database,
                                     dbusername=self.username,
                                     dbpassword=self.password):
            if self.dsn:
                log.info(self.dsn)
                self.conn = psycopg2.connect(self.dsn)
                return True
            elif dsn:
                log.info(dsn)
                self.conn = psycopg2.connect(dsn)
                return True
            else:
                log.info('DB initialize() DB not initialized')
                raise exceptions.DatabaseParameters('Invalid dsn.')
        else:
            log.error('DB initialize() Invalid DB parameters')
            raise exceptions.DatabaseParameters('DB initialize() Invalid DB parameters')

    def insert(self, query, content):
        """

        :param query:
        :param content:
        :return:
        Example:

            INSERT INTO application (description, is_installed, application_params)
            VALUES ( 'ucm dev', '1', '{"hypervisor": "esxi", "hostname": "", "publisher": 1 }')
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                content = bleach.clean(content)

                final_query = query + " VALUES (" + content + ") RETURNING id;"
                log.info('DB insert() Executing SQL query: ' + final_query)
                cur.execute(final_query)
                id_of_new_row = cur.fetchone()[0]
                self.conn.commit()
                return id_of_new_row

            else:
                log.error('DB insert() Invalid DB parameters No connection to DB')
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise

        except Exception as exception:
            log.exception('DB.update() Unable to insert query: {}'.format(exception))
            raise

    def update(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                self.conn.commit()
            else:
                log.error('DB update() Invalid db parameters')
                print 'No connection to DB'
                return
        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.update() Unable to query: {}'.format(exception))
            raise

    def query(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                # A commit may go here...
                rows = cur.fetchall()
                return rows
            else:
                log.error('DB.query() Invalid db parameters')
                print 'No connection to db'
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.query() Unable to query: {}'.format(exception))
            raise
