__author__ = 'gogasca'
"""
    Connects to database
"""
import sys
import platform

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')
import logging
from conf import logging_conf, settings

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)
import psycopg2
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DatabaseSession(object):
    """

    """

    def __init__(self, db_url):
        self.db_url = db_url
        self.session = None

    def get_session(self):
        """

        :return:
        """
        try:
            self.engine = create_engine(self.db_url,
                                        pool_size=settings.SQLALCHEMY_POOL_SIZE,
                                        max_overflow=settings.SQLALCHEMY_MAX_OVERFLOW)
            Session = sessionmaker()
            Session.configure(bind=self.engine)
            self.session = Session()
            self.session._model_changes = {}
            return self.session
        except Exception, e:
            log.error('DatabaseSession() get_session(): ' + str(e.message))

    def execute(self, sqlcommand):
        """

        :param sqlcommand:
        :return:
        """
        try:
            session_active = False
            # Check if session
            if self.session:
                session_active = True
            else:
                if self.get_session():
                    session_active = True
                else:
                    session_active = False

            if session_active:
                connection = self.engine.connect()
                result = connection.execute(sqlcommand)
                return result
            else:
                log.error('DatabaseSession() Unable to start session. Session is not active')

        except psycopg2.OperationalError as exception:
            log.exception('DatabaseSession() OperationalError(): {}'.format(exception))
        except Exception as exception:
            log.exception('DatabaseSession() execute(): '.format(exception))

    def query(self):
        pass
