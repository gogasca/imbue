__author__ = 'gogasca'


class AsyncNotify():
    '''Class to trigger a function via PostgreSQL NOTIFY messages.
Refer to the documentation for more information on LISTEN, NOTIFY and UNLISTEN.
http://www.postgresql.org/docs/8.3/static/sql-notify.html

This obscure feature is very useful. You can create a trigger function on a
table that executes the NOTIFY command. Then any time something is inserted,
updated or deleted your Python function will be called using this class.

As far as I know this is unique to PostgreSQL.'''

    def __init__(self, dsn):
        '''The dsn is passed here. This class requires the psycopg2 driver.'''
        import psycopg2

        try:
            print 'Connecting to database...'
            self.conn = psycopg2.connect(dsn)
        except Exception, e:
            print 'Unable to connect to database'
            print str(e)

        print 'Connected to database...'
        self.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        self.curs = self.conn.cursor()
        self.__listening = False

    def __listen(self):
        import select

        if self.__listening:
            return 'already listening!'
        else:
            self.__listening = True
            try:
                while self.__listening:
                    # http://psycopg.lighthouseapp.com/projects/62710/tickets/19-missing-cursorisready-and-cursorfileno-in-230
                    if select.select([self.conn], [], [], 15) == ([], [], []):
                        from time import gmtime, strftime

                        print str(strftime("%Y-%m-%d %H:%M:%S", gmtime())) + ' Keepalive()'
                    else:
                        self.conn.poll()
                        while self.conn.notifies:
                            #notify = self.conn.notifies.pop()
                            notify = self.curs.connection.notifies.pop()
                            print "Got NOTIFY:", notify.pid, notify.channel, notify.payload
                            self.gotNotify(notify.pid, notify.channel)
            except Exception, e:
                print str(e)

    def addNotify(self, notify):
        print 'AsyncNotify addNotify() ' + str(notify)
        '''Subscribe to a PostgreSQL NOTIFY'''
        sql = "LISTEN %s" % notify
        self.curs.execute(sql)

    def removeNotify(self, notify):
        '''Unsubscribe a PostgreSQL LISTEN'''
        print 'AsyncNotify removeNotify() ' + str(notify)
        sql = "UNLISTEN %s" % notify
        self.curs.execute(sql)

    def stop(self):
        '''Call to stop the listen thread'''
        self.__listening = False

    def run(self):
        '''Start listening in a thread and return that as a deferred'''
        print 'AsyncNotify run()'
        from twisted.internet import threads

        return threads.deferToThread(self.__listen)

    def gotNotify(self, pid, notify):
        '''Called whenever a notification subscribed to by addNotify() is detected.
Unless you override this method and do someting this whole thing is rather pointless.'''
        pass