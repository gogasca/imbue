__author__ = 'gogasca'
import psycopg2
from conf import settings
from utils import validator
import sys
import warnings
import platform
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/IMBUExApp/')
import logging
from conf import logging_conf, settings
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def start():
    """

    :return:
    """
    import sys

    host = settings.dbhost
    dbname = settings.dbname
    user = settings.dbusername
    password = settings.dbpassword
    port = settings.dbport

    conn_string = "host='%s' dbname='%s' user='%s' password='%s' port='%i'" \
                  % (host, dbname, user, password, port)

    # print the connection string we will use to connect
    # print "db_initialization() Connecting to database ->%s" % (conn_string)

    try:
        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        cursor = conn.cursor()
        # print "db_initialization() Connected to database ->%s" % (conn_string)
        return True
    except:
        # Get the most recent exception
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        sys.exit("Database connection failed!\n ->%s" % (exceptionValue))


def database_check_parameters(dbhost=None, dbport=None, dbname=None, dbusername=None, dbpassword=None, **kwargs):
    """

    :param dbhost:
    :param dbport:
    :param dbname:
    :param dbusername:
    :param dbpassword:
    :param kwargs:
    :return:
    """
    # Read DBparameters from Configuration file. Assuming previous param check already did his job...
    # Database hostname
    if dbhost is None and hasattr(settings, 'dbhost'):
        dbhost = settings.dbhost
        if validator.isValidTarget(dbhost):
            pass
        else:
            print 'check_db_parameters() Invalid dbhostname'
    else:
        if len(dbhost) > 0 and isinstance(dbhost, (str, unicode)):
            if validator.isValidTarget(dbhost):
                pass
            else:
                print 'check_db_parameters Invalid dbhostname'
        else:
            print 'check_db_parameters Invalid dbhostname type'

    # Database port use 5432
    if dbport is None and hasattr(settings, 'dbport'):
        dbport = settings.dbport
        if validator.isValidPort(dbport):
            pass
        else:
            print 'check_db_parameters Invalid dbport'
    else:
        if isinstance(dbport, int):
            if validator.isValidPort(dbport):
                pass
            else:
                print 'check_db_parameters Invalid dbport'
                return False

    if dbusername is None and hasattr(settings, 'dbusername'):
        dbusername = settings.dbusername
        if validator.isValidString(dbusername, checkLength=True):
            pass
        else:
            print 'check_db_parameters Invalid username'
            return False
    else:
        if validator.isValidString(dbusername, checkLength=True):
            pass
        else:

            print 'check_db_parameters Invalid username'

    # Password could be empty
    if dbpassword is None and hasattr(settings, 'dbpassword'):
        dbpassword = settings.dbpassword
        if len(dbpassword) == 0:
            pass
        else:
            if validator.isValidString(dbpassword, checkLength=True):
                pass
            else:
                return False

    else:
        if validator.isValidString(dbusername, checkLength=True):
            pass
        else:
            print 'check_db_parameters Invalid username'
            return False

    if dbname is None and hasattr(settings, 'dbname'):
        dbname = settings.dbname
        if len(dbname) > 0:
            pass
    else:
        if validator.isValidString(dbname, checkLength=True):
            pass
        else:
            print 'check_db_parameters Invalid dbname'
    return True


def database_get_parameters():
    """

    :return:
    """
    dbParams = {}

    if hasattr(settings, 'dbhost'):
        dbhost = settings.dbhost
        dbParams['dbhost'] = dbhost

    if hasattr(settings, 'dbport'):
        dbport = settings.dbport
        # No need to convert int to str!
        dbParams['dbport'] = dbport

    if hasattr(settings, 'dbusername'):
        dbusername = settings.dbusername
        dbParams['dbusername'] = dbusername

    if hasattr(settings, 'dbpassword'):
        dbpassword = settings.dbpassword
        dbParams['dbpassword'] = dbpassword
    if hasattr(settings, 'dbname'):
        dbname = settings.dbname
        dbParams['dbname'] = dbname

    return dbParams


def database_read_parameters(dbhost='', dbport='', dbusername='', dbpassword='', dbname='', readConfigurationFile=True,
                     **kwargs):
    """

    :param dbhost:
    :param dbport:
    :param dbusername:
    :param dbpassword:
    :param dbname:
    :param readConfigurationFile:
    :param kwargs:
    :return:
    """
    if readConfigurationFile:
        if database_check_parameters():
            dsn = database_get_parameters()
            dsn = 'dbname=%s host=%s port=% user=%s password=%s' % (
                dsn['dbname'], dsn['dbhost'], dsn['dbport'], dsn['dbusername'], dsn['dbpassword'])
            return dsn
        else:
            return None
    else:
        if database_check_parameters(dbhost=dbhost, dbport=dbport, dbusername=dbusername, dbpassword=dbpassword,
                               dbname=dbname):
            dsn = 'dbname=%s host=%s port=% user=%s password=%s' % (dbname, dbhost, dbport, dbusername, dbpassword)
            return dsn


def database_initialize(dsn=None, host='localhost', dbname='template1', user='postgres', password='', **kwargs):
    """

    :param dsn:
    :param host:
    :param dbname:
    :param user:
    :param password:
    :param kwargs:
    :return:
    """
    try:
        # http://initd.org/psycopg/docs/module.html
        if dsn:
            conn = psycopg2.connect(dsn)
        else:
            conn = psycopg2.connect("dbname=" + dbname + " user=" + user + " host=" + host + " password=" + password)

    except Exception, e:
        print 'dbDriver() Unable to connect to database'
        print e
        raise

    print 'dbDriver() Connected to database...'
    cur = conn.cursor()
    cur.execute("""SELECT datname FROM pg_database LIMIT 100;""")
    # A commit may go here...
    rows = cur.fetchall()
    print "dbDriver() Databases detected in system:"
    import pprint

    pprint.pprint(rows)

    cur.execute("""SELECT server.hostname,network_services.properties FROM server,network_services
    WHERE server.id=1 AND server.fk_network_services=network_services.id;""")

    """
    SELECT
    tools.name,
    ip_settings.ip,
    network_services.properties
    FROM tools
    JOIN ip_settings ON (ip_settings.id = tools.fk_ip_settings)
    JOIN network_services ON (network_services.id = tools.fk_network_services)
    WHERE tools.id=1
    """
    """
    SELECT DISTINCT
    server.hostname,
    ip_settings.ip
    FROM server
    INNER JOIN network_interfaces ON server.fk_network_interfaces = network_interfaces.id
    INNER JOIN ip_settings ON ip_settings.id = network_interfaces.fk_ip_settings
    WHERE server.id=1
    """

    """
    INSERT INTO server_authentication(id,ssh_username, ssh_password) VALUES ('',crypt('test', gen_salt('md5')));

    """
    rows = cur.fetchall()
    print "dbDriver() Servers in system: " + str(len(rows))
    for row in rows:
        # ('parzee-ciscobe6k-sf-1', {u'vnc': {u'active': 0, u'port': 5901}, u'ssh': {u'active': 0, u'port': 22}, u'https': {u'active': 1, u'port': 443}})
        network_services = row[1]
        if network_services['https']: https = network_services['https']
        if network_services['ssh']: ssh = network_services['ssh']
        if network_services['vnc']: vnc = network_services['vnc']
        # Read SSH Parameters
        ssh_port = ssh['port']
        https_port = https['port']
        vnc_port = vnc['port']


#if __name__ == '__main__':
    # Read parameters from Configuration file
#    dsn = dbReadParameters()
#    dbInitialize(dsn=dsn)
