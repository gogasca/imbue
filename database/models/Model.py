__author__ = 'gogasca'
# coding: utf-8

# Import Database Application
from conf import settings
import datetime
import os
from collections import OrderedDict
from sqlalchemy import BigInteger, Boolean, Column, DateTime, Float, ForeignKey, Integer, String, Table, text
from sqlalchemy.inspection import inspect
from sqlalchemy.dialects.postgresql.json import JSON
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from utils.security import common

Base = declarative_base()
metadata = Base.metadata
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()
from passlib.apps import custom_app_context as sec_functions
from itsdangerous import (TimedJSONWebSignatureSerializer as SecSerializer, BadSignature, SignatureExpired)


# sqlacodegen postgresql://imbueapi:imbueapi@localhost/imbuedb > database/models.py

class Serializer(object):
    """
        Serialize information from database to a JSON Dictionary
    """

    def serialize(self):
        return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]


class AutoSerialize(object):
    """
        Mixin for retrieving public fields of model in json-compatible format'
    """
    __public__ = None

    def get_public(self, exclude=(), extra=()):
        "Returns model's PUBLIC data for jsonify"
        data = {}
        keys = self._sa_instance_state.attrs.items()
        public = self.__public__ + extra if self.__public__ else extra
        for k, field in keys:
            if public and k not in public: continue
            if k in exclude: continue
            value = self._serialize(field.value)
            if value:
                data[k] = value
        return data

    @classmethod
    def _serialize(cls, value, follow_fk=False):
        if type(value) in (datetime,):
            ret = value.isoformat()
        elif hasattr(value, '__iter__'):
            ret = []
            for v in value:
                ret.append(cls._serialize(v))
        elif AutoSerialize in value.__class__.__bases__:
            ret = value.get_public()
        else:
            ret = value

        return ret


class DictSerializable(object):
    """

    """

    def _asdict(self):
        result = OrderedDict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result


t_installation_completed = Table(
    'installation_completed', metadata,
    Column('description', String(250)),
    Column('application_params', JSON),
    Column('is_completed', Boolean),
    Column('id', Integer, nullable=False, server_default=text("nextval('installation_completed_id_seq'::regclass)")),
    Column('fk_server', ForeignKey(u'server.id', ondelete=u'CASCADE', onupdate=u'CASCADE')),
    Column('owner_id', ForeignKey(u'auth_user.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
)


class AuthUser(db.Model):
    __tablename__ = 'auth_user'

    id = Column(Integer, primary_key=True, unique=True, server_default=text("nextval('auth_user_id_seq'::regclass)"))
    password = Column(String(128), nullable=False)
    last_login = Column(DateTime(True))
    is_superuser = Column(Boolean, nullable=False)
    username = Column(String(30), nullable=False, unique=True)
    first_name = Column(String(30), nullable=False)
    last_name = Column(String(30), nullable=False)
    email = Column(String(254), nullable=False)
    is_staff = Column(Boolean, nullable=False)
    is_active = Column(Boolean, nullable=False)
    date_joined = Column(DateTime(True), nullable=False)


class ApiUser(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'api_user'
    __public__ = ('id', 'username', 'created')
    id = Column(db.Integer, primary_key=True)
    username = Column(db.String(50), index=True)
    password = Column(db.String(250))
    created = Column(DateTime)

    def __init__(self, username, password, created):
        self.username = username
        self.password = password
        self.created = created

    def hash_password(self, password):
        """

        :param password:
        :return:
        """
        self.password = common.encrypt(password)

    def verify_password(self, password):
        """

        :param password:
        :return:
        """
        if password:
            if common.decrypt(self.password) == password:
                return True
            else:
                return False

        return False

    def generate_auth_token(self, expiration=600):
        s = SecSerializer(os.environ.get('SECRET_FERNET_KEY'), expires_in=expiration)
        return s.dumps({"id": self.id})

    @staticmethod
    def verify_auth_token(token):
        s = SecSerializer(os.environ.get('SECRET_FERNET_KEY'))
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = ApiUser.query.get(data["id"])
        return user

    def serialize(self):
        d = Serializer.serialize(self)
        del d['password']
        return d


class Beacon(db.Model):
    __tablename__ = 'beacon'

    id = Column(Integer, primary_key=True, server_default=text("nextval('beacon_identifier_seq'::regclass)"))
    hostname = Column(String(200), nullable=False)
    version = Column(String(100))
    remote_path = Column(String(200))
    filename = Column(String(250))
    description = Column(String(250))
    owner_id = Column(ForeignKey(u'auth_user.id'))
    owner = relationship(u'AuthUser')


class Container(db.Model):
    __tablename__ = 'container'

    id = Column(Integer, primary_key=True, server_default=text("nextval('container_identifier_seq'::regclass)"))
    hostname = Column(String(250))
    description = Column(String(250))
    owner_id = Column(ForeignKey(u'auth_user.id'))

    owner = relationship(u'AuthUser')


class Hardware(db.Model):
    __tablename__ = 'hardware'

    id = Column(Integer, primary_key=True, unique=True)
    cpu_speed = Column(Float)
    cpu_number = Column(Integer)
    memory = Column(Float)
    disk_size = Column(Float)
    cpu_model = Column(String(200))
    disk_number = Column(Integer)
    raid = Column(String(100))


class Hypervisor(db.Model):
    __tablename__ = 'hypervisor'

    id = Column(Integer, primary_key=True, unique=True, server_default=text("nextval('hypervisor_id_seq'::regclass)"))
    name = Column(String(250), nullable=False)
    version = Column(String(250))
    vendor = Column(String(100), nullable=False)
    status = Column(Boolean, nullable=False)
    api_version = Column(String(100))


class IpSetting(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'ip_settings'

    id = Column(Integer, primary_key=True, unique=True, server_default=text("nextval('ip_settings_id_seq'::regclass)"))
    ip = Column(String(100), nullable=False)
    mask = Column(String(100))
    gateway = Column(String(100))
    dns1 = Column(String(100))
    dns2 = Column(String(100))
    domain = Column(String(100))

    def __init__(self, ip):
        self.ip = ip

    def serialize(self):
        d = Serializer.serialize(self)
        return d


class Job(db.Model, AutoSerialize, Serializer):
    """

    """
    __tablename__ = 'job'
    __public__ = ('status', 'description', 'reference', 'job_start', 'job_end')
    id = Column(Integer, primary_key=True, server_default=text("nextval('job_id_seq'::regclass)"))
    description = Column(String(256))
    reference = Column(String(32))
    job_start = Column(DateTime)
    job_end = Column(DateTime)
    owner_id = Column(ForeignKey(u'auth_user.id'))
    is_cluster = Column(Boolean, server_default="false")
    host_information = Column(String(1024))
    status = Column(Integer, nullable=False)
    request_data = Column(String(4096))
    is_cancelled = Column(Boolean, server_default="false")
    job_type = Column(String(256))
    report_created = Column(Boolean, server_default="false")
    owner = relationship(u'AuthUser')

    def serialize(self):
        """

        :return:
        """
        import json
        d = Serializer.serialize(self)
        info = dict()
        info['owner_id'] = d['owner_id']
        info['is_cluster'] = d['is_cluster']
        info['is_cancelled'] = d['is_cancelled']

        if d['host_information']:
            info['host'] = json.loads(d['host_information'])
        else:
            info['host'] = None

        d['info'] = info
        del d['is_cancelled']
        del d['host_information']
        del d['id']
        del d['owner']
        del d['owner_id']
        del d['is_cluster']

        return d

    def __init__(self, status, description, reference, job_start, request_data, job_type):
        """

        :param status:
        :param description:
        :param reference:
        :param job_start:
        :param request_data:
        :param job_type:
        :return:
        """
        self.status = status
        self.description = description
        self.reference = reference
        self.job_start = job_start
        self.request_data = request_data
        self.job_type = job_type


class Licensing(db.Model):
    __tablename__ = 'licensing'

    id = Column(Integer, primary_key=True, unique=True, server_default=text("nextval('licensing_id_seq'::regclass)"))
    key = Column(String(250))
    type = Column(String(100))
    active = Column(Boolean)


class Marketplace(Base):
    __tablename__ = 'marketplace'

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    version = Column(String(100))
    vendor = Column(String(100))
    is_clustered = Column(Boolean)
    application_params = Column(JSON)


class NetworkInterface(db.Model):
    __tablename__ = 'network_interfaces'

    id = Column(Integer, primary_key=True, unique=True,
                server_default=text("nextval('network_interfaces_id_seq'::regclass)"))
    name = Column(String(100))
    status = Column(Boolean)
    fk_ip_settings = Column(ForeignKey(u'ip_settings.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))

    ip_setting = relationship(u'IpSetting')


class NetworkService(db.Model):
    __tablename__ = 'network_services'

    id = Column(Integer, primary_key=True, unique=True,
                server_default=text("nextval('network_services_id_seq'::regclass)"))
    properties = Column(JSON)


class Server(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'server'
    __public__ = (
        'id', 'discovered', 'name', 'description', 'created', 'status', 'serialnumber', 'image_folder',
        'install_folder')

    id = Column(Integer, primary_key=True, server_default=text("nextval('server_identifier_seq'::regclass)"))
    discovered = Column(Boolean, nullable=False, server_default=text("false"))
    status = Column(Integer, nullable=False)
    serialnumber = Column(String(128), nullable=False)
    name = Column(String(256), nullable=False)
    description = Column(String(2048))
    created = Column(DateTime)
    last_synchronized = Column(DateTime)
    iso_files = Column(String(2048))
    datastores = Column(String(16384))
    datastores_path = Column(String(16384))
    networks = Column(String(2048))
    image_folder = Column(String(256))
    install_folder = Column(String(256))
    fk_licensing = Column(ForeignKey(u'licensing.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    fk_server_model = Column(ForeignKey(u'server_model.id'))
    fk_network_services = Column(ForeignKey(u'network_services.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    fk_network_interfaces = Column(ForeignKey(u'network_interfaces.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    fk_server_credentials = Column(ForeignKey(u'server_credentials.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    owner_id = Column(ForeignKey(u'auth_user.id'))
    owner = relationship(u'AuthUser')
    licensing = relationship(u'Licensing')
    network_interface = relationship(u'NetworkInterface')
    network_service = relationship(u'NetworkService')
    server_credential = relationship(u'ServerCredential')
    server_model = relationship(u'ServerModel')

    def serialize(self):
        d = Serializer.serialize(self)
        return d


class ServerCredential(db.Model):
    __tablename__ = 'server_credentials'

    id = Column(Integer, primary_key=True, unique=True,
                server_default=text("nextval('server_credentials_id_seq'::regclass)"))
    ssh_username = Column(String(250), nullable=False)
    ssh_password = Column(String(2048), nullable=False)
    https_username = Column(String(250))
    https_password = Column(String(2048))
    vnc_password = Column(String(250))


class ServerModel(db.Model):
    __tablename__ = 'server_model'

    id = Column(Integer, primary_key=True, unique=True, server_default=text("nextval('server_model_id_seq'::regclass)"))
    model = Column(String(250), nullable=False)
    vendor = Column(String(250), nullable=False)
    fk_hypervisor = Column(ForeignKey(u'hypervisor.id'))
    status = Column(Boolean, nullable=False)
    fk_hardware = Column(ForeignKey(u'hardware.id'))

    hardware = relationship(u'Hardware')
    hypervisor = relationship(u'Hypervisor')


class Tool(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'tools'
    __public__ = ('id', 'hostname', 'description', 'status')

    id = Column(Integer, primary_key=True, server_default=text("nextval('tools_identifier_seq'::regclass)"))
    hostname = Column(String(250), nullable=False)
    description = Column(String(500))
    status = Column(Integer, nullable=False)
    fk_ip_settings = Column(ForeignKey(u'ip_settings.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    fk_server_credentials = Column(ForeignKey(u'server_credentials.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    fk_network_services = Column(ForeignKey(u'network_services.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    ip_setting = relationship(u'IpSetting')
    server_credential = relationship(u'ServerCredential')
    network_service = relationship(u'NetworkService')

    def serialize(self):
        d = Serializer.serialize(self)
        return d


class Virtualmachine(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'virtualmachine'
    __public__ = ('vmid', 'hostname', 'description', 'status', 'power_state', 'app_id', 'screen')

    id = Column(Integer, primary_key=True, server_default=text("nextval('virtualmachine_id_seq'::regclass)"))
    vmid = Column(Integer)
    hostname = Column(String(64), nullable=False)
    status = Column(Integer)
    last_synchronized = Column(DateTime, nullable=False)
    fk_server = Column(ForeignKey(u'server.id'))
    fk_ip_settings = Column(ForeignKey(u'ip_settings.id', ondelete=u'CASCADE', onupdate=u'CASCADE'))
    description = Column(String(2048))
    uuid = Column(String(128))
    app_id = Column(Integer)
    screen = Column(String(128))
    power_state = Column(Boolean, nullable=False)
    uptime = Column(String(256))
    directory = Column(String(1024))
    ip_setting = relationship(u'IpSetting')
    server = relationship(u'Server')

    def __init__(self, vmid, hostname, status, description, fk_server, fk_ip_settings, uuid, app_id, power_state,
                 uptime, directory, screen_url):
        """

        :param vmid:
        :param hostname:
        :param status:
        :param description:
        :return:
        """
        self.vmid = vmid
        self.hostname = hostname
        self.status = status
        self.description = description
        self.last_synchronized = 'now()'
        self.uuid = uuid
        self.app_id = app_id
        self.power_state = power_state
        self.uptime = uptime
        self.fk_server = fk_server
        self.fk_ip_settings = fk_ip_settings
        self.screen = screen_url

        if directory:
            self.directory = directory

    def serialize(self):
        d = Serializer.serialize(self)
        return d
