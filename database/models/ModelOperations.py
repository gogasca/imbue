__author__ = 'gogasca'

from database.models import Model
from database.models.Model import db as db
from flask import request
from utils.security import common


def delete_object(instance):
    """

    :param instance:
    :return:
    """
    try:
        db.session.delete(instance)
        db.session.commit()
        return True
    except Exception, exception:
        db.session.rollback()
        print exception


def add_object(instance):
    """

    :param instance:
    :return:
    """
    try:
        db.session.add(instance)
        db.session.commit()
        return True
    except Exception, exception:
        db.session.rollback()
        print exception


def insert_job(job_reference, request_data, job_type):
    """

    :param job_reference:
    :param request_data:
    :return:
    """
    try:
        # status, description, reference, start_time
        job = Model.Job(0, "Job queued...", job_reference, "now()",
                        request_data.replace('\n', ''), str(job_type))
        db.session.add(job)
        db.session.commit()
        return job.id
    except Exception, e:
        db.session.rollback()
        print e


def insert_api_user(username, password):
    """

    :param username:
    :param password:
    :return:
    """
    try:
        # Encrypt password
        password = common.encrypt(password)
        if password:
            user = Model.ApiUser(username, password, "now()")
            db.session.add(user)
            db.session.commit()
            return user.id
    except Exception, e:
        db.session.rollback()
        print e


def infrastructure_details(host, host_id):
    """

    :param host:
    :param id:
    :return:
    """
    try:
        if host and id:
            ip_address = db.session.query(Model.IpSetting.ip).select_from(Model.Tool).join(Model.IpSetting).filter(
                Model.Tool.id == int(host_id))

            network_services = db.session.query(Model.NetworkService.properties).select_from(Model.Tool).join(
                Model.NetworkService).filter(Model.Tool.id == int(host_id))
            # Get Server information
            host_details = host.get_public()
            try:
                host_details['ip'] = ip_address[0][0]
            except IndexError:
                pass

            try:
                network_services_arr = []
                network_services_dict = dict()
                services = network_services[0][0]
                for key, value in services.iteritems():
                    network_services_dict[key] = value
                    network_services_arr.append(network_services_dict)
                    network_services_dict = {}

                host_details['network_services'] = network_services_arr
            except IndexError:
                pass
            return host_details
        else:
            return

    except Exception, exception:
        print exception


def server_details(host, host_id):
    """

    :param host:
    :param host_id:
    :return:
    """

    try:
        if host and host_id:
            # Get IP Address information
            ip_address = db.session.query(Model.IpSetting.ip).select_from(Model.Server).join(
                Model.NetworkInterface).join(Model.IpSetting).filter(Model.Server.id == int(host_id))

            # Get Server information
            network_services = db.session.query(Model.NetworkService.properties).select_from(Model.Server).join(
                Model.NetworkService).filter(Model.Server.id == int(host_id))

            # Get all details
            host_details = host.get_public()

            # Assign IP information
            try:
                host_details['ip'] = ip_address[0][0]
            except IndexError:
                pass

            # Assign Port informationsplit

            try:
                network_services_arr = []
                network_services_dict = dict()
                services = network_services[0][0]
                for key, value in services.iteritems():
                    network_services_dict[key] = value
                    network_services_arr.append(network_services_dict)
                    network_services_dict = {}

                host_details['network_services'] = network_services_arr

            except IndexError:
                pass

            try:
                # Get Virtualmachines vmId and hostname.
                # You can add more fields.
                vms = db.session.query(Model.Virtualmachine.vmid,
                                       Model.Virtualmachine.hostname,
                                       Model.Virtualmachine.power_state,
                                       Model.Virtualmachine.app_id).select_from(Model.Server).join(
                    Model.Virtualmachine).filter(Model.Server.id == int(host_id))

                virtualmachine = []
                virtualmachine_info = dict()
                # Create Dictionary of VMs
                for vm in vms:
                    virtualmachine_info['vmid'] = vm[0]
                    virtualmachine_info['name'] = vm[1]
                    virtualmachine_info['power_state'] = vm[2]
                    virtualmachine_info['app_id'] = vm[3]
                    virtualmachine.append(virtualmachine_info)
                    virtualmachine_info = {}
                host_details['vm'] = virtualmachine

            except Exception, exception:
                print exception

            # iso_files
            try:
                iso_files_arr = []
                # =========================================================
                # Get Server ISO Files
                # =========================================================

                iso_files_db = db.session.query(Model.Server.iso_files).filter(Model.Server.id == int(host_id)).all()
                iso_files_db = [tuple(filter(None, iso_image)) for iso_image in iso_files_db]  # We get Tuple
                import itertools

                if iso_files_db[0][0]:

                    iso_files = iso_files_db[0][0].split(',')
                    index = 0
                    iso_files_info = dict()
                    # Create dictionary of ISO files
                    for iso_file in iso_files:
                        iso_files_info[index] = iso_file
                        iso_files_arr.append(iso_files_info)
                        iso_files_info = dict()
                        index += 1

                host_details['iso_files'] = iso_files_arr

            except Exception, exception:
                print exception

            try:

                datastores_arr = []
                # =========================================================
                # Get Datastore list
                # =========================================================
                datastore_listdb = db.session.query(Model.Server.datastores_path).filter(
                    Model.Server.id == int(host_id)).all()
                datastore_listdb = [tuple(filter(None, datastore)) for datastore in datastore_listdb]
                if len(datastore_listdb[0]) > 0:
                    datastore_list = datastore_listdb[0][0].split(',')
                    datastore_info = dict()
                    # Create dictionary of datastore files
                    for datastore in datastore_list:
                        datastore_details = datastore.split(":")
                        if len(datastore_details) == 2:
                            datastore_info[datastore_details[1]] = datastore_details[0]
                            datastores_arr.append(datastore_info)
                            datastore_info = dict()
                host_details['datastores'] = datastores_arr

            except Exception, exception:
                print exception

            try:

                network_arr = []
                # =========================================================
                # Get Datastore list
                # =========================================================
                network_listdb = db.session.query(Model.Server.networks).filter(Model.Server.id == int(host_id)).all()
                network_listdb = [tuple(filter(None, network)) for network in network_listdb]
                if len(network_listdb[0]) > 0:
                    network_list = network_listdb[0][0].split(',')
                    index = 0
                    network_info = dict()
                    # Create dictionary of network
                    for network in network_list:
                        network_info[index] = network
                        network_arr.append(network_info)
                        network_info = dict()
                        index += 1
                host_details['networks'] = network_arr

            except Exception, exception:
                print exception

            # Return
            return host_details

        else:
            return

    except Exception, exception:
        print exception
        return host.get_public()


def user_filter(request):
    """

    :param request:
    :return:
    """
    from conf import settings
    username = request.args.get('username')
    users = None

    if username:
        users = Model.Job.query.filter(Model.ApiUser.username == int(username)).limit(settings.items_per_page).all()

    return users


def job_filter(request):
    """

    :param request:
    :return:
    """
    from conf import settings
    owner_value = request.args.get('owner_id')
    status_value = request.args.get('status')
    jobs = None

    if owner_value and status_value:
        if status_value.isdigit() and owner_value.isdigit():
            jobs = Model.Job.query.filter(Model.Job.owner_id == int(owner_value),
                                          Model.Job.status == int(status_value)).limit(settings.items_per_page).all()
        return jobs

    if owner_value:
        if owner_value.isdigit():
            jobs = Model.Job.query.filter(Model.Job.owner_id == int(owner_value)).limit(settings.items_per_page).all()

    if status_value:
        if status_value.isdigit():
            jobs = Model.Job.query.filter(Model.Job.status == int(status_value)).limit(settings.items_per_page).all()

    return jobs


def vm_filter(request, id):
    """

    :param request:
    :return:
    """
    hostname_value = request.args.get('name')
    app_type_value = request.args.get('type')
    virtual_machines = None
    # http://stackoverflow.com/questions/3050518/what-http-status-response-code-should-i-use-if-the-request-is-missing-a-required

    if hostname_value and app_type_value:
        if app_type_value.isdigit():
            virtual_machines = Model.Virtualmachine.query.filter(
                Model.Virtualmachine.hostname.like('%' + hostname_value + '%'),
                Model.Virtualmachine.app_id == app_type_value,
                Model.Virtualmachine.fk_server == id).all()
        return virtual_machines

    if hostname_value:
        virtual_machines = Model.Virtualmachine.query.filter(
            Model.Virtualmachine.hostname.like('%' + hostname_value + '%'),
            Model.Virtualmachine.fk_server == id).all()

    if app_type_value:
        if app_type_value.isdigit():
            virtual_machines = Model.Virtualmachine.query.filter(Model.Virtualmachine.app_id == app_type_value,
                                                                 Model.Virtualmachine.fk_server == id).all()

    return virtual_machines


def server_filter(request):
    """

    :param request:
    :return:
    """
    name_value = request.args.get('name')
    status_value = request.args.get('status')
    owner_value = request.args.get('owner_id')

    servers = None
    # http://stackoverflow.com/questions/3050518/what-http-status-response-code-should-i-use-if-the-request-is-missing-a-required

    if name_value and status_value and owner_value:
        if status_value.isdigit() and owner_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.name.like('%' + name_value + '%'),
                                                Model.Server.status == int(status_value),
                                                Model.Server.owner_id == int(owner_value)).all()
        return servers

    if name_value and status_value:
        if status_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.name.like('%' + name_value + '%'),
                                                Model.Server.status == int(status_value)).all()
        return servers

    if owner_value and status_value:
        if status_value.isdigit() and owner_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.owner_id == int(owner_value),
                                                Model.Server.status == int(status_value)).all()
        return servers

    if name_value and owner_value:
        if owner_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.name.like('%' + name_value + '%'),
                                                Model.Server.owner_id == int(owner_value)).all()
        return servers

    if owner_value:
        if owner_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.owner_id == int(owner_value)).all()

    if status_value:
        if status_value.isdigit():
            servers = Model.Server.query.filter(Model.Server.status == int(status_value)).all()

    if name_value:
        servers = Model.Server.query.filter(Model.Server.name.like('%' + name_value + '%')).all()

    return servers
