__author__ = 'gogasca'

from twisted.internet import reactor

from database.asyncnotify import AsyncNotify


# #database_init(host='localhost',dbname='template1',user='gogasca')

def errorHandler(error):
    print str(error)
    reactor.stop()
    #notifier.stop()


def shutdown(notifier):
    print 'Shutting down the reactor'
    reactor.stop()


def tableUpdated(notify, pid):
    # This function is called by magic.
    tablename, op = notify.split('_')
    from time import gmtime, strftime

    print str(strftime("%Y-%m-%d %H:%M:%S", gmtime())) + ' %s just occured on %s table from process ID %s' % (
    op, tablename, pid)
    # Select operation and proceed
    if op == 'insert':
        pass
    elif op == 'update':
        pass
    elif op == 'delete':
        pass
    else:
        raise


class myAsyncNotify(AsyncNotify):
    # gotNotify is called with the notification and pid.
    # Override it and do something great.
    def gotNotify(self, pid, notify):
        if notify == 'quit':
            # The stop method will end the deferred thread
            # which is listening for notifications.
            print 'Stopping the listener thread.'
            self.stop()
        elif notify.split('_')[0] in ('server'):
            tableUpdated(notify, pid)
        else:
            print "got asynchronous notification '%s' from process id '%s'" % (notify, pid)


def initialize(dsn=None, **kwargs):
    try:
        from conf import settings
        host = settings.dbhost
        dbname = settings.dbname
        user = settings.dbusername
        password = settings.dbpassword

        dsn = 'dbname=%s host=%s user=%s password=%s' % (dbname, host, user, password)
        print 'Creating notifier'
        notifier = myAsyncNotify(dsn)
        # Start listening for subscribed notifications in a deferred thread.
        listener = notifier.run()

        # What to do when the AsyncNotify stop method is called to
        listener.addCallback(shutdown)
        listener.addErrback(errorHandler)

        # Call the gotNotify method when any of the following notifies are detected.
        #notifier.addNotify('test1')
        #notifier.addNotify('test2')
        #notifier.addNotify('mytestdb')
        notifier.addNotify('server_insert')
        notifier.addNotify('server_update')
        notifier.addNotify('server_delete')
        #notifier.addNotify('quit')

        # Unsubscribe from one
        """print 'reactor.callLater()'
        _delayobj = reactor.callLater(15, notifier.removeNotify,'test2')"""
        print 'reactor.run()'
        #reactor.run()
        import threading

        threading.Thread(target=reactor.run, args=(False,)).start()

    except Exception, e:
        print str(e)


if __name__ == '__main__':
    initialize()
    pass
