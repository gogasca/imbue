__author__ = 'gogasca'
import logging
import os
import datastore_operations as datastore
from hypervisor.esxi import general
from utils import validator
from error import exceptions
from conf import settings
from conf import logging_conf
from utils import db_utils

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def get_image_folder(host_id):
    """

    :param configuration:
    :return:
    """
    try:
        if host_id:
            sqlquery = """SELECT image_folder FROM server WHERE server.id=""" + str(host_id)
            image_folder = db_utils.execute_query(sqlquery)
            image_folder = str(list(image_folder[0])[0])
            if image_folder and image_folder != 'None':
                return image_folder

        return settings.image_folder

    except Exception, exception:
        log.exception(exception)


def get_install_folder(host_id):
    """

    :param configuration:
    :return:
    """
    try:
        if host_id:
            sqlquery = """SELECT install_folder FROM server WHERE server.id=""" + str(host_id)
            install_folder = db_utils.execute_query(sqlquery)
            install_folder = str(list(install_folder[0])[0])
            if install_folder and install_folder != 'None':
                return install_folder

        return settings.install_folder

    except Exception, exception:
        log.exception(exception)


def check_remote_file_exists(server=None,
                             username=None,
                             password=None,
                             port=None,
                             fileName=None,
                             softlink=False,
                             private_key=None,
                             **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param fileName:
    :param kwargs:
    :return:
    """
    try:
        log.info('check_remote_file_exists() Checking file: ' + fileName)
        if fileName is None or fileName == '':
            log.exception('check_remote_file_exists() Invalid file name')
            raise exceptions.UndefinedDirectory('check_remote_file_exists() Empty file')

        if fileName and isinstance(fileName, (str, unicode)):
            pass
        else:
            raise exceptions.InvalidConfigurationFile(
                'check_remote_file_exists() Invalid file path' + fileName)

        log.info(
            'check_remote_file_exists() Connecting to server and checking file: ' + fileName)
        test = 'test -f '
        if softlink:
            test = 'test -L '
        res = general.send_command(commandList=[test + fileName + ' ; echo $? '],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True,
                                   timeout=20,
                                   private_key=private_key)  # Get directory output
        log.info('check_remote_file_exists() Result: ' + str(res))
        # res = True if directory exists. 1 directory doesnt exists
        if '0' in res[0].encode('utf-8'):
            log.info('check_remote_file_exists() File exists')
            return True
        else:
            log.error('check_remote_file_exists() File ' +
                      fileName + ' does not exist')
            return False

    except Exception, exception:
        log.exception(exception.__repr__())
        raise exceptions.InvalidConfigurationFile(
            'check_remote_file_exists() File ' + fileName + ' does not exist')


def check_local_file(filename=""):
    """

    :param filename:
    :return:
    """
    try:
        return os.path.isfile(filename)
    except Exception, exception:
        log.exception(exception.__repr__())
        return False


def check_file_system_access(server=None, username=None, password=None, port=None, directory='/', **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param directory:
    :param kwargs:
    :return:
    """
    try:
        log.info('check_file_system_access()')
        if validator.check_authentication_parameters(server=server, username=username, password=password, sshPort=port):
            pass
        else:
            raise exceptions.ConnectivityError(
                'check_file_system_access() Invalid network parameters')
            return False

        log.info(
            'check_remote_directory_exists() Connecting to server and checking directory: ' + directory)
        res = general.send_command(commandList=['ls -alh; echo $?'],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)  # Get directory output

        # res = True if directory exists. 1 directory doesnt exists
        if res:
            log.info('check_file_system_access() Access succeeded')
            return True
        else:
            log.error('check_file_system_access() Access failed')
            return False

    except Exception, exception:
        log.exception(exception.__repr__())


def check_remote_directory_exists(server=None, username=None, password=None, port=None, directory='/', **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param directory:
    :param kwargs:
    :return:
    """
    try:
        log.info('check_remote_directory_exists()')
        if validator.check_authentication_parameters(server=server, username=username, password=password, sshPort=port):
            pass
        else:
            raise exceptions.ConnectivityError(
                'check_remote_directory_exists() Invalid network parameters')

        log.info('check_remote_directory_exists() Checking directory: ' + directory)

        if directory == None or directory == '':
            log.exception(
                'check_remote_directory_exists() Invalid directory name')
            raise exceptions.UndefinedDirectory(
                'check_remote_directory_exists() Empty directory')

        if directory and isinstance(directory, (str, unicode)):
            pass
        else:
            raise exceptions.InvalidDirectoryPath(
                'check_remote_directory_exists() Invalid directory path' + directory)

        log.info(
            'check_remote_directory_exists() Connecting to server and checking directory: ' + directory)
        res = general.send_command(commandList=['test -d ' + directory + ' ; echo $? '],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)  # Get directory output

        # res = True if directory exists. 1 directory doesnt exists
        if '0' in res[0].encode('utf-8'):
            log.info('check_remote_directory_exists() Directory exists')
            return True
        else:
            log.warn('check_remote_directory_exists() Directory ' +
                     directory + ' does not exist')
            return False

    except Exception, exception:
        log.exception(exception.__repr__())


# Check directory name for customer
def check_directory_name_syntax(directoryPath):
    """

    :param directoryPath:
    :return:
    """
    if directoryPath == None or directoryPath == '':
        log.error('check_directory_name_syntax() Invalid customer name')
        raise exceptions.UndefinedDirectory(
            'check_directory_name_syntax() Empty directory')

    # /cloudtree/uc/ct-east-ny-pub1/
    if directoryPath and isinstance(directoryPath, (str, unicode)):
        if directoryPath.endswith('/'):
            directoryPath[:-1]
        if directoryPath.startswith('/'):
            directoryPath[1:]
        log.info("check_directory_name_syntax() Directory: " + directoryPath)
        return True
    else:
        raise exceptions.InvalidDirectoryPath('Invalid directory path' + directoryPath)


# This function deletes the directory layout required for server
def delete_directory(server=None, username=None, password=None, sshPort=None, directory=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param httpsPort:
    :param datastoreName:
    :param directory:
    :param kwargs:
    :return:
    """
    try:
        log.info('delete_directory()')

        # Validate Directory syntax.
        if check_directory_name_syntax(directory):
            pass
        else:
            raise exceptions.InvalidDirectoryPath(
                'delete_directory() Invalid customer directory')

        log.info('delete_directory() Deleting directory')
        # Check if remote directory exists
        directory_exists = check_remote_directory_exists(directory=directory,
                                                         server=server,
                                                         username=username,
                                                         password=password,
                                                         port=sshPort)

        # ************* Validate directory exists after being created. ********
        # If directory doesnt exist, create a new one, otherwise exit with
        # error
        if directory_exists:
            log.info(
                'delete_directory() Deleting directory: ' + directory)
            general.send_command(commandList=['rm -rf ' + directory],
                                 server=server,
                                 username=username,
                                 password=password,
                                 port=sshPort)
        else:
            raise exceptions.InvalidDirectoryPath('delete_directory() Directory does not exist')

        # ************* Directory was created. Validate directory no longer exists
        directory_exists = check_remote_directory_exists(directory=directory,
                                                         server=server,
                                                         username=username,
                                                         password=password,
                                                         port=sshPort)

        # Directory shouldnt exist anymore
        if directory_exists:
            log.error('delete_directory() Directory deleted failed: ' + directory)
            return False
        else:
            log.info('delete_directory() Directory deleted successfully:')
            return True

    except Exception, exception:
        log.exception(exception.__repr__())


# This function creates the directory layout required for server
def create_directory_layout(server=None, username=None, password=None, sshPort=None, httpsPort=None, datastoreName=None,
                            directory=None, overwrite=False, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param sshPort:
    :param httpsPort:
    :param datastoreName:
    :param directory:
    :param kwargs:
    :return:
    """
    try:
        log.info('create_directory_layout()')
        if validator.check_authentication_parameters(server=server, username=username, password=password,
                                                     sshPort=sshPort,
                                                     httpsPort=httpsPort):
            pass
        else:
            raise exceptions.ConnectivityError(
                'create_directory_layout() Invalid network parameters')

        # Validate Directory syntax.
        if check_directory_name_syntax(directory):
            pass
        else:
            raise exceptions.InvalidDirectoryPath(
                'create_directory_layout() Invalid customer directory')

        # Check valid datastore
        if datastoreName == None or datastoreName == '':
            log.error('create_directory_layout() Invalid datastore')
            return False

        # SSL warning
        import ssl
        if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context

        # Validate datastore exists uses vsphere Port
        if datastore.find_datastore_list(server=server,
                                         username=username,
                                         password=password,
                                         httpsPort=httpsPort,
                                         datastoreName=datastoreName):

            log.info('create_directory_layout() Datastore found proceeding...')

        else:
            raise exceptions.UndefinedDataStore(
                'create_directory_layout() Undefined datastore')

        log.info('create_directory_layout() Creating layout...')
        # Check if remote directory exists
        directoryExists = check_remote_directory_exists(directory=directory,
                                                        server=server,
                                                        username=username,
                                                        password=password,
                                                        port=sshPort)

        # ************* Validate directory exists after being created. ********
        # If directory doesnt exist, create a new one, otherwise exit with
        # error
        if not directoryExists:
            log.info(
                'create_directory_layout() New directory. Trying to create remote directory remotely: ' + directory)
            general.send_command(commandList=['mkdir -p ' + directory],
                                 server=server,
                                 username=username,
                                 password=password,
                                 port=sshPort,
                                 close=True)
        else:
            raise exceptions.InvalidDirectoryPath(
                'create_directory_layout() Directory already exists')

        # ************* Directory was created. Validate new directory exists no
        directoryExists = check_remote_directory_exists(directory=directory,
                                                        server=server,
                                                        username=username,
                                                        password=password,
                                                        port=sshPort)

        if directoryExists:
            log.info(
                'create_directory_layout() Directory created successfully: ' + directory)
            return True
        else:
            raise exceptions.CreateDirectoryFailure(
                'create_directory_layout() Unable to create directory')

    except Exception, exception:
        log.exception(exception.__repr__())


# We create the floppy drive in the remote tools server, we invoke the create_floppy.sh script which creates images
# using mkfs command

def create_floppy(server=None, username=None, password=None, port=None, floppyImageDir='', floppyImageName='',
                  answerFile='', clusterFile='', **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param floppyImageDir:
    :param floppyImageName:
    :param answerFile:
    :param kwargs:
    :return:
    """

    # Verify all parameters are populated or valid.
    # This is the Tools Application server!
    if validator.check_authentication_parameters(server=server, username=username, password=password, sshPort=port):
        pass
    else:
        raise exceptions.ConnectivityError(
            'create_floppy() Invalid network parameters')

    if floppyImageDir == None or floppyImageDir == '':
        log.error('create_floppy() Invalid imageDir name')
        return False

    if floppyImageName == None or floppyImageName == '':
        log.error('create_floppy() Invalid floppyImage name')
        return False

    if answerFile == None or answerFile == '':
        log.error('create_floppy() Invalid answerFile name')
        return False

    log.info("create_floppy() Creating floppy drive image...")
    log.info("create_floppy() Floppy drive parameters: ")
    # ./create_floppy.sh -d /usr/local/src/utils/ -f floppy.img -p /usr/local/src/utils/platformConfig.xml
    log.info('create_floppy() Floppy Script:           ' +
             settings.floppy_script)
    log.info('create_floppy() Floppy Image Directory:  ' + floppyImageDir)
    log.info('create_floppy() Floppy Image Name:       ' + floppyImageName)
    log.info('create_floppy() Answer file:             ' + answerFile)
    log.info('create_floppy() Cluster file:            ' + clusterFile)
    try:

        if clusterFile:
            res = general.send_command(commandList=[settings.floppy_script + ' -d ' + floppyImageDir + ' -f ' +
                                                    floppyImageName + ' -c ' + answerFile + ',' + clusterFile],
                                       server=server,
                                       username=username,
                                       password=password,
                                       port=port,
                                       display=True)
        else:
            res = general.send_command(commandList=[settings.floppy_script + ' -d ' + floppyImageDir + ' -f ' +
                                                    floppyImageName + ' -p ' + answerFile],
                                       server=server,
                                       username=username,
                                       password=password,
                                       port=port,
                                       display=True)

        log.info('create_floppy() Remote command response: ' + str(res))
        if res:
            if 'Floppy image created succesfully\n' in res:
                log.info('create_floppy() Floppy drive image created successfully')
                return True
            else:
                log.error('create_floppy() Unable to create floppy drive')
                return False
        else:
            log.exception('create_floppy() Command failed')
            return False

    except Exception, exception:
        log.exception(exception.__repr__())


# Validates default datastore is defined in Datastore list
def get_default_datastore(datastore_list, datastore=None, **kwargs):
    """

    :param datastore_list:
    :param datastore:
    :param kwargs:
    :return:
    """
    default_datastore_name = None
    # Check there is a default Datastore defined in parameters

    try:
        if datastore:
            log.info('get_default_datastore() Datastore defined')
            default_datastore_name = datastore
        else:
            if hasattr(settings, 'default_datastore'):
                log.warn('get_default_datastore() Using default datastore')
                default_datastore_name = settings.default_datastore
                # Check its string
                if not isinstance(default_datastore_name, (str, unicode)):
                    log.exception(
                        'get_default_datastore() No default Datastore defined')
                    return None
            else:
                log.exception(
                    'get_default_datastore() No default Datastore defined')
                raise RuntimeError

        log.info('get_default_datastore() Verifying selected datastore exists in datastore list:')
        for datastore_item in datastore_list:
            if datastore_item == default_datastore_name:
                return datastore_item.lower()

        # Not default datastore defined
        if not default_datastore_name:
            raise exceptions.UndefinedDataStore('Default datastore not found in ESXi')

    except Exception, exception:
        log.exception(exception.__repr__())


def create_harddisk(server=None, username=None, password=None, port=None, remoteName=None, size=None,
                    diskformat=settings.thickProvisioning, adaptertype='lsilogic', overwrite=False, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param remoteName:
    :param size:
    :param diskformat:
    :param adaptertype:
    :param overwrite:
    :param kwargs:
    :return:
    """
    # vmkfstools -c 80G -d zeroedthick -a lsilogic cupertino_ucm_publisher.vmdk
    try:
        res = general.send_command(commandList=['vmkfstools -c ' + size + 'G -d ' + diskformat + ' -a ' +
                                                adaptertype + ' ' + remoteName],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True,
                                   close=True)

        log.info('create_harddisk() ' + str(res))
        # Command returns None
        if res:
            log.info('create_harddisk() VMK disk created successfully.')
            return True
        else:
            raise exceptions.CreateVmkException('create_harddisk() VMK disk failed')

    except Exception, exception:
        log.exception(exception.__repr__())
