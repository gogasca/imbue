# -*- coding: utf-8 -*-
import json
from error.exceptions import InvalidCredentials, DiscoveryException
import warnings

warnings.filterwarnings("ignore")
import logging
import psycopg2
from celery.task import task
from conf import logging_conf
from conf import settings
from hypervisor.esxi.file_operations import get_image_folder
from hypervisor.esxi.file_operations import get_install_folder
from server import Server
from shared import Report
from utils import helper
from utils import db_utils
from utils import validator


@task(bind=True, default_retry_delay=15, max_retries=3)
def imbue_sync(self,
               job=None,
               host_id=None,
               force=False,
               reset=False,
               log_file=""):
    # =========================================================
    # Process ESXi server
    # =========================================================

    log = logging_conf.LoggerManager().getLogger("__app___", logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '---------------------------------------Initializing IMBUE Synchronize---------------------------------------')
    try:
        error_detected = False
        if host_id:
            if job.id:
                log.info('imbue_sync() Updating host infrastructure')
                # =========================================================
                # Update job end time
                # =========================================================
                job.update_info(host_id, None)
                owner_id = helper.get_owner_id(host_id=host_id)
                server_json = json.dumps(job.server_info)
                if owner_id and server_json:
                    log.info('imbue_sync() Updating job information')
                    sqlquery = """UPDATE job SET status=0, owner_id=""" + str(owner_id) + \
                               """,description='Server synchronization started', host_information='""" + \
                               server_json + """' WHERE job.id=""" + str(job.id)
                    db_utils.update_database(sqlquery)
                else:
                    log.error('imbue_sync() Job information is missing..Please continue')

                log.info('imbue_sync() Server synchronization started')

            esx_instance = helper.get_server_instance(host_id)

            if esx_instance.synchronize(force=force, reset=reset):
                log.info('imbue_sync() Server synchronization was successful')
            else:
                log.info('imbue_sync() Server synchronization failed')
                error_detected = True
        else:
            error_detected = True

    except AttributeError as exception:
        log.exception('imbue_sync() Attribute Error {}'.format(exception))
        error_detected = True

    except Exception as exception:
        log.exception('imbue_sync() Exception Error {}'.format(exception))
        error_detected = True

    finally:

        # =========================================================
        # Update job end time
        # =========================================================
        log.info('imbue_sync() Job: ' + job.reference + ' has been completed')

        if error_detected:
            log.warn('imbue_sync() Error detected')
            if job.id:
                sqlquery = """UPDATE job SET status='-1',description='Server synchronization failed', job_end='now()' WHERE job.id=""" + \
                           str(job.id)
            self.update_state(state='FAILURE', meta={
                'status': 'Failure detected'})
            message = 'ERROR Server synchronization failed'
            result = -1
        else:
            if job.id:
                sqlquery = """UPDATE job SET status='1',description='Server synchronization completed', job_end='now()' WHERE job.id=""" + \
                           str(job.id)
            message = 'Server synchronization completed'
            result = 1

        db_utils.update_database(sqlquery)
        return {'status': message, 'result': result}


@task(bind=True, default_retry_delay=300, max_retries=3)
def imbue_esx(self,
              configuration={},
              job=None,
              log_file="",
              **kwargs):
    # =========================================================
    # Process ESXi server
    # =========================================================

    log = logging_conf.LoggerManager().getLogger("__app___", logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '-------------------------------------Initializing IMBUE ESXi deployment-------------------------------------')

    try:

        # =========================================================
        # API Request. Validated in API server
        # =========================================================

        log.info('deploy_esx() API call request: ' + str(configuration))

        error_detected = False
        log.info('deploy_esx() Job reference: {0}'.format(job.reference))
        report = Report.Report(settings.report_path, job.reference)
        if report.initialize():
            log.info('deploy_esx() Report initialized successfully')
        else:
            log.error('deploy_esx() Report initialized failed')

        if job.id:
            sqlquery = """UPDATE job SET status=0,description='Initializing Server' WHERE job.id=""" + \
                       str(job.id)
            db_utils.update_database(sqlquery)
        else:
            error_detected = True
            log.error('deploy_esx() Invalid job information')

        # =========================================================
        # GET SUPPORTED Flags
        # =========================================================

        log.info("deploy_esx() Reading configuration flags")

        # "owner_id": n
        if validator.check_parameter_flag(configuration, 'owner_id'):
            owner_id = configuration['host']['hypervisor']['owner_id']
            sqlquery = """UPDATE job SET owner_id=""" + str(owner_id) + """ WHERE job.id=""" + str(job.id)
            db_utils.update_database(sqlquery)
            log.info('deploy_esx() owner_id parameter: {0}'.format(owner_id))
        else:
            owner_id = None

        # "discover":true
        execute_discovery = validator.check_discover_flag(configuration)
        log.info('deploy_esx() discover parameter: {0}'.format(str(execute_discovery)))

        # "sync": true
        sync = validator.check_parameter_flag(configuration, 'sync')
        log.info('deploy_esx() sync parameter: {0}'.format(str(sync)))

        # =========================================================
        # Define HYPERVISOR type support
        # =========================================================

        if configuration['host']['hypervisor']['type'] == settings.esx_vendor_id:
            log.info('deploy_esx() |ESXI| Host information: {0}'.format(configuration['host']['hypervisor']))

        else:
            error_detected = True
            message = 'ERROR Invalid Hypervisor configuration.'
            if report:
                report.add_log(message)
            log.exception('deploy_esx() ' + message)

            # =========================================================
            # Send ALERT
            # =========================================================
            self.update_state(state='FAILURE', meta={'status': message})
            helper.generate_alert(job.id, message, job.reference)
            return

        try:

            # =========================================================
            # Process ESXi server name
            # =========================================================

            esx_name = configuration['host']['hypervisor']['name']

            if esx_name:  # If esx_name is defined
                # Create instance
                log.info('deploy_esx()  Initializing Server: {0}'.format(esx_name))
                esx1 = Server.Server(name=esx_name)
            else:
                error_detected = True
                message = 'ERROR Invalid Hypervisor name'
                if report:
                    report.add_log(message)
                log.error('deploy_esx() ' + message)
                # =========================================================
                # Send ALERT
                # =========================================================
                self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)
                return

        except KeyError:
            log.warn('deploy_esx()  Server name not found. Reading settings file. Default server name')
            if settings.esx_:
                log.warn('deploy_esx() Using default server name')
                esx_name = settings.esx_
                esx1 = Server.Server(name=esx_name)
            else:
                error_detected = True
                message = 'ERROR Server name not found'
                if report:
                    report.add_log(message)
                log.error('deploy_esx() ' + message)

                # =========================================================
                # Send ALERT
                # =========================================================
                self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)
                return

        except Exception as exception:
            log.exception(exception.__repr__())

        # =========================================================
        #  Initialize server. Creates object and assign properties
        # =========================================================

        if esx_name:

            # =========================================================
            # ESX name is defined JSON Post. Check DB.
            # =========================================================
            try:

                if esx1.get_server(name=esx_name, owner_id=owner_id):

                    # =========================================================
                    # EXISTING SERVER ! UPDATE ESX in database
                    # =========================================================

                    log.warn('deploy_esx() Server already in database: ' + esx_name)
                    # If server already in database update it with current
                    # YAML/JSON information
                    if validator.check_host_config(configuration):
                        log.info('deploy_esx() Server already in database: ' + esx_name + ' updating information()')

                        if esx1.update(id=esx1.id, configuration=configuration):
                            message = 'Server updated successfully ' + esx_name
                            if report:
                                report.add_log(message)
                        else:
                            raise RuntimeError

                    else:
                        error_detected = True
                        message = 'ERROR Server configuration file error'
                        if report:
                            report.add_log(message)
                        log.error('deploy_esx() ' + message)

                        # =========================================================
                        # Send ALERT
                        # =========================================================
                        self.update_state(state='FAILURE', meta={
                            'status': message})
                        helper.generate_alert(job.id, message, job.reference)
                        raise RuntimeError
                else:

                    # =========================================================
                    # NEW SERVER ! Insert ESX in database
                    # =========================================================

                    message = 'New server will be inserted: ' + esx_name
                    log.info('deploy_esx() ' + message)
                    if report:
                        report.add_log(message)
                    if configuration['host']['hypervisor'].has_key('image_folder'):
                        image_folder = configuration['host']['hypervisor']['image_folder']
                    else:
                        image_folder = None

                    # Insert into Database
                    esx1.insert(
                        ip_address=configuration['host']['hypervisor']['ip'],
                        name=configuration['host']['hypervisor']['name'],
                        interface_name='eth0',
                        description=configuration['host']['hypervisor']['description'],
                        properties='{"ssh": { "active": 1, "port": ' +
                                   str(
                                       configuration['host']['hypervisor']['ssh']) +
                                   ' }, "https": { "active": 1, "port": ' +
                                   str(
                                       configuration['host']['hypervisor']['https']) +
                                   ' }, "vnc": { "active": 0, "port": ' +
                                   '"' +
                                   str(
                                       configuration['host']['hypervisor']['vnc']) +
                                   '"' +
                                   ' }}',
                        ssh_username=configuration['host']['hypervisor']['username'],
                        ssh_password=configuration['host']['hypervisor']['password'],
                        image_folder=image_folder)

                    message = 'Server info | Name: ' + configuration['host']['hypervisor']['name'] + \
                              ' IP: ' + configuration['host']['hypervisor']['ip'] + \
                              ' SSH: ' + str(configuration['host']['hypervisor']['ssh']) + \
                              ' HTTPS: ' + str(configuration['host']['hypervisor']['ip']) + \
                              ' Monitor: ' + str(configuration['host']['hypervisor']['vnc'])
                    if report:
                        report.add_log(message)
            except psycopg2.OperationalError, excpt:
                log.exception(excpt)
                error_detected = True
                return False

        else:
            error_detected = True
            message = 'ERROR Server invalid name'
            if report:
                report.add_log(message)
            log.exception('deploy_esx() ' + message)

            # =========================================================
            # Send ALERT
            # =========================================================
            self.update_state(state='FAILURE', meta={'status': message})
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Find user_id and insert it in database
        # =========================================================
        try:

            if job:
                log.info('deploy_esx() Updating host infrastructure')
                job.update_info(esx1.id, None)
                server_json = json.dumps(job.server_info)

            else:
                server_json = '{}'
                log.warn('deploy_esx() Job server info not defined')

            if owner_id:

                res = esx1.update_user_id(
                    configuration['host']['hypervisor']['owner_id'])
                if res:

                    # =========================================================
                    # Add job id to Database
                    # =========================================================

                    sqlquery = """UPDATE job SET owner_id=""" + str(owner_id) + \
                               """, host_information='""" + server_json + """' WHERE job.id=""" + str(job.id)

                    db_utils.update_database(sqlquery)
                    log.info('deploy_esx() ESXi added owner_id and server_information successfully')

                else:
                    log.error('deploy_esx() ESXi unable to update owner_id')
            else:
                log.error('deploy_esx() owner_id is not defined')
                sqlquery = """UPDATE job SET host_information='""" + \
                           server_json + """' WHERE job.id=""" + str(job.id)
                db_utils.update_database(sqlquery)
                log.warn('deploy_esx() Job added server_information')
                return

        except Exception as exception:
            log.exception(exception.__repr__())
            return

        # =========================================================
        # Initialize server Instance.
        # =========================================================

        if esx1.initialize(name=esx_name, owner_id=owner_id):

            try:
                # =========================================================
                # Discover server
                # =========================================================

                if execute_discovery:

                    log.info('deploy_esx() Server discovery starting...')
                    res = esx1.discover(init=True, get_status_code=True)
                    if res:
                        # =========================================================
                        #  Discover was successful
                        # =========================================================
                        sqlquery = """UPDATE server SET discovered=True WHERE server.id=""" + str(esx1.id)
                        db_utils.update_database(sqlquery)
                        sqlquery = """UPDATE server SET status=0 WHERE server.id=""" + str(esx1.id)
                        db_utils.update_database(sqlquery)

                    else:
                        error_detected = True
                        message = 'ERROR Server discovery failed.'
                        if report:
                            report.add_log(message)
                        log.error('deploy_esx() ' + message)

                        # =========================================================
                        # Send ALERT
                        # =========================================================
                        self.update_state(state='FAILURE', meta={
                            'status': message})
                        helper.generate_alert(job.id, message, job.reference)
                        sqlquery = """UPDATE server SET discovered=False WHERE server.id=""" + str(esx1.id)
                        db_utils.update_database(sqlquery)
                        sqlquery = """UPDATE server SET status=-1 WHERE server.id=""" + str(esx1.id)
                        db_utils.update_database(sqlquery)
                        return

            except InvalidCredentials:
                error_detected = True
                message = 'ERROR Server discovery failed. Invalid credentials'
                if report:
                    report.add_log(message)
                log.exception('deploy_esx() ' + message)

                # =========================================================
                # Send ALERT
                # =========================================================
                self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)

                sqlquery = """UPDATE server SET discovered=False WHERE server.id=""" + str(esx1.id)
                db_utils.update_database(sqlquery)

                sqlquery = """UPDATE server SET status=-1 WHERE server.id=""" + str(esx1.id)
                db_utils.update_database(sqlquery)
                return

            except DiscoveryException:
                error_detected = True
                message = 'ERROR Server discovery failed. Please verify HTTPS/SSH ports'
                if report:
                    report.add_log(message)
                log.exception('deploy_esx() ' + message)

                # =========================================================
                # Send ALERT
                # =========================================================
                self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)

                sqlquery = """UPDATE server SET discovered=False WHERE server.id=""" + str(esx1.id)
                db_utils.update_database(sqlquery)

                sqlquery = """UPDATE server SET status=-1 WHERE server.id=""" + str(esx1.id)
                db_utils.update_database(sqlquery)
                return

            except Exception as exception:
                log.exception(exception.__repr__())

        else:
            error_detected = True
            message = 'ERROR Server initialization error'
            if report:
                report.add_log(message)
            log.exception('deploy_esx() ' + message)

            # =========================================================
            # Send ALERT
            # =========================================================
            self.update_state(state='FAILURE', meta={'status': message})
            helper.generate_alert(job.id, message, job.reference)
            return

        # =========================================================
        # Check if server was discovered
        # =========================================================
        if execute_discovery:

            if esx1.discovered:
                message = 'Server discovery was successful'
                log.info('deploy_esx() ' + message)

            else:
                message = 'Server not discovered'
                log.info('deploy_esx() ' + message)
                if report:
                    report.add_log(message)
                return

            sqlquery = """UPDATE job SET description='Server found' WHERE job.id=""" + \
                       str(job.id)
            db_utils.update_database(sqlquery)
            if report:
                report.add_log(message)
            esx1.read_storage()
            esx1.read_network_interfaces()

            # =========================================================
            # Define ISO/Image folder
            # =========================================================
            message = 'Getting Image folder settings...'
            if report:
                report.add_log(message)
            log.info('deploy_esx() ' + message)
            if configuration['host']['hypervisor'].has_key('image_folder'):
                image_folder = configuration['host']['hypervisor']['image_folder']
                log.info('deploy_esx() Server set image folder: {}'.format(image_folder))

            else:
                image_folder = get_image_folder(esx1.id)

            message = 'Image folder: ' + image_folder
            if report:
                report.add_log(message)
            esx1.set_image_folder(image_folder)
            esx1.get_image_files()  # Read ISO Files

            message = 'Getting Install folder settings...'
            if report:
                report.add_log(message)
            log.info('deploy_esx() ' + message)
            if configuration['host']['hypervisor'].has_key('install_folder'):
                install_folder = configuration['host']['hypervisor']['install_folder']
                log.info('deploy_esx() Server set install folder: {}'.format(install_folder))
            else:
                install_folder = get_install_folder(esx1.id)

            message = 'Install folder: ' + install_folder
            if report:
                report.add_log(message)
            esx1.set_install_folder(install_folder)

            message = 'Gathering datastore details'
            log.info('deploy_esx() ' + message)
            # =========================================================
            # Discover datastores from ESXi
            # =========================================================
            log.info(
                'deploy_esx() Valid ESX object. Gathering datastore details...')
            datastore_list = esx1.discover_datastores()

            if datastore_list:
                esx1.set_datastore(datastore_list)
                esx1.get_datastores_details()
                esx1.get_datastore_details(esx1.datastore)
                esx1.get_uuid()
                esx1.get_networks()
                esx1.get_datastores_path()

            else:
                message = 'ERROR No datastores found'
                if report:
                    report.add_log(message)
                log.error('deploy_esx() No datastores found')
                sqlquery = """UPDATE job SET description='No datastores found', job_end='now()' WHERE job.id=""" + \
                           str(job.id)
                db_utils.update_database(sqlquery)
                error_detected = True
                return False

            # =========================================================
            # Sync database between ESXi and PG.
            # =========================================================

            if sync:
                res = esx1.synchronize()
                if res:
                    message = 'Server synchronized successfully'
                    log.info('deploy_esx() ' + message)
                    if report:
                        report.add_log(message)
                    if esx1.get_version():
                        message = 'Server version: ' + esx1.get_version()
                        if report:
                            report.add_log(message)
                else:
                    message = 'Server synchronization failed'
                    log.error('deploy_esx() ' + message)
                    if report:
                        report.add_log(message)
                    sqlquery = """UPDATE job SET description='Server synchronization failed', job_end='now()' WHERE job.id=""" + \
                               str(job.id)
                    db_utils.update_database(sqlquery)
                    error_detected = True
            else:
                log.warn('deploy_esx() Sync is set to False')

        message = 'Server configuration completed'
        log.info('deploy_esx() ' + message)
        if report:
            report.add_log(message)
        return esx1

    except Exception as exception:
        log.exception(exception.__repr__())
        return

    finally:

        # =========================================================
        # Update job end time
        # =========================================================
        log.info('deploy_esx() Job ' + job.reference + ' has been completed')
        if report:
            report.add_log('Job ' + job.reference + ' has been completed')

        if error_detected:
            log.warn('deploy_uc() Error detected')
            sqlquery = """UPDATE job SET status='-1',description='Server configuration failed', job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            message = 'ERROR Server configuration failed'
            if report:
                report.add_log(message)
            self.update_state(state='FAILURE', meta={'status': message})
            result = -1
        else:
            sqlquery = """UPDATE job SET status='1',description='Server configuration completed successfully',job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            message = 'Server deployment completed successfully'
            if report:
                report.add_log(message)
            result = 1
        db_utils.update_database(sqlquery)
        if report:
            report.generate_report()

        return {'status': message, 'result': result}
