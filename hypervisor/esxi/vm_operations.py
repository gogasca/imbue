__author__ = 'gogasca'

import atexit
import json
import logging
import time
from celery.task import task
from conf import settings, logging_conf
from database.models import Model
from error import exceptions
from hypervisor.esxi import general, file_operations, template_handler
from pyVim import connect
from pyVmomi import vim
from shared import Lock
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from utils import helper
from utils import validator
from utils import display_utils
from utils import db_utils
from utils import task_utils

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


@task(bind=True, default_retry_delay=30, max_retries=3)
def cancel_job(self, job=None, **kwargs):
    """

    :param self:
    :param job:
    :param kwargs:
    :return:
    """
    try:

        # =========================================================
        # Cancel installation
        # =========================================================

        log.warn('cancel_job() Cancelling single install job: {}'.format(job.parent_reference))

        # =========================================================
        # Get server information
        # =========================================================

        host_info = helper.get_job_host_information(job.parent_reference)

        # =========================================================
        # Single Installation: {}, Server, Server VM
        # =========================================================

        if host_info and host_info != 'None':  # When there is Host information cancel task
            host_info_d = json.loads(host_info)  # Convert Host information string to JSON dictionary
            log.info('cancel_job() Host information found.')
        else:
            log.warn('cancel_job() No Host information found.')
            if self.request.id:

                # =========================================================
                # Revoke job. No Host. First seconds of installation
                # =========================================================

                res = task_utils.revoke_task(job=job)

                # =========================================================
                #  1: Task cancelled successfully
                # -1: Task cancellation failed
                # =========================================================

                if res == 1:
                    log.info('cancel_job() Re-read Host information')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    if host_info and host_info != 'None':  # Host information has changed...
                        log.warn('cancel_cluster_job() Host information has changed')
                        host_info_d = json.loads(host_info)  # Convert string to JSON dictionary
                    else:
                        log.info('cancel_job() Host information has not changed')
                        sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        db_utils.update_database(sqlquery)
                        log.info('cancel_job() Task was cancelled successfully')
                        if self.request.id:
                            return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                else:
                    log.error('cancel_job() Revoke_task failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}

        if isinstance(host_info_d, dict):

            log.info('cancel_job() Host information: {}'.format(host_info))
            host_info = helper.process_cluster_host_information(host_info_d)

            # =========================================================
            # Host information
            # We read host information from Parent job. Task can be in 2 different states:
            # STARTED: {}, {Host:[]}, {Host:[VM]}
            # =========================================================

            if len(host_info) == 1:  # {Host:[]}

                log.info('cancel_job() (1) Host: {}'.format(host_info[0]))
                # =========================================================
                # Revoke job. First stage of installation. VM not available yet...
                # =========================================================

                res = task_utils.revoke_task(job=job)

                if res == 1:
                    log.info('cancel_job() Task was cancelled successfully. Verifying host changes')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    host_info_d = json.loads(host_info)  # Convert string to JSON dictionary
                    host_info = helper.process_cluster_host_information(host_info_d)

                    if len(host_info) == 2:  # Host VM
                        log.warn('cancel_job() Host information has changed')
                    else:
                        log.info('cancel_job() Host information has not changed')
                        sqlquery = """UPDATE job SET status=1,
                        description='Installation cancelled',is_cancelled=true, job_end='now()' WHERE job.id=""" \
                                   + str(job.parent_id)
                        db_utils.update_database(sqlquery)
                        if self.request.id:
                            delete_directory(host_info[0], job)
                            # Update Server status
                            lock = Lock.Lock()
                            lock.id = host_info[0]
                            lock.release()
                            return {'status': 'OK: Job was cancelled successfully', 'result': 1}

                else:
                    log.error('cancel_job() Revoke_task failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

            if len(host_info) == 2:  # Host VM
                log.info('cancel_job() (2) Host: {} VM: {}'.format(host_info[0], host_info[1]))

                # =========================================================
                # Revoke job
                # =========================================================

                res = task_utils.revoke_task(job=job)
                if res == 1:
                    log.info('cancel_job() Job was cancelled successfully. Now cleaning up VM')
                else:
                    log.error('cancel_job() Revoke_task failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}

                # =========================================================
                # Clean VM
                # =========================================================

                res = remove_vm(host_id=host_info[0], vm_id=host_info[1], job=job)
                if res:
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        db_utils.update_database(sqlquery)
                        if self.request.id:
                            delete_directory(host_info[0], job)
                            # Update Server status
                            lock = Lock.Lock()
                            lock.id = host_info[0]
                            lock.release()
                            return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                else:
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

            log.error('cancel_job() Invalid host information')
            sqlquery = """UPDATE job SET status=-1, description='Job cancel failed' WHERE job.id=""" + str(
                job.id)
            helper.update_database(sqlquery)
            sqlquery = """UPDATE job SET status=-1, description='Job cancel failed' WHERE job.id=""" + str(
                job.parent_id)
            helper.update_database(sqlquery)
            if self.request.id:
                return {'status': 'WARN: Invalid host information', 'result': -1}

        else:
            log.error('cancel_job() Invalid data from database')
            if self.request.id:
                return {'status': 'WARN: Invalid data from database', 'result': -1}
            return False

    except ValueError:
        log.exception('cancel_job() Unable to decode JSON object ')
        return False

    except Exception, exception:
        log.exception('cancel_job( {0}'.format(exception))

    finally:
        if job.id:
            sqlquery = """UPDATE job SET status=1, description='Job child cancel request completed', is_cancelled='false', job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            helper.update_database(sqlquery)


@task(bind=True, default_retry_delay=60, max_retries=3)
def cancel_cluster_job(self, job=None, **kwargs):
    """
    Read Host information from job.
    Invoke task_util to revoke Celery task
    Check status and re-reread host information in case job is in progress.
    Check for New host and new vm along this process.

    :param self:
    :param job:
    :param kwargs:
    :return:
    """
    try:

        # =========================================================
        # Cancel installation
        # =========================================================

        log.warn('cancel_cluster_job() Cancelling cluster install job: {}'.format(job.parent_reference))

        # =========================================================
        # Get server information
        # =========================================================

        host_info = helper.get_job_host_information(job.parent_reference)

        # =========================================================
        # Cluster Installation: {}, Server, Server VM
        # =========================================================

        if host_info and host_info != 'None':
            host_info_d = json.loads(host_info)  # Convert Host information string to JSON dictionary
            log.info('cancel_cluster_job() Host information found.')
        else:

            log.warn('cancel_cluster_job() No Host information found.')
            if self.request.id:

                # =========================================================
                # Revoke job. No Host. First seconds of installation
                # =========================================================

                res = task_utils.revoke_task(job=job)

                # =========================================================
                #  1: Task cancelled successfully
                # -1: Task cancellation failed
                # =========================================================

                if res == 1:
                    log.info('cancel_cluster_job() Re-read Host information')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    if host_info and host_info != 'None':
                        log.warn('cancel_cluster_job() (0) Host information has changed')
                        host_info_d = json.loads(host_info)
                    else:
                        log.info('cancel_cluster_job() Host information has not changed')
                    log.info('cancel_cluster_job() Task was cancelled successfully')
                    sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                               str(job.parent_id)
                    db_utils.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                else:
                    log.error('cancel_cluster_job() Task cancellation failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}

        if isinstance(host_info_d, dict):

            log.info('cancel_cluster_job() Host information: {}'.format(host_info))
            host_info = helper.process_cluster_host_information(host_info_d)

            # =========================================================
            # Host information
            # We read host information from Parent job. Task can be in 4 different states:
            # STARTED: {Host:[]}, {Host:[VM]}, {Host:[VM, VM]} {Host:[VM] {Host:[VM]}}
            # =========================================================

            if len(host_info) == 1:  # {Host:[]}

                log.info('cancel_cluster_job() (1) Host: {}'.format(host_info[0]))

                # =========================================================
                # Revoke job. First stage of installation. VM not available yet...
                # =========================================================

                res = task_utils.revoke_task(job=job)

                if res == 1:
                    log.info('cancel_cluster_job() Task was cancelled successfully. Verifying host changes')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    host_info_d = json.loads(host_info)
                    host_info = helper.process_cluster_host_information(host_info_d)

                    if len(host_info) == 2:  # { Host:[VM] }
                        log.warn('cancel_cluster_job() (2) Host information has changed')
                    else:
                        log.info('cancel_cluster_job() Host information has not changed')
                        sqlquery = """UPDATE job SET status=1,
                        description='Installation cancelled',is_cancelled=true, job_end='now()' WHERE job.id=""" \
                                   + str(job.parent_id)
                        db_utils.update_database(sqlquery)
                        if self.request.id:
                            delete_directory(host_info[0], job)
                            lock = Lock.Lock()  # Update Server status
                            lock.id = host_info[0]
                            lock.release()
                            return {'status': 'OK: Job was cancelled successfully', 'result': 1}

                else:
                    log.error('cancel_cluster_job() Job cancel failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

            if len(host_info) == 2:  # {Host:[VM]}

                log.info('cancel_cluster_job() (2) Host: {} VM: {}'.format(host_info[0], host_info[1]))

                # =========================================================
                # Revoke job
                # =========================================================

                res = task_utils.revoke_task(job=job)

                if res == 1:
                    log.info('cancel_cluster_job() Task was cancelled successfully. Verifying host changes')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    host_info_d = json.loads(host_info)
                    host_info = helper.process_cluster_host_information(host_info_d)

                    if len(host_info) == 3:  # Host VM, VM
                        log.warn('cancel_cluster_job() (3) Host information has changed')
                    else:
                        # =========================================================
                        # Clean VM
                        # =========================================================

                        log.info('cancel_cluster_job() Job was cancelled successfully. Now cleaning up VM')
                        res = remove_vm(host_id=host_info[0], vm_id=host_info[1], job=job)
                        if res:
                            if job.parent_id:
                                sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                                           str(job.parent_id)
                                db_utils.update_database(sqlquery)
                                if self.request.id:
                                    delete_directory(host_info[0], job)
                                    lock = Lock.Lock()  # Update Server status
                                    lock.id = host_info[0]
                                    lock.release()
                                    return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                            else:
                                log.error('cancel_cluster_job() Job cancellation failed')
                                if self.request.id:
                                    return {'status': 'ERROR: Job cancelled failed', 'result': -1}

                else:
                    log.error('cancel_cluster_job() Task revoked failed')
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

            if len(host_info) == 3:  # {Host:[VM, VM]}
                log.info('cancel_cluster_job() (3) Host: {} VM: {} VM: {}'.format(host_info[0],
                                                                                  host_info[1],
                                                                                  host_info[2]))
                host_id = host_info[0]
                vm_id_1 = host_info[1]
                vm_id_2 = host_info[2]
                host_info[2] = host_id
                host_info.append(vm_id_2)  # {Host:[VM, VM]} is converted to {Host,VM,Host,VM}

                # =========================================================
                # Revoke job
                # =========================================================

                res = task_utils.revoke_task(job=job)

                if res == 1:
                    log.info('cancel_cluster_job() Task was cancelled successfully. Verifying host changes')
                    host_info = helper.get_job_host_information(job.parent_reference)
                    host_info_d = json.loads(host_info)  # Convert string to JSON dictionary
                    host_info = helper.process_cluster_host_information(host_info_d)
                    if len(host_info) == 4:  # Host: VM, Host: VM
                        log.warn('cancel_cluster_job() (4) Host information has changed')
                    else:
                        # =========================================================
                        # Clean VM
                        # =========================================================
                        log.info('cancel_cluster_job() Job was cancelled successfully. Now cleaning up VM')
                        res = remove_vm(host_id=host_info[0], vm_id=vm_id_1, job=job)
                        if res:
                            if job.parent_id:
                                if self.request.id:
                                    delete_directory(host_info[0], job)
                                    lock = Lock.Lock()  # Update Server status
                                    lock.id = host_info[0]
                                    lock.release()
                        else:
                            if job.parent_id:
                                sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                           str(job.parent_id)
                                helper.update_database(sqlquery)
                            if self.request.id:
                                return {'status': 'ERROR: Job cancel failed', 'result': -1}

                        res = remove_vm(host_id=host_info[2], vm_id=vm_id_2, job=job)
                        if res:
                            if job.parent_id:
                                sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                                           str(job.parent_id)
                                db_utils.update_database(sqlquery)
                                if self.request.id:
                                    delete_directory(host_info[2], job)
                                    lock = Lock.Lock()  # Update Server status
                                    lock.id = host_info[0]
                                    lock.release()
                                    return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                        else:
                            if job.parent_id:
                                sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                           str(job.parent_id)
                                helper.update_database(sqlquery)
                            if self.request.id:
                                return {'status': 'ERROR: Job cancel failed', 'result': -1}
                            else:
                                log.error('cancel_cluster_job() Job cancellation failed')
                                if self.request.id:
                                    return {'status': 'ERROR: Job cancelled failed', 'result': -1}



            if len(host_info) == 4:  # Host VM, Host VM
                log.info('cancel_cluster_job() (4) Host: {} VM: {}, Host: {} VM: {}'.format(host_info[0],
                                                                                            host_info[1],
                                                                                            host_info[2],
                                                                                            host_info[3]))

                # =========================================================
                # Revoke job
                # =========================================================

                res = task_utils.revoke_task(job=job)
                if res == 1:
                    log.info('cancel_cluster_job() Job was cancelled successfully. Now cleaning up VM')
                else:
                    log.error('cancel_cluster_job() Job cancellation failed')
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}

                # =========================================================
                # Clean VM
                # =========================================================

                res = remove_vm(host_id=host_info[0], vm_id=host_info[1], job=job)
                if res:
                    if job.parent_id:
                        if self.request.id:
                            delete_directory(host_info[0], job)
                            lock = Lock.Lock()  # Update Server status
                            lock.id = host_info[0]
                            lock.release()
                else:
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

                res = remove_vm(host_id=host_info[2], vm_id=host_info[3], job=job)
                if res:
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=1, description='Installation cancelled', is_cancelled=true, job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        db_utils.update_database(sqlquery)
                        if self.request.id:
                            delete_directory(host_info[2], job)
                            lock = Lock.Lock()  # Update Server status
                            lock.id = host_info[2]
                            lock.release()
                            return {'status': 'OK: Job was cancelled successfully', 'result': 1}
                else:
                    if job.parent_id:
                        sqlquery = """UPDATE job SET status=-1, description='Installation cancelled failed', job_end='now()' WHERE job.id=""" + \
                                   str(job.parent_id)
                        helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancel failed', 'result': -1}

            log.error('cancel_cluster_job() Invalid host information')
            sqlquery = """UPDATE job SET status=-1, description='Job cancel failed' WHERE job.id=""" + str(
                job.id)
            helper.update_database(sqlquery)
            sqlquery = """UPDATE job SET status=-1, description='Job cancel failed' WHERE job.id=""" + str(
                job.parent_id)
            helper.update_database(sqlquery)
            if self.request.id:
                return {'status': 'WARN: Invalid host information', 'result': -1}

        else:
            log.error('cancel_cluster_job() Invalid data from database')
            if self.request.id:
                return {'status': 'WARN: Invalid data from database', 'result': -1}
            return False

    except ValueError:
        log.exception('cancel_cluster_job() Unable to decode JSON object ')
        return False

    except Exception, exception:
        log.exception('cancel_cluster_job( {0}'.format(exception))

    finally:
        if job.id:
            sqlquery = """UPDATE job SET status=1, description='Job child cancel request completed', is_cancelled='false', job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            helper.update_database(sqlquery)


def delete_directory(host_info, job):
    """

    :param host_info:
    :param job:
    :return:
    """
    try:
        esx = helper.get_server_instance(host_info)
        # Cleaning up directory
        log.info('delete_directory() Clean up directory')
        sqlquery = """SELECT working_directory FROM job WHERE job.reference='""" + str(job.parent_reference) + "'"
        working_directory = db_utils.execute_query(sqlquery)
        log.info('cancel_job() Working directory: {}'.format(working_directory))
        if working_directory and len(working_directory) == 1:
            working_directory = (working_directory[0])[0]
            if working_directory:
                esx.delete_working_directory(working_directory)
                return True
    except Exception:
        return False

@task(bind=True, default_retry_delay=30, max_retries=3)
def remove_vm(self, host_id=None, vm_id=None, job=None, **kwargs):
    """

    :param self:
    :param host_id:
    :param vm_id:
    :param job:
    :param kwargs:
    :return:
    """
    try:

        # =========================================================
        # Get Virtual machine status
        # =========================================================

        log.info('remove_vm() Host information: {}'.format(host_id))

        if host_id and vm_id:  # When there is no Host information just cancel task
            log.info('remove_vm() Host information: {} {}'.format(host_id, vm_id))
        else:
            log.error('remove_vm() No Host information found')
            return False

        if job.id:
            sqlquery = """UPDATE job SET description='VM deletion started' WHERE job.id=""" + str(job.id)
            helper.update_database(sqlquery)

        # =========================================================
        # Delete VM
        # =========================================================

        esx_instance = helper.get_server_instance(host_id)
        if not esx_instance:
            log.info('remove_vm() Invalid server.Not a valid server')
            return False

        # =========================================================
        # Get Virtual machine state
        # =========================================================

        if vm_id != -1:
            log.info('remove_vm() Get current VM state')
            command_list = []
            command_list.append('vim-cmd vmsvc/power.getstate ' + str(vm_id) + ' | grep \'Power\'')
            res = general.send_command(commandList=command_list,
                                       server=esx_instance.server,
                                       username=esx_instance.username,
                                       password=esx_instance.password,
                                       port=esx_instance.sshPort,
                                       display=True)
            if res:
                result = res[0].encode('utf-8').rstrip()
                if result == 'Powered on':
                    log.warn('remove_vm() VM powered on. Proceed to power off')

                    # =========================================================
                    # Power off Virtual machine
                    # =========================================================

                    res = power_off(server=esx_instance.server,
                                    username=esx_instance.username,
                                    password=esx_instance.password,
                                    port=esx_instance.sshPort,
                                    vmId=str(vm_id))

                    if res:
                        log.info('remove_vm() Power off successful')
                        time.sleep(15)  # Sleep to let the machine be off
                    else:
                        log.error(
                            'remove_vm() Power off failed. Unable to power off')
                        if job.id:
                            sqlquery = """UPDATE job SET status=-1, description='VM deletion failed. Power off failed', job_end='now()' WHERE job.id=""" + \
                                       str(job.id)
                            helper.update_database(sqlquery)
                        if self.request.id:
                            return {'status': 'ERROR: Job cancelled failed', 'result': -1}
                        return False
            else:
                log.error('remove_vm() Power off failed. VM probably doesnt exist anymore')
                if job.id:
                    sqlquery = """UPDATE job SET status=1, description='VM no longer exists', job_end='now()' WHERE job.id=""" + \
                               str(job.id)
                    helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}
                    return False

            if settings.destroy_vm:
                res = destroy(server=esx_instance.server,
                              username=esx_instance.username,
                              password=esx_instance.password,
                              port=esx_instance.sshPort,
                              vmId=str(vm_id))

                if not res:
                    log.error('remove_vm() Destroy failed')
                    if job.id:
                        sqlquery = """UPDATE job SET status=-1, description='VM deletion failed', job_end='now()' WHERE job.id=""" + \
                                   str(job.id)
                        helper.update_database(sqlquery)
                    if self.request.id:
                        return {'status': 'ERROR: Job cancelled failed', 'result': -1}
                    return False

                log.info('remove_vm() VM Destroy was successful!')
                if job.id:
                    sqlquery = """DELETE FROM virtualmachine WHERE fk_server=""" + str(
                        host_id) + """ AND vmid=""" + str(vm_id)
                    helper.update_database(sqlquery)

            else:
                log.warn(
                    'remove_vm() Dont destroy VM. Just power it off')
                sqlquery = """DELETE FROM virtualmachine WHERE fk_server=""" + str(
                    host_id) + """ AND vmid=""" + str(vm_id)
                helper.update_database(sqlquery)

        if self.request.id:
            return {'status': 'OK: Job was cancelled successfully', 'result': 1}
        return True

    except ValueError:
        log.exception('remove_vm() Unable to decode JSON object ')
        if self.request.id:
            return {'status': 'ERROR: Job cancelled failed', 'result': -1}
        return False

    except Exception as exception:
        log.exception('remove_vm() Exception {}'.format(exception))
        if self.request.id:
            return {'status': 'ERROR: Job cancelled failed', 'result': -1}
        return False


def register_vm(server=None,
                username=None,
                password=None,
                port=None,
                vmxFile=None,
                commandTimeout=10, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param vmxFile:
    :param commandTimeout:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.register_vm() Check login options')

    if file_operations.check_remote_file_exists(server=server,
                                                username=username,
                                                password=password,
                                                sshPort=port,
                                                fileName=vmxFile):
        log.info('vm_operations.register_vm() VMX file exist')
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.register_vm() VMX file doesnt exist')

    try:
        log.info('vm_operations.register_vm()')
        res = general.send_command(
            commandList=['vim-cmd solo/registervm ' + vmxFile],
            server=server,
            username=username,
            password=password,
            port=port,
            display=True)

        vmId = res[0].encode('utf-8')
        vmId = vmId.rstrip()
        # res = True if directory exists. 1 directory doesnt exists
        if vmId:
            return vmId
        else:
            log.info('vm_operations.register_vm() check_remote_file_exists() File ' +
                     vmxFile + ' does not exist')
            return None

        log.info('vm_operations.register_vm() vmk disk created successfully')

    except Exception, exception:
        log.exception(str(exception))


def get_vm_uuid(server=None, username=None, password=None, port=None, vmId=None, commandTimeout=10, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param vmId:
    :param commandTimeout:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.get_vm_uuid() Check login options')
    try:
        log.info('vm_operations.get_vm_uuid() Sending get.summary command: ' + vmId)
        res = general.send_command(
            commandList=['vim-cmd vmsvc/get.summary ' +
                         vmId + ' | grep uuid | awk \'{print $3}\''],
            server=server,
            username=username,
            password=password,
            port=port,
            display=True)

        if res:
            return res[0].replace(',', '').strip()
        else:
            log.exception('vm_operations.get_vm_uuid() Unknown uuid')
            return None

    except Exception, exception:
        log.exception(str(exception))


def get_summary(server=None, username=None, password=None, port=None, vmId=None, commandTimeout=10, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param vmId:
    :param commandTimeout:
    :param kwargs:
    :return:
    """
    log.info('vm_operations.get_summary()')
    """
    guest = (vim.vm.Summary.GuestSummary) {
      dynamicType = <unset>,
      guestId = "rhel5Guest",
      guestFullName = "Red Hat Enterprise Linux 5 (32-bit)",
      toolsStatus = "toolsOld",
      toolsVersionStatus = "guestToolsNeedUpgrade",
      toolsVersionStatus2 = "guestToolsSupportedOld",
      toolsRunningStatus = "guestToolsRunning",
      hostName = "pub",
      ipAddress = "110.10.0.201",
    },
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.get_summary() Check login options')

    try:
        log.info(
            'vm_operations.get_summary() Sending get.summary command: ' + str(vmId))
        res = general.send_command(
            commandList=[
                'vim-cmd vmsvc/get.summary ' + str(vmId) + ' |  grep -E \'toolsRunningStatus|hostName|ipAddress\''],
            server=server,
            username=username,
            password=password,
            port=port,
            display=True)

        log.info('vm_operations.get_summary() Result: ' + str(res))
        if res:
            cucmInfo = []
            import re
            for item in res:
                matches = re.search('"([^"]*)"', item.strip(' \t\n\r'))
                if matches:
                    log.info('vm_operations.get_summary(): ' +
                             str(matches.group(0).replace("\"", "")))
                    cucmInfo.append(matches.group(0).replace("\"", ""))
                else:
                    continue
            # [u'guestToolsRunning', u'pub', u'110.10.0.201']
            return cucmInfo

        else:
            log.exception('vm_operations.get_summary() Unknown VM status')
            return

    except Exception, e:
        log.exception(str(e))


def getVmInfo(vm, depth=1):
    """
        Print information for a particular virtual machine or recurse into a folder
        with depth protection

    :param vm:
    :param depth:
    :return:
    """
    maxdepth = 10

    # if this is a group it will have children. if it does, recurse into them
    # and then return
    if hasattr(vm, 'childEntity'):
        if depth > maxdepth:
            return
        vmList = vm.childEntity
        for c in vmList:
            PrintVmInfo(c, depth + 1)
        return

    summary = vm.summary

    print("UUID       : ", summary.config.instanceUuid)
    print("Name       : ", summary.config.name)
    print("Path       : ", summary.config.vmPathName)
    print("Guest      : ", summary.config.guestFullName)
    annotation = summary.config.annotation
    if annotation and annotation != "":
        print("Annotation : ", annotation)
    print("State      : ", summary.runtime.powerState)
    if summary.guest:
        ip = summary.guest.ipAddress
        if ip != None and ip != "":
            print("IP         : ", ip)
    if summary.runtime.question:
        print("Question  : ", summary.runtime.question.text)
    print


def getIPInformation(vm, depth=1):
    """
    Print information for a particular virtual machine or recurse into a folder
     with depth protection

    :param vm:
    :param depth:
    :return:
    """
    maxdepth = 10

    # if this is a group it will have children. if it does, recurse into them
    # and then return
    if hasattr(vm, 'childEntity'):
        if depth > maxdepth:
            return
        vmList = vm.childEntity
        for c in vmList:
            PrintVmInfo(c, depth + 1)
        return

    summary = vm.summary
    vm_info = dict()
    # print("Name       : ", summary.config.name)
    if summary.guest:
        ip = summary.guest.ipAddress
        if ip and ip != "":
            # print("IP         : ", ip)
            vm_info['name'] = summary.config.name
            vm_info['ip'] = ip
            vm_info['uuid'] = summary.config.instanceUuid
        else:
            vm_info['name'] = summary.config.name
            vm_info['ip'] = ""
            vm_info['uuid'] = summary.config.instanceUuid
    else:
        vm_info['name'] = summary.config.name
        vm_info['ip'] = ""
        vm_info['uuid'] = summary.config.instanceUuid

    # print vm_info
    return vm_info


def PrintVmInfo(vm, depth=1):
    """
    Print information for a particular virtual machine or recurse into a folder
     with depth protection

    :param vm:
    :param depth:
    :return:
    """
    maxdepth = 10

    # if this is a group it will have children. if it does, recurse into them
    # and then return
    if hasattr(vm, 'childEntity'):
        if depth > maxdepth:
            return
        vmList = vm.childEntity
        for c in vmList:
            PrintVmInfo(c, depth + 1)
        return

    summary = vm.summary

    print("UUID       : ", summary.config.instanceUuid)
    print("Name       : ", summary.config.name)
    print("Path       : ", summary.config.vmPathName)
    print("Guest      : ", summary.config.guestFullName)
    annotation = summary.config.annotation
    if annotation and annotation != "":
        print("Annotation : ", annotation)
    print("State      : ", summary.runtime.powerState)
    if summary.guest:
        ip = summary.guest.ipAddress
        if ip and ip != "":
            print("IP         : ", ip)
    if summary.runtime.question:
        print("Question  : ", summary.runtime.question.text)
    print


def get_info(server=None, username=None, password=None, port=None, getIpInfo=False, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param getIpInfo:
    :param kwargs:
    :return:
    """
    import ssl
    if hasattr(ssl, '_create_unverified_context'):
        ssl._create_default_https_context = ssl._create_unverified_context

    si = connect.SmartConnect(host=server,
                              user=username,
                              pwd=password,
                              port=int(port))
    if not si:
        print("get_vm_information() Could not connect to the specified host using specified "
              "username and password")
        return False

    ip_information = []
    atexit.register(connect.Disconnect, si)
    content = si.RetrieveContent()
    for child in content.rootFolder.childEntity:
        if hasattr(child, 'vmFolder'):
            datacenter = child
            vmFolder = datacenter.vmFolder
            vmList = vmFolder.childEntity
            for vm in vmList:
                if getIpInfo:
                    ip_information.append(getIPInformation(vm))

    return ip_information


def power_on(server=None, username=None, password=None, port=None, vmId=None, commandTimeout=10, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param vmId:
    :param commandTimeout:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'register_vm() Check login options')

    try:
        log.info('vm_operations.power_on()')
        res = general.send_command(commandList=['vim-cmd vmsvc/power.on ' + vmId],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)

        if res:
            log.info('vm_operations.power_on() VM power on successfully')
        else:
            log.info('vm_operations.power_on() VM power on failed')

    except Exception, exception:
        log.exception(str(exception))
        raise


def find_vm_duplicates(server=None,
                       username=None,
                       password=None,
                       port=None,
                       commandTimeout=10,
                       hostname=None,
                       **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param commandTimeout:
    :param hostname:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.check_vm_duplicates() Check login options')
    try:
        log.info('vm_operations.check_vm_duplicates()')
        res = general.send_command(commandList=['vim-cmd vmsvc/getallvms | awk \'{print $2}\''],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)

        if res:
            for filesystem in res:
                if filesystem.strip() == hostname:
                    print 'Match found: ' + filesystem.strip()
                    return True
        else:
            return False

    except Exception, exception:
        log.exception(str(exception))
        raise


def destroy(server=None, username=None, password=None, port=None, commandTimeout=10, vmId=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param commandTimeout:
    :param vmId:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.destroy() Deleting machine')

    try:
        log.info('vm_operations.destroy()')
        res = general.send_command(commandList=['vim-cmd vmsvc/destroy ' + vmId],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)

        if res is None:
            log.info('vm_operations.destroy() VM destroy successfully')
            return True
        else:
            log.info('vm_operations.destroy() VM destroy failed')
            return False

    except Exception, exception:
        log.exception(str(exception))
        raise


def power_off(server=None, username=None, password=None, port=None, commandTimeout=10, vmId=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param commandTimeout:
    :param vmId:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.power_off() Check login options')

    try:
        log.info('vm_operations.power_off()')
        res = general.send_command(commandList=['vim-cmd vmsvc/power.off ' + vmId],
                                   server=server,
                                   username=username,
                                   password=password,
                                   port=port,
                                   display=True)

        if res:
            log.info('vm_operations.power_off() VM power off successfully')
            return True
        else:
            log.info('vm_operations.power_off() VM power off failed')
            return False

    except Exception, exception:
        log.exception(str(exception))
        raise


def get_status(server=None, username=None, password=None, port=None, commandTimeout=10, vmId=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param commandTimeout:
    :param vmId:
    :param kwargs:
    :return:
    """
    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        raise exceptions.SSHConnectivityError(
            'vm_operations.get_status() Check login options')

    try:
        log.info('vm_operations.get_status()')
        if vmId:
            res = general.send_command(commandList=['vim-cmd vmsvc/get.summary ' + vmId + ' | grep overallStatus'],
                                       server=server,
                                       username=username,
                                       password=password,
                                       port=port,
                                       display=True)

            if 'green' in res[0].encode('utf-8'):
                log.info('vm_operations.get_status() Status Ok')
                return True
            else:
                log.exception('vm_operations.get_status() Unknown status')
                raise exceptions.VirtualMachineStatus(
                    'vm_operations.get_status() Unknown status')
                return False
        else:
            log.error('vm_operations.get_status() Invalid vmId')
            return False

    except Exception, exception:
        log.exception(str(exception))
        raise


def insert_vm(vmId,
              vm_hostname,
              status,
              description,
              ip_information,
              uuid,
              app_id,
              power_state,
              uptime=None,
              esx_hostname=None,
              directory=None,
              url=None,
              host_id=None,
              owner_id=None,
              **kwargs):
    """

    :param vmId:
    :param vm_hostname:
    :param status:
    :param description:
    :param ip_information:
    :param uuid:
    :param app_id:
    :param power_state:
    :param uptime:
    :param esx_hostname:
    :param directory:
    :param url:
    :param host_id:
    :param owner_id:
    :param kwargs:
    :return:
    """
    log.info('vm_operations.insert_vm() Started')

    # Create Session object
    database_engine = create_engine(settings.SQLALCHEMY_DATABASE_URI,
                                    pool_size=settings.SQLALCHEMY_POOL_SIZE,
                                    max_overflow=settings.SQLALCHEMY_MAX_OVERFLOW)

    Session = sessionmaker(bind=database_engine)
    session = Session()
    session._model_changes = {}

    # Find ESXi server
    if host_id:
        log.info('vm_operations.insert_vm() host_id present {}'.format(host_id))
        server = session.query(Model.Server).filter_by(id=int(host_id)).all()
    else:
        server = session.query(Model.Server).filter_by(name=esx_hostname).all()

    if server:
        # Get Server Id
        host_id_db = server[0].serialize()['id']
        # Insert IP settings if available
        if ip_information:
            ip_settings = Model.IpSetting(ip_information)
            session.add(ip_settings)
            session.flush()
            fk_ip_settings = ip_settings.id
        else:
            # Most likely machine is power off
            fk_ip_settings = None

        if uptime:
            uptime = display_utils.display_time(uptime)
        # Insert VirtualMachine
        try:
            vm = Model.Virtualmachine(int(vmId),
                                      vm_hostname,
                                      status,
                                      description,
                                      host_id_db,
                                      fk_ip_settings,
                                      uuid,
                                      app_id,
                                      power_state,
                                      uptime,
                                      directory,
                                      url)
            session.add(vm)
            session.commit()
            session.close()
            return True
        except Exception, e:
            session.rollback()
            log.exception(str(e))
    else:
        log.error('vm_operations.insert_vm() VM insertion failed. Server not found')
        return False


def get_vm_info(vm):
    """
    Print information for a particular virtual machine or recurse into a
    folder with depth protection
    """

    # {u'116': {'ip': '192.168.1.45','name': 'cpca1','uuid': '52be2710-0b7d-7967-f182-9ae816fe701b'}
    log.info('vm_operations.get_vm_info()')
    vm_info = {}
    summary = vm.summary
    log.info("Name       : " + summary.config.name)
    vm_info['name'] = summary.config.name

    log.info("Guest      : " + summary.config.guestFullName)
    vm_info['guest'] = summary.config.guestFullName

    log.info("UUID       : " + summary.config.instanceUuid)
    vm_info['uuid'] = summary.config.instanceUuid

    vm_info['annotation'] = ''
    vm_info['app_id'] = 0
    vm_info['ip'] = ''

    annotation = summary.config.annotation
    if annotation:
        log.info("Annotation : " + annotation)
        vm_info['annotation'] = summary.config.annotation
        vm_info['app_id'] = get_app_type(summary.config.annotation)
        log.info("Application : {}".format(vm_info['app_id']))

    log.info("State      : " + summary.runtime.powerState)
    if summary.runtime.powerState == 'poweredOff':
        vm_info['power_state'] = False
    else:
        vm_info['power_state'] = True

    if summary.quickStats.uptimeSeconds:
        log.info("Uptime      : {}".format(summary.quickStats.uptimeSeconds))
        vm_info['uptime'] = summary.quickStats.uptimeSeconds
    else:
        vm_info['uptime'] = None

    if summary.guest is not None:
        ip = summary.guest.ipAddress
        if ip:
            log.info("IP         : " + ip)
            vm_info['ip'] = summary.guest.ipAddress

    return vm_info


def get_virtualmachines(server=None, username=None, password=None, port=None):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :return:
    """

    log.info('vm_operations.get_virtualmachines()')

    si = connect.SmartConnect(host=server, user=username, pwd=password, port=int(port))
    content = si.RetrieveContent()
    children = content.rootFolder.childEntity
    for child in children:
        log.info('vm_operations.get_virtualmachines() {0} '.format(child))

    log.info('Datacenter: {0}'.format(children[0].name))

    content = si.RetrieveContent()
    object_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                          [], True)
    vm_list = {}
    for obj in object_view.view:
        if isinstance(obj, vim.VirtualMachine):
            vmid = str(obj)
            vmid = vmid.replace("'", "")
            vm_list[vmid.split(":")[1]] = get_vm_info(obj)
            log.info('VM object: {0}'.format(obj))

    object_view.Destroy()
    return vm_list


def get_app_type(annotation):
    """

    :param annotation:
    :return:
    """
    if 'CUCM' in annotation:
        return 200001
    if 'CUC' in annotation:
        return 200002
    if 'CUP' in annotation:
        return 200003
    if 'UCCX' in annotation:
        return 200004
    if 'CER' in annotation:
        return 200005
    if 'VCS' in annotation:
        return 200006
    if 'TPS' in annotation:
        return 200007
    if 'COND' in annotation:
        return 200008
    return 0


def create_vmx_file(hypervisor, fileName, appId, params):
    """

    :rtype : object
    :param hypervisor:
    :param fileName:
    :param appId:
    :param params:
                params['hostname']
                params['vmdk']
                params['floppy_image']
                params['bootable_iso']
                params['vnc_port']
    :return:
    """
    try:
        log.info('vm_operations.create_vmx_file()')

        if hypervisor == 1:

            if appId == 200001:

                log.info("vm_operations.create_vmx_file() Application ucm")

                ova_size = params['size']

                if params['version'] == '90':
                    template_handler.generateNewFile(
                        settings.ucmVmxTemplate + '90/' + ova_size + '/ucm90.vmx', fileName)

                elif params['version'] == '91':
                    template_handler.generateNewFile(
                        settings.ucmVmxTemplate + '91/' + ova_size + '/ucm91.vmx', fileName)

                elif params['version'] == '10':
                    template_handler.generateNewFile(
                        settings.ucmVmxTemplate + '10/' + ova_size + '/ucm10.vmx', fileName)

                elif params['version'] == '105':
                    template_handler.generateNewFile(
                        settings.ucmVmxTemplate + '105/' + ova_size + '/ucm105.vmx', fileName)

                elif params['version'] == '11':
                    template_handler.generateNewFile(
                        settings.ucmVmxTemplate + '11/' + ova_size + '/ucm11.vmx', fileName)

                else:
                    from error import app_exceptions
                    raise app_exceptions.CiscoVmxNotFound(
                        'vm_operations.create_vmx_file() param version not found')

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)

                # Update parameters in template
                template_handler.validateParameters(params)
                template_handler.updateParamInTemplate(
                    fileName, 'displayName', '"' + params['hostname'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'scsi0:0.fileName', '"' + params['vmdk'] + '"')
                template_handler.updateParamInTemplate(fileName, 'floppy0.fileName',
                                                       '"' + params['esx_floppy_file'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'ide1:0.fileName', '"' + params['bootable_iso'] + '"')
                template_handler.updateParamInTemplate(fileName, 'RemoteDisplay.vnc.port',
                                                       '"' + params['vnc_port'] + '"')

                # VMX file in version 11 doesnt look for nvram
                if params['version'] != '11':
                    template_handler.updateParamInTemplate(
                        fileName, 'nvram', '"' + params['hostname'] + '.nvram"')

                try:
                    # ethernet0.networkName = "Net10Dot"
                    if params['network_name'] and len(params['network_name']) > 0:
                        template_handler.updateParamInTemplate(fileName, 'ethernet0.networkName',
                                                               '"' + params['network_name'] + '"')
                except:
                    pass

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)

            elif appId == 200002:

                log.info("vm_operations.create_vmx_file() Application cuc")

                ova_size = params['size']

                if params['version'] == '90':
                    template_handler.generateNewFile(
                        settings.cucVmxTemplate + '90/' + ova_size + '/cuc90.vmx', fileName)
                elif params['version'] == '91':
                    template_handler.generateNewFile(
                        settings.cucVmxTemplate + '91/' + ova_size + '/cuc91.vmx', fileName)
                elif params['version'] == '10':
                    template_handler.generateNewFile(
                        settings.cucVmxTemplate + '10/' + ova_size + '/cuc10.vmx', fileName)
                # Fix Issue 33
                elif params['version'] == '105':
                    template_handler.generateNewFile(
                        settings.cucVmxTemplate + '105/' + ova_size + '/cuc105.vmx', fileName)
                elif params['version'] == '11':
                    template_handler.generateNewFile(
                        settings.cucVmxTemplate + '11/' + ova_size + '/cuc11.vmx', fileName)
                else:
                    from error import app_exceptions
                    raise app_exceptions.CiscoVmxNotFound(
                        'vm_operations.create_vmx_file() param version not found')

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)

                # TODO Validate params
                # Update parameters in template
                template_handler.validateParameters(params)

                template_handler.updateParamInTemplate(
                    fileName, 'displayName', '"' + params['hostname'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'scsi0:0.fileName', '"' + params['vmdk'] + '"')
                template_handler.updateParamInTemplate(fileName, 'floppy0.fileName',
                                                       '"' + params['esx_floppy_file'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'ide1:0.fileName', '"' + params['bootable_iso'] + '"')
                template_handler.updateParamInTemplate(fileName, 'RemoteDisplay.vnc.port',
                                                       '"' + params['vnc_port'] + '"')

                template_handler.updateParamInTemplate(
                    fileName, 'nvram', '"' + params['hostname'] + '.nvram"')
                try:
                    # ethernet0.networkName = "Net10Dot"
                    if params['network_name'] and len(params['network_name']) > 0:
                        template_handler.updateParamInTemplate(fileName, 'ethernet0.networkName',
                                                               '"' + params['network_name'] + '"')
                except:
                    pass

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)

            elif appId == 200003:

                log.info("vm_operations.create_vmx_file() Application cup")

                ova_size = params['size']

                if params['version'] == '90':
                    template_handler.generateNewFile(
                        settings.cupVmxTemplate + '90/' + ova_size + '/cup90.vmx', fileName)
                elif params['version'] == '91':
                    template_handler.generateNewFile(
                        settings.cupVmxTemplate + '91/' + ova_size + '/cup91.vmx', fileName)
                elif params['version'] == '10':
                    template_handler.generateNewFile(
                        settings.cupVmxTemplate + '10/' + ova_size + '/cup10.vmx', fileName)
                # Fix Issue 33
                elif params['version'] == '105':
                    template_handler.generateNewFile(
                        settings.cupVmxTemplate + '105/' + ova_size + '/cup105.vmx', fileName)
                elif params['version'] == '11':
                    template_handler.generateNewFile(
                        settings.cupVmxTemplate + '11/' + ova_size + '/cup11.vmx', fileName)
                else:
                    from error import app_exceptions
                    raise app_exceptions.CiscoVmxNotFound(
                        'vm_operations.create_vmx_file() param version not found')

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)

                # Update parameters in template
                template_handler.validateParameters(params)

                template_handler.updateParamInTemplate(
                    fileName, 'displayName', '"' + params['hostname'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'scsi0:0.fileName', '"' + params['vmdk'] + '"')
                template_handler.updateParamInTemplate(fileName, 'floppy0.fileName',
                                                       '"' + params['esx_floppy_file'] + '"')
                template_handler.updateParamInTemplate(
                    fileName, 'ide1:0.fileName', '"' + params['bootable_iso'] + '"')
                template_handler.updateParamInTemplate(fileName, 'RemoteDisplay.vnc.port',
                                                       '"' + params['vnc_port'] + '"')

                template_handler.updateParamInTemplate(
                    fileName, 'nvram', '"' + params['hostname'] + '.nvram"')
                try:
                    # ethernet0.networkName = "Net10Dot"
                    if params['network_name'] and len(params['network_name']) > 0:
                        template_handler.updateParamInTemplate(fileName, 'ethernet0.networkName',
                                                               '"' + params['network_name'] + '"')
                except:
                    pass

                config = template_handler.parse_configuration(fileName)
                template_handler.checkVmxParameters(config)
            else:
                log.error('vm_operations.create_vmx_file() Unknown application')
                raise app_exceptions.AppNotFound(
                    'create_vmx_file() Unknown application')
        else:
            raise exceptions.UnknownHypervisor('vm_operations.create_vmx_file() '
                                               'Hypervisor model not available yet')

    except Exception, exception:
        log.exception(exception)
