__author__ = 'gogasca'
import re
import logging
from conf import logging_conf
from subprocess import call
from error import exceptions

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class vmxConfigurationTemplate():
    def __init__(self):
        log.info(
            'ConfigurationTemplate() - New vmx configuration template() object created()')
        self.parameters = {}

    def dumpContents(self):
        return self.parameters

    def updateParameter(self, parameter, value):
        if parameter in self.parameters:
            self.parameters[parameter] = value

        else:
            log.exception('ConfigurationTemplate() parameter not found')
            raise exceptions.ParameterNotFoundInTemplate(parameter)

    def removeParameter(self, parameter):
        if parameter in self.parameters:
            del self.parameters[parameter]
        else:
            log.exception('ConfigurationTemplate() parameter not found')
            raise exceptions.ParameterNotFoundInTemplate(parameter)

    def addParameter(self, parameter, value):
        if checkParamValueSyntax(value):
            self.parameters[parameter] = value
            return True
        else:
            log.exception(
                'ConfigurationTemplate() Invalid parameter: ' + value)
            raise exceptions.InvalidTemplateParameter(value)

    def getParameter(self, parameter):
        return self.parameters[parameter]


def init():
    """
    Initialize configuration for vmx file
    :return:
    """
    configuration = vmxConfigurationTemplate()
    return configuration


def checkParamValueSyntax(value):
    """

    :param value:
    :return:
    """

    if value.startswith('"') and value.endswith('"'):
        return True
    else:
        return False


def parse_configuration(filename):
    """

    :param filename:
    :return:
    """

    config = init()
    log.info("parse_configuration() Reading vmx configuration file: " + filename)
    for line in open(filename, 'r').readlines():
        config_line = line.rstrip('\n')
        m = re.search('(.*)\=(.*)', config_line)
        try:
            paramName = m.group(1)
            paramValue = m.group(2)
            # We use .strip() to remove beginning and ending spaces
            config.addParameter(paramName.strip(), paramValue.strip())
        except Exception, e:
            log.exception(str(e))
            raise exceptions.InvalidTemplateParameter(config_line)

    return config


# TODO
def validateParameters(params):
    log.info('validateParameters() Parameters are not being validated')
    pass


def addParamInTemplate(filename, param, value):
    """

    :param filename:
    :param param:
    :param value:
    :return:
    """
    log.info("addParamInTemplate() Updating configuration file and validating parameters |" + param + '|' + value)
    with open(filename, 'a') as f:
        log.info('addParamInTemplate() Updating file {}'.format(filename))
        f.write(param + " = \"" + value + "\"\n")
        f.close()


def updateParamInTemplate(filename, param, value):
    """
    Update configuration file
    :param filename:
    :param param:
    :param value:
    :return:
    """
    log.info("updateParamInTemplate() Updating configuration file and validating parameters |" + param + '|' + value)
    paramUpdated = False
    import fileinput

    for line in fileinput.input(filename, inplace=True):
        if line.startswith(param):
            # ATTENTION! Print to file! This is not print to console.
            # DO NOT CHANGE
            print param + ' = ' + value
            paramUpdated = True
        else:
            # ATTENTION! Print to file! This is not print to console.
            # DO NOT CHANGE
            print line.strip()

    if paramUpdated:
        log.info(
            'updateParamInTemplate() Param updated successfully |' + param + '|' + value)
        return True
    else:
        raise exceptions.InvalidTemplateParameter(
            'updateParamInTemplate() param not found: ' + param)


def checkVmxParameters(configObject):
    """

    :param configObject:
    :return:
    """
    # VMX File
    log.info("checkParameters() Reading configuration file and validating parameters")
    config = configObject.dumpContents()
    index = 1
    for parameter, value in config.iteritems():
        if parameter and value:
            if value.startswith('"') and value.endswith('"'):
                pass
        else:
            import exceptions
            raise exceptions.InvalidConfigurationFile(
                'checkParameters() Invalid line: ' + str(index))
            return False
        index = +1

    log.info("checkParameters() File format is correct")
    return True


def obtain_cache_configuration(configObject):
    """

    :param configObject:
    :return:
    """
    log.info(configObject.__class__.__name__)
    if configObject.__class__.__name__ == "ConfigurationTemplate":
        return configObject.dumpContents()


def write_configuration(configObject, fileName):
    # Dump config in File
    with open(fileName, 'w') as fout:
        for param in configObject:
            fout.write(param + " = " + configObject[param])


def generateNewFile(source, destination):
    """

    :param source:
    :param destination:
    :return:
    """
    log.info('generateNewFile()')
    log.info('generateNewFile() Source vmx file: ' + source)
    log.info('generateNewFile() Destination vmx: ' + destination)
    call(['cp ' + source + ' ' + destination], shell=True)
