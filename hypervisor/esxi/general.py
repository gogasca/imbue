import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')

import time
import logging
import paramiko
import subprocess
import os.path
from scp import SCPClient
from paramiko import SSHClient
from pyVim import connect
from pyVmomi import vim
from subprocess import call
from utils import validator, port_scanner
from hypervisor.esxi import service_operations
from error import exceptions
from conf import settings, logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

__author__ = 'gogasca'


def check_network_connectivity(server=None,
                               username=None,
                               password=None,
                               sshPort=None,
                               httpsPort=None,
                               vncPort=None,
                               vncPassword=None,
                               sshTimeout=15,
                               mode='all',
                               init=False,
                               get_status_code=False,
                               **kwargs):
    """
    Verify network connectivity to ESXi Server
    Initialization, check server is reachable, we start Thread to verify server is reachable and not hang

    """

    try:

        # =========================================================
        # Check mode option is valid
        # =========================================================

        log.info('general.check_network_connectivity()')
        if isinstance(mode, (str, unicode)):
            # ************ We will check HTTPS and SSH if SSH is disabled we will proceed to enable it.
            if mode == 'all':
                log.info('general.check_network_connectivity() Mode: all - Check https and ssh')

                # =========================================================
                # Check HTTPS
                # =========================================================

                if (validator.check_authentication_parameters(server=server,
                                                              username=username,
                                                              password=password,
                                                              httpsPort=httpsPort)):
                    # Check HTTPS Connectivity to host
                    try:
                        log.info('general.check_network_connectivity() all mode Checking https connectivity')
                        if httpsPort:
                            log.info('general.check_network_connectivity() HTTPS port defined')
                            if port_scanner.is_port_opened(server=server, port=httpsPort):
                                res = check_https_connectivity(server=server,
                                                               port=httpsPort,
                                                               username=username,
                                                               password=password)

                                if res:

                                    https_connectivity = True

                                    if res == -1:
                                        # =========================================================
                                        # HTTPS connectivity is good, Credentials wrong
                                        # =========================================================

                                        log.info('general.check_network_connectivity() Invalid HTTPS credentials')
                                        return -1

                                        # =========================================================
                                        # HTTPS is good, check SSH now
                                        # =========================================================

                                else:
                                    log.error('general.check_network_connectivity() HTTPS Connectivity failed')
                                    if get_status_code:
                                        return -2
                                    return False
                            else:
                                log.error('general.check_network_connectivity() Port is not opened')
                                if get_status_code:
                                    return -2
                                return False

                        else:

                            # =========================================================
                            # Best Effort
                            # =========================================================

                            log.info('general.check_network_connectivity() HTTPS Best effort...')
                            if hasattr(settings, 'defaultHTTPSPort'):
                                log.info('general.check_network_connectivity() using defaultHTTPSPort ')
                                res = port_scanner.is_port_opened(server=server,
                                                                  port=settings.defaultHTTPSPort)
                            else:

                                # =========================================================
                                # Default port is 443
                                # =========================================================
                                res = port_scanner.is_port_opened(server=server, port='443')

                            if res:
                                if check_https_connectivity(server=server,
                                                            username=username,
                                                            password=password):
                                    https_connectivity = True
                                    # =========================================================
                                    # Continue to SSH
                                    # =========================================================
                                else:
                                    log.error('general.check_network_connectivity() HTTPS Connectivity failed')
                                    if get_status_code:
                                        return -1
                                    return False
                            else:
                                log.error('general.check_network_connectivity() HTTPS Port is not opened')
                                if get_status_code:
                                    return -2
                                return False

                    except Exception, e:
                        log.exception(str(e.message))
                        return False

                # =========================================================
                # Check SSH
                # =========================================================

                if (validator.check_authentication_parameters(server=server,
                                                              username=username,
                                                              password=password,
                                                              sshPort=sshPort,
                                                              sshTimeout=sshTimeout)):

                    # =========================================================
                    # Check SSH Connectivity to host only
                    # =========================================================

                    log.info('general.check_network_connectivity() all mode Checking ssh connectivity')
                    try:
                        # =========================================================
                        # Verify SSH Port is defined
                        # =========================================================

                        if sshPort:
                            log.info('general.check_network_connectivity() SSH Port defined: (' + str(sshPort) + ')')
                            if port_scanner.is_port_opened(server=server, port=sshPort):
                                if check_ssh_connectivity(server=server,
                                                          port=sshPort,
                                                          username=username,
                                                          password=password):
                                    if get_status_code:
                                        return 1
                                    return True
                                else:
                                    log.error('general.check_network_connectivity() SSH Connectivity failed')
                                    if get_status_code:
                                        return -2
                                    return False
                            else:
                                log.error('general.check_network_connectivity() SSH Port is not opened by scanner')

                                # =========================================================
                                # HTTPS Connectivity is ok and we are enforcing opening the service
                                # =========================================================

                                if (https_connectivity and init):

                                    # =========================================================
                                    # Try to enable SSH
                                    # =========================================================

                                    log.info('general.check_network_connectivity() Trying to enable SSH Service')
                                    # Enable Service via HTTPS operation=1
                                    if service_operations.ssh_service_activation(server=server,
                                                                                 username=username,
                                                                                 password=password,
                                                                                 port=int(httpsPort),
                                                                                 operation=1):

                                        if check_ssh_connectivity(server=server,
                                                                  port=sshPort,
                                                                  username=username,
                                                                  password=password):
                                            log.info(
                                                'general.check_network_connectivity() SSH service was enabled')
                                            if get_status_code:
                                                return 1
                                            return True
                                        else:
                                            log.error(
                                                'general.check_network_connectivity() SSH Connectivity failed after retry')
                                            if get_status_code:
                                                return -2
                                            return False
                                    else:
                                        log.error(
                                            'general.check_network_connectivity() SSH Connectivity service start failed')
                                        if get_status_code:
                                            return -2
                                        return False
                                else:
                                    log.error('general.check_network_connectivity() SSH Connectivity failed. Use init')
                                    if get_status_code:
                                        return -2
                                    return False

                        # =========================================================
                        # Verify SSH Port is not defined
                        # =========================================================

                        else:
                            log.error('general.check_network_connectivity() SSH Port is not defined')
                            return False

                    except Exception, exception:
                        log.exception(str(exception))
                        raise

            elif mode == 'ssh':

                # =========================================================
                # Check SSH Connectivity to host only
                # =========================================================

                log.info('general.check_network_connectivity() SSH mode. Checking SSH connectivity...')
                if sshPort:
                    log.info('general.check_network_connectivity() SSH Port defined: (' + str(sshPort) + ')')
                    if port_scanner.is_port_opened(server=server, port=sshPort):
                        return check_ssh_connectivity(server=server,
                                                      port=sshPort,
                                                      username=username,
                                                      password=password)
                    else:
                        return False
                else:

                    # =========================================================
                    # Default SSH port is 22
                    # =========================================================

                    if hasattr(settings, 'defaultSSHPort'):
                        log.info(
                            'general.check_network_connectivity() Using default SSH port in settings: defaultSSHPort')
                        res = port_scanner.is_port_opened(server=server,
                                                          port=settings.defaultSSHPort)
                    else:
                        res = port_scanner.is_port_opened(server=server, port='22')

                    if res:
                        return check_ssh_connectivity(server=server,
                                                      username=username,
                                                      password=password)
                    else:
                        log.error('general.check_network_connectivity() Using default port SSH Connectivity failed')
                        raise exceptions.SSHConnectivityError(
                            'general.check_network_connectivity() Using default port SSH Connectivity failed')

            elif mode == 'https':

                # =========================================================
                # Check HTTPS Connectivity to host
                # =========================================================

                log.info('general.check_network_connectivity() HTTPS mode. Checking HTTPS connectivity...')
                if httpsPort:
                    log.info('general.check_network_connectivity() HTTPS Port: {}'.format(httpsPort))
                    if port_scanner.is_port_opened(server=server, port=httpsPort):
                        res = check_https_connectivity(server=server,
                                                       port=httpsPort,
                                                       username=username,
                                                       password=password,
                                                       get_status_code=True)

                        if res:
                            if res == -1:
                                log.info('general.check_network_connectivity() Wrong credentials')
                                # raise exceptions.InvalidCredentials('Invalid HTTPS credentials')
                                return -1
                                # =========================================================
                                # HTTPS is good, check SSH now
                                # =========================================================

                        else:
                            log.error('general.check_network_connectivity() HTTPS Connectivity failed')
                            if get_status_code:
                                return -2
                            return False
                    else:
                        return False
                else:

                    # =========================================================
                    # Port is not defined Default HTTPS port is 443
                    # =========================================================

                    if hasattr(settings, 'defaultHTTPSPort'):
                        log.info(
                            'general.check_network_connectivity() using default HTTPS port in settings: defaultHTTPSPort')
                        res = port_scanner.is_port_opened(server=server,
                                                          port=settings.defaultHTTPSPort)
                    else:
                        res = port_scanner.is_port_opened(server=server,
                                                          port='443')

                    if res:
                        return check_https_connectivity(server=server,
                                                        username=username,
                                                        password=password)
                    else:
                        return False

            elif mode == 'vnc':
                log.info('general.check_network_connectivity() VNC Mode. Checking VNC connectivity...')
                log.info('general.check_network_connectivity() VNC Server: ' + server)
                log.info('general.check_network_connectivity() VNC Port: ' + vncPort)

                if (validator.check_authentication_parameters(server=server, vncPort=vncPort, vncPassword=vncPassword)):

                    # =========================================================
                    # Check VNC Connectivity to host, VNC requires no username
                    # =========================================================

                    log.info('general.check_network_connectivity() Checking if VNC port is opened...')
                    if vncPort:
                        log.info('general.check_network_connectivity() VNC port defined')
                        if port_scanner.is_port_opened(server=server, port=vncPort):
                            if vncPassword:
                                return check_vnc_connectivity(server=server,
                                                              password=vncPassword,
                                                              port=vncPort,
                                                              key=None)
                            else:

                                # =========================================================
                                # Use defined port. No password
                                # =========================================================

                                return check_vnc_connectivity(server=server,
                                                              port=vncPort,
                                                              key=None)
                        else:
                            return False
                    else:

                        # =========================================================
                        # Use Default port is 5901
                        # =========================================================

                        if hasattr(settings, 'defaultVNCPort'):
                            log.warn(
                                'general.check_network_connectivity() using default HTTPS port in settings: defaultVNCPort')
                            res = port_scanner.is_port_opened(server=server, port=settings.defaultVNCPort)

                            if res:
                                # Default password Configured

                                if hasattr(settings, 'defaultVNCPassword'):
                                    return check_vnc_connectivity(server=server,
                                                                  password=settings.defaultVNCPassword,
                                                                  port=vncPort)
                                else:
                                    return check_vnc_connectivity(server=server,
                                                                  password=password)
                            else:
                                return False

                        else:

                            # =========================================================
                            # No VNC port defined
                            # =========================================================

                            log.exception('general.check_network_connectivity() No VNC port defined')
                            return False
                else:
                    log.error('general.check_network_connectivity() Invalid VNC parameters')
                    return False

            else:

                # =========================================================
                # Invalid mode: all, https, ssh or vnc
                # =========================================================

                log.exception('general.check_network_connectivity() Invalid connectivity mode')
                raise exceptions.ConnectivityError('general.check_network_connectivity() Invalid connectivity mode')
        else:

            # =========================================================
            # Invalid mode type str_: all, https, ssh or vnc
            # =========================================================

            log.exception('general.check_network_connectivity() Invalid mode: ' + str(mode))
            raise exceptions.ConnectivityError(
                'general.check_network_connectivity() Invalid connectivity mode instance')

    except Exception, exception:
        log.exception(exception)


def check_ssh_connectivity(server=None, username=None, password=None, port=None, timeout=15, **kwargs):
    """
    :param server:
    :param port:
    :param username:
    :param password:
    :param timeout:
    :param kwargs:
    :return:
    Verify ssh connectivity to ESXi Server
    """

    log.info('general.check_ssh_connectivity()')
    # Default port is None which is default used in libraries
    # we would not check it unless is defined in settings.py
    # Verify all parameters are populated or valid.

    # Create SSH client
    try:
        client = SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        log.info('general.check_ssh_connectivity() Connecting to server ' + server + ' ...')
        if port:
            client.connect(server, port=int(port), username=username, password=password, timeout=timeout)
        else:
            client.connect(server, username=username, password=password, timeout=timeout)

        log.info("general.check_ssh_connectivity() SSH connection established to %s" % server)
        stdin, stdout, stderr = client.exec_command('hostname')
        err = stderr.readlines()
        output = stdout.readlines()
        if err:
            log.err("stderr: ")
            for line in err:
                log.error(line.rstrip())
            return False

        for line in output:
            log.info("general.check_ssh_connectivity() Connected to " + line.rstrip())
        log.info("general.check_ssh_connectivity() SSH Test connectivity is successful...")
        return True

    except paramiko.AuthenticationException:
        log.exception("general.check_ssh_connectivity() Authentication failed")
    except Exception, exception:
        log.exception("general.check_ssh_connectivity() SSH Test failed..." + str(exception))
    finally:
        client.close()


def check_https_connectivity(server=None, username=None, password=None, port=None, **kwargs):
    """
        Verify Https connectivity to ESXi Server
    :param server:
    :param username:
    :param password:
    :param port:
    :param kwargs:
    :return:
    """

    log.info('general.check_https_connectivity()')
    service_instance = None
    try:
        # Port defined
        # https://www.python.org/dev/peps/pep-0476/ Python 2.7.9 Enable Certificate validation
        import ssl
        if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context

        if port:
            service_instance = connect.SmartConnect(host=server,
                                                    port=int(port),
                                                    user=username,
                                                    pwd=password)
            log.info("general.check_https_connectivity() HTTPS Test connectivity is successful...")
            return True

        # Not port defined
        else:
            log.info('general.check_https_connectivity() HTTPS port not defined using default enterprise')
            service_instance = connect.SmartConnect(host=server,
                                                    port=443,
                                                    user=username,
                                                    pwd=password)
            log.info("general.check_https_connectivity() HTTPS Test connectivity is successful...")
            return True

    except IOError as exception:
        if not service_instance:
            log.info("general.check_https_connectivity() Could not connect to the specified host using specified "
                     "username and password" + str(exception))
            return False

    except vim.fault.InvalidLogin:
        log.exception("general.check_https_connectivity() HTTPS Test failed... Check credentials username/password")
        return -1

    except Exception, exception:
        log.exception("general.check_https_connectivity() HTTPS Test failed {0}".format(exception))
        raise

    finally:
        connect.Disconnect(service_instance)


def check_vnc_connectivity(server=None, password=None, port=None, key=None, **kwargs):
    """

    :param server:
    :param port:
    :param password:
    :param kwargs:
    #vncdo -s <ServerName>::<Port> key enter; echo $?
    :return:
    """
    log.info('general.check_vnc_connectivity()')
    if validator.check_authentication_parameters(server=server, password=password, vncPort=port):
        if password:
            if key:
                vncCommand = 'vncdo -s ' + server + '::' + port + ' -p ' + password + ' key ' + key + ' ; echo $?'
            else:
                vncCommand = 'vncdo -s ' + server + '::' + port + ' -p ' + password + ' key up ; echo $?'
        else:
            if key:
                vncCommand = 'vncdo -s ' + server + '::' + port + ' key ' + key + ' echo $?'
            else:
                vncCommand = 'vncdo -s ' + server + '::' + port + ' key up ; echo $?'

        # print 'general.check_vnc_connectivity() vnc command: ' + vncCommand

        try:
            # http://stackoverflow.com/questions/4760215/running-shell-command-from-python-and-capturing-the-output
            res = subprocess.Popen([vncCommand], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = res.communicate()
            # print 'general.check_vnc_connectivity() Out: ' + out.rstrip()
            if err:
                log.error('general.check_vnc_connectivity() Err: ' + err.rstrip())
                log.error('general.check_vnc_connectivity() VNC Failed')
                return False
            else:
                log.info('general.check_vnc_connectivity() VNC Successful')
                return True
        except Exception, exception:
            log.error('general.check_vnc_connectivity() Exception' + str(exception))
            return False

    else:
        return False


def check_local_file(filename=""):
    """

    :param filename:
    :return:
    """
    try:
        return os.path.isfile(filename)
    except Exception, exception:
        log.exception(exception.__repr__())
        return False


# Create a SSH Client object
def create_ssh_client(server=None, username=None, password=None, port=settings.defaultSSHPort, timeout=15,
                      private_key=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param timeout:
    :param private_key:
    :param kwargs:
    :return:
    """

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    if private_key:
        res = check_local_file(private_key)
        if res:
            log.info('create_ssh_client() private key exist')
            key = paramiko.RSAKey.from_private_key_file(private_key)
        else:
            log.error('create_ssh_client() private key doesnt exist')
            return

    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if private_key:
        client.connect(server, port=int(port), username=username, timeout=timeout, pkey=key)
    else:
        client.connect(server, port=int(port), username=username, password=password, timeout=timeout)

    return client


# Takes care of output when formatting information
def disable_paging(remote_conn):
    """

    :param remote_conn:
    :return:
    """
    '''Disable paging on a EsxI'''
    remote_conn.send("terminal length 0\n")
    time.sleep(1)
    # Clear the buffer on the screen
    output = remote_conn.recv(1000)
    return output


def connect_to_server(server=None, port=None, username=None, password=None, **kwargs):
    """
    :param server:
    :param port:
    :param username:
    :param password:
    :param kwargs:
    :return:
    """
    log.info('Using settings default parameters to initialize SSH connection')
    # Default port is None which is default used in libraries
    # we would not check it unless is defined in settings.py
    # Verify all parameters are populated or valid.
    # TODO need to check server is resolvable or valid IP address

    try:
        client = paramiko.SSHClient()
        # Automatically add untrusted hosts (make sure okay for security policy in your environment)
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if port:
            client.connect(server, port, username, password)
        else:
            client.connect(server, username=username, password=password)

        log.info("SSH connection established to %s" % server)
        # Use invoke_shell to establish an 'interactive session'
        remote_conn = client.invoke_shell()
        log.info("Interactive SSH session established")

        # output = remote_conn.recv(1000)
        # print output
        # Turn off paging
        disable_paging(remote_conn)
        return client
    except Exception, exception:
        log.exception(str(exception))
        return False


def run_command(command=None, display=False, **kwargs):
    """

    :param command:
    :param display:
    :param kwargs:
    :return:
    """
    try:
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if display:
            for line in p.stdout.readlines():
                print line,
        retval = p.wait()
        return retval
    except Exception, exception:
        log.exception(str(exception))
        return False


# Connects via SSH and executes a command remotely
def send_command(commandList=None,
                 server=None,
                 username=None,
                 password=None,
                 port=None,
                 timeout=settings.socketTimeout,
                 display=False,
                 close=False,
                 num_retries=3,
                 private_key=None,
                 **kwargs):
    """

    :param commandList:
    :param server:
    :param username:
    :param password:
    :param port:
    :param timeout:
    :param display:
    :param close:
    :param num_retries:
    :param private_key:
    :param kwargs:
    :return:
    """
    # Default port is None which is default used in libraries
    # we would not check it unless is defined in settings.py
    # Verify all parameters are populated or valid.
    # TODO need to check server is resolvable or valid IP address

    if commandList is None:
        # commandList = settings.cmd
        log.exception('general.send_command Invalid command')
        return False

    if not validator.check_authentication_parameters(server=server,
                                                     username=username,
                                                     password=password,
                                                     sshPort=port):
        log.error('general.send_command Invalid parameters')
        return False

    log.info('general.send_command() Command list: ' + str(commandList))

    if private_key:
        client = create_ssh_client(server, username, port, private_key=private_key, timeout=timeout)
    else:
        client = create_ssh_client(server, username, password, port, timeout=timeout)

    retry = 0
    while retry < num_retries:

        try:
            log.info("general.send_command() SSH connection established to %s" % server)
            for command in commandList:
                log.info("general.send_command() Executing command: " + command)
                stdin, stdout, stderr = client.exec_command(command)
                err = stderr.readlines()
                out = stdout.readlines()
                if err:
                    log.error("general.send_command() stderr: " + str(err))
                    log.error("general.send_command() Error executing command: " + str(command))
                    for line in err:
                        log.error(line.rstrip())
                    return False
                if out:
                    # Valid command no error
                    if display:
                        for line in out:
                            log.info(line.rstrip())
                        return out
                    else:
                        for line in out:
                            log.info(line.rstrip())
                if close:
                    client.close()

            # No output
            return

        except paramiko.BadHostKeyException:
            if settings.delete_host_key:
                log.error('BadHostKeyException Updating known_hosts: '.format(server))
                # ssh-keygen -f "/root/.ssh/known_hosts" -R [ziro1.noip.me]:8127
                call(['ssh-keygen -f "/root/.ssh/known_hosts" -R [' + server + ']:' + str(port)], shell=True)
                retry += 1

        except EOFError:
            log.info('general.send_command() Unexpected Error from SSH Connection, retry in 5 seconds')
            time.sleep(5)
            retry += 1


def transfer_file_scp(server=None,
                      username=None,
                      password=None,
                      port=None,
                      remoteFilePath=None,
                      localFile=None,
                      private_key=None,
                      check=False,
                      retries=3,
                      **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param remoteFilePath:
    :param localFile:
    :param private_key:
    :param check:
    :param retries:
    :param kwargs:
    :return:
    """
    log.info('general.transfer_file_scp()')  # We need to validate if local file exists
    if os.path.exists(localFile):
        log.info('general.transfer_file_scp Local file exist')
    else:
        log.exception('general.transfer_file_scp() Local file doesnt exist: ' + localFile)
        raise exceptions.LocalFileDoesntExist('general.transfer_file_scp() Local file doesnt exist: ' + localFile)

    if private_key:
        ssh = create_ssh_client(server, username, port, private_key=private_key)
    else:
        ssh = create_ssh_client(server, username, password, port)

    if ssh:
        scp = SCPClient(ssh.get_transport(), socket_timeout=settings.socketTimeout)
        log.info('general.transfer_file_scp() Server: ' + server)
        log.info('general.transfer_file_scp() Remote folder: ' + remoteFilePath)
        log.info('general.transfer_file_scp() Local file: ' + localFile)
        retry = 0
        while retry < retries:
            try:
                scp.put(remote_path=remoteFilePath, files=localFile)
                if check:  # Verify remote file exists
                    from hypervisor.esxi import file_operations
                    head, tail = os.path.split(localFile)
                    if file_operations.check_remote_file_exists(server=server,
                                                                username=username,
                                                                password=password,
                                                                port=port,
                                                                fileName=remoteFilePath + tail,
                                                                private_key=private_key):
                        log.info('general.transfer_file_scp() Check: File ' + localFile + \
                                 ' transferred successfully!')
                        return True
                    else:
                        log.warn('general.transfer_file_scp() File not found remotely...retrying')
                        time.sleep(5)
                        retry += 1
                log.info('general.transfer_file_scp() Check: File ' + localFile + ' transferred successfully!')
                return True

            except Exception as exception:
                log.exception('general.transfer_file_scp {}'.format(exception))
                time.sleep(5)
                retry += 1

        log.error('general.transfer_file_scp() File ' + localFile + ' transferred failed!')
        return False
    else:
        log.info('general.transfer_file_scp SSH instance was not created')
        return False


def get_file_scp(server=None, username=None, password=None, port=None, remoteFile=".", localPath='.', **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param remoteFile:
    :param localPath:
    :param kwargs:
    :return:
    """
    if localPath == '' or localPath == None:
        log.info('get_file_scp() Invalid directory path')
        return False
    if remoteFile == '' or remoteFile == None:
        log.warn('get_file_scp() Invalid file')
        return False

    if (validator.check_authentication_parameters(server=server,
                                                  username=username,
                                                  password=password,
                                                  sshPort=port)):
        pass
    else:
        log.error('general.get_file_scp Invalid parameters')
        return False

    ssh = create_ssh_client(server, username, password, port)
    # Add Fix for Socket timeout
    scp = SCPClient(ssh.get_transport(), socket_timeout=settings.socketTimeout)
    log.info('general.get_file_scp() Remote file: ' + remoteFile)
    log.info('general.get_file_scp() Local file:  ' + localPath)
    scp.get(remote_path=remoteFile, local_path=localPath)
    log.info("general.get_file_scp() File obtained successfully")
    return True
