__author__ = 'gogasca'

"""Lists all files on all datastores attached to managed datacenters."""
import time

from psphere.scripting import BaseScript
from psphere.client import Client


class DatastoreFiles(BaseScript):
    def list_files(self):
        for dc in self.client.find_entity_views("Datacenter"):
            print("Datacenter: %s" % dc.name)
            for ds in dc.datastore:
                print("Datastore: %s" % ds.info.name)

            for ds in dc.datastore:
                print("Datastore: %s" % ds.info.name)
                root_folder = "[%s] /" % ds.info.name
                task = ds.browser.SearchDatastoreSubFolders_Task(datastorePath=root_folder)

                while task.info.state == "running":
                    time.sleep(3)
                    task.update()

                for array_of_results in task.info.result:
                    # The first entry in this array is a type descriptor
                    # not a data object, so skip over it
                    for result in array_of_results[1:]:
                        for r in result:
                            try:
                                for f in r.file:
                                    print("%s%s" % (r.folderPath, f.path))
                            except AttributeError:
                                pass


def main():
    client = Client()
    dsf = DatastoreFiles(client)
    dsf.list_files()
    client.logout()


if __name__ == '__main__':
    main()