__author__ = 'gogasca'

from conf import settings
from utils import validator
from error import exceptions

def esx_validator():
    if hasattr(settings, 'esxServer'):  # Default param is defined
        if settings.esxServer != '' and isinstance(settings.esxServer, (str, unicode)):
            if validator.isIP(settings.esxServer):
                pass
            else:
                if validator.isValidHostName(settings.esxServer):
                    pass
                else:
                    raise exceptions.InvalidHostName('Invalid esxServer hostname')
                    return False
        else:
            print 'Invalid server. Check server esxServer'
            raise exceptions.InvalidHostName('Invalid server. Check server esxServer settings')
            return False
    else:
        raise exceptions.InvalidHostName('No esxServer parameter defined')  # No Server and not Default param is defined
        return False

    if hasattr(settings, 'esxUsername'):  # Default param is defined
        if settings.esxUsername != '' and isinstance(settings.esxUsername, (str, unicode)):
            if len(settings.esxUsername) < 255:
                pass
            else:
                raise exceptions.InvalidUserName('Invalid username length')
                return False
        else:
            raise exceptions.InvalidUserName('Invalid parameter username defined')
            return False
    else:
        raise exceptions.InvalidUserName(
            'No username parameter defined')  # No Password and not Default param is defined
        return False

    if hasattr(settings, 'esxPassword'):  # Default param is defined
        if settings.esxPassword != '' and isinstance(settings.esxPassword, (str, unicode)):
            if len(settings.esxPassword) < 255:
                pass
            else:
                raise exceptions.InvalidPassword('Invalid password length')
                return False
        else:
            raise exceptions.InvalidPassword('Invalid password in settings')
            return False
    else:
        raise exceptions.InvalidPassword(
            'No password parameter defined')  # No Password and not Default param is defined
        return False

    # SSH port is defined, we will use default value
    if hasattr(settings, 'esxSSHPort'):
        try:
            if settings.esxSSHPort != '' and isinstance(int(settings.esxSSHPort), int):
                if int(settings.esxSSHPort) > 0 and int(settings.esxSSHPort) < 65535:
                    pass
                else:
                    raise exceptions.InvalidPort('Invalid esxSSHPort port range defined')
            else:
                raise exceptions.InvalidPort('Invalid esxSSHPort port')

        except Exception:
            raise exceptions.InvalidPort('Invalid esxSSHPort port')
    else:
        # SSH port is defined, we will use default value
        if hasattr(settings, 'defaultSSHPort'):
            try:
                if settings.defaultSSHPort != '' and isinstance(int(settings.defaultSSHPort), int):
                    if int(settings.defaultSSHPort) > 0 and int(settings.defaultSSHPort) < 65535:
                        pass
                    else:
                        raise exceptions.InvalidPort('Invalid defaultSSHPort port range defined')
                else:
                    raise exceptions.InvalidPort('Invalid defaultSSHPort port')

            except Exception:
                raise exceptions.InvalidPort('Invalid defaultSSHPort port')
        else:
            raise exceptions.InvalidPort('Undefined esxSSHPort or defaultSSHPort port')


    # HTTPS port is defined, we will use default value
    if hasattr(settings, 'esxHTTPSPort'):
        try:
            if settings.esxHTTPSPort != '' and isinstance(int(settings.esxHTTPSPort), int):
                if int(settings.esxHTTPSPort) > 0 and int(settings.esxHTTPSPort) < 65535:
                    pass
                else:
                    raise exceptions.InvalidPort('Invalid esxHTTPSPort port range defined')
            else:
                raise exceptions.InvalidPort('Invalid esxHTTPSPort port')

        except Exception:
            raise exceptions.InvalidPort('Invalid esxHTTPSPort port')
    else:
        # Check if DEFAULT HTTPS is defined
        if hasattr(settings, 'defaultHTTPSPort'):
            try:
                if settings.defaultHTTPSPort != '' and isinstance(int(settings.defaultHTTPSPort), int):
                    if int(settings.defaultHTTPSPort) > 0 and int(settings.defaultHTTPSPort) < 65535:
                        pass
                    else:
                        raise exceptions.InvalidPort('Invalid defaultHTTPSPort port range defined')
                else:
                    raise exceptions.InvalidPort('Invalid defaultHTTPSPort port')

            except Exception:
                raise exceptions.InvalidPort('Invalid defaultHTTPSPort port')
        else:
            raise exceptions.InvalidPort('Undefined defaultHTTPSPort or defaultHTTPSPort port')