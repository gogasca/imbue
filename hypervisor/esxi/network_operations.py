__author__ = 'gogasca'
import logging
from utils import validator
from error import exceptions
from conf import logging_conf
from hypervisor.esxi import general
from pyVmomi import vim
from pyVim import connect

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def get_esx_version(server=None, username=None, password=None, port=None):
    """
    Get ESXi server information
    :param server:
    :param username:
    :param password:
    :param port:
    :return:
    """
    log.info('get_esx_version() ')
    command = ['echo -n $(esxcli system version get | grep -E \'Version|Build|Update\')']
    res = general.send_command(commandList=command,
                               server=server,
                               port=port,
                               username=username,
                               password=password,
                               display=True)
    if res:
        if res[0].encode('utf-8').rstrip():
            version = res[0].encode('utf-8').rstrip()
            log.info('get_esx_version() ESXi version: {}'.format(version))
            return version
    return


def get_networks(server=None, username=None, password=None, port=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param kwargs:
    :return:
    """
    try:
        import ssl
        if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context

        si = connect.SmartConnect(host=server, user=username, pwd=password, port=port)
        content = si.RetrieveContent()
        object_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                              [], True)
        networks = list()
        for obj in object_view.view:
            if isinstance(obj, vim.Network):
                log.info('Network: {}'.format(obj.name))
                networks.append(obj.name)

        object_view.Destroy()
        return networks

    except Exception:
        log.exception('get_networks()')

# Update firewall
# Issue#13
def list_firewall(server=None, username=None, password=None, port=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param kwargs:
    :return:
    """
    if validator.check_authentication_parameters(server=server, username=username, password=password, port=port):
        # esxcli network firewall ruleset set --enabled true --ruleset-id=gdbserver
        try:

            command = ['esxcli network firewall ruleset list | grep gdb | awk \'{print $2}\'']
            print "network_operations.update_firewall() Sending command..." + str(command)
            res = general.send_command(commandList=command,
                                       server=server,
                                       port=port,
                                       username=username,
                                       password=password,
                                       display=True)

            if res[0].encode('utf-8').rstrip():
                result = res[0].encode('utf-8').rstrip()
                if result == 'false':
                    logging.info('network_operations.list_firewall(): ' + res[0].encode('utf-8').rstrip())
                    return False
                elif result == 'true':
                    logging.info('network_operations.list_firewall(): ' + res[0].encode('utf-8').rstrip())
                    return True
                else:
                    raise exceptions.FirewallException('update_firewall Firewall list failed...')
            else:
                logging.exception("network_operations.list_firewall() list_firewall Firewall list failed...")
                raise exceptions.FirewallException('update_firewall Firewall list failed...')

        except Exception, e:
            logging.exception(str(e))
            raise
    else:
        logging.exception("network_operations.update_firewall() Firewall update failed...Invalid parameters")
        return False


def update_firewall(server=None, username=None, password=None, port=None, service='gdbserver', set='false', **kwargs):
    """
    Update firewall rules in ESXi
    :param server:
    :param username:
    :param password:
    :param port:
    :param service:
    :param set:
    :param kwargs:
    :return:
    """
    if validator.check_authentication_parameters(server=server, username=username, password=password, port=port):
        # esxcli network firewall ruleset set --enabled true --ruleset-id=gdbserver
        try:

            command = ['esxcli network firewall ruleset set --enabled ' + set + ' --ruleset-id=' + service]
            logging.info("network_operations.update_firewall() Sending command..." + str(command))
            res = general.send_command(commandList=command,
                                       server=server,
                                       port=port,
                                       username=username,
                                       password=password)
            if res is None:
                log.info('network_operations.update_firewall() firewall updated successfully...')
                return True
            else:
                logging.exception("network_operations.update_firewall() update_firewall Firewall updated failed...")
                return False

        except Exception, e:
            logging.exception(str(e))
    else:
        logging.error("network_operations.update_firewall() Firewall update failed...Invalid parameters")
        return False
