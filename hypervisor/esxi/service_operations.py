__author__ = 'gogasca'
import httplib
import re
import logging
from conf import settings, logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

# ###################################################################

# Server settings
debug_xml = False
server = settings.esxServer
port = settings.esxHTTPSPort
# Credentials
username = settings.esxUsername
password = settings.esxPassword
userAgent = settings.vSphereUA
operationId = '48E49BB5-0000000'
transaction = 1
SOAPAction = "urn:internalvim25/5.0"

""" Create HTTPS Client to send data to ESXi """


class HttpsClient(object):
    """

    """
    def __init__(self, server=None, port=None):
        self.server = server
        self.port = port
        self.debug = debug_xml

    def set_debug(self):
        # Debug
        if self.debug:
            httplib.HTTPSConnection.debuglevel = 1

    def connect(self):
        log.info('HTTPS client connecting...')
        https_client_headers = {'User-Agent': userAgent,
                                'Host'      : self.server, 'Connection': 'Keep-Alive'}
        try:

            # Disable Certificate validation for ESXi
            import ssl
            log.info('Check certificate is enabled')

            conn = httplib.HTTPSConnection(self.server, self.port)
            conn.set_debuglevel(1)
            log.info(https_client_headers)
        except ImportError:
            log.exception("No SSL support")
        except Exception, e:
            log.exception("Exception found: " + str(e))

        try:
            conn.request("GET", "/client/clients.xml",
                         headers=https_client_headers)
            log.info("----->>>")
            r1 = conn.getresponse()
            data = r1.read()
            reply = r1.getheaders()
            if self.debug:
                log.info("<<<-----")
                log.info(r1.msg)
                log.info(str(r1.status) + ' ' + str(r1.reason))
                log.info(reply)
                log.info(data)

        except Exception, e:
            log.exception(str(e))


""" Generate OperationId for Services enablement"""


def generateOperationId():
    """

    :return:
    """
    global transaction
    global operationId
    if transaction > 10:
        operationId = operationId[:-1]

    operationId = operationId + str(transaction)
    transaction += 1

    return operationId


""" Create SOAP Client to connect to ESXi and enable functions"""


class SoapClient():
    def __init__(self, server=None, port=None, headers=None, xml=None, debug=False, cookie=False, **kwarg):
        self.server = server
        self.port = port
        self.headers = headers
        self.xml = xml
        self.debug = debug
        self.cookie = cookie
        self.conn = httplib.HTTPSConnection(self.server, self.port, timeout=15)
        self.reply = None

    def connect(self, headers=None, xml=None, debug=False, cookie=False, close=False, **kwargs):
        if debug:
            httplib.HTTPConnection.debuglevel = 1

        # self.conn._set_hostport(host=self.host,port=self.port)
        self.conn.request("POST", "/sdk", body=xml, headers=headers)
        log.info("----->>>")
        r1 = self.conn.getresponse()
        data = r1.read()
        self.reply = r1.getheaders()
        if debug:
            log.info("<<<-----")
            log.info(r1.msg)
            log.info(str(r1.status) + ' ' + str(r1.reason))
            log.info(self.reply)
            log.info(data)

        if close:
            self.conn.close()

    def getCookie(self):
        log.info("Processing: " + str(self.reply))
        if type(self.reply) is list:
            # Convert tuple to dictionary
            headers = dict(self.reply)
            cookie = headers['set-cookie']
            # ('set-cookie', 'vmware_soap_session="52ba1bb0-5c86-688d-d21b-f04a0fda79ac"; Path=/; HttpOnly; Secure;')
            m = re.search("vmware_soap_session=\"(.+?)\";", cookie)
            if m:
                found = m.group(1)
                log.info("Cookie found: " + str(found))
                return found
            else:
                log.info("Cookie not found")
                return None
        else:
            return None


def ssh_service_activation(server=None, username=None, password=None, port=None, operation=1, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param port:
    :param operation:
    :param kwargs:
    :return:
    """
    try:
        log.info('ssh_service_activation() Creating HTTPS client...')
        # Port comes as string.
        https_client = HttpsClient(server, port)
        https_client.connect()

        log.info('ssh_service_activation() Defining HTTP Headers')

        http_headers = {'User-Agent'  : userAgent, 'Host': server, 'Connection': 'Keep-Alive',
                        'Content-Type': 'application/xml; charset="utf-8"', 'SOAPAction': SOAPAction}

        log.info(http_headers)
        log.info('ssh_service_activation() Creating SOAP Client')
        sc = SoapClient(server, port)

        sc.connect(headers=http_headers, xml="""
        <soap:Envelope
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                <soap:Body>
                    <RetrieveServiceContent xmlns="urn:internalvim25">
                        <_this xsi:type="ManagedObjectReference" type="ServiceInstance" serverGuid="">ServiceInstance</_this>
                    </RetrieveServiceContent>
                </soap:Body>
        </soap:Envelope>
        """, debug=debug_xml, cookie=True)
        cookie = sc.getCookie()
        # Set Cookie
        log.info('ssh_service_activation() Setting cookie: ' + cookie)
        http_headers['Cookie'] = 'vmware_soap_session=\"' + cookie + '\"'
        log.info(str(http_headers))

        # Increment Transaction
        # RetrieveInternalContent

        sc.connect(headers=http_headers, xml="""
        <soap:Envelope
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                <soap:Body>
                    <RetrieveInternalContent xmlns="urn:internalvim25">
                        <_this xsi:type="ManagedObjectReference" type="ServiceInstance" serverGuid="">ServiceInstance</_this>
                    </RetrieveInternalContent>
                </soap:Body>
        </soap:Envelope>
        """, debug=debug_xml)

        # http_headers.pop('')("key", None)

        sc.connect(headers=http_headers, xml="""
        <soap:Envelope
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                <soap:Body>
                    <Login xmlns="urn:internalvim25">
                        <_this xsi:type="ManagedObjectReference" type="SessionManager" serverGuid="">ha-sessionmgr</_this>
                        <userName>""" + username.strip() + """</userName>
                        <password>""" + password.strip() + """</password>
                        <locale>en_US</locale>
                    </Login>
                </soap:Body>
        </soap:Envelope>
        """, debug=debug_xml)

        if operation == 0:
            # Stop service
            log.info('ssh_service_activation() Stopping service...')
            sc.connect(headers=http_headers, xml="""
            <soap:Envelope
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                    <soap:Body>
                        <StopService xmlns="urn:internalvim25">
                        <_this xsi:type="ManagedObjectReference" type="HostServiceSystem" serverGuid="">serviceSystem</_this>
                        <id>TSM-SSH</id>
                        </StopService>
                    </soap:Body>
            </soap:Envelope>
            """, debug=debug_xml)

        if (operation == 1):
            log.info('ssh_service_activation() Starting service...')
            sc.connect(headers=http_headers, xml="""
            <soap:Envelope
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                    <soap:Body>
                        <StartService xmlns="urn:internalvim25">
                        <_this xsi:type="ManagedObjectReference" type="HostServiceSystem" serverGuid="">serviceSystem</_this>
                        <id>TSM-SSH</id>
                        </StartService>
                    </soap:Body>
            </soap:Envelope>
            """, debug=debug_xml)

        log.info('ssh_service_activation() Updating policy service...')
        sc.connect(headers=http_headers, xml="""
        <soap:Envelope
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                <soap:Body>
                    <UpdateServicePolicy xmlns="urn:internalvim25">
                    <_this xsi:type="ManagedObjectReference" type="HostServiceSystem" serverGuid="">serviceSystem</_this>
                    <id>TSM-SSH</id>
                    <policy>on</policy>
                    </UpdateServicePolicy>
                </soap:Body>
        </soap:Envelope>
        """, debug=debug_xml)

        log.info('ssh_service_activation() Logout...')
        sc.connect(headers=http_headers, xml="""
        <soap:Envelope
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Header><operationID>""" + generateOperationId() + """</operationID></soap:Header>
                <soap:Body>
                    <Logout xmlns="urn:internalvim25">
                    <_this xsi:type="ManagedObjectReference" type="SessionManager" serverGuid="">ha-sessionmgr</_this>
                    </Logout>
                </soap:Body>
        </soap:Envelope>
        """, debug=debug_xml)

        import time

        log.info('ssh_service_activation Sleeping for 5 seconds...')
        time.sleep(5)
        return True

    except Exception, e:
        log.exception(str(e))
        return False

# ssh_service_activation(server='110.10.0.144',username='root',password='password',operation=1)
