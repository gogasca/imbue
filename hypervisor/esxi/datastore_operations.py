import platform
import sys
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append(
        '/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
import atexit
import logging
from pyVim import connect
from pyVmomi import vim
from pyVmomi import vmodl
from hypervisor.esxi.tools import vm
from error import exceptions
from conf import settings, logging_conf
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

__author__ = 'gogasca'


def parse_service_instance(service_instance):
    """
    Print some basic knowledge about your environment
    """
    log.info('datastore_operations.parse_service_instance()')
    content = service_instance.RetrieveContent()
    object_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                          [], True)
    for obj in object_view.view:
        log.info(obj)
        if isinstance(obj, vim.VirtualMachine):
            vm.print_vm_info(obj)
            #log.info('datastore_operations.parse_service_instance() VM found')
    object_view.Destroy()
    return


# http://stackoverflow.com/questions/1094841/
def sizeof_fmt(num):
    """
    Returns the human readable version of a file size

    :param num:
    :return:
    """
    for item in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0:
            return "%3.1f%s" % (num, item)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')


def print_datastore_format(fs):
    """

    :param fs:
    :return:
    """
    print "{}\t{}\t".format("Datastore:     ", fs.volume.name)
    print "{}\t{}\t".format("UUID:          ", fs.volume.uuid)
    print "{}\t{}\t".format("Capacity:      ", sizeof_fmt(fs.volume.capacity))
    print "{}\t{}\t".format("VMFS Version:  ", fs.volume.version)
    print "{}\t{}\t".format("Is Local VMFS: ", fs.volume.local)
    print "{}\t{}\t".format("SSD:           ", fs.volume.ssd)


def print_fs(host_fs, datastoreName):
    """
    Prints the host file system volume info
    :param host_fs:
    :return:
    """
    log.info("{}\t{}\t".format("Datastore:     ", host_fs.volume.name))
    log.info("{}\t{}\t".format("UUID:          ", host_fs.volume.uuid))
    log.info("{}\t{}\t".format("Capacity:      ", sizeof_fmt(
        host_fs.volume.capacity)))
    log.info("{}\t{}\t".format("VMFS Version:  ", host_fs.volume.version))
    log.info("{}\t{}\t".format("Is Local VMFS: ", host_fs.volume.local))
    log.info("{}\t{}\t".format("SSD:           ", host_fs.volume.ssd))

    log.info("Searching: " + datastoreName)
    if datastoreName == host_fs.volume.name:
        return host_fs.volume.uuid
    else:
        log.exception('Undefined Datastore')
        raise exceptions.UndefinedDataStore('Undefined Datastore')


# Print Datastore directories
def print_datastore_directories(server=None,
                                username=None,
                                password=None,
                                httpsPort=None,
                                datastoreName=None,
                                **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastoreName:
    :param kwargs:
    :return:
    """
    try:
        log.info("datastore_operations.print_datastore_directories()")
        serviceInstance = connect.SmartConnect(host=server,
                                               user=username,
                                               pwd=password,
                                               port=int(httpsPort))
        if not serviceInstance:
            log.info("datastore_operations.print_datastore_directories() Could not connect to the specified host")
            return -1

        # Disconnect cleanly
        atexit.register(connect.Disconnect, serviceInstance)

        # ## Do the actual parsing of data ###
        parse_service_instance(serviceInstance)

    except vmodl.MethodFault, e:
        log.exception(
            "datastore_operations.print_datastore_directories() Caught vmodl fault : " + str(e.msg))

    except Exception, e:
        log.exception(str(e))

    # Return no error
    return 0


def get_datastores_details(server=None, username=None, password=None, https_port=None, json_format=False, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param kwargs:
    :return:
    """
    try:
        import json
        si = None
        try:
            si = connect.SmartConnect(host=server,
                                      user=username,
                                      pwd=password,
                                      port=int(https_port))
        except IOError, e:
            pass
        if not si:
            log.error("Could not connect to the specified host using specified username and password")
            return -1

        atexit.register(connect.Disconnect, si)

        content = si.RetrieveContent()
        # Search for all ESXi hosts
        objView = content.viewManager.CreateContainerView(content.rootFolder, [vim.HostSystem], True)
        esxi_hosts = objView.view
        objView.Destroy()

        datastores = {}
        for esxi_host in esxi_hosts:
            if json_format == False:
                print "{}\t{}\t\n".format("ESXi Host:    ", esxi_host.name)

            # All Filesystems on ESXi host
            fss = esxi_host.configManager.storageSystem.fileSystemVolumeInfo.mountInfo

            datastore_dict = {}
            # Map all filesystems
            for fs in fss:
                # Extract only VMFS volumes
                if fs.volume.type == "VMFS":

                    extents = fs.volume.extent
                    if json_format == False:
                        print_datastore_format(fs)
                    else:
                        datastore_details = {'uuid'        : fs.volume.uuid, 'capacity': fs.volume.capacity,
                                             'vmfs_version': fs.volume.version, 'local': fs.volume.local,
                                             'ssd'         : fs.volume.ssd}

                    extent_arr = []
                    extent_count = 0
                    for extent in extents:
                        if json_format == False:
                            print "{}\t{}\t".format("Extent[" + str(extent_count) + "]:", extent.diskName)
                            extent_count += 1
                        else:
                            # create an array of the devices backing the given datastore
                            extent_arr.append(extent.diskName)
                            # add the extent array to the datastore info
                            datastore_details['extents'] = extent_arr
                            # associate datastore details with datastore name
                            datastore_dict[fs.volume.name] = datastore_details
                    if json_format == False:
                        print

            # associate ESXi host with the datastore it sees
            datastores[esxi_host.name] = datastore_dict

        if json_format:
            return json.dumps(datastores)

    except vmodl.MethodFault, e:
        log.exception("Caught vmodl fault : " + e.msg)
        return -1
    except Exception, e:
        log.exception("Caught exception : " + str(e))
        return -1


# get Server UUDI
def get_uuid(server=None, username=None, password=None, httpsPort=None, **kwargs):
    """
   Simple command-line program for listing all ESXi datastores and their
   associated devices
   """
    log.info('datastore_operations.get_UUID()')
    # We use Vsphere Port!
    if httpsPort is None and hasattr(settings, 'defaultHTTPSPort'):
        httpsPort = settings.defaultHTTPSPort

    try:
        service_instance = connect.SmartConnect(host=server,
                                                port=int(httpsPort),
                                                user=username,
                                                pwd=password)
        if not service_instance:
            log.error("datastore_operations.printDSDirectory() Could not connect to the specified host using specified "
                      "username and password")
            return -1

        atexit.register(connect.Disconnect, service_instance)
        content = service_instance.RetrieveContent()
        # Search for all ESXi hosts
        objview = content.viewManager.CreateContainerView(content.rootFolder,
                                                          [vim.HostSystem],
                                                          True)
        esxi_hosts = objview.view
        objview.Destroy()

        for esxi_host in esxi_hosts:
            log.info("{}\t{}\t".format("ESXi Host:    ", esxi_host.name))
            # All Filesystems on ESXi host
            storage_system = esxi_host.configManager.storageSystem
            host_file_sys_vol_mount_info = \
                storage_system.fileSystemVolumeInfo.mountInfo

            datastore_dict = {}
            # Map all filesystems
            for host_mount_info in host_file_sys_vol_mount_info:
                # Extract only VMFS volumes
                if host_mount_info.volume.type == "VMFS":
                    UUID = host_mount_info.volume.uuid

        if UUID:
            log.info("{}\t{}\t".format("UUID:    ", UUID))
            return UUID
        else:
            from error import exceptions
            raise exceptions.UndefinedDataStore('Invalid UUID')

    except vmodl.MethodFault, e:
        log.exception("Caught vmodl fault : " + str(e.msg))
        return -1

    except Exception, e:
        log.exception(str(e))
        raise

    return 0


# Validate datastore exists
def find_datastore_list(server=None, username=None, password=None, httpsPort=None, datastoreName=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param datastoreName:
    :param kwargs:
    :return:
    """
    log.info('datastore_operations.find_datastore_list()')
    # This port is the Vsphere HTTPS port!

    try:
        log.info('datastore_operations.find_datastore_list(): ' + datastoreName)
        service_instance = None
        try:
            service_instance = connect.SmartConnect(host=server,
                                                    port=int(httpsPort),
                                                    user=username,
                                                    pwd=password)
        except IOError as e:
            log.exception(
                'datastore_operations.find_datastore_list() ' + str(e))
            raise
        except Exception, e:
            log.exception(str(e))
            raise

        if not service_instance:
            log.error(
                "datastore_operations.find_datastore_list() Could not connect to the specified host using specified "
                "username and password")
            return

        # Ensure that we cleanly disconnect in case our code dies
        atexit.register(connect.Disconnect, service_instance)

        content = service_instance.RetrieveContent()
        session_manager = content.sessionManager

        # Get the list of all datacenters we have available to us
        datacenters_object_view = content.viewManager.CreateContainerView(
            content.rootFolder,
            [vim.Datacenter],
            True)

        # Find the datastore and datacenter we are using
        isFound = False

        for dc in datacenters_object_view.view:
            datastores_object_view = content.viewManager.CreateContainerView(
                dc, [vim.Datastore], True)
            for ds in datastores_object_view.view:
                if datastoreName == ds.info.name:
                    log.info(
                        'datastore_operations.find_datastore_list() Datastore found: ' + ds.info.name)
                    isFound = True
                    break

        # Clean up the views now that we have what we need
        datastores_object_view.Destroy()
        datacenters_object_view.Destroy()
        return isFound

    except vmodl.MethodFault, e:
        log.exception(
            "datastore_operations.find_datastore_list() Caught vmodl fault : " + str(e.msg))

    except Exception, e:
        log.exception(str(e))


def get_datastore_list(server=None, username=None, password=None, httpsPort=None, **kwargs):
    """

    :param server:
    :param username:
    :param password:
    :param httpsPort:
    :param kwargs:
    :return:
    """
    log.info('datastore_operations.get_datastore_list()')

    try:
        log.info(
            'datastore_operations.get_datastore_list() Settings: |'
            ' server ' + server +
            ' username: ' + username +
            ' HTTPS: ' + str(httpsPort))

        try:
            service_instance = connect.SmartConnect(host=server,
                                                    port=int(httpsPort),
                                                    user=username,
                                                    pwd=password)
        except IOError as e:
            log.exception(
                "datastore_operations.get_datastore_list() IOError Exception " + str(e))
            return None
        except Exception, e:
            log.exception(str(e))
            return None

        if not service_instance:
            log.error(
                "datastore_operations.get_datastore_list() Could not connect to the specified host...")
            return

        # Ensure that we cleanly disconnect in case our code dies
        atexit.register(connect.Disconnect, service_instance)

        content = service_instance.RetrieveContent()
        session_manager = content.sessionManager

        # Get the list of all datacenters we have available to us
        datacenters_object_view = content.viewManager.CreateContainerView(
            content.rootFolder,
            [vim.Datacenter],
            True)

        # Find the datastore and datacenter we are using
        datacenter = None
        datastores = []

        for dc in datacenters_object_view.view:
            datastores_object_view = content.viewManager.CreateContainerView(
                dc, [vim.Datastore], True)
            for ds in datastores_object_view.view:
                datastores.append(ds.info.name)

        # Clean up the views now that we have what we need
        datastores_object_view.Destroy()
        datacenters_object_view.Destroy()
        log.info('datastore_operations.get_datastore_list() Found datastores {0}'.format(datastores))

        return datastores

    except vmodl.MethodFault, e:
        log.exception(
            "datastore_operations.get_datastore_list() Caught vmodl fault: " + str(e))

    except Exception, e:
        log.exception(str(e))


__version__ = '0.1'
