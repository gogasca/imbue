__author__ = 'gogasca'

import platform
import sys
from twilio.rest import TwilioRestClient
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/imbue/application/imbue/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app/')
from conf import settings

def send_sms_alert(body='Parzee', destination_number=None, **kwargs):
    """
    Send alarms via Twilio SMS API
    :param body:
    :param destination_number:
    :param kwargs:
    :return:
    """
    try:
        if settings.sms_alerts:
            account_sid = settings.twilio_accountId
            auth_token = settings.twilio_tokenId
            client = TwilioRestClient(account_sid, auth_token)

            if destination_number:
                message = client.messages.create(to=destination_number, from_=settings.twilio_from,body=body)
                print str(message)
            else:
                for number in settings.phone_numbers:
                    client.messages.create(to=number, from_=settings.twilio_from,body=body)

    except Exception,e:
        print str(e)

if __name__ == '__main__':

    # python send_sms.py phone_number body

    if 2 <= len(sys.argv) <= 3:
        if len(sys.argv) == 2:
            send_sms_alert(body=sys.argv[1])
        if len(sys.argv) == 3:
            send_sms_alert(body=sys.argv[1],destination_number=sys.argv[2])
    else:
        print 'Invalid arguments python send_sms.py "Install successful" "+14082186575" | send_sms message E164'