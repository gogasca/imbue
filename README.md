# IMBUE version 1.0
-------------------------

#Application installation (Includes PostgreSQL)
-----------------------------------------------

    apt-get update; sudo apt-get upgrade -y; sudo apt-get dist-upgrade -y; sudo apt-get install -y build-essential git-core
    apt-get install python-dev -y python-pip libjpeg-dev zlib1g-dev tesseract-ocr libpq-dev imagemagick supervisor -y    
    apt-get install postgresql-client-9.3
    apt-get install rabbitmq-server -y    
    apt-get install curl -y
    apt-get install lighttpd -y
    apt-get install php5-common php5-cgi php5 -y
    apt-get install libffi-dev -y
    
    lighty-enable-mod fastcgi-php
    service lighttpd force-reload
    
    apt-get install postgresql postgresql-contrib -y 
    
    mkdir -p /usr/local/src/imbue/application

Git clone
------------------------------------------------------------------------------

```
#!python

git clone https://gogasca@bitbucket.org/gogasca/imbue.git
```

        
#Install defined libraries

	pip install -r requirements.txt

#Install VMWare API, VNCdotool and Flower

	pip install --upgrade pyvmomi
	pip install vncdotool
    pip install coverage
    pip install flower

If getting errors: 

ImportError: cannot import name 'IncompleteRead'
Take a look at:
http://stackoverflow.com/questions/27341064/how-do-i-fix-importerror-cannot-import-name-incompleteread

    Please run:
    easy_install -U pip

#Encryption
------------------------------------------------------------------------------

    Our mini web server handle HTTP request to encrypt passwords
    http://www.penguintutor.com/linux/light-webserver

Image recognition software:
------------------------------------------------------------------------------
    pip install -I Pillow

Testing:
------------------------------------------------------------------------------
Code coverage

    http://pymbook.readthedocs.org/en/latest/testing.html
    pip install coverage

Code analysis

    pylint
    Example: pylint hypervisor/esxi/deploy_esx.py
    pip install pylint

Configuration
------------------------------------------------------------------------------

Define .bashrc

```
#!python

alias 'imbue=cd /usr/local/src/imbue/application/imbue/'
export C_FORCE_ROOT="true"

```

Install floppy drive scripts
 - mkdir -p /usr/local/src/imbue/
 - cd /usr/local/src/imbue/
 - mkdir application
 - mkdir -p customers/test
 - mkdir utils

Install and configure supervisord
------------------------------------------------------------------------------
The program configuration files for Supervisor programs are found in the /etc/supervisor/conf.d directory, 
normally with one program per file and a .conf extension

Install cronjob
------------------------------------------------------------------------------
crontab -e

```
#!python

0 * * * * imbue && cat /dev/null > log/celeryd.err
0 * * * * imbue && cat /dev/null > log/imbueapid.err
* * * * * imbue && find log/*.log -mtime +7 -delete

```

Database
------------------------------------------------------------------------------
Configure:
PostgreSQL 9.5

Enable all interfaces to listen on port 5432
```
#!python

/etc/postgresql/9.3/main/postgresql.conf
listen_addresses = '*'          # what IP address(es) to listen on;

Add remote IP addresses connecting to DB:
 /etc/postgresql/9.5/main/pg_hba.conf

```
Export database
------------------------------------------------------------------------------
    su imbue
    pg_dump -s imbuedb > imbuedb.db
------------------------------------------------------------------------------

Import database
--------
Default password for user postgres is "postgres"
For user imbue you can use password as "imbue"


--------------------------
    sudo -i -u postgres
    createuser --interactive
    Enter name of role to add: imbue
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) n
    Shall the new role be allowed to create more new roles? (y/n) n

    createuser --interactive
    Enter name of role to add: root
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) n
    Shall the new role be allowed to create more new roles? (y/n) n

    sudo passwd imbue


    psql -c '\l'
    psql -d template1
    template1=> CREATE DATABASE imbuedb WITH OWNER imbue ENCODING 'UTF8';
    CREATE DATABASE
    template1=> grant all privileges on database imbuedb to imbue;
    template1=> grant all privileges on database imbuedb to root;
    template1=> ALTER USER "imbue" WITH PASSWORD 'imbue';
    template1=> ALTER USER "root" WITH PASSWORD 'imbue';
    template1=> ALTER ROLE root WITH Superuser;
    template1=>\q

    psql imbuedb < imbuedb.db


#RabbitMQ

```
#!shell

rabbitmqctl add_user imbue imbue
rabbitmqctl set_user_tags imbue administrator
rabbitmqctl set_permissions -p / imbue ".*" ".*" ".*"

```

#Docker

```
#!shell

docker run -t -i ubuntu /bin/bash

Install application and exit

docker ps -a
docker commit 5e9fc9f179a5 parzee/imbue
docker run -t -i parzee/imbue /bin/bash

docker build -t parzee/application .
docker build -t parzee/loadbalancer .
docker build -t parzee/lighttpd .
docker build -t parzee/rabbitmq-server .
docker build -t parzee/database .

Go to imbue folder

docker-compose up -d

```
Please read Docker configuration document

#Troubleshooting

Time to connect via API

curl -o /dev/null -s -w %{time_connect}:%{time_starttransfer}:%{time_total} -u ACda38cb38f1654-00b439724edec211c1:43b6e91ca96217e0be09c6f3fe94ec31 -H "Content-Type: application/json" http://0.0.0.0:8080/api/1.0/host/

http://virtuallygone.net/2010/03/06/thin-provisioning-slow/

Known issues:
Error:

```
#!shell

InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. For more information
```

Celery
http://blog.mapado.com/task-specific-logging-in-celery/


http://stackoverflow.com/questions/29099404/ssl-insecureplatform-error-when-using-requests-package