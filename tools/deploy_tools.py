import warnings
warnings.filterwarnings("ignore")
import logging
from alarms import send_sms
from celery.task import task
from conf import logging_conf
from conf import settings
from database import db_connection
from database import Db
import utils.helper, utils.validator, tools.Tools
from utils import helper


@task(bind=True, default_retry_delay=300, max_retries=3)
def imbue_tools(self, configuration,
                job=None,
                log_file="", **kwargs):
    """

    :param self:
    :param configuration:
    :param job:
    :param log_file:
    :param kwargs:
    :return:
    """

    # =========================================================
    # Process Tools server
    # =========================================================

    log = logging_conf.LoggerManager().getLogger(
        "__app___", logging_file=log_file)
    log.setLevel(level=logging.DEBUG)
    log.info(
        '---------------------------------------Initializing IMBUE Tools---------------------------------------')
    error_detected = False

    log.info('deploy_tools() API call request: ' + str(configuration))
    log.info(str(type(configuration)))

    try:

        log.info('deploy_tools() Tools Connecting to database...')

        # =========================================================
        # Connect to database
        # =========================================================

        if db_connection.start():

            log.info('deploy_tools() Database connection is successful')
            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            # =========================================================
            # Initialize Database
            # =========================================================
            if db_instance.initialize():
                log.info('deploy_tools() Database initialization is successful')
            else:
                log.error('deploy_tools() Database initialization failed')
                return

            # Create Query
            if job.id:
                log.info('deploy_tools() Updating Job status')
                sqlquery = """UPDATE job SET status=0,description='Initializing Tools' WHERE job.id=""" + \
                           str(job.id)
                # Update status to 1 for all new jobs
                helper.update_database(sqlquery)
            else:
                log.error('deploy_tools() No valid Job')
                return
        else:
            message = 'Unable to connect to database'
            log.exception('deploy_tools() ' + message)
            send_sms.send_sms_alert(
                body=message + ' : ' + str(job.reference))

            # =========================================================
            # Update Celery TASK
            # =========================================================
            self.update_state(state='FAILURE', meta={'status': message})
            raise RuntimeError

    except KeyError:
        log.info('deploy_tools() Tools Manual mode. No DB key defined')

    except Exception, e:
        log.exception('deploy_tools() ' + e.message)
        return

    try:
        # =========================================================
        # Check Tools server information *****
        # =========================================================

        try:

            if configuration['infrastructure']['instance']['type'] == settings.tools_type:

                # =========================================================
                # Read Tools server
                # =========================================================

                tools_name = configuration['infrastructure']['instance']['name']
                if utils.validator.isValidHostName(tools_name):
                    # Create instance from YAML
                    log.info('deploy_tools() Tools server found')
                    tools1 = tools.Tools.Tools(name=tools_name)
                else:
                    error_detected = True
                    message = 'Invalid Tools Server ' + tools_name
                    log.error('deploy_tools() ' + message)
                    helper.generate_alert(job.id, message, job.reference)

                    # =========================================================
                    # Send ALERT
                    # =========================================================
                    self.update_state(state='FAILURE', meta={'status': message})
                    helper.generate_alert(job.id, message, job.reference)
                    return False
            else:
                raise KeyError

        except KeyError:
            # Tools server config doesnt exist. Check if exist in default configuration
            log.warn('deploy_tools() No valid Infrastructure Server defined. Using default configuration')
            if settings.tools_primary:
                tools_name = settings.tools_primary
                tools1 = tools.Tools.Tools(name=tools_name)
            else:
                error_detected = True
                message = 'ERROR: No Tools server found'
                log.error('deploy_tools() ' + message)
                helper.generate_alert(job.id, message, job.reference)
                # =========================================================
                # Send ALERT
                # =========================================================
                self.update_state(state='FAILURE', meta={'status': message})
                helper.generate_alert(job.id, message, job.reference)
                return False

        except Exception, e:
            log.exception('deploy_tools() Exception' + str(e))
            return False

        # =========================================================
        # Initialize Tools server. Tools server takes care of creating floppy image.
        # =========================================================

        if tools1.get_tools_server(name=tools_name):
            log.info('deploy_tools() Tools server found in database. Using database value: ' + tools1.id)
            if tools1.update(name=tools_name, configuration=configuration):
                log.info('deploy_tools() Updating Tools server information')
            else:
                log.error('deploy_tools() Unable to update Tools server information')
                raise RuntimeError

        else:
            # When there is no Tools server in database. Add it
            try:
                if configuration['infrastructure']['instance']['ip']:
                    # *********** Database mode read if tools server exists, otherwise insert it into database

                    tools1.insert(ip_address=configuration['infrastructure']['instance']['ip'],
                                  properties='{"ssh": { "active": 1, "port": ' +
                                             str(configuration['infrastructure']['instance']['ssh']) + ' }}',
                                  ssh_username=configuration['infrastructure']['instance']['username'],
                                  ssh_password=configuration['infrastructure']['instance']['password'])

            except KeyError:
                log.info('deploy_tools() Adding new Tools server using default parameters')
                tools1.insert(ip_address=settings.tools_hostname,
                              properties='{"ssh": { "active": 1, "port": ' + str(settings.tools_port) + ' }}',
                              ssh_username=settings.tools_username,
                              ssh_password=settings.tools_enc_password)

        sqlquery = """UPDATE job SET status=0,description='Tools server found' WHERE job.id=""" + str(job.id)
        db_instance.update(sqlquery)

        # =========================================================
        # Initializing
        # =========================================================

        if tools1.initialize(name=tools_name, update=False):
            log.info('deploy_tools() Tools server initialized')

            # =========================================================
            # Job
            # =========================================================

            import json

            if job:
                log.info('deploy_tools() Updating Tools information')
                job.update_info(tools1.id, None)
                server_json = json.dumps(job.server_info)

            else:
                server_json = '{}'
                log.warn('deploy_tools() Job server info not defined')

            sqlquery = """UPDATE job SET is_cluster=False, host_information='""" + server_json + """' WHERE job.id=""" + str(
                job.id)
            helper.update_database(sqlquery)

        else:
            log.info('deploy_tools() Tools server not initialized')
            return False

        # ************ Discover Tools server **********************************************************************

        if tools1.discover():
            log.info('deploy_tools() Tools server discovered successfully')
            sqlquery = """UPDATE job SET status=1,description='Tools server found' WHERE job.id=""" + str(job.id)
            db_instance.update(sqlquery)
            sqlquery = """UPDATE tools SET status=0 WHERE tools.hostname='""" + str(tools_name) + "'"
            db_instance.update(sqlquery)
        else:
            error_detected = True
            message = 'ERROR: Tools server down'
            log.error('deploy_tools() ' + message)
            helper.generate_alert(job.id, message, job.reference)
            sqlquery = """UPDATE tools SET status=-1 WHERE tools.hostname='""" + str(tools_name) + "'"
            db_instance.update(sqlquery)
            return False

        log.info('deploy_tools() Tools server is successfully connected')
        return tools1

    except Exception, e:
        log.exception(e)
        return False

    finally:

        # =========================================================
        # Update job end time
        # =========================================================

        log.info('deploy_tools() Job: ' + job.reference + ' has been completed')
        if error_detected:
            log.warn('deploy_tools() Error detected')
            sqlquery = """UPDATE job SET status='-1',description='Tools deployment failed',job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            self.update_state(state='FAILURE', meta={
                'status': 'Failure detected'})
            message = 'ERROR: Tools deployment failed'
            result = -1
        else:
            sqlquery = """UPDATE job SET status='1',description='Tools deployment completed',job_end='now()' WHERE job.id=""" + \
                       str(job.id)
            message = 'INFO: Tools deployment completed'
            result = 1

        helper.update_database(sqlquery)
        return {'status': message, 'result': result}
