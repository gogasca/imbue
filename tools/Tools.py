__author__ = 'gogasca'

import logging
from conf import settings
from conf import logging_conf
from database import Db
from error import exceptions
from hypervisor.esxi import general
from utils.security import common
import utils.validator
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Tools(object):
    """
    Create new Tools server. Tools server creates floppy image.
    """

    def __init__(self, name=None, **kwargs):

        if general.validator.isValidHostName(name):
            self.name = name
        else:
            raise exceptions.InvalidHostName('Tools() Invalid hostname')

        # Network information
        # These values are based on Tools db table (models.py) Django framework in IMBUE_API
        self.id = None
        self.server = None
        self.hypervisor = 1
        self.description = None
        self.ip_settings = None
        self.network_services = None
        self.server_credentials = None
        self.status = -1

        # These values are extracted from database using DB queries
        self.username = None
        self.password = None
        self.sshPort = None

        log.info('Tools() Instance created')

    # Fix #39. This function pushes changes to an existing server in a database
    def update(self, name=None, configuration=None, **kwargs):
        """

        :param name:
        :param configuration:
        :param kwargs:
        :return:
        """
        try:
            log.info('tools.update() Updating Tools server started...')
            if configuration.has_key('infrastructure'):
                log.info('tools.update() Update Tools server')
            else:
                log.warn('tools.update() Tools information is not available')
                return False

            from database import Db
            from conf import settings

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()

            # Encrypt password
            log.info('tools.update() Encrypting password')
            password = common.encrypt(configuration['infrastructure']['instance']['password'])
            if not password:
                log.exception('tools.update() Unable to encrypt password')

            # =========================================================
            # Check fk_ip_settings
            # =========================================================

            sqlquery = """SELECT fk_ip_settings FROM tools WHERE tools.hostname='""" + name + "'"

            fk_ip_settings = db_instance.query(sqlquery)
            if len(fk_ip_settings) > 0:
                if list(fk_ip_settings[0])[0]:
                    log.info('tools.update() Tools fk_ip_settings: ' +
                             str(list(fk_ip_settings[0])[0]))
            else:
                return False

            # =========================================================
            # Check fk_network_services
            # =========================================================

            sqlquery = """SELECT fk_network_services FROM tools WHERE tools.hostname='""" + str(name) + "'"
            fk_network_services = db_instance.query(sqlquery)
            if len(fk_network_services) > 0:
                if list(fk_network_services[0])[0]:
                    log.info('tools.update() Tools fk_network_services: ' +
                             str(list(fk_network_services[0])[0]))
            else:
                return False

            # =========================================================
            # Check fk_server_credentials
            # =========================================================

            sqlquery = """SELECT fk_server_credentials FROM tools WHERE tools.hostname='""" + \
                       str(name) + "'"
            fk_server_credentials = db_instance.query(sqlquery)
            if len(fk_server_credentials) > 0:
                if list(fk_network_services[0])[0]:
                    log.info('tools.update() Tools fk_server_credentials: ' +
                             str(list(fk_server_credentials[0])[0]))
            else:
                return False

            # =========================================================
            # Update database values now
            # =========================================================

            if fk_server_credentials and fk_network_services and fk_ip_settings:
                if configuration['infrastructure']['instance'].has_key('username'):
                    username = configuration['infrastructure']['instance']['username']

                # Add encryption
                # password = configuration['infrastructure']['instance']['password']

                sqlquery = """UPDATE server_credentials SET ssh_username='""" + username + """',ssh_password='""" + \
                           password + """',https_username='""" + username + """',https_password='""" + password + "'" \
                           + """ WHERE server_credentials.id='""" + str(list(fk_server_credentials[0])[0]) \
                           + "'"

                db_instance.update(sqlquery)

                # =========================================================
                # Update network services values.
                # =========================================================
                properties = '{"ssh": { "active": 1, "port": ' + str(
                    configuration['infrastructure']['instance']['ssh']) + ' }}'

                sqlquery = """UPDATE network_services SET properties='""" + properties + "'" + \
                           """ WHERE network_services.id='""" + \
                           str(list(fk_network_services[0])[0]) + "'"
                db_instance.update(sqlquery)

                sqlquery = """UPDATE ip_settings SET ip='""" + configuration['infrastructure']['instance']['ip'] + \
                           """' WHERE ip_settings.id='""" + str(list(fk_ip_settings[0])[0]) + "'"
                db_instance.update(sqlquery)
                log.info('tools.update() Tools server update successful')
                return True

            else:
                log.error('server.update() failed')
                return False

        except Exception, exception:
            log.exception(exception.__repr__())

    def insert(self,
               ip_address=None,
               description='Tools server',
               properties=None,
               ssh_username=None,
               ssh_password=None,
               **kwargs):
        """
             Insert Tools server into Database
             Add ip_settings
             ip, mask, gateway
             Get return id (fk_ip_settings)
             INSERT INTO ip_settings (ip,mask,gateway) values ('1.1.1.1','255.255.255.0','1.1.1.254') RETURNING id;

             Add network services
             Properties: {"ssh": { "active": 1, "port": 22 }}
             Get return id (fk_network_services)
             INSERT INTO network_services (properties) VALUES ('{"ssh": { "active": 1, "port": 22 }}') RETURNING id;

             Add server_credentials
             ssh_username, ssh_password
             Get return id (fk_server_credentials)
             INSERT INTO server_credentials (ssh_username,ssh_password) VALUES ('root','M1Nub3') RETURNING id;

             Add tools server
             INSERT INTO tools (hostname,description,fk_ip_settings,fk_server_credentials,fk_network_services)
        """

        db_instance = Db.Db(server=settings.dbhost,
                            username=settings.dbusername,
                            password=settings.dbpassword,
                            database=settings.dbname,
                            port=settings.dbport)

        db_instance.initialize()
        ssh_password = common.encrypt(ssh_password)
        if not ssh_password:
            log.exception('tools.update() Unable to encrypt password')

        # INSERT ip_settings
        sqlquery = """INSERT INTO ip_settings (ip)"""
        if utils.validator.is_valid_ipv4_address(ip_address):
            content = "'" + ip_address + "'"
            fk_ip_settings = db_instance.insert(sqlquery, content)
            log.info('Tools.insert() fk_ip_settings: ' + str(fk_ip_settings))
            self.ip_settings = str(fk_ip_settings)
        else:
            raise exceptions.InvalidHostName('Tools.insert() fk_ip_settings invalid ip address')

        # INSERT network_services
        sqlquery = """INSERT INTO network_services (properties)"""
        if utils.validator.is_json(properties):
            content = "'" + properties + "'"
            fk_network_services = db_instance.insert(sqlquery, content)
            log.info('Tools.insert() fk_network_services: ' + str(fk_network_services))
            self.network_services = str(fk_network_services)
        else:
            raise exceptions.InvalidHostName('Tools.insert() fk_network_services invalid json')

        sqlquery = """INSERT INTO server_credentials (ssh_username,ssh_password)"""
        if utils.validator.isValidString(ssh_username):
            content = "'" + ssh_username + "','" + ssh_password + "'"
            fk_server_credentials = db_instance.insert(sqlquery, content)
            log.info('Tools.insert() fk_server_credentials: ' + str(fk_server_credentials))
            self.server_credentials = str(fk_server_credentials)

        sqlquery = """INSERT INTO tools (hostname,description,status,fk_ip_settings,fk_server_credentials,fk_network_services)"""
        if self.description is None:
            self.description = description

        if utils.validator.isValidHostName(self.name) and utils.validator.isValidString(self.description):
            log.info('Tools.insert(): ' + self.name + '|' + self.description + '|' + str(
                self.status) + '|' + self.ip_settings + '|' + self.server_credentials + '|' + self.network_services)
            content = "'" + self.name + "','" + self.description + "','" + str(
                self.status) + "','" + self.ip_settings + "','" + self.server_credentials + "','" + self.network_services + "'"
            self.id = db_instance.insert(sqlquery, content)
            log.info('Tools.insert() New Tools server Id: ' + str(self.id))
        else:
            log.error('Tools.insert() Invalid Tools server name')

    def set_hypervisor(self, value):
        """
        Define hypervisor value
        Version 1.0 Supports only 1 which is ESXi 5.5
        :param value:
        :return:
        """
        self.hypervisor = value

    def get_tools_server(self, name=None, get_id=False, **kwargs):
        try:
            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            sqlquery = """SELECT COUNT(*) FROM tools WHERE tools"""
            if db_instance.initialize():
                log.info('deploy_tools() Database initialization is successful')
            else:
                log.error('deploy_tools() Database initialization failed')
                return

            if name:
                sqlquery = sqlquery + ".hostname=" + "'" + str(name) + "'"

            server_info = db_instance.query(sqlquery)
            if len(server_info) > 0:
                if list(server_info[0])[0] == 0:
                    log.warn('Tools.get_server() Server not found!')
                    if get_id:
                        return
                    return False
                elif list(server_info[0])[0] == 1:
                    log.info('Tools.get_server() Server found in database')
                    sqlquery = """SELECT id FROM tools WHERE tools""" + ".hostname=" + "'" + str(name) + "'"
                    server_id = db_instance.query(sqlquery)
                    self.id = str(list(server_id[0])[0])
                    log.info('Tools.get_server() Tools id:' + str(self.id))
                    if get_id:
                        return self.id
                    return True
                else:
                    log.error('Tools.get_server() More than 1 Server found. This should not happen')
                    return False
            else:
                return False

        except Exception, exception:
            log.exception(str(exception))
            raise exceptions.DBReadException('Server.initialization failed. DB query error')

    def initialize(self, id=None, name=None, **kwargs):
        """

        :param id:
        :param name:
        :param configuration:
        :param kwargs:
        :return:
        """
        from conf import settings

        try:
            # Using database.

            # server=None,username=None,password=None,database=None,port=None
            from database import Db

            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)
            sqlquery = """
                        SELECT
                        tools.hostname,
                        tools.description,
                        tools.status,
                        ip_settings.ip,
                        server_credentials.ssh_username,
                        server_credentials.ssh_password,
                        network_services.properties
                        FROM tools
                        JOIN ip_settings ON (ip_settings.id = tools.fk_ip_settings)
                        JOIN network_services ON (network_services.id = tools.fk_network_services)
                        JOIN server_credentials ON (server_credentials.id = tools.fk_server_credentials)
                        WHERE tools"""

            db_instance.initialize()

            if id:
                sqlquery = sqlquery + ".id=" + str(id)

            elif name:
                sqlquery = sqlquery + ".hostname=" + "'" + str(name) + "'"

            else:
                sqlquery = sqlquery + ".hostname=" + "'" + str(self.name) + "'"

            tools_info = db_instance.query(sqlquery)
            log.info('tools.initialize() Tools info: ' + str(tools_info))

            if tools_info:
                tools_name = tools_info[0][0]
                tools_desc = tools_info[0][1]
                tools_status = tools_info[0][2]
                tools_server = tools_info[0][3]
                tools_username = tools_info[0][4]
                tools_enc_password = tools_info[0][5]

                # Need to decrypt password
                tools_password = common.decrypt(tools_enc_password)
                if tools_password:
                    log.info('tools.initialize() Password decrypted')

                try:
                    if isinstance(tools_info[0][6], dict):
                        log.info('tools.initialize() Dictionary detected')
                        tools_ssh_port = tools_info[0][6]['ssh']['port']
                        log.info('tools.initialize() Tools SSH port:' + str(tools_ssh_port))
                    else:
                        log.info('tools.initialize() Non-Dictionary detected')
                        import json
                        j = json.loads(tools_info[0][6])
                        tools_ssh_port = j['ssh']['port']
                        log.info('tools.initialize() Tools SSH port:' + str(tools_ssh_port))

                except Exception, exception:
                    log.exception('tools.initialize() Exception getting Tools port: ' + str(exception))
                    return False

                # Validate Network information
                log.info('tools.initialize() Tools server status: ' + str(tools_status))
                log.info('tools.initialize() DB verifying network info: ' + str(tools_name) + ' ' + str(tools_desc))
                self.set_network_info(tools_server, tools_username, tools_password, tools_ssh_port)
                return True
            else:
                log.exception('tools.initialize() DB error. No DB records in tools_info')
                raise exceptions.DBReadException('tools.initialize() DB error')

        except Exception, exception:
            log.exception(str(exception))

    def get_hypervisor(self):
        return self.hypervisor

    def is_ssh_opened(self):
        return general.check_ssh_connectivity(self.server, self.username, self.password, self.sshPort)

    def set_network_info(self, server, username, password, sshPort):
        """

        :param server:
        :param username:
        :param password:
        :param sshPort:
        :return:
        """
        log.info('tools.set_network_info()')
        import utils.validator

        if utils.validator.check_authentication_parameters(server=server,
                                                           username=username,
                                                           password=password,
                                                           sshPort=sshPort):
            self.server = server
            self.username = username
            self.password = password
            self.sshPort = sshPort
            log.info('tools.set_network_info() Tools ip information was verified successfully')
        else:
            log.exception('tools.set_network_info() Invalid parameters')
            raise exceptions.ConnectivityError('tools.set_network_info() Invalid parameters')

    def discover(self, mode='ssh'):
        """

        :param mode:
        :return:
        """
        try:
            log.info("tools.discover()")
            if self.get_hypervisor() == 1:
                # Check if I can connect to ESXi via SSH and HTTPS
                check = general.check_network_connectivity(server=self.server,
                                                           username=self.username,
                                                           password=self.password,
                                                           sshPort=self.sshPort,
                                                           mode=mode)
                if check:
                    # "Print log connectivity was successful"
                    log.info('tools.discover() Tools discovery was successful')
                    self.status = 0
                    log.info('tools.initialization Tools server status: ' + str(self.status))
                    return True
                else:
                    self.status = -1
                    log.info('tools.initialization Tools server status: ' + str(self.status))
                    raise exceptions.ConnectivityError('discover() Unable to connect to Tools host')
            else:
                log.exception('tools.discover() Hypervisor model not available yet')
                return False

        except Exception, exception:
            log.exception(str(exception.message))
