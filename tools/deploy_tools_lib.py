__author__ = 'gogasca'


import logging
from conf import logging_conf
log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


def get_tools_instance(tools_name, discover=False):
    """

    :param tools_name:
    :param discover:
    :return:
    """

    from utils import validator
    from tools import Tools
    try:
        if validator.isValidHostName(tools_name):
            # =========================================================
            # Tools server
            # =========================================================

            log.info('get_tools_instance() Create Tools instance')
            tools_instance = Tools.Tools(name=tools_name)
            tools_instance.initialize(name=tools_name)

            if discover:
                log.info('get_tools_instance() Discovering Tools server...')
                if tools_instance.discover():
                    return tools_instance
            else:
                return tools_instance
        else:
            log.error('get_tools_instance() Invalid hostname')

        return

    except Exception, exception:
        print exception
        return