import logging
import os
import subprocess
import time
from celery.task import task
from conf import logging_conf
from conf import settings
from hypervisor.esxi import general
from hypervisor.esxi import file_operations
from datetime import datetime
from utils import file_utils
from utils import db_utils

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Report(object):
    """
        Class that generates a log file and a system report
    """

    def __init__(self, path, filename):
        self.report_path = path + filename + '/'
        self.filename = self.report_path + filename + '.log'
        self.job = filename  # Filename is Job Id
        self.job_path = None
        self.zip_file = None
        self.initialized = False
        log.info('report.__init__() created <{}>'.format(self.filename))

    def initialize(self):
        """

        :return:
        """
        try:
            if self.create_report_directory():
                self.create_report_file()
                self.initialized = True
                self.add_log('Report created [' + self.job + ']')
                self.add_log('Job started')
                return True
            return False

        except Exception:
            log.exception('report.initialize() Failed')
            return False

    def create_report_directory(self):
        """

        :return:
        """
        if self.report_path:
            log.info('report.create_report_directory() Creating local report directory: ' + self.report_path)
            command = 'mkdir ' + self.report_path

            # =========================================================
            # Check command was executed successfully
            # =========================================================
            res = general.run_command(command)
            if res == 0:
                log.info('report.create_report_directory() Directory created successfully')
                return True

        log.warn('report.create_report_directory() Unable to create local log folder')
        return False

    def create_report_file(self):
        """
        Create file in public web folder. This file will contain log information
        :return:
        """
        if self.filename:
            if os.path.exists(self.filename):
                log.error('report.create_report_file() File already exists')
                os.utime(self.filename, None)
            else:
                open(self.filename, 'a').close()
            return True
        else:
            log.error('report.create_report_file() self.filename is undefined')
            return False

    def add_log(self, data):
        """
        Add log info to file
        :param data:
        :return:
        """
        try:
            if self.initialized:
                with open(self.filename, "ab") as report_file:
                    current_timestamp = time.time()
                    timestamp = datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d %H:%M:%S')
                    report_file.write(timestamp + " " + data + '\r\n')  #Issue244 Add CR
                    report_file.close()
            else:
                log.error('report.add_log() Report is not initalized')

        except Exception, excpt:
            log.exception(excpt)

    def generate_video(self):
        """

        :return:
        """
        try:
            res = file_utils.sequence_files(self.job_path)
            if res > 0:
                log.info('report.generate_video() Image files are not in sequence will run script to reorder')
                subprocess.call([settings.reorder_files_sequence + ' ' + self.job_path], shell=True)

            elif res == 0:
                log.info('report.generate_video() Image files exists')
            else:
                log.error('report.generate_video() Image files not found')
                return

            log.info('report.generate_video() Will create video now')

            # Output video
            movie_path = self.report_path + self.job + '.mp4'
            command = settings.ffmpeg_command + ' \'' + str(self.job_path) + '*.png\' -vcodec mpeg4 ' + movie_path

            # Executes command
            cli_command = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, err = cli_command.communicate()
            if file_operations.check_local_file(movie_path):
                log.info('report.generate_video() Video created {}'.format(err))
                return movie_path

            log.error('report.generate_video() Video was not created {}'.format(err))
            return

        except Exception as ex:
            log.info('report.generate_video() In except of create_video: {}'.format(ex))

    def zip(self):
        """

        :return:
        """
        log.info('report.zip() Zipping file')
        self.zip_file = self.report_path + self.job + '.zip'
        command = 'zip -j ' + self.zip_file + ' ' + self.report_path + '*.*'
        cli_command = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = cli_command.communicate()
        if file_operations.check_local_file(self.zip_file):
            log.info('report.zip() Zip file created {}'.format(err))
            return True
        else:
            log.error('report.zip() Zip file was not created {}'.format(err))
            return False

    def generate_report(self, video_supported=False):
        """

        :param video_supported:
        :return:
        """
        if video_supported:
            self.generate_video()
        if self.zip():
            log.info('report.generate_report() Zip file created successfully')
        else:
            log.error('report.generate_report() Zip file was not created')
            return False

        sqlquery = """UPDATE job SET report_created=true WHERE job.reference='""" + str(self.job) + "'"
        db_utils.update_database(sqlquery)
        log.info('report.generate_report() Report was generated successfully')
        return True


@task(bind=True, default_retry_delay=30, max_retries=3)
def send_report(self, zip_file):
    """

    :param zip_file:
    :return:
    """
    if zip_file:
        if general.transfer_file_scp(server=settings.report_web_primary,
                                     username=settings.report_web_username,
                                     private_key=settings.report_web_private_key,
                                     remoteFilePath=settings.report_web_path,
                                     localFile=zip_file,
                                     check=True):
            message = 'Report file transfer succeeded'

            if self.request.id:
                return {'status': message, 'result': 1}

    message = 'Report file transfer failed'
    log.error('send_report() ' + message)
    if self.request.id:
        return {'status': message, 'result': -1}

