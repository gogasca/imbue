__author__ = 'gogasca'

from database import Db
from conf import settings
from utils import Generator
from utils import helper

class Jobd(object):
    """ Job object
    """

    def __init__(self):
        """

        :param vmid:
        :param hostname:
        :param status:
        :param description:
        :return:
        """

        self.reference = Generator.Generator().get_uuid()
        self.id = None
        self.is_cluster = False
        self.infrastructure = False
        self.server_info = dict()
        self.parent_id = None
        self.parent_reference = None
        self.owner_id = None

    def get_owner_id(self,reference=None):
        """

        :return:
        """
        try:
            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            # =========================================================
            # Initialize job in database
            # =========================================================

            db_instance.initialize()  # Initialize Database
            sqlquery = """SELECT owner_id FROM job WHERE job.reference='""" + str(reference) + "'"
            owner_id = db_instance.query(sqlquery)
            if owner_id:
                owner_id = int(list(owner_id[0])[0])
                return owner_id
            return
        except Exception, exception:
            print exception

    def cancel_job(self, reference):
        """

        :return:
        """
        if reference:
            helper.update_database('UPDATE job SET is_cancelled=true WHERE job.reference=\'' + reference + '\'')

    def update_info(self, server_id, vm_id):
        """

        :param server_id:
        :param vm_id:
        :return:
        """
        try:

            if server_id:
                # If server already exists. Retrieve information
                if self.server_info.has_key(int(server_id)):
                    vm_list = self.server_info[int(server_id)]  # Retrieve existing server and vm

                else:
                    # If new server add key
                    vm_list = []
            else:
                return

            if vm_id:
                # Add server
                vm_list.append(int(vm_id))

            self.server_info[server_id] = vm_list

        except Exception, exception:
            print exception

    def __repr__(self):
        return self.reference

    def __str__(self):
        return self.reference
