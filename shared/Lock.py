import logging
from conf import settings
from database import Db
from database import database_session
from conf import logging_conf

log = logging_conf.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)

__author__ = 'gogasca'


class Lock():
    """
    Validates installations
    """

    def __init__(self, esx_name=None):
        """


        :type esx_name: object
        :param name: ESXi server name
        :return:
        """
        self.name = esx_name
        self.owner_id = None
        self.id = None

    def in_progress(self):
        """

        :return:
        """
        status = self.get_status()
        if status >= 1:
            return True
        else:
            return False

    def aquire(self):

        status = self.get_status()
        log.info('Lock.aquire() Status: {0}'.format(str(status)))
        if status >= 1:
            status += 1
            log.warn('Lock.aquire() Install in progress after status change: {0}'.format(str(status)))
        else:
            log.info('Lock.aquire() New install starting status: {0}'.format(str(status)))
            status = 1

        log.warn('Lock.aquire() after status: {0}'.format(str(status)))
        if status <= settings.max_installs:
            if self.owner_id and self.name:
                sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.name='""" + str(
                    self.name) + "' AND owner_id=" + str(self.owner_id)
            elif self.name:
                log.warn('Lock.aquire() No owner_id')
                sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.name='""" + str(
                    self.name) + "'"
            else:
                log.error('Lock.aquire()  No server name defined')
                return False
            self.update_database(sqlquery)
            return True
        else:
            return False

    def release(self):
        """

        :param id:
        :param kwargs:
        :return:
        """
        status = self.get_status()
        if status >= 1:
            log.info('Lock.release() Status: {0}'.format(str(status)))
            status -= 1
            log.warn('Lock.release() Install in progress. After status change: {0}'.format(str(status)))
            if self.id:
                log.info('Lock.release() Updating status using server.id')
                if self.owner_id:
                    sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.id=""" + str(
                        self.id) + " AND owner_id=" + str(self.owner_id)
                else:
                    sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.id=""" + str(
                        self.id) + ""
            else:
                log.info('Lock.release() Updating status using owner_id and server.name')
                if self.owner_id and self.name:
                    sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.name='""" + str(
                        self.name) + "' AND owner_id=" + str(self.owner_id)
                elif self.name:
                    sqlquery = """UPDATE server SET status=""" + str(status) + """ WHERE server.name='""" + str(
                        self.name) + "'"
                else:
                    log.error('Lock.release() No server name defined')
                    return False
            self.update_database(sqlquery)
            return True
        else:
            log.warn('Lock.release() No Install in progress. Server status: {0}'.format(str(status)))
            return False

    def get_status(self):
        """

        :return:
        """
        try:
            db_instance = Db.Db(server=settings.dbhost,
                                username=settings.dbusername,
                                password=settings.dbpassword,
                                database=settings.dbname,
                                port=settings.dbport)

            db_instance.initialize()
            if self.id:
                if self.owner_id:
                    sqlquery = """SELECT status FROM server WHERE server.id=""" + str(
                        id) + " AND server.owner_id=" + str(self.owner_id)
                else:
                    log.info('Lock.get_status() Using server.id')
                    sqlquery = """SELECT status FROM server WHERE server.id=""" + str(self.id)
            else:
                if self.owner_id and self.name:
                    sqlquery = """SELECT status FROM server WHERE server.name='""" + str(
                        self.name) + "' AND server.owner_id=" + str(self.owner_id)
                elif self.name:
                    log.warn('Lock.get_status() No owner_id')
                    sqlquery = """SELECT status FROM server WHERE server.name='""" + str(self.name) + "'"
                else:
                    log.error('Lock.get_status() No server name defined')
                    return -1
            db_status = db_instance.query(sqlquery)
            status = int(list(db_status[0])[0])
            return status
        except Exception, exception:
            log.exception(exception)

    def update_database(self, sqlquery):
        """

        :param sqlquery:
        :return:
        """
        try:
            db = database_session.DatabaseSession(settings.SQLALCHEMY_DATABASE_URI)
            db.execute(sqlquery)
        except Exception, exception:
            log.exception(exception)
