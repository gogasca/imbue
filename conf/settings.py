import platform

__author__ = 'gogasca'

# #################################### Absolute path #####################################


if platform.system() == 'Linux':
    filepath = '/usr/local/src/imbue/application/imbue'
    report_path = "/usr/local/src/imbue/external/customers/reports/"
else:
    filepath = '/Users/gogasca/Documents/OpenSource/Development/Python/parzee_app'
    report_path = "/Users/gogasca/Documents/OpenSource/Development/Python/reports/"

# =========================================================
# ESXi
# =========================================================
esx_vendor_id = 101001
esx_ = 'parzee-esx-dev-1'
esxServer = 'ziro1.noip.me'
esxHTTPSPort = '8026'
esxSSHPort = '8126'
esxUsername = 'root'
esxPassword = 'password'
esx_model_index = 13
show_esx_info = False  # Not implemented
esx_discovery = True  # When adding or updating a server we can decide not to perform discovery. DEFAULT: True
hypervisor_id = 1
serial_number = 'Not Available'
ec2_type = 300001
ovf_url = "http://198.199.110.24:8083/vmware/ovftool.tar.gz"  # This URL needs to be available for all servers
ovf_filename = "ovftool.tar.gz"
report_web_primary = '54.183.220.55'
report_web_username = 'ubuntu'
report_web_private_key = '/usr/local/src/imbue/backup/keys/parzee_srv_tools.pem'  # Change for AWS private key access
report_web_path = '/var/www/web/log/reports'  # HTTP Lighttpd
reorder_files_sequence = filepath + '/utils/shell/rename_screen.sh'
ffmpeg_command = 'ffmpeg -f image2 -r 1 -pattern_type glob -i'

# =========================================================
# Application specific
# =========================================================
install_timeout = 5400  # Total time of a single app installation
versionFile = filepath + '/conf/versions/versions.yaml'
buffer_size = 5000  # SSH buffer
customer = 'test'
cmd = 'ls -alh'
overwrite_check = True
ova_wait_deployment = 180   # 3 minutes
cspc_wait_time =  240       # 4 minutes
vm_startup = 150
media_check_wait = 120
bootup_time_wait = 90
check_install_in_progress = True  # Check max_installs value when installing an Application.
max_installs = 5  # Maximum simultaneous installs per ESXi server
screen = filepath + '/backup/img/screen'
delete_host_key = True
url_encryption = 'http://lighttpd:8083/security/des/main/encrypt.php?password='  # HTTP Lightd

# =========================================================
# Cisco UC specific parameter
# =========================================================
vmInstanceDefaultName = 'pub'
ucmVersion = '9.0'
ucxVersion = '9.0'
cupVersion = '9.0'
ucmVmxTemplate = filepath + '/catalogue/cisco/voice/ucm/templates/'
cucVmxTemplate = filepath + '/catalogue/cisco/voice/cuc/templates/'
cupVmxTemplate = filepath + '/catalogue/cisco/voice/cup/templates/'
vmxPath = filepath + '/backup/vmx/'
default_ova_size = 2500

# =========================================================
# Floppy information
# =========================================================
local_floppy_image = filepath + '/backup/floppy.img'
floppy_image_name = 'floppy.img'
floppy_script = '/usr/local/src/imbue/application/imbue/utils/shell/create_floppy.sh'
dhcp_support = False
service = 'uc'
customerDirectory = '/vmfs/volumes/datastore1/test'
floppyTempFolder = filepath + '/backup'

answerFile = filepath + '/catalogue/cisco/voice/ucm/templates/90/pub/platformConfig.xml'
answerFileTemplateDir = filepath + '/catalogue/cisco/voice/common/'  # Mandatory
answerFileCustomerDir = filepath + '/catalogue/cisco/voice/common/'  # Mandatory

# =========================================================
# Tools
# =========================================================

tools_type = 100001
tools_primary = 'application1'
tools_secondary = 'application2'
tools_floppy_folder = '/usr/local/src/imbue/external/customers/floppy/'

# Default Tools parameters
tools_hostname = '127.0.0.1'
tools_port = '8022'
tools_username = 'root'
# Encrypted password
tools_enc_password = 'gAAAAABW0CPRck6eAqdj19WLEQK4t3yD02LgJZjV7oDjYumORk9qKK7jV3unqHgRBKMc4dE120_6U4S7IIlt4zPQcnFpBVA0aw=='

# =========================================================
# Logging
# =========================================================
log_folder = filepath + '/log/jobs/'
logfile = filepath + '/log/imbued.log'
test_logfile = filepath + '/log/testd.log'
api_logfile = filepath + '/log/apid.log'
log_level = "INFO"  # ERROR,INFO,WARN,DEBUG
debug_vnc = False
# =========================================================
# Beacon
# =========================================================

beacon_file = filepath + '/server/beacon/image/10/beacon.zip'

# =========================================================
# Images
# =========================================================
application_catalogue = filepath + '/conf/versions/app_catalogue.yaml'
cisco_vendor_id = 200000

cisco_list = {200000: "cisco",
              200001: "ucm",
              200002: "cuc",
              200003: "cup",
              200004: "uccx",
              200005: "cer",
              200006: "vcs",
              200007: "tps",
              200008: "cond",
              200009: "cspc"}

ucm_version_pattern = {'90': 'Bootable_UCSInstall_UCOS_9.0', '91': 'Bootable_UCSInstall_UCOS_9.1',
                       '100': 'Bootable_UCSInstall_UCOS_10.0', '105': 'Bootable_UCSInstall_UCOS_10.5',
                       '110': 'Bootable_UCSInstall_UCOS_11'}
cuc_version_pattern = {'90': 'Bootable_UCSInstall_UCOS_9.0', '91': 'Bootable_UCSInstall_UCOS_9.1',
                       '100': 'Bootable_UCSInstall_UCOS_10.0', '105': 'Bootable_UCSInstall_UCOS_10.5',
                       '110': 'Bootable_UCSInstall_UCOS_11'}
cup_version_pattern = {'90': 'Bootable_UCSInstall_CUP_9.0.1', '91': 'Bootable_UCSInstall_CUP_9.1',
                       '100': 'Bootable_UCSInstall_CUP_10.0', '105': 'Bootable_UCSInstall_CUP_10.5',
                       '110': 'Bootable_UCSInstall_CUP_11.0'}
cluster_max_nodes = 2
# Default images
ucmImage = 'Bootable_UCSInstall_UCOS_9.0.1.10000-28.sgn.iso'
ucImage = 'Bootable_UCSInstall_UCOS_9.0.1.10000-28.sgn.iso'
cupImage = 'Bootable_UCSInstall_CUP_9.0.1.10000-22.sgn.iso'
ucm_suffix = '/uc/ucm/'
cuc_suffix = '/uc/cuc/'
cup_suffix = '/uc/cup/'
vcs_suffix = '/uc/vcs/'
tps_suffix = '/uc/tps/'
cond_suffix = '/uc/cond/'
cspc_suffix = '/uc/cspc'
image_coefficient = 5
image_folder = '/vmfs/volumes/datastore1/OVA-ISO/'
install_folder = '/vmfs/volumes/datastore1/parzee/'

# =========================================================
# CSPC
# =========================================================
cspc_dev_mode = True
cspc_default_password = 'Admin!23'
cspc_dev_password = 'M1Nub3!'

# =========================================================
# Vsphere
# =========================================================
cancel_task_wait = 5
destroy_vm = True
user_intervention = False
vSphereUA = 'VMware VI Client/4.0.0'
sshTimeout = 20
socketTimeout = 12
scanner_threads = 1
num_cores = 4
default_datastore = 'datastore1'
defaultSSHPort = '22'
defaultHTTPSPort = '443'
defaultVNCPort = '5901'
defaultVNCPassword = ''
defaultFs = '/vmfs/volumes/'
diskSize = '80G'
thickProvisioning = 'zeroedthick'  # zeroedthick
lsilogic = 'lsilogic'

# =========================================================
# Database
# =========================================================
# psql -h 198.199.110.24 -d imbuedb -U imbue -W
dbhost = 'database'
dbport = 5432
dbusername = 'imbue'
dbpassword = 'imbue'
dbname = 'imbuedb'
dbPasswordAllowEmptyPassword = True

# =========================================================
# Alerts
# =========================================================
sms_alerts = True
twilio_accountId = 'ACed0b00c221c2e58a3f6dd33308c84321'
twilio_tokenId = '68314e5765c801ba4e38bdb4c730f90e'
phone_numbers = ['+14082186575', '+14084641502', '+19098599612']
twilio_from = '+14088053951'

# =========================================================
# API
# =========================================================
base_api_url = '/api/1.0/'
api_account = 'ACda38cb38f1654200b439724edec211c1'
api_password = '43b6e91ca96217e0be09c6f3fe94ec31'
api_version = '1.0'
api_scheme = 'https'
api_ip_address = '0.0.0.0'
api_port = 8081
items_per_page = 20
celery_config = filepath + '/conf/celeryconfig.py'
api_key = {'SECRET_KEY': 'tSjCEs6qkcsH6MTwmORA'}  # Security key

# =========================================================
# SQLALCHEMY
# =========================================================

# "postgresql://imbue:imbue@198.199.110.24/imbuedb
SQLALCHEMY_DATABASE_URI = "postgresql://" + dbusername + ":" + dbpassword + "@" + dbhost + "/" + dbname
SQLALCHEMY_ECHO = False
SQLALCHEMY_POOL_SIZE = 100
SQLALCHEMY_POOL_TIMEOUT = 1
SQLALCHEMY_MAX_OVERFLOW = 0
SQLALCHEMY_POOL_RECYCLE = 1800
SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = None
SQLALCHEMY_COMMIT_ON_TEARDOWN = True
